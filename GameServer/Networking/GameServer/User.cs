﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - User.cs
// Last Edit: 2019/11/24 19:03
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System;
using System.Net.Sockets;
using FtwCore.Common.Enums;
using FtwCore.Interfaces;
using FtwCore.Networking.Packets;
using FtwCore.Networking.Sockets;
using FtwCore.Security;
using GameServer.Structures.Entities;
using GameServer.World;

#endregion

namespace GameServer.Networking.GameServer
{
    /// <summary>
    /// This class encapsulates the player's client. The client contains the player's information from all 
    /// initialized game structures. It also contains the player's passport, which contains the client's remote
    /// socket and variables for processing packets.
    /// </summary>
    public sealed class User : Passport
    {
        // Account Holder & User Specific Variable Declarations:
        public uint Identity; // The character's unique identification number from the database.
        public uint AccountIdentity; // The account's unique identification number from the database.
        public uint Authority; // The amount of authority the account holder has over server actions. 
        public string Language; // The language specified in the client's StrRes.dat file.
        public byte VipLevel = 0;
        public string MacAddress = "00-00-00-00-00-00";
        public uint LastLogin;
        public string LastIpAddress;

        // Global-Scope Game Structure Initializations:
        public Character Character; // The player's game character persona.
        public NetDragonDHKeyExchange Exchange; // The DH key exchange instance for the client.
        public Screen Screen; // Encapsulates all other players in the character's screen.
        public Tile Tile; // The current tile the character is standing on.
        private AsynchronousServerSocket m_socket;

        public DateTime LastActivity;

        /// <summary>
        /// This class encapsulates the player's client. The client contains the player's information from all 
        /// initialized game structures. It also contains the player's passport, which contains the client's remote
        /// socket and variables for processing packets.
        /// </summary>
        /// <param name="server">The server that owns the client.</param>
        /// <param name="socket">The client's remote socket from connect.</param>
        /// <param name="cipher">The client's initialized cipher for packet processing.</param>
        public User(AsynchronousServerSocket server, Socket socket, ICipher cipher)
            : base(server, socket, cipher)
        {
            Exchange = new NetDragonDHKeyExchange();
            m_socket = server;

            LastActivity = DateTime.Now;
        }

        /// <summary>
        /// This method will send a text message to the user interface. It can only be sent after a successful login.
        /// </summary>
        /// <param name="text">The text message that will be sent to the user.</param>
        /// <param name="tone">Where the message will be located.</param>
        public void SendMessage(string text, ChatTone tone = ChatTone.TopLeft)
        {
            Send(new MsgTalk(text, tone));
        }

        public new int Send(byte[] msg)
        {
            ServerKernel.Analytics.IncrementSentPackets();
            ServerKernel.Analytics.IncrementSentBytes(msg.Length);
            return base.Send(msg);
        }
    }
}
﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - Game Socket.cs
// Last Edit: 2019/11/24 19:03
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#define USENEWCAST5

#region References

using System;
using System.Net.Sockets;
using FtwCore.Networking.Packets;
using FtwCore.Networking.Sockets;
using FtwCore.Security;

#endregion

namespace GameServer.Networking.GameServer
{
    /// <summary>
    /// This class encapsulates the message server. It inherits functionality from the asynchronous server socket 
    /// class, allowing the server to be created and instantiated as a server socket system. It also contains the
    /// socket events used in processing clients, packets, and other socket events.
    /// </summary>
    public sealed class GameSocket : AsynchronousServerSocket
    {
        // Local-Scope Variable Declarations:
        PacketProcessor<PacketHandlerType, PacketType, Action<User, byte[]>> _processor;

        /// <summary>
        /// This class encapsulates the message server. It inherits functionality from the asynchronous server socket 
        /// class, allowing the server to be created and instantiated as a server socket system. It also contains the
        /// socket events used in processing clients, packets, and other socket events.
        /// </summary>
        public GameSocket()
            : base("MsgServer", "TQServer", AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp)
        {
            OnClientConnect = Connect;
            OnClientReceive = Receive;
            OnClientExchange = Exchange;
            OnClientDisconnect = Disconnect;

            // Initialize Handlers:
            _processor = new PacketProcessor<PacketHandlerType, PacketType, Action<User, byte[]>>
                (new GamePacketHandler());
        }

        /// <summary>
        /// This method is invoked when the client has been approved of connecting to the server. The client should
        /// be constructed in this method, and cipher algorithms should be initialized. If any packets need to be
        /// sent in the connection state, they should be sent here.
        /// </summary>
        /// <param name="state">Represents the status of an asynchronous operation.</param>
        public void Connect(AsynchronousState state)
        {
            // Create the client for the asynchronous state:
#if USENEWCAST5
            User client = new User(this, state.Socket, new NewCast5Cipher());
#else
            User client = new User(this, state.Socket, new Cast5Cipher());
#endif
            state.Client = client;
            client.Send(client.Exchange.Request());
        }

        /// <summary>
        /// This method is invoked when the client is exchanging keys with the server to generate a common key for
        /// cipher encryptions and decryptions. This method is only implemented in the message server after
        /// patch 5017 (when Blowfish and the DH Key Exchange was implemented).
        /// </summary>
        /// <param name="state">Represents the status of an asynchronous operation.</param>
        public unsafe void Exchange(AsynchronousState state)
        {
            // Retrieve client information from the asynchronous state:
            User client = state.Client as User;
            if (client == null)
                return;
            ServerKernel.Analytics.IncrementRecvPackets();
            ServerKernel.Analytics.IncrementRecvBytes(client.Packet.Length);

            if (client.Packet != null)
                fixed (byte* packet = client.Packet)
                {
                    // Process the client's packet:
                    int position = 7;
                    int packetLength = *(int*) (packet + position);
                    position += 4;
                    //if (packetLength + 7 == client.Packet.Length)
                    {
                        // The exchange is valid. Process it:
                        int junkLength = *(int*) (packet + position);
                        position += 4 + junkLength;
                        int keyLength = *(int*) (packet + position);
                        position += 4;

                        if (position + keyLength > packetLength)
                        {
                            client.Disconnect();
                            return;
                        }

                        string key = new string((sbyte*) packet, position, keyLength);

                        // Process the key and configure Blowfish:
#if USENEWCAST5
                        client.Cipher = client.Exchange.Respond(key, client.Cipher as NewCast5Cipher);
#else
                        client.Cipher = client.Exchange.Respond(key, client.Cipher as Cast5Cipher);
#endif
                    }
                    //else
                    //{
                    //    client.Disconnect();
                    //}
                }
        }

        /// <summary>
        /// This method is invoked when the client has data ready to be processed by the server. The server will
        /// switch between the packet type to find an appropriate function for processing the packet. If the
        /// packet was not found, the packet will be outputted to the console as an error.
        /// </summary>
        /// <param name="state">Represents the status of an asynchronous operation.</param>
        public void Receive(AsynchronousState state)
        {
            User client = state.Client as User;
            if (client == null)
                return;

            client.LastActivity = DateTime.Now;

            ServerKernel.Analytics.IncrementRecvPackets();
            ServerKernel.Analytics.IncrementRecvBytes(client.Packet.Length);
            if (client.Packet != null)
            {
                // Get the packet handler from the packet processor:
                PacketType type = (PacketType) BitConverter.ToUInt16(client.Packet, 2);
                Action<User, byte[]> action = _processor[type];

                // Process the client's packet:
                if (action != null) action(client, client.Packet);
                else GamePacketHandler.Report(client.Packet);
            }
        }

        /// <summary>
        /// This method is invoked when the client is disconnecting from the server. It disconnects the client
        /// from the map server and disposes of game structures. Upon disconnecting from the map server, the
        /// character will be removed from the map and screen actions will be terminated. Then, character
        /// data will be disposed of.
        /// </summary>
        /// <param name="state">Represents the status of an asynchronous operation.</param>
        public void Disconnect(object state)
        {
            User client = state as User;
            if (client == null)
                return;

            try
            {
                if (ServerKernel.UserManager.IsOnCreationQueue(client))
                {
                    ServerKernel.UserManager.RemoveFromCreationQueue(client);
                    return;
                }

                client.Character?.OnDisconnect();
            }
            finally
            {
                client.UserState = PlayerState.Disconnecting;
                ServerKernel.UserManager.LogoutUser(client.Character);
            }
        }
    }
}
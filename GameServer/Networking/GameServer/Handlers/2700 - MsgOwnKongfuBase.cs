﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - 2700 - MsgOwnKongfuBase.cs
// Last Edit: 2019/12/12 00:47
// Created: 2019/12/12 00:32
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System.Collections.Generic;
using FtwCore.Common;
using FtwCore.Networking.Packets;
using GameServer.Structures.Entities;

#endregion

namespace GameServer.Networking.GameServer
{
    public static partial class Handlers
    {
        public static void HandleMsgOwnKongfuBase(Character user, MsgOwnKongfuBase msg)
        {
            switch (msg.Mode)
            {
                case KongfuBaseMode.SetName:
                {
                    user.KongFu.Create(msg.Name);
                    break;
                }
                case KongfuBaseMode.UpdateTime:
                {
                    user.KongFu.Send(msg.Mode);
                    break;
                }
                case KongfuBaseMode.QueryTargetInfo:
                    if (uint.TryParse(msg.Name, out uint idTarget))
                    {
                        Character target = ServerKernel.UserManager.GetUser(idTarget);
                        if (target != null)
                        {
                            target.KongFu.Send(KongfuBaseMode.SendInfo, user);
                            target.KongFu.SendAllStars(user);
                            target.KongFu.Send(KongfuBaseMode.UpdateStar, user);
                            target.KongFu.Send(KongfuBaseMode.UpdateTime, user);
                        }
                    }
                    break;
                case KongfuBaseMode.UpdateStar:
                    break;
                case KongfuBaseMode.RestoreStar:
                    List<string> strs = msg.GetStrings();
                    if (strs.Count < 2)
                        return;

                    if (!byte.TryParse(strs[0], out byte stage) || !byte.TryParse(strs[1], out byte star))
                        return;

                    user.KongFu.Restore(stage, star);
                    break;
                default:
                    Program.WriteLog($"{user.Identity}:{user.Name} MsgOwnKongfuBase unknown or unhandled {msg.Mode}",
                        LogType.WARNING);
                    GamePacketHandler.Report(msg);
                    break;
            }
        }
    }
}
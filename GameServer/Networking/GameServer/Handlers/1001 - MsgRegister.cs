﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - 1001 - MsgRegister.cs
// Last Edit: 2019/11/24 19:03
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System;
using System.Collections.Generic;
using System.Linq;
using FtwCore.Common;
using FtwCore.Common.Enums;
using FtwCore.Database.Entities;
using FtwCore.Database.Repositories;
using FtwCore.Networking.Packets;
using GameServer.Structures.Items;

#endregion

namespace GameServer.Networking.GameServer
{
    public static partial class Handlers
    {
        private const uint _START_MAP = 1002;

        private static readonly string[] _invalidNameChar =
        {
            "{", "}", "[", "]", "(", ")", "\"", "[gm]", "[pm]", "'", "´", "`", "admin", "helpdesk", " ",
            "bitch", "puta", "whore", "ass", "fuck", "cunt", "fdp", "porra", "poha", "caralho", "caraio"
        };

        private static readonly ushort[] m_startX = {430, 423, 439, 428, 452, 464, 439};
        private static readonly ushort[] m_startY = {378, 394, 384, 365, 365, 378, 396};

        public static void HandleMsgRegister(User client, MsgRegister msg)
        {
            if (msg.CancelRequest)
            {
                client.Disconnect();
                return;
            }

            if (ServerKernel.UserManager.IsOnCreationQueue(client))
            {
                if (ServerKernel.DeletedCharacters.Values.Count(x => x.AccountId == client.Identity) > 3)
                {
                    client.Send(ServerMessages.CharacterCreation.CreationLimitReached);
                    return;
                }

                if (!CheckName(msg.Name))
                {
                    client.Send(ServerMessages.CharacterCreation.InvalidName);
                    return;
                }

                CharacterRepository repository = new CharacterRepository();
                if (repository.AccountHasCharacter(client.AccountIdentity))
                {
                    client.Send(ServerMessages.CharacterCreation.AccountHasCharacter);
                    client.Disconnect();
                    return;
                }

                if (repository.CharacterExists(msg.Name))
                {
                    client.Send(ServerMessages.CharacterCreation.NameTaken);
                    return;
                }

                ProfessionType profession = (ProfessionType) msg.Profession > ProfessionType.InternTaoist
                    ? ProfessionType.InternTaoist
                    : (ProfessionType) (msg.Profession / 10 * 10);
                if (!Enum.IsDefined(typeof(BodyType), msg.Body) ||
                    !Enum.IsDefined(typeof(ProfessionType), profession))
                {
                    // The client is a proxy exploiting the server. Disconnect the client.
                    client.Send(ServerMessages.CharacterCreation.AccessDenied);
                    client.Disconnect();
                    return;
                }

                switch (profession)
                {
                    case ProfessionType.InternArcher:
                    case ProfessionType.InternNinja:
                    case ProfessionType.InternTaoist:
                    case ProfessionType.InternTrojan:
                    case ProfessionType.InternWarrior:
                    case ProfessionType.InternMonk:
                    case ProfessionType.InternPirate:
                        break;
                    default:
                    {
                        client.Send(ServerMessages.CharacterCreation.AccessDenied);
                        client.Disconnect();
                        return;
                    }
                }

                ushort hair = 410;
                uint lookface = 0;
                if (msg.Body == (ushort) BodyType.ThinMale || msg.Body == (ushort) BodyType.HeavyMale)
                {
                    if (msg.Profession / 10 == 5)
                    {
                        lookface = (uint) new Random().Next(103, 107);
                    }
                    else if (msg.Profession / 10 == 6)
                    {
                        lookface = (uint) new Random().Next(109, 113);
                    }
                    else if (msg.Profession / 10 == 7)
                    {
                        lookface = (uint) new Random().Next(153, 161);
                    }
                    else
                    {
                        lookface = (uint) new Random().Next(1, 102);
                    }
                }
                else
                {
                    hair = 410;
                    if (msg.Profession / 10 == 5)
                    {
                        lookface = (uint) new Random().Next(291, 295);
                    }
                    else if (msg.Profession / 10 == 6)
                    {
                        lookface = (uint) new Random().Next(300, 304);
                    }
                    else if (msg.Profession / 10 == 7)
                    {
                    }
                    else
                    {
                        lookface = (uint) new Random().Next(201, 290);
                    }
                }

                PointAllotEntity points =
                    ServerKernel.PointAllot.FirstOrDefault(x => x.Profession == msg.Profession / 10 && x.Level == 1);

                if (points == null)
                {
                    points = new PointAllotEntity
                    {
                        Strength = 3,
                        Agility = 3,
                        Vitality = 3,
                        Spirit = 3
                    };
                }

                ushort startLife =
                    (ushort) ((points.Agility + points.Strength + points.Spirit) * 3 + points.Vitality * 24);

                int idx = new Random().Next(m_startX.Length - 1);
                var newUser = new CharacterEntity
                {
                    AccountId = client.AccountIdentity,
                    Name = msg.Name,
                    Lookface = msg.Body + lookface * 10000,
                    Profession = (byte) profession,
                    Mate = "None",
                    AdditionalPoints = 0,
                    Agility = points.Agility,
                    Strength = points.Strength,
                    Vitality = points.Vitality,
                    Spirit = points.Spirit,
                    AutoAllot = 1,
                    AutoExercise = 0,
                    BoundEmoney = 43000,
                    Business = 255,
                    CoinMoney = 0,
                    CurrentLayout = 0,
                    Donation = 0,
                    Emoney = 0,
                    Experience = 0,
                    Level = 1,
                    FirstProfession = 0,
                    Metempsychosis = 0,
                    Flower = 0,
                    HomeId = 0,
                    LastLogin = (uint) UnixTimestamp.Now(),
                    LastLogout = 0,
                    LastProfession = 0,
                    Life = startLife,
                    LockKey = 0,
                    Hair = hair,
                    Mana = 0,
                    MapId = _START_MAP,
                    MapX = m_startX[idx],
                    MapY = m_startY[idx],
                    MeteLevel = 0,
                    Money = 100,
                    MoneySaved = 0,
                    Orchids = 0,
                    PkPoints = 0,
                    RedRoses = 0,
                    StudentPoints = 0,
                    Tulips = 0,
                    Virtue = 0,
                    WhiteRoses = 0,
                    EnlightPoints = 0,
                    HeavenBlessing = (uint) (UnixTimestamp.Now() + 60 * 60 * 24 * 120),
                    ExperienceExpires = (uint) (UnixTimestamp.Now() + 60 * 60 * 24),
                    ExperienceMultiplier = 25,
                    ChiPoints = 4000,
#if DEBUG
                    StudyPoints = 15000,
#endif
                };

                if (repository.CreateNewCharacter(newUser))
                {
                    client.Identity = newUser.Identity;
                    Program.WriteLog(
                        $"User [{client.AccountIdentity}] has created character {newUser.Identity}:{newUser.Name}");

                    try
                    {
                        CreateEquipments(client.Identity, profession);
                    }
                    finally
                    {
                        client.Send(ServerMessages.CharacterCreation.AnswerOk);
                    }
                }
                else
                {
                    client.Send(ServerMessages.CharacterCreation.AccessDenied);
                    return;
                }

                ServerKernel.UserManager.RemoveFromCreationQueue(client);
            }
        }

        public static bool CheckName(string szName)
        {
            foreach (var c in szName)
            {
                if (c < ' ')
                    return false;
                switch (c)
                {
                    case ' ':
                    case ';':
                    case ',':
                    case '/':
                    case '\\':
                    case '=':
                    case '%':
                    case '@':
                    case '\'':
                    case '"':
                    case '[':
                    case ']':
                    case '?':
                    case '{':
                    case '}':
                        return false;
                }
            }

            string lower = szName.ToLower();
            foreach (string part in _invalidNameChar)
                if (lower.Contains(part))
                    return false;

            return true;
        }

        private static void CreateEquipments(uint idUser, ProfessionType profession)
        {
            ItemEntity item = null;
            item = Item.CreateEntity(723753);
            item.PlayerId = idUser;
            Database.ItemRepository.Save(item);

            item = Item.CreateEntity(1000000);
            item.StackAmount = 5;
            item.PlayerId = idUser;
            Database.ItemRepository.Save(item);

            item = Item.CreateEntity(1001000);
            item.StackAmount = 5;
            item.PlayerId = idUser;
            Database.ItemRepository.Save(item);

            switch (profession)
            {
                case ProfessionType.InternTrojan:
                case ProfessionType.InternWarrior:
                {
                    item = Item.CreateEntity(410301);
                    item.Position = 4;
                    item.StackAmount = 1;
                    item.PlayerId = idUser;
                    Database.ItemRepository.Save(item);
                    break;
                }

                case ProfessionType.InternArcher:
                {
                    item = Item.CreateEntity(500301);
                    item.Position = 4;
                    item.StackAmount = 1;
                    item.PlayerId = idUser;
                    Database.ItemRepository.Save(item);

                    item = Item.CreateEntity(1050000);
                    item.Position = 5;
                    item.PlayerId = idUser;
                    Database.ItemRepository.Save(item);

                    item = Item.CreateEntity(613301);
                    item.StackAmount = 1;
                    item.PlayerId = idUser;
                    Database.ItemRepository.Save(item);

                    item = Item.CreateEntity(613301);
                    item.StackAmount = 1;
                    item.PlayerId = idUser;
                    Database.ItemRepository.Save(item);
                    break;
                }

                case ProfessionType.InternTaoist:
                {
                    item = Item.CreateEntity(421301);
                    item.Position = 4;
                    item.StackAmount = 1;
                    item.PlayerId = idUser;
                    Database.ItemRepository.Save(item);
                    break;
                }

                case ProfessionType.InternNinja:
                {
                    item = Item.CreateEntity(601301);
                    item.Position = 4;
                    item.StackAmount = 1;
                    item.PlayerId = idUser;
                    Database.ItemRepository.Save(item);
                    break;
                }

                case ProfessionType.InternMonk:
                {
                    item = Item.CreateEntity(610301);
                    item.Position = 4;
                    item.StackAmount = 1;
                    item.PlayerId = idUser;
                    Database.ItemRepository.Save(item);
                    break;
                }

                case ProfessionType.InternPirate:
                {
                    item = Item.CreateEntity(611301);
                    item.Position = 4;
                    item.StackAmount = 1;
                    item.PlayerId = idUser;
                    Database.ItemRepository.Save(item);

                    item = Item.CreateEntity(612301);
                    item.StackAmount = 1;
                    item.PlayerId = idUser;
                    Database.ItemRepository.Save(item);
                    break;
                }
            }

            item = Item.CreateEntity(132005);
            item.StackAmount = 1;
            item.PlayerId = idUser;
            Database.ItemRepository.Save(item);

#if DEBUG
            List<ItemEntity> allItems = new List<ItemEntity>();

            switch (profession)
            {
                case ProfessionType.InternTrojan:
                {
                    allItems.Add(GenerateBonusItem(410029, idUser));
                    allItems.Add(GenerateBonusItem(420029, idUser));
                    allItems.Add(GenerateBonusItem(480029, idUser));
                    allItems.Add(GenerateBonusItem(120009, idUser));
                    allItems.Add(GenerateBonusItem(150019, idUser));
                    allItems.Add(GenerateBonusItem(118009, idUser));
                    allItems.Add(GenerateBonusItem(130009, idUser));
                    break;
                }

                case ProfessionType.InternWarrior:
                {
                    allItems.Add(GenerateBonusItem(410029, idUser));
                    allItems.Add(GenerateBonusItem(420029, idUser));
                    allItems.Add(GenerateBonusItem(480029, idUser));
                    allItems.Add(GenerateBonusItem(120009, idUser));
                    allItems.Add(GenerateBonusItem(150019, idUser));
                    allItems.Add(GenerateBonusItem(111009, idUser));
                    allItems.Add(GenerateBonusItem(131009, idUser));
                    break;
                }

                case ProfessionType.InternArcher:
                {
                    allItems.Add(GenerateBonusItem(500019, idUser));
                    allItems.Add(GenerateBonusItem(613029, idUser));
                    allItems.Add(GenerateBonusItem(613029, idUser));
                    allItems.Add(GenerateBonusItem(120009, idUser));
                    allItems.Add(GenerateBonusItem(150019, idUser));
                    allItems.Add(GenerateBonusItem(113009, idUser));
                    allItems.Add(GenerateBonusItem(133009, idUser));
                    break;
                }

                case ProfessionType.InternTaoist:
                {
                    allItems.Add(GenerateBonusItem(421029, idUser));
                    allItems.Add(GenerateBonusItem(121009, idUser));
                    allItems.Add(GenerateBonusItem(152019, idUser));
                    allItems.Add(GenerateBonusItem(114009, idUser));
                    allItems.Add(GenerateBonusItem(134009, idUser));
                    break;
                }

                case ProfessionType.InternNinja:
                {
                    allItems.Add(GenerateBonusItem(601029, idUser));
                    allItems.Add(GenerateBonusItem(601029, idUser));
                    allItems.Add(GenerateBonusItem(120009, idUser));
                    allItems.Add(GenerateBonusItem(150019, idUser));
                    allItems.Add(GenerateBonusItem(112009, idUser));
                    allItems.Add(GenerateBonusItem(135009, idUser));
                    break;
                }

                case ProfessionType.InternMonk:
                {
                    allItems.Add(GenerateBonusItem(610029, idUser));
                    allItems.Add(GenerateBonusItem(610029, idUser));
                    allItems.Add(GenerateBonusItem(120009, idUser));
                    allItems.Add(GenerateBonusItem(150019, idUser));
                    allItems.Add(GenerateBonusItem(143009, idUser));
                    allItems.Add(GenerateBonusItem(136009, idUser));
                    break;
                }

                case ProfessionType.InternPirate:
                {
                    allItems.Add(GenerateBonusItem(611029, idUser));
                    allItems.Add(GenerateBonusItem(612029, idUser));
                    allItems.Add(GenerateBonusItem(120009, idUser));
                    allItems.Add(GenerateBonusItem(150019, idUser));
                    allItems.Add(GenerateBonusItem(139009, idUser));
                    allItems.Add(GenerateBonusItem(144009, idUser));
                    break;
                }
            }

            allItems.Add(GenerateBonusItem(160019, idUser));

            allItems.Add(GenerateBonusItem(721259, idUser));
            allItems.Add(GenerateBonusItem(723701, idUser));

            allItems.Add(GenerateBonusItem(1080001, idUser));
            allItems.Add(GenerateBonusItem(721062, idUser));

            Database.ItemRepository.Save(allItems.ToArray());
#endif
        }

#if DEBUG

        private static ItemEntity GenerateBonusItem(uint idType, uint idOwner, bool bound = true)
        {
            ItemEntity item = Item.CreateEntity(idType);
            switch (Calculations.GetItemPosition(idType))
            {
                case ItemPosition.Headwear:
                case ItemPosition.Necklace:
                case ItemPosition.Ring:
                case ItemPosition.RightHand:
                case ItemPosition.LeftHand:
                case ItemPosition.Armor:
                case ItemPosition.Boots:
                case ItemPosition.AttackTalisman:
                case ItemPosition.DefenceTalisman:
                    item.Gem1 = 255;
                    item.Magic3 = 3;
                    break;
                case ItemPosition.Steed:
                    item.Magic3 = 3;
                    break;
            }

            item.PlayerId = idOwner;
            item.Monopoly = (byte) (bound ? 3 : 0);
            return item;
        }

#endif
    }
}
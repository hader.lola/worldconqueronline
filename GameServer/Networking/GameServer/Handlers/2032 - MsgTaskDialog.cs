﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - 2032 - MsgTaskDialog.cs
// Last Edit: 2019/11/24 19:03
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using FtwCore.Common;
using FtwCore.Database.Entities;
using FtwCore.Networking.Packets;
using GameServer.Structures.Entities;

#endregion

namespace GameServer.Networking.GameServer
{
    public static partial class Handlers
    {
        public static void HandleMsgTaskDialog(Character user, MsgTaskDialog msg)
        {
            // todo check if user is alive
            if (!user.IsAlive)
            {
                return;
            }

            switch (msg.InteractType)
            {
                case MsgTaskDialog.MESSAGE_BOX:
                {
                    if (user.RequestBox == null)
                        return;

                    if (msg.OptionId == 0) // cancel
                    {
                        user.RequestBox.OnCancel(user);
                    }
                    else if (msg.OptionId == 255) // ok
                    {
                        user.RequestBox.OnOk(user);
                    }

                    user.RequestBox = null;
                    break;
                }

                case MsgTaskDialog.ANSWER:
                {
                    if (msg.OptionId == 255)
                    {
                        //user.CancelInteraction();
                        return;
                    }

                    if (user.InteractingNpc != null
                        && user.InteractingNpc.MapIdentity != 5000
                        && user.GetDistance(user.InteractingNpc.MapX, user.InteractingNpc.MapY) >
                        Calculations.SCREEN_DISTANCE)
                    {
                        user.CancelInteraction();
                        return;
                    }

                    if (user.InteractingNpc == null && user.InteractingItem == null)
                    {
                        user.CancelInteraction();
                        return;
                    }

                    uint idTask = user.GetTaskId(msg.OptionId);
                    if (!ServerKernel.Tasks.TryGetValue(idTask, out var task))
                    {
                        user.CancelInteraction();
                        return;
                    }

                    user.ClearTaskId();
                    user.GameAction.ProcessAction(user.TestTask(task) ? task.IdNext : task.IdNextfail, user,
                        user.InteractingNpc, user.InteractingItem, msg.Text);
                    break;
                }

                case MsgTaskDialog.TEXT_INPUT:
                {
                    if (msg.TaskId == 31100) // syndicate kick member
                    {
                        user.Syndicate.ExpelMember(user, msg.Text, true);
                        user.Syndicate.SendMembers(user, 0);
                            return;
                    }
                    break;
                }

                default:
                {
                    switch (msg.OptionId)
                    {
                        case 0: // new interaction
                        {
                            user.ClearTaskId();
                            ScreenObject obj;
                            GameTaskEntity task;
                            if (user.Map.RoleSet.TryGetValue(msg.TaskId, out obj)
                                || ServerKernel.Maps[5000].RoleSet.TryGetValue(msg.TaskId, out obj))
                            {
                                uint idAction = 0u;
                                if (!(obj is BaseNpc npc)
                                    || !ServerKernel.Tasks.TryGetValue(npc.Task0, out task)
                                    || (idAction = TestNpcTasks(npc, user)) == 0u)
                                {
                                    user.CancelInteraction();
                                    return;
                                }

                                user.GameAction.ProcessAction(idAction, user, npc, null, "");
                            }

                            break;
                        }

                        case 255: // closing window
                        default:
                        {
                            user.CancelInteraction();
                            break;
                        }
                    }

                    break;
                }
            }
        }

        private static uint TestNpcTasks(BaseNpc npc, Character user)
        {
            for (int i = 0; i < 8; i++)
            {
                GameTaskEntity task;
                switch (i)
                {
                    case 0:
                        if (!ServerKernel.Tasks.TryGetValue(npc.Task0, out task))
                            continue;
                        break;
                    case 1:
                        if (!ServerKernel.Tasks.TryGetValue(npc.Task1, out task))
                            continue;
                        break;
                    case 2:
                        if (!ServerKernel.Tasks.TryGetValue(npc.Task2, out task))
                            continue;
                        break;
                    case 3:
                        if (!ServerKernel.Tasks.TryGetValue(npc.Task3, out task))
                            continue;
                        break;
                    case 4:
                        if (!ServerKernel.Tasks.TryGetValue(npc.Task4, out task))
                            continue;
                        break;
                    case 5:
                        if (!ServerKernel.Tasks.TryGetValue(npc.Task5, out task))
                            continue;
                        break;
                    case 6:
                        if (!ServerKernel.Tasks.TryGetValue(npc.Task6, out task))
                            continue;
                        break;
                    case 7:
                        if (!ServerKernel.Tasks.TryGetValue(npc.Task7, out task))
                            continue;
                        break;
                    default:
                        return 0;
                }

                if (user.TestTask(task))
                    return task.IdNext;
            }

            return 0;
        }
    }
}
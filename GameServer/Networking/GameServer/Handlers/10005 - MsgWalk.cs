﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - 10005 - MsgWalk.cs
// Last Edit: 2019/11/24 19:03
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System;
using FtwCore.Common.Enums;
using FtwCore.Networking.Packets;
using GameServer.Structures.Entities;

#endregion

namespace GameServer.Networking.GameServer
{
    public static partial class Handlers
    {
        public static void HandleMsgWalk(Character user, MsgWalk msg)
        {
            if (msg.Identity != user.Identity)
                return; 

            if (msg.Action != MovementType.SHIFT)
                user.ProcessOnMove(msg.Action);

            switch (msg.Action)
            {
                case MovementType.WALK:
                case MovementType.RUN:
                    if (user.MoveToward((FacingDirection)(msg.Direction % 8), false))
                    {
                        user.Send(msg);
                        user.Screen.SendMovement(msg);
                    }
                    break;
                case MovementType.RIDE:
                    if (user.MoveTowardRide((byte) (msg.Direction % 24), false))
                    {
                        user.Vigor = Math.Max(user.Vigor - 2u, 0);
                        user.Send(msg);
                        user.Screen.SendMovement(msg);
                    }
                    break;
            }
        }
    }
}
﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - 2261 - Msg2ndPw.cs
// Last Edit: 2019/11/24 19:03
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using FtwCore.Networking.Packets;
using GameServer.Structures.Entities;

#endregion

namespace GameServer.Networking.GameServer
{
    public static partial class Handlers
    {
        public static void HandleMsg2ndPw(Character user, Msg2ndPsw msg)
        {
            switch (msg.Request)
            {
                case PasswordRequestType.VerifyPassword:
                {
                    if (!user.IsUnlocked() && user.CanUnlock2ndPassword())
                    {
                        if (user.WarehousePassword != msg.NewPassword)
                        {
                            msg.Request = PasswordRequestType.WrongPassword;
                            user.Send(msg);
                            return;
                        }
                        user.Unlock2ndPassword();
                        msg.Request = PasswordRequestType.CorrectPassword;
                        user.Send(msg);
                    }
                    break;
                }

                case PasswordRequestType.SetNewPassword:
                {
                    if (user.WarehousePassword != 0)
                        return;

                    user.WarehousePassword = msg.NewPassword;
                    user.Unlock2ndPassword();
                    msg.Request = PasswordRequestType.CorrectPassword;
                    msg.Password = 0x0101;
                    break;
                }


            }
        }
    }
}
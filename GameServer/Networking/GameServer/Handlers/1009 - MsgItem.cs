﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - 1009 - MsgItem.cs
// Last Edit: 2019/11/24 19:03
// Created: 2019/10/30 15:41
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System.Drawing;
using System.Linq;
using CO2_CORE_DLL.IO;
using FtwCore;
using FtwCore.Common;
using FtwCore.Common.Enums;
using FtwCore.Database.Entities;
using FtwCore.Networking.Packets;
using GameServer.Structures.Entities;
using GameServer.Structures.Items;

#endregion

namespace GameServer.Networking.GameServer
{
    public static partial class Handlers
    {
        public static void HandleMsgItem(Character user, MsgItem msg)
        {
            if (!user.IsAlive && msg.Action != ItemAction.Ping)
            {
                user.SendSysMessage(Language.StrDie);
                return;
            }

            Item item = null;
            ScreenObject obj = null;
            Npc npc = null;
            switch (msg.Action)
            {
                case ItemAction.Buy:
                    bool isHonor = false;

                    if (user.InteractingNpc != null
                        && msg.Identity == user.InteractingNpc.Identity
                        && !Calculations.InScreen(user.MapX, user.MapY, user.InteractingNpc.MapX,
                            user.InteractingNpc.MapY)
                        && user.InteractingNpc.MapIdentity != 5000
                        && user.InteractingNpc is Npc)
                    {
                        npc = user.InteractingNpc as Npc;
                    }
                    else if (msg.Identity != 2888 && msg.Identity != 6000)
                    {
                        if (!user.Map.RoleSet.TryGetValue(msg.Identity, out obj))
                            return;

                        if (!Calculations.InScreen(user.MapX, user.MapY, obj.MapX, obj.MapY))
                            return;

                        npc = obj as Npc;
                        if (npc == null)
                            return;
                    }
                    else if (msg.Identity == 6000)
                    {
                        isHonor = true;
                    }
                    else
                    {
                        if (!ServerKernel.RoleManager.GetRole(msg.Identity, out ScreenObject screenObject) ||
                            !(screenObject is Npc))
                            return;

                        npc = (Npc) screenObject;

                        if (npc.MapIdentity != user.MapIdentity && npc.MapIdentity != 5000)
                            return;
                    }

                    var goods = ServerKernel.Goods.FirstOrDefault(x =>
                        x.OwnerIdentity == msg.Identity && x.Itemtype == msg.Param1);
                    if (goods == null)
                        return;

                    if (!ServerKernel.Itemtype.TryGetValue(msg.Param1, out ItemtypeEntity itemtype))
                    {
                        ServerKernel.Log.GmLog("cheat", $"{user.Identity}:{user.Name} not authentic shop");
                        return;
                    }

                    int amount = (int) msg.Param2;

                    if (user.UserPackage.IsPackFull(itemtype.Type, amount))
                    {
                        user.SendSysMessage(Language.StrYourBagIsFull, Color.Red);
                        return;
                    }

                    const int bound = ItemType.ITEM_STORAGE_MASK | ItemType.ITEM_MONOPOLY_MASK;
                    int monopoly = 0;
                    if (msg.Param4 == 0) // money or honor
                    {
                        if (!isHonor)
                        {
                            if (goods.Moneytype != 0)
                                return;
                            if (!user.SpendMoney((int) (itemtype.Price * amount)))
                            {
                                user.SendSysMessage(Language.StrNotEnoughMoney, Color.Red);
                                return;
                            }

                            ServerKernel.Empire.AwardMoney((int)(itemtype.Price * amount));
                        }

                        // todo handle honor
                    }
                    else if (msg.Param4 == 1) // CPs
                    {
                        if (goods.Moneytype != 1)
                            return;
                        if (!user.SpendEmoney((int) (itemtype.EmoneyPrice * amount), EmoneySourceType.Shop, npc))
                        {
                            user.SendSysMessage(Language.StrNotEnoughCPs, Color.Red);
                            return;
                        }

                        ServerKernel.Empire.AwardEmoney((int)(itemtype.EmoneyPrice * amount));
                    }
                    else if (msg.Param4 == 2) // CPs(B)
                    {
                        if (itemtype.BoundEmoneyPrice <= 0)
                            return;
                        if (!user.SpendBoundEmoney((int) (itemtype.BoundEmoneyPrice * amount)))
                        {
                            user.SendSysMessage(Language.StrNotEnoughCPsB, Color.Red);
                            return;
                        }

                        monopoly = bound;
                    }

                    ItemEntity entity = Item.CreateEntity(itemtype.Type);
                    entity.StackAmount = (ushort) amount;
                    entity.Monopoly = (byte) monopoly;
                    user.UserPackage.AwardItem(entity);
                    break;
                case ItemAction.Sell:
                    if (msg.Identity == 2888 || msg.Identity == 6000)
                        return;

                    if (!user.Map.RoleSet.TryGetValue(msg.Identity, out var opt) || !(opt is Npc))
                        return;
                    if (!Calculations.InScreen(user.MapX, user.MapY, opt.MapX, opt.MapY))
                        return;

                    item = user.UserPackage[msg.Param1];
                    if (item == null)
                        return;

                    int price = item.GetSellPrice();
                    user.AwardMoney(price);
                    user.UserPackage.RemoveFromInventory(item.Identity, RemovalType.Delete);

                    ServerKernel.Log.GmLog("item_sell",
                        $"Item(id:{item.Identity}) has been sold to (npcid:{opt.Identity}) by (userid:{user.Identity})");
                    break;
                case ItemAction.Unequip:
                    if (!user.UserPackage.Unequip((ItemPosition) msg.Param1))
                        user.SendSysMessage(Language.StrYourBagIsFull, Color.Red);
                    break;
                case ItemAction.Use:
                case ItemAction.Equip:
                    if (!user.UserPackage.UseItem(msg.Identity, (ItemPosition) msg.Param1))
                        user.SendSysMessage(Language.StrUnableToUseItem, Color.Red);
                    break;

                case ItemAction.QueryMoneySaved:
                {
                    msg.Param1 = user.MoneySaved;
                    user.Send(msg);
                    break;
                }

                case ItemAction.SaveMoney:
                {
                    if (user.SpendMoney((int) msg.Param1, true))
                    {
                        if (!user.AwardMoneySaved((int) msg.Param1))
                        {
                            user.AwardMoney((int) msg.Param1);
                        }
                        else
                        {
                            MsgItem msgItem = new MsgItem
                            {
                                Action = ItemAction.QueryMoneySaved,
                                Param1 = user.MoneySaved
                            };
                            user.Send(msgItem);
                        }
                    }

                    break;
                }

                case ItemAction.DrawMoney:
                {
                    if (user.SpendMoneySaved((int) msg.Param1))
                    {
                        user.AwardMoney((int) msg.Param1);
                        MsgItem pMsg = new MsgItem
                        {
                            Action = ItemAction.QueryMoneySaved,
                            Param1 = user.MoneySaved
                        };
                        user.Send(msg);
                        user.Send(pMsg);
                    }

                    break;
                }

                case ItemAction.Repair:
                {
                    Item target = user.UserPackage[msg.Identity];
                    target?.RepairItem();
                    break;
                }

                case ItemAction.RepairAll:
                {
                    if (user.Owner.VipLevel < 2)
                        return;

                    foreach (var repair in user.UserPackage.GetEquipment())
                        repair.RepairItem();
                    break;
                }

                case ItemAction.Improve:
                {
                    Item target = user.UserPackage[msg.Identity];
                    if (target != null)
                    {
                        if (target.Durability / 100 < target.MaxDurability / 100)
                        {
                            user.SendSysMessage(Language.StrItemErrRepairItem);
                            return;
                        }

                        if (target.Type % 10 == 0)
                        {
                            user.SendSysMessage(Language.StrItemErrUpgradeFixed);
                            return;
                        }

                        uint idNewType = 0;
                        double nChance = 0.00;

                        target.GetUpEpQualityInfo(out nChance, out idNewType);

                        if (target.Type % 10 < 6 && target.Type % 10 > 0)
                        {
                            nChance = 100.00;
                        }

                        if (idNewType == 0)
                        {
                            user.SendSysMessage(Language.StrItemCannotImprove);
                            return;
                        }

                        if (!user.UserPackage.SpendDragonBalls(1, true))
                        {
                            user.SendSysMessage(Language.StrItemErrNoDragonBall);
                            return;
                        }

                        if (Calculations.ChanceCalc((float) nChance))
                        {
                            target.ChangeType(idNewType);
                            user.SendEffect("improveSuc", false);
                        }
                        else
                        {
                            target.Durability /= 2;
                        }

                        if (Calculations.ChanceCalc(1f))
                        {
                            bool changed = false;
                            if (target.SocketOne == SocketGem.NoSocket)
                            {
                                changed = true;
                                target.SocketOne = SocketGem.EmptySocket;
                            }
                            else if (target.SocketTwo == SocketGem.NoSocket)
                            {
                                changed = true;
                                target.SocketTwo = SocketGem.EmptySocket;
                            }

                            if (changed)
                                user.SendSysMessage(Language.StrUpgradeAwardSocket);
                        }

                        target.Save();
                        user.Send(target.CreateMsgItemInfo(ItemMode.Update));
                        ServerKernel.Log.GmLog("Improve",
                            $"User[{user.Identity}] item [{target.Identity}] type [{target.Type}] nChance[{nChance}] used[{SpecialItem.TYPE_DRAGONBALL}]");
                    }

                    break;
                }

                case ItemAction.Uplev:
                {
                    Item item1 = user.UserPackage[msg.Identity];
                    if (item1 != null)
                    {
                        if (item1.Durability / 100 < item1.MaxDurability / 100)
                        {
                            user.SendSysMessage(Language.StrItemErrRepairItem);
                            return;
                        }

                        int idNewType = 0;
                        double nChance = 0.00;

                        if (!item1.GetUpLevelChance((int) item1.Type, out nChance, out idNewType))
                        {
                            user.SendSysMessage(Language.StrItemErrMaxLevel);
                            return;
                        }

                        ItemtypeEntity dbNewType =
                            ServerKernel.Itemtype.Values.FirstOrDefault(x => x.Type == idNewType);
                        if (dbNewType == null)
                        {
                            user.SendSysMessage(Language.StrItemErrMaxLevel);
                            return;
                        }

                        if (!user.UserPackage.SpendMeteors(1))
                        {
                            user.SendSysMessage(string.Format(Language.StrItemErrNotEnoughMeteors, 1));
                            return;
                        }

                        if (Calculations.ChanceCalc((float) nChance))
                        {
                            item1.ChangeType((uint) idNewType);
                            user.SendEffect("improveSuc", false);
                        }
                        else
                        {
                            item1.Durability /= 2;
                        }

                        if (Calculations.ChanceCalc(1f))
                        {
                            bool changed = false;
                            if (item1.SocketOne == SocketGem.NoSocket)
                            {
                                changed = true;
                                item1.SocketOne = SocketGem.EmptySocket;
                            }
                            else if (item1.SocketTwo == SocketGem.NoSocket)
                            {
                                changed = true;
                                item1.SocketTwo = SocketGem.EmptySocket;
                            }

                            if (changed)
                                user.SendSysMessage(Language.StrUpgradeAwardSocket);
                        }

                        item1.Save();
                        user.Send(item1.CreateMsgItemInfo(ItemMode.Update));
                        ServerKernel.Log.GmLog("Upgrade",
                            $"User[{user.Identity}] item [{item1.Identity}] type [{item1.Type}] nChance[{nChance}] used[{SpecialItem.TYPE_DRAGONBALL}]");
                    }

                    break;
                }

                case ItemAction.BoothQuery:
                {
                    var pRoleList =
                        user.Map.CollectMapThing(Calculations.SCREEN_DISTANCE, new Point(user.MapX, user.MapY));
                    Character pUser = null;
                    foreach (var role in pRoleList)
                    {
                        if (role is Character tempUser)
                        {
                            if (tempUser.Booth == null || !tempUser.Booth.Vending)
                                continue;
                            if (tempUser.Booth.Identity == msg.Identity)
                            {
                                pUser = tempUser;
                                break;
                            }
                        }
                    }

                    if (pUser == null) return;

                    foreach (var boothItem in pUser.Booth.Items.Values.OrderBy(x => x.Value))
                    {
                        if (pUser.UserPackage[boothItem.Item.Identity] == null)
                        {
                            pUser.Booth.Items.TryRemove(boothItem.Item.Identity, out _);
                            return;
                        }

                        Item pItem = boothItem.Item;
                        MsgItemInfoEx msgItem = new MsgItemInfoEx
                        {
                            Identity = pItem.Identity,
                            Bless = pItem.ReduceDamage,
                            Color = pItem.Color,
                            Durability = pItem.Durability,
                            Enchant = pItem.Enchantment,
                            Itemtype = pItem.Type,
                            MaximumDurability = pItem.MaxDurability,
                            Plus = pItem.Addition,
                            Price = boothItem.Value,
                            ViewType = (ushort) (boothItem.IsSilver ? 1 : 3),
                            SocketOne = pItem.SocketOne,
                            SocketTwo = pItem.SocketTwo,
                            TargetIdentity = msg.Identity,
                            SocketProgress = pItem.SocketProgress,
                            StackAmount = pItem.StackAmount,
                            Position = ItemPosition.Inventory
                        };
                        user.Send(msgItem);
                        pItem.SendPurification(user);
                    }

                    break;
                }

                case ItemAction.BoothAdd:
                {
                    if (user.Booth == null || !user.Booth.Vending)
                        return;
                    if (user.Booth.Items.ContainsKey(msg.Identity))
                        return;

                    if ((item = user.UserPackage[msg.Identity]) == null)
                        return;

                    if (item.CanBeSold)
                    {
                        var pSale = new BoothItem();
                        if (!pSale.Create(item, msg.Param1, true))
                            return;
                        user.Booth.Items.TryAdd(item.Identity, pSale);
                        user.Send(msg);
                    }

                    break;
                }

                case ItemAction.BoothDelete:
                {
                    if (user.Booth == null || !user.Booth.Vending)
                        return;

                    if (!user.Booth.Items.TryRemove(msg.Identity, out _))
                        return;

                    user.Send(msg);
                    break;
                }

                case ItemAction.BoothBuy:
                {
                    if (!user.UserPackage.IsPackSpare(1)) return;

                    var pScreenObj = user.Map.QueryRole(msg.Param1);
                    if (pScreenObj == null)
                        return;

                    uint idTarget = (pScreenObj.Identity - pScreenObj.Identity % 100000) * 10 +
                                    pScreenObj.Identity % 100000;
                    var pRoleTarget = user.Map.QueryRole(idTarget);
                    if (user.GetDistance(pRoleTarget.MapX, pRoleTarget.MapY) > Calculations.SCREEN_DISTANCE)
                        return;

                    var pUser = pRoleTarget as Character;
                    if (pUser?.Booth == null || !pUser.Booth.Vending)
                        return;

                    BoothItem boothItem;
                    if (pUser.Booth.Items.TryGetValue(msg.Identity, out boothItem))
                    {
                        if (boothItem.IsSilver)
                        {
                            if (user.SpendMoney((int) boothItem.Value, true))
                                pUser.AwardMoney((int) boothItem.Value);
                            else return;
                        }
                        else
                        {
                            if (user.SpendEmoney((int) boothItem.Value, EmoneySourceType.Booth, pUser))
                                pUser.AwardEmoney((int) boothItem.Value, EmoneySourceType.Booth, user);
                            else return;
                        }

                        var msgItem = new MsgItem(msg);
                        msgItem.Action = ItemAction.BoothDelete;
                        pUser.Send(msgItem);

                        pUser.Booth.Items.TryRemove(msgItem.Identity, out boothItem);
                        pUser.UserPackage.RemoveFromInventory(msgItem.Identity, RemovalType.RemoveAndDisappear);
                        boothItem.Item.ChangeOwner(user);
                        user.UserPackage.AddItem(boothItem.Item);
                        user.Send(msgItem);
                        pUser.Send(msgItem);

                        msgItem.Action = ItemAction.Remove;
                        pUser.Send(msgItem);

                        user.SendSysMessage(string.Format(Language.StrBoothBought, boothItem.Item.Name, boothItem.Value,
                            boothItem.IsSilver ? "silvers" : "CPs"));
                        pUser.SendMessage(
                            string.Format(Language.StrBoothSold, user.Name, boothItem.Item.Name, boothItem.Value,
                                boothItem.IsSilver ? "silvers" : "CPs"), Color.White, ChatTone.Talk);

                        ServerKernel.Log.GmLog("booth", string.Format(
                            "[userid({0}), bought({1}:{3}), from({2})] {3}\r\n{4}"
                            , user.Identity, boothItem.Item.Identity, pUser.Identity, boothItem.Item.Type,
                            boothItem.Item.ToJson()));
                    }

                    break;
                }

                case ItemAction.Ping: // 27
                {
                    // todo handle ping messages :)
                    user.Send(msg);
                    break;
                }

                case ItemAction.Enchant:
                {
                    Item target = user.UserPackage.Find(msg.Identity), source = user.UserPackage[msg.Param1];
                    if (target != null && source != null)
                    {
                        if (target.Enchantment >= 255)
                            return; // max enchant already

                        if (source.Type / 100 != 7000)
                            return; // not a gem

                        user.UserPackage.RemoveFromInventory(source, RemovalType.Delete);

                        byte gemId = (byte) (source.Type % 100);

                        byte enchant = 1;
                        if (gemId % 10 == 1) //Normal
                            enchant = (byte) ThreadSafeRandom.RandGet(1, 59);
                        else if (gemId == 2 || gemId == 52 || gemId == 62) //Reffined (Phoenix/Violet/Moon)
                            enchant = (byte) ThreadSafeRandom.RandGet(60, 109);
                        else if (gemId == 22 || gemId == 42 || gemId == 72) //Reffined (Fury/Kylin/Tortoise)
                            enchant = (byte) ThreadSafeRandom.RandGet(40, 89);
                        else if (gemId == 12) //Reffined (Dragon)
                            enchant = (byte) ThreadSafeRandom.RandGet(100, 159);
                        else if (gemId == 32) //Reffined (Rainbow)
                            enchant = (byte) ThreadSafeRandom.RandGet(80, 129);
                        else if (gemId == 3 || gemId == 33 || gemId == 73) //Super (Phoenix/Rainbow/Tortoise)
                            enchant = (byte) ThreadSafeRandom.RandGet(170, 229);
                        else if (gemId == 53 || gemId == 63) //Super (Violet/Moon)
                            enchant = (byte) ThreadSafeRandom.RandGet(140, 199);
                        else if (gemId == 13) //Reffined (Dragon)
                            enchant = (byte) ThreadSafeRandom.RandGet(200, 255);
                        else if (gemId == 23) //Reffined (Fury)
                            enchant = (byte) ThreadSafeRandom.RandGet(90, 149);
                        else if (gemId == 43) //Reffined (Kylin)
                            enchant = (byte) ThreadSafeRandom.RandGet(70, 119);

                        msg.Param1 = enchant;
                        user.Send(msg);

                        if (enchant > target.Enchantment)
                        {
                            target.Enchantment = enchant;
                            target.Save();
                            user.Send(target.CreateMsgItemInfo(ItemMode.Update));
                        }

                        ServerKernel.Log.GmLog("enchant",
                            $"User[{user.Identity}] Enchant[Gem: {source.Type}|{source.Identity}][Target: {target.Type}|{target.Identity}] with {enchant} points.");
                    }

                    break;
                }

                case ItemAction.BoothAddCp:
                {
                    if (user.Booth == null || !user.Booth.Vending)
                        return;
                    if (user.Booth.Items.ContainsKey(msg.Identity))
                        return;

                    if ((item = user.UserPackage[msg.Identity]) == null)
                        return;

                    if (item.CanBeSold)
                    {
                        var pSale = new BoothItem();
                        if (!pSale.Create(item, msg.Param1, false))
                            return;
                        user.Booth.Items.TryAdd(item.Identity, pSale);
                        user.Send(msg);
                    }

                    break;
                }

                case ItemAction.TalismanSocketProgress:
                {
                    Item target = user.UserPackage.GetEquipById(msg.Identity),
                        source = user.UserPackage[msg.ReadUInt(88)];
                    if (target != null && source != null)
                    {
                        uint type = source.Type / 1000;
                        if (type == 201 || type == 202 || type == 203)
                            return;

                        if (!user.UserPackage.RemoveFromInventory(source.Identity, RemovalType.Delete))
                            return;

                        target.SocketProgress += source.CalculateSocketingProgress();
                        if (target.SocketOne == SocketGem.NoSocket && target.SocketProgress >= 8000)
                        {
                            target.SocketProgress = 0;
                            target.SocketOne = SocketGem.EmptySocket;
                        }
                        else if (target.SocketTwo == SocketGem.NoSocket
                                 && target.SocketProgress >= 20000)
                        {
                            target.SocketProgress = 0;
                            target.SocketTwo = SocketGem.EmptySocket;
                        }

                        target.Save();
                        user.Send(target.CreateMsgItemInfo(ItemMode.Update));
                        user.Send(msg);
                    }

                    break;
                }

                case ItemAction.TalismanSocketProgressCps:
                {
                    Item pTalismanCps = user.UserPackage.GetEquipById(msg.Identity);
                    uint dwPrice = 0;
                    if (pTalismanCps == null)
                        return;

                    if (pTalismanCps.SocketOne == SocketGem.NoSocket)
                    {
                        if (pTalismanCps.SocketProgress < 2400) return;
                        dwPrice = (uint) (5600 * (1 - pTalismanCps.SocketProgress / 8000f));
                    }
                    else if (pTalismanCps.SocketTwo == SocketGem.NoSocket)
                    {
                        if (pTalismanCps.SocketProgress < 6000) return;
                        dwPrice = (uint) (14000 * (1 - pTalismanCps.SocketProgress / 20000f));
                    }
                    else
                    {
                        return; // player has 2 sockets
                    }

                    if (!user.SpendEmoney((int) dwPrice, EmoneySourceType.ItemImprove, null))
                        return;

                    pTalismanCps.SocketProgress = 0;
                    if (pTalismanCps.SocketOne == SocketGem.NoSocket)
                        pTalismanCps.SocketOne = SocketGem.EmptySocket;
                    else
                        pTalismanCps.SocketTwo = SocketGem.EmptySocket;

                    user.Send(pTalismanCps.CreateMsgItemInfo(ItemMode.Update));
                    user.Send(msg);
                    break;
                }

                case ItemAction.Drop:
                {
                    user.DropItem(msg.Identity, user.MapX, user.MapY);
                    break;
                }

                case ItemAction.TortoiseCompose:
                {
                    Item target = user.UserPackage.Find(msg.Identity);
                    if (target != null)
                    {
                        if (target.Durability == 0)
                            return;
                        if (target.ReduceDamage >= 7)
                        {
                            user.SendSysMessage(Language.StrItemMaximumBlessing);
                            return;
                        }

                        if (target.Durability / 100 < target.MaxDurability / 100)
                        {
                            user.SendSysMessage(Language.StrItemErrRepairItem);
                            return;
                        }

                        ItemPosition position = Calculations.GetItemPosition(target.Type);
                        if (position <= ItemPosition.Inventory
                            || position == ItemPosition.AltBottle
                            || position > ItemPosition.Boots)
                        {
                            user.SendSysMessage("This item cannot be blessed.");
                            return;
                        }

                        switch (target.ReduceDamage)
                        {
                            case 0:
                            {
                                if (!user.UserPackage.MultiSpendItem(700073, 700073, 5, target.IsBound))
                                {
                                    user.SendSysMessage(Language.StrItemBlessingNoTortoise);
                                    return;
                                }

                                target.ReduceDamage = 1;
                                break;
                            }

                            case 1:
                            {
                                if (!user.UserPackage.MultiSpendItem(700073, 700073, 1))
                                {
                                    user.SendSysMessage(Language.StrItemBlessingNoTortoise);
                                    return;
                                }

                                target.ReduceDamage = 3;
                                break;
                            }

                            case 3:
                            {
                                if (!user.UserPackage.MultiSpendItem(700073, 700073, 3))
                                {
                                    user.SendSysMessage(Language.StrItemBlessingNoTortoise);
                                    return;
                                }

                                target.ReduceDamage = 5;
                                break;
                            }

                            case 5:
                            {
                                if (!user.UserPackage.MultiSpendItem(700073, 700073, 5))
                                {
                                    user.SendSysMessage(Language.StrItemBlessingNoTortoise);
                                    return;
                                }

                                target.ReduceDamage = 7;
                                break;
                            }

                            default:
                                return;
                        }

                        target.Save();
                        user.Send(target.CreateMsgItemInfo(ItemMode.Update));
                        msg.Param1 = 1;
                        user.Send(msg);

                        MsgName msgName = new MsgName
                        {
                            Action = StringAction.RoleEffect,
                            Identity = user.Identity
                        };
                        msgName.Append("Aegis4");
                        user.Screen.Send(msgName, true);
                    }

                    break;
                }

                case ItemAction.ActivateAccessory:
                {
                    Item p1 = user.UserPackage[msg.Identity];
                    if (p1 != null)
                    {
                        if (p1.RemainingTime > 0 || p1.Itemtype.LifeTime <= 0)
                            return;
                        p1.RemainingTime = (uint) UnixTimestamp.Now() + p1.Itemtype.LifeTime * 60;
                        p1.Save();
                        user.Send(p1.CreateMsgItemInfo(ItemMode.Update));
                        user.Send(msg);
                        return;
                    }

                    return;
                }

                case ItemAction.SocketEquipment:
                {
                    Item target = user.UserPackage.GetEquipById(msg.Identity) ?? user.UserPackage[msg.Identity];
                    if (target != null)
                    {
                        if (target.Durability / 100 < target.MaxDurability / 100)
                        {
                            user.SendSysMessage(Language.StrItemErrRepairItem);
                            return;
                        }

                        ItemPosition position = Calculations.GetItemPosition(target.Type);
                        msg.Param1 = 0;
                        switch (msg.Param2)
                        {
                            // 1 DB or 1 Tough
                            case 1:
                            {
                                if (position == ItemPosition.RightHand
                                    && target.SocketOne == SocketGem.NoSocket)
                                {
                                    if (!user.UserPackage.SpendDragonBalls(1, target.IsBound))
                                    {
                                        user.SendSysMessage(Language.StrItemErrNoDragonBall);
                                        user.Send(msg);
                                        return;
                                    }

                                    target.SocketOne = SocketGem.EmptySocket;
                                    target.Save();
                                    user.Send(target.CreateMsgItemInfo(ItemMode.Update));
                                    msg.Param1 = 1;
                                    user.Send(msg);
                                    ServerKernel.Log.GmLog("socket_item",
                                        $"User[{user.Name}] Id[{user.Identity}] OpenHole[1] ItemId[{target.Identity}] Itemtype[{target.Itemtype.Type}]");
                                    return;
                                }

                                if (target.SocketOne > SocketGem.NoSocket)
                                {
                                    if (target.SocketTwo != SocketGem.NoSocket)
                                    {
                                        user.SendSysMessage(Language.StrSocketItemMaxSockets);
                                        user.Send(msg);
                                        return;
                                    }

                                    if (!user.UserPackage.SpendItem(SpecialItem.TYPE_TOUGHDRILL))
                                    {
                                        user.SendSysMessage(Language.StrSocketNotEnoughTough);
                                        user.Send(msg);
                                        return;
                                    }

                                    if (!Calculations.ChanceCalc(14.3f))
                                    {
                                        user.SendSysMessage(Language.StrToughDrillSocketFailed);
                                        user.Send(msg);
                                        user.UserPackage.AwardItem(Item.CreateEntity(SpecialItem.TYPE_STARDRILL));
                                        return;
                                    }

                                    ServerKernel.UserManager.SendToAllUser(
                                        string.Format(Language.StrToughDrillSocketSuccess, user.Name));

                                    target.SocketTwo = SocketGem.EmptySocket;
                                    target.Save();
                                    user.Send(target.CreateMsgItemInfo(ItemMode.Update));
                                    msg.Param1 = 1;
                                    user.Send(msg);
                                    return;
                                }

                                //target.UpdateTotemPole();
                                user.Send(msg);
                                ServerKernel.Log.GmLog("socket_item",
                                    $"User[{user.Name}] Id[{user.Identity}] OpenHole[2] ItemId[{target.Identity}] Itemtype[{target.Itemtype.Type}]");
                                return;
                            }

                            // 5 Dragon Balls 2 Socket Weapon
                            case 5:
                            {
                                if (target.SocketOne == SocketGem.NoSocket)
                                {
                                    user.SendSysMessage("Your item has no socket.");
                                    user.Send(msg);
                                    return;
                                }

                                if (target.SocketTwo > SocketGem.NoSocket)
                                {
                                    user.SendSysMessage("Your item already have the max amount of sockets.");
                                    user.Send(msg);
                                    return;
                                }

                                if (!user.UserPackage.SpendDragonBalls(5, target.IsBound))
                                {
                                    user.SendSysMessage("You don't have enough DragonBalls.");
                                    user.Send(msg);
                                    return;
                                }

                                target.SocketTwo = SocketGem.EmptySocket;
                                target.Save();
                                user.Send(target.CreateMsgItemInfo(ItemMode.Update));
                                msg.Param1 = 1;
                                user.Send(msg);
                                //target.UpdateTotemPole();
                                ServerKernel.Log.GmLog("socket_item",
                                    string.Format("User[{0}] Id[{1}] OpenHole[2] ItemId[{2}] Itemtype[{3}]",
                                        user.Name, user.Identity, target.Identity, target.Itemtype.Type));
                                return;
                            }

                            // 7 StarDrills 2 Sockets
                            case 7:
                            {
                                if (target.SocketOne == SocketGem.NoSocket)
                                {
                                    user.SendSysMessage(Language.StrEmbedNoSocket);
                                    user.Send(msg);
                                    return;
                                }

                                if (target.SocketTwo > SocketGem.NoSocket)
                                {
                                    user.SendSysMessage(Language.StrSocketItemMaxSockets);
                                    user.Send(msg);
                                    return;
                                }

                                if (!user.UserPackage.MultiSpendItem(SpecialItem.TYPE_STARDRILL,
                                    SpecialItem.TYPE_STARDRILL, 7))
                                {
                                    user.SendSysMessage(Language.StrEmbedNoRequiredItem);
                                    user.Send(msg);
                                    return;
                                }

                                target.SocketTwo = SocketGem.EmptySocket;
                                target.Save();
                                user.Send(target.CreateMsgItemInfo(ItemMode.Update));
                                msg.Param1 = 1;
                                user.Send(msg);

                                ServerKernel.Log.GmLog("socket_item",
                                    $"User[{user.Name}] Id[{user.Identity}] OpenHole[2] ItemId[{target.Identity}] Itemtype[{target.Itemtype.Type}]");
                                return;
                            }

                            case 12:
                            {
                                if (target.SocketOne > SocketGem.NoSocket)
                                {
                                    user.SendSysMessage("Your item is already socketed.");
                                    user.Send(msg);
                                    return;
                                }

                                if (!user.UserPackage.SpendDragonBalls(12, target.IsBound))
                                {
                                    user.SendSysMessage("You don't have the required items.");
                                    user.Send(msg);
                                    return;
                                }

                                target.SocketOne = SocketGem.EmptySocket;
                                target.Save();
                                user.Send(target.CreateMsgItemInfo(ItemMode.Update));
                                msg.Param1 = 1;
                                user.Send(msg);
                                ServerKernel.Log.GmLog("socket_item",
                                    $"User[{user.Name}] Id[{user.Identity}] OpenHole[1] ItemId[{target.Identity}] Itemtype[{target.Itemtype.Type}]");
                                return;
                            }
                        }
                    }

                    break;
                }

                case ItemAction.AlternativeEquipment:
                    user.IsAlternativeEquipment = !user.IsAlternativeEquipment;
                    user.UpdateAttributes();
                    user.UserPackage.SetCharacter();
                    user.Screen.RefreshSpawnForObservers();
                    user.Send(msg);
                    break;

                case ItemAction.MergeItems:
                    user.UserPackage.CombineItem(msg.Identity, msg.Param1);
                    break;
                case ItemAction.SplitItems:
                    if (!user.UserPackage.SplitItem(msg.Identity, (int) msg.Param1))
                        user.SendSysMessage(Language.StrUnableToUseItem, Color.Red);
                    break;

                case ItemAction.RequestItemTooltip:
                {
                    ItemEntity dbItem = Database.ItemRepository.FetchByIdentity(msg.Identity);
                    if (dbItem == null)
                    {
                        user.SendSysMessage(Language.StrItemTooltipFailed);
                        return;
                    }

                    Item item1 = new Item();
                    if (item1.Create(user, dbItem))
                    {
                        user.Send(item1.CreateMsgItemInfo(ItemMode.ChatItem));
                        item1.SendPurification(user);
                    }

                    break;
                }

                case ItemAction.DegradeEquipment:
                {
                    Item item1 = user.UserPackage[msg.Identity];
                    if (item1 != null)
                    {
                        if (user.Emoney < 54)
                        {
                            user.SendSysMessage(Language.StrNotEnoughCPs);
                            return;
                        }

                        if (!item1.DegradeItem())
                        {
                            user.SendSysMessage(Language.StrItemCantBeDowngraded);
                            return;
                        }

                        user.SpendEmoney(54, EmoneySourceType.ItemImprove, user);
                    }

                    break;
                }

                case ItemAction.ForgingBuy:
                {
                    npc = ServerKernel.RoleManager.GetRole(2888) as Npc;
                    var goodsEntity = ServerKernel.Goods.FirstOrDefault(x =>
                        x.OwnerIdentity == msg.Identity && x.Itemtype == msg.Param1);
                    if (goodsEntity == null)
                        return;

                    if (!ServerKernel.Itemtype.TryGetValue(msg.Param1, out ItemtypeEntity itemtypeEntity))
                    {
                        ServerKernel.Log.GmLog("cheat", $"{user.Identity}:{user.Name} not authentic shop");
                        return;
                    }

                    int nAmount = (int) msg.Param2;

                    if (user.UserPackage.IsPackFull(itemtypeEntity.Type, nAmount))
                    {
                        user.SendSysMessage(Language.StrYourBagIsFull, Color.Red);
                        return;
                    }

                    const int _BOUND = ItemType.ITEM_STORAGE_MASK | ItemType.ITEM_MONOPOLY_MASK;
                    int nMonopoly = 0;
                    if (msg.Param4 == 0) // money or honor
                    {
                        if (goodsEntity.Moneytype != 0)
                            return;
                        if (!user.SpendMoney((int) (itemtypeEntity.Price * nAmount)))
                        {
                            user.SendSysMessage(Language.StrNotEnoughMoney, Color.Red);
                            return;
                        }
                    }
                    else if (msg.Param4 == 1) // CPs
                    {
                        if (goodsEntity.Moneytype != 1)
                            return;
                        if (!user.SpendEmoney((int) (itemtypeEntity.EmoneyPrice * nAmount), EmoneySourceType.Shop, npc))
                        {
                            user.SendSysMessage(Language.StrNotEnoughCPs, Color.Red);
                            return;
                        }
                    }
                    else if (msg.Param4 == 2) // CPs(B)
                    {
                        if (itemtypeEntity.BoundEmoneyPrice <= 0)
                            return;
                        if (!user.SpendBoundEmoney((int) (itemtypeEntity.BoundEmoneyPrice * nAmount)))
                        {
                            user.SendSysMessage(Language.StrNotEnoughCPsB, Color.Red);
                            return;
                        }

                        nMonopoly = _BOUND;
                    }

                    ItemEntity itemEntity = Item.CreateEntity(itemtypeEntity.Type);
                    itemEntity.StackAmount = (ushort) nAmount;
                    itemEntity.Monopoly = (byte) nMonopoly;
                    user.UserPackage.AwardItem(itemEntity);

                    break;
                }
            }
        }
    }
}
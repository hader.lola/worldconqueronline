﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - 10010 - MsgAction.cs
// Last Edit: 2019/12/11 23:00
// Created: 2019/12/07 18:33
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System;
using System.Drawing;
using System.Linq;
using FtwCore;
using FtwCore.Common;
using FtwCore.Common.Enums;
using FtwCore.Database.Entities;
using FtwCore.Database.Repositories;
using FtwCore.Networking.Packets;
using FtwCore.Networking.Sockets;
using GameServer.Structures;
using GameServer.Structures.Entities;
using GameServer.World;

#endregion

namespace GameServer.Networking.GameServer
{
    public static partial class Handlers
    {
        public static void HandleMsgAction(Character user, MsgAction msg)
        {
            try
            {
                if (user.BattleSystem.IsActive())
                    user.BattleSystem.ResetBattle();

                if (user.Magics.QueryMagic() != null)
                    user.Magics.AbortMagic(true);
            }
            catch
            {
            }

            switch (msg.Action)
            {
                case GeneralActionType.SetLocation:
                {
                    Map map;
                    if (ServerKernel.Maps.TryGetValue(user.MapIdentity, out map))
                    {
                        msg.Data = map.MapDoc;
                        //msg.Details = 2;
                        msg.X = user.MapX;
                        msg.Y = user.MapY;
                        //msg.ResponseFlag = true;
                        user.EnterMap();
                        user.Send(msg);
                    }
                    else
                    {
                        // set record pos
                        user.SetRecordPos(1002, 430, 378);
                        ServerKernel.UserManager.KickoutSocket(user, "invalid map set location");
                    }

                    break;
                }

                case GeneralActionType.Hotkeys:
                {
                    user.Send(Reply(msg));
                    break;
                }

                case GeneralActionType.ConfirmFriends:
                {
                    var friend = new FriendRepository().GetUserFriends(user.Identity);
                    if (friend != null)
                    {
                        foreach (var fr in friend)
                        {
                            user.AddFriend(fr);
                        }

                        var msgFriend = new MsgFriend
                        {
                            Identity = user.Identity,
                            Name = user.Name,
                            Online = true,
                            Mode = RelationAction.SET_ONLINE_FRIEND
                        };
                        foreach (var fr in user.Friends.Values.Where(x => x.IsOnline))
                            fr.User.Send(msgFriend);
                    }

                    var enemy = new EnemyRepository().GetUserEnemies(user.Identity);
                    if (enemy != null)
                    {
                        foreach (var en in enemy)
                        {
                            user.AddEnemy(en);
                        }
                    }

                    var buddy = new BusinessRepository().FetchByUser(user.Identity);
                    if (buddy != null)
                    {
                        foreach (var tp in buddy)
                        {
                            TradePartner tradeBuddy = new TradePartner(user, tp);
                            user.Send(tradeBuddy.ToArray(TradePartnerType.ADD_PARTNER));
                            user.TradePartners.TryAdd(tp.Business, tradeBuddy);
                        }
                    }

                    user.Send(Reply(msg));
                    break;
                }

                case GeneralActionType.ConfirmProficiencies:
                {
                    user.WeaponSkill.SendAll();
                    user.Send(Reply(msg));
                    break;
                }

                case GeneralActionType.ConfirmSpells:
                {
                    foreach (var magic in user.Magics.Magics.Values)
                        magic.SendSkill(SkillAction.AddExsisting);

                    user.Send(Reply(msg));
                    break;
                }

                case GeneralActionType.ChangeDirection:
                {
                    user.ChangeDirection(msg.Direction);
                    user.Screen.Send(msg, true);
                    break;
                }

                case GeneralActionType.ChangeAction:
                {
                    user.Action = (EntityAction) msg.Data;
                    if (user.Action == EntityAction.Cool)
                    {
                        int effect = user.IsFullQuality;
                        if (effect == 3)
                        {
                            msg.Data = msg.Details = msg.Data | ((uint) user.Profession * 0x10000 + 0x01000000);
                        }
                        else if (effect >= 1)
                        {
                            msg.Details = msg.Data | ((uint) user.Profession * 0x10000);
                        }
                    }

                    user.Screen.Send(msg, true);
                    break;
                }

                case GeneralActionType.UsePortal:
                {
                    Tile tile = user.Map[msg.LeftData, msg.RightData];
                    if (tile.Access != TileType.Portal)
                    {
                        if (user.Map.RebornMapPoint.X != 0
                            && user.Map.RebornMapPoint.Y != 0)
                        {
                            user.FlyMap(user.Map.RebornMapId > 0 ? user.Map.RebornMapId : 1002u,
                                (ushort) user.Map.RebornMapPoint.X,
                                (ushort) user.Map.RebornMapPoint.Y);
                        }
                        else
                        {
                            user.FlyMap(1002, 430, 378);
                        }

                        return;
                    }

                    if (Calculations.GetDistance(user.MapX, user.MapY,
                            msg.LeftData, msg.RightData) > 2)
                    {
                        user.SendSysMessage(Language.StrInvalidCoordinate, Color.Red);
                        ServerKernel.UserManager.KickoutSocket(user, "invalid portal");
                        return;
                    }

                    user.ChangeMap((uint) tile.Index);
                    break;
                }

                case GeneralActionType.XpClear:
                {
                    if (user.QueryStatus(FlagInt.START_XP) != null)
                        user.DetachStatus(FlagInt.START_XP);
                    break;
                }

                case GeneralActionType.Revive:
                {
                    if (user.IsAlive || !user.CanRevive())
                        return;

                    user.Reborn(msg.Data == 0);
                    break;
                }

                case GeneralActionType.DelRole:
                {
                    user.Delete();
                    user.Send(msg);
                    break;
                }

                case GeneralActionType.ChangePkMode:
                {
                    if (!Enum.IsDefined(typeof(PkModeType), (PkModeType) msg.Data))
                        return;

                    if (user.PkMode == PkModeType.JiangHu)
                    {
                        user.KongFu.LeaveJiangHu();
                    }

                    PkModeType pkMode = (PkModeType) msg.Data;
                    string message = "";
                    switch (pkMode)
                    {
                        case PkModeType.FreePk: // PK Mode
                            message = Language.StrFreePkMode;
                            break;
                        case PkModeType.Peace: // peace
                            message = Language.StrSafePkMode;
                            break;
                        case PkModeType.Team: // team
                            message = Language.StrTeamPkMode;
                            break;
                        case PkModeType.Capture: // capture
                            message = Language.StrArrestmentPkMode;
                            break;
                        case PkModeType.Revenge: // revenge
                            message = Language.StrRevengePkMode;
                            break;
                        case PkModeType.JiangHu:
                            if (!user.KongFu.HasStarted)
                            {
                                user.ChangePkMode(user.PkMode);
                                return;
                            }
                            user.KongFu.SetStatus(true);
                            break;
                        }

                    user.PkMode = pkMode;

                    if (!string.IsNullOrEmpty(message))
                        user.SendSysMessage(message, Color.Red);

                    user.Send(msg);
                    break;
                }

                case GeneralActionType.ConfirmGuild:
                {
                    user.Screen.LoadSurroundings();
                    user.SubClass.Init();

                    user.SyndicateMember = ServerKernel.SyndicateManager.GetMember(user.Identity);
                    if (user.SyndicateMember != null)
                    {
                        user.Syndicate =
                            ServerKernel.SyndicateManager.GetSyndicate(user.SyndicateMember.SyndicateIdentity);
                        user.SyndicateMember?.SendSyndicate();
                    }

                    FamilyMemberEntity family = new FamilyMemberRepository().FetchByUser(user.Identity);
                    if (family != null)
                    {
                        if (ServerKernel.Families.TryGetValue(family.FamilyIdentity, out var pFamily))
                        {
                            user.Family = pFamily;
                            user.FamilyMember = pFamily.Members.Values.FirstOrDefault(x => x.Identity == user.Identity);
                            user.FamilyIdentity = user.FamilyIdentity;
                            user.FamilyPosition = user.FamilyMember.Position;
                            user.FamilyName = user.Family.Name;
                            user.Family.SendFamily(user);
                            user.Family.SendRelation(user);
                        }
                    }

                    user.Send(Reply(msg));
                    break;
                }

                case GeneralActionType.Mine:
                {
                    if (!user.IsAlive)
                    {
                        user.SendSysMessage(Language.StrDie);
                        return;
                    }

                    if (!user.Map.IsMineField())
                    {
                        user.SendSysMessage(Language.StrNoMine);
                        return;
                    }

                    user.DetachStatus(FlagInt.RIDING);

                    user.Mine();
                    break;
                }
                case GeneralActionType.TeamMemberPos:
                {
                    if (user.Team != null && user.Team.Members.ContainsKey(msg.Data))
                    {
                        user.Team.SendMemberPosition(user, msg);
                    }

                    break;
                }

                case GeneralActionType.RequestEntitySpawn:
                {
                    if (msg.Data >= 1000000)
                    {
                        Character pTarget = ServerKernel.UserManager.GetUser(msg.Data);
                        if (pTarget?.IsArenaWitness == true)
                            pTarget.SendSpawnTo(user);
                    }
                    else
                    {
                        ScreenObject target = ServerKernel.RoleManager.GetRole(msg.Data);
                        target?.SendSpawnTo(user);
                    }

                    break;
                }


                case GeneralActionType.QueryTeamMember:
                {
                    if (user.Team != null && user.Team.Members.ContainsKey(msg.Data))
                    {
                        Character pMember = ServerKernel.UserManager.GetUser(msg.Data);
                        if (pMember == null) // should remove from team?
                            return;

                        //MsgTeamMember member = new MsgTeamMember();
                        //member.AppendMember(pMember.Name, pMember.Identity, pMember.Lookface, (ushort) pMember.MaxLife,
                        //    (ushort) pMember.Life);
                        //user.Send(member);

                        //msg.X = pMember.MapX;
                        //msg.Y = pMember.MapY;
                        //user.Send(msg);
                        user.Team.SendMemberPosition(user, msg);
                    }

                    break;
                }

                case GeneralActionType.CreateBooth:
                {
                    if (!user.Map.IsBoothEnable())
                        return;

                    if (user.Booth == null)
                        user.Booth = new PlayerBooth(user);

                    if (user.Booth.Vending)
                    {
                        user.Booth.Destroy();
                    }

                    if (!user.Booth.Create()) return;

                    msg.Data = user.Booth.Identity;
                    msg.X = user.Booth.MapX;
                    msg.Y = user.Booth.MapY;
                    user.Send(msg);
                    break;
                }

                case GeneralActionType.GetSurroundings:
                {
                    if (user.Booth?.Vending == true)
                    {
                        user.Booth.Destroy();
                        user.Booth = null;
                        user.Send(msg);
                        return;
                    }

                    user.Screen.LoadSurroundings();
                    user.Send(Reply(msg));
                    break;
                }

                case GeneralActionType.ObserveEquipment:
                {
                    Character pTarget;
                    if ((pTarget = ServerKernel.UserManager.GetUser(msg.Data)) != null)
                    {
                        foreach (var item in pTarget.UserPackage.GetEquipment())
                        {
                            user.Send(item.CreateMsgItemInfoEx(4));
                            item.SendPurification(user);
                        }

                        msg.ResponseFlag = true;
                        user.Send(msg);
                    }

                    break;
                }

                case GeneralActionType.AbortTransform:
                {
                    if (user.QueryTransformation != null)
                        user.ClearTransformation();
                    break;
                }

                case GeneralActionType.EndFly:
                {
                    if (user.QueryStatus(FlagInt.FLY) != null)
                        user.DetachStatus(FlagInt.FLY);
                    break;
                }

                case GeneralActionType.ViewEnemyInfo:
                {
                    Relationship pRel;
                    if (!user.Enemies.TryGetValue(msg.Data, out pRel))
                    {
                        msg.ResponseFlag = true;
                        user.Send(msg);
                        return;
                    }

                    MsgFriendInfo pMsgInfo = new MsgFriendInfo
                    {
                        Identity = pRel.Identity,
                        IsEnemy = true
                    };
                    if (pRel.IsOnline)
                    {
                        Character pRole = pRel.User;
                        pMsgInfo.Mate = pRole.Mate;
                        pMsgInfo.Level = pRole.Level;
                        pMsgInfo.Mesh = pRole.Lookface;
                        pMsgInfo.SyndicateIdentity = (ushort) pRole.SyndicateIdentity;
                        pMsgInfo.SyndicateRank = pRole.SyndicatePosition;
                        pMsgInfo.Profession = (byte) pRole.Profession;
                        pMsgInfo.PkPoints = pRole.PkPoints;
                        pMsgInfo.Identity = pRole.Identity;
                    }

                    user.Send(pMsgInfo);
                    break;
                }

                case GeneralActionType.Jump:
                {
                    if (!user.IsAlive)
                    {
                        user.SendSysMessage(Language.StrDie, Color.Red);
                        return;
                    }

                    ushort newX = msg.LeftData;
                    ushort newY = msg.RightData;
                    int distance = user.GetDistance(newX, newY);
                    if (!user.IsGm() && distance >= Calculations.SCREEN_DISTANCE * 2)
                    {
                        user.SendSysMessage(Language.InvalidName, Color.Red);
                        ServerKernel.UserManager.KickoutSocket(user, "big jump");
                        return;
                    }

                    int deltaX = newX - user.MapX;
                    int deltaY = newY - user.MapY;
                    if (!user.Map.SampleElevation(distance, user.MapX, user.MapY, deltaX, deltaY,
                        user.Elevation))
                    {
                        user.Kickback(user.MapX, user.MapY);
                        return;
                    }

                    uint dwVigorConsume = 0;
                    if (user.QueryStatus(FlagInt.RIDING) != null)
                    {
                        dwVigorConsume = (uint) (1 * distance);
                        if (dwVigorConsume > 0 && dwVigorConsume > user.Vigor)
                        {
                            user.Kickback(user.MapX, user.MapY);
                            return;
                        }
                    }

                    if (user.GetDistance(newX, newY) >= Calculations.SCREEN_DISTANCE * 2)
                    {
                        user.ProcessOnMove(MovementType.TRANS);
                        user.TransPos(newX, newY);
                    }
                    else
                    {
                        user.ProcessOnMove(MovementType.JUMP);
                        user.JumpPos(newX, newY);
                    }

                    user.Vigor -= dwVigorConsume;
                    user.Send(msg);
                    user.Screen.SendMovement(msg);
                    break;
                }

                case GeneralActionType.DieQuestion:
                {
                    if (user.IsAlive)
                        return;
                    user.SetGhost();
                    break;
                }

                case GeneralActionType.EndTeleport:
                {
                    // get surroundings?
                    user.Screen.LoadSurroundings();
                    break;
                }

                case GeneralActionType.ViewFriendInfo:
                {
                    if (!user.Friends.TryGetValue(msg.Data, out var pRel))
                    {
                        msg.ResponseFlag = true;
                        user.Send(msg);
                        return;
                    }

                    MsgFriendInfo pMsgInfo = new MsgFriendInfo
                    {
                        Identity = pRel.Identity
                    };
                    if (pRel.IsOnline)
                    {
                        Character pRole = pRel.User;
                        pMsgInfo.Mate = pRole.Mate;
                        pMsgInfo.Level = pRole.Level;
                        pMsgInfo.Mesh = pRole.Lookface;
                        pMsgInfo.SyndicateIdentity = (ushort) pRole.SyndicateIdentity;
                        pMsgInfo.SyndicateRank = pRole.SyndicatePosition;
                        pMsgInfo.Profession = (byte) pRole.Profession;
                        pMsgInfo.PkPoints = pRole.PkPoints;
                        pMsgInfo.Identity = pRole.Identity;
                    }

                    user.Send(pMsgInfo);
                    break;
                }

                case GeneralActionType.ChangeFace:
                {
                    if (!user.SpendMoney(500))
                        return;

                    user.Avatar = (ushort) msg.Data;
                    user.Screen.Send(msg, true);
                    break;
                }

                case GeneralActionType.ViewPartnerInfo:
                {
                    Character pTarget = ServerKernel.UserManager.GetUser(msg.Data);
                    if (pTarget == null)
                    {
                        msg.ResponseFlag = true;
                        user.Send(msg);
                        return;
                    }

                    if (user.TradePartners.ContainsKey(pTarget.Identity))
                    {
                        MsgTradeBuddyInfo pInfo = new MsgTradeBuddyInfo
                        {
                            Identity = pTarget.Identity,
                            Level = pTarget.Level,
                            Lookface = pTarget.Lookface,
                            Name = pTarget.Mate,
                            PkPoints = pTarget.PkPoints,
                            Profession = pTarget.Profession,
                            SyndicateIdentity = pTarget.SyndicateIdentity,
                            SyndicateRank = pTarget.SyndicatePosition
                        };
                        user.Send(pInfo);
                    }

                    msg.ResponseFlag = true;
                    user.Send(msg);
                    break;
                }

                case GeneralActionType.Away:
                {
                    user.IsAway = msg.Data != 0;
                    user.Screen.Send(msg, true);
                    break;
                }

                case GeneralActionType.ChangeLook:
                {
                    msg.Identity = user.Identity;
                    user.CurrentLayout = (byte) (msg.Data % 4);
                    user.Screen.Send(msg, true);
                    break;
                }

                case GeneralActionType.CompleteLogin:
                {
                    user.Owner.UserState = PlayerState.Connected;
                    user.LoginComplete = true;


                    user.UserPackage.SendAllItems();
                    //user.UpdateAttributes();
                    user.GameAction.ProcessAction(1000000, user, null, null, null);
#if DEBUG
                    user.GameAction.ProcessAction(1500000, user, null, null, null);
#endif

                    user.UpdateClient(ClientUpdateType.PkPoints, user.PkPoints, true);

                    ServerKernel.Pigeon.SendLastMessage(user);

                    if ( /*user.Gender == 1
                        && */user.Level >= 40
                             && user.RedRoses < uint.Parse(DateTime.Now.ToString("yyyyMMdd")))
                    {
                        user.Send(new MsgFlower
                        {
                            Mode = user.Gender == 1 ? 2u : 3u,
                            RedRoses = 1
                        });
                    }

                    user.Send(new MsgFlower
                    {
                        Mode = user.Gender == 1 ? 3u : 2u,
                        RedRoses = 0,
                        RedRosesToday = 0,
                        WhiteRoses = 0,
                        WhiteRosesToday = 0,
                        Orchids = 0,
                        OrchidsToday = 0,
                        Tulips = 0,
                        TulipsToday = 0
                    });

                    if (user.CoinMoney > 0)
                        user.Send(new MsgAction(user.Identity, 1197u, 0, 0, GeneralActionType.OpenCustom));

                    user.ChiSystem?.SendInfo(true);
                    user.KongFu.Initialize();
                    user.DoLevelCheck();
                    user.Nobility?.SendNobilityIcon();

                    LoadTitles(user);

                    int now = UnixTimestamp.Now();
                    var dbStatus = new StatusRepository().LoadStatus(user.Identity);
                    if (dbStatus != null)
                    {
                        foreach (var stts in dbStatus)
                        {
                            if (now > stts.EndTime)
                            {
                                new StatusRepository().Delete(stts);
                                continue;
                            }

                            int remaining = (int) (stts.EndTime - now);
                            user.AttachStatus(user, (int) stts.Status, 0, remaining, 1, 0);
                            if (stts.Status == FlagInt.CURSED)
                                user.UpdateClient(ClientUpdateType.CursedTimer, (uint) remaining);
                        }
                    }

                    user.SendBless();

                    user.UpdateClient(ClientUpdateType.PkPoints, user.PkPoints);
                    user.PkPoints = user.PkPoints;

                    MsgHangUp autoHuntTest = new MsgHangUp();
                    autoHuntTest.WriteUShort(341, 6);
                    user.Send(autoHuntTest);

                    if (!user.IsUnlocked())
                    {
                        Msg2ndPsw pw2nd = new Msg2ndPsw
                        {
                            Request = PasswordRequestType.CorrectPassword,
                            Password = 0x1
                        };
                        user.Send(pw2nd);
                    }

                    user.Send(new MsgAction(user.Identity, user.CurrentLayout, user.CurrentLayout, 0, GeneralActionType.ChangeLook));

                    user.SendMultipleExp();

                    user.MailBox.Init();

                    user.UpdateClient(ClientUpdateType.Merchant, 255, false);

                    ServerKernel.QuizShow.InsertPlayer(user);

                    user.Achievement.SendAll();

                    user.UpdateAttributes();

                    if (user.Life == 0)
                    {
                        user.Reborn(true);
                    }
                    else if (user.Life >= user.MaxLife)
                    {
                        user.Life = user.MaxLife;
                    }

                    user.Send(Reply(msg));
                    break;
                }

                case GeneralActionType.UpgradeMagicSkill:
                    const int minCps = 1;
                {
                    Magic skill = user.Magics[(ushort) msg.Data];
                    if (skill == null)
                        return;

                    if (skill.Level >= skill.MaxLevel)
                        return;

                    int amount = Math.Max(minCps,
                        (int) ((1 - skill.Experience / (double) skill.NeedExp) * skill.CpsCost));
                    if (!user.SpendEmoney(amount, EmoneySourceType.ItemImprove, null, true))
                        return;

                    skill.Level += 1;
                    skill.Experience = 0;

                    user.Send(new MsgMagicInfo(skill.Experience, skill.Level, skill.Type));
                    user.Send(Reply(msg));
                    break;
                }

                case GeneralActionType.UpgradeWeaponSkill:
                {
                    int[] values =
                        {0, 27, 27, 27, 27, 27, 54, 81, 135, 162, 270, 324, 324, 324, 324, 375, 548, 799, 1154, 1420};
                    if (!user.WeaponSkill.Skills.TryGetValue((ushort) msg.Data, out WeaponSkillEntity weapon) ||
                        weapon.Level >= MsgWeaponSkill.MAX_PROFICIENCY_LEVEL)
                        return;

                    int nAmount = (int) Math.Ceiling(values[weapon.Level] *
                                                     (1 - weapon.Experience /
                                                      (double) MsgWeaponSkill.EXP_PER_LEVEL[weapon.Level]));
                    if (!user.SpendEmoney(nAmount, EmoneySourceType.ItemImprove, null, true))
                        return;

                    weapon.Level += 1;
                    weapon.Experience = 0;

                    new WeaponSkillRepository().Save(weapon);
                    user.Send(new MsgWeaponSkill(weapon.Type, weapon.Level, weapon.Experience));
                    user.Send(Reply(msg));
                    break;
                }

                case GeneralActionType.ObserveFriendEquipment:
                {
                    msg.ResponseFlag = true;
                    Character pTarget = ServerKernel.UserManager.GetUser(msg.Data);
                    if (pTarget == null)
                    {
                        user.Send(msg);
                        return;
                    }

                    pTarget.SendWindowSpawnTo(user);
                    user.Send(msg);
                    break;
                }

                case GeneralActionType.ActionRequestUserInfo:
                {
                    //if (msg.Identity == user.Identity)
                    {
                        user.UpdateAttributes();
                        user.SendUserAttribInfo();
                    }

                    break;
                }

                default:
                    Program.WriteLog($"MsgAction:{msg.Action} not handled");
                    if (user.IsGm())
                        user.SendMessage($"MsgAction:{msg.Action} not handled", ChatTone.Service);
                    break;
            }
        }

        private static MsgAction Reply(MsgAction msg)
        {
            return msg;
            //return new MsgAction(msg) { ResponseFlag = true, TimeStamp = Time.Now };
        }
    }
}
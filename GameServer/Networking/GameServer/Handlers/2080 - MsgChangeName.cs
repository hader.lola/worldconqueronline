﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - 2080 - MsgChangeName.cs
// Last Edit: 2019/11/24 19:03
// Created: 2019/11/12 22:52
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System.Drawing;
using FtwCore;
using FtwCore.Common;
using FtwCore.Common.Enums;
using FtwCore.Database.Entities;
using FtwCore.Database.Repositories;
using FtwCore.Networking.Packets;
using GameServer.Structures.Entities;

#endregion

namespace GameServer.Networking.GameServer
{
    public static partial class Handlers
    {
        public static void HandleMsgChangeName(MsgChangeName msg, Character user)
        {
            const int _MAX_CHANGE_TIMES = 3;
            const int _CHECK_CHANGE_TIME = UnixTimestamp.TIME_SECONDS_DAY * 365;
            const int _CHANGE_NAME_PRICE = 215000;

            NameChangeLogRepository repository = new NameChangeLogRepository();
            int remainingTimes = repository.RemainingAmount(user.Identity, _CHECK_CHANGE_TIME);
            bool queuedChange = repository.HasQueuedChange(user.Identity);

            switch (msg.Mode)
            {
                case ChangeNameMode.REQUEST_INFO:
                {
                    msg.Param1 = (ushort) (_MAX_CHANGE_TIMES - remainingTimes);
                    msg.Param2 = (ushort) remainingTimes;
                    user.Send(msg);
                    break;
                }

                case ChangeNameMode.CHANGE_NAME:
                {
                    if (queuedChange)
                    {
                        user.SendSysMessage(Language.StrChangeNameQueued);
                        return;
                    }

                    if (remainingTimes <= 0)
                    {
                        user.SendSysMessage(Language.StrChangeNameMaximumReached);
                        return;
                    }

                    msg.Name = msg.Name.Replace(' ', '~');

                    if (!CheckName(msg.Name))
                    {
                        user.SendSysMessage(Language.StrChangeNameInvalidCharacters);
                        return;
                    }

                    if (!repository.NameIsDisponible(msg.Name))
                    {
                        user.SendSysMessage(Language.StrChangeNameBlockedName);
                        return;
                    }

                    if (Database.CharacterRepository.CharacterExists(msg.Name))
                    {
                        user.SendSysMessage(Language.StrChangeNameNameAlreadyInUse);
                        return;
                    }

                    if (!user.SpendEmoney(_CHANGE_NAME_PRICE, EmoneySourceType.ChangeName, null, true))
                    {
                        return;
                    }

                    if (!repository.Save(new NameChangeLogEntity
                    {
                        NewName = msg.Name,
                        OldName = user.Name,
                        UserIdentity = user.Identity,
                        Timestamp = (uint) UnixTimestamp.Now(),
                        Changed = 0
                    }))
                    {
                        user.AwardEmoney(_CHANGE_NAME_PRICE, EmoneySourceType.ChangeName, null);
                        user.SendSysMessage(string.Format(Language.StrChangeNameFailed, _CHANGE_NAME_PRICE));
                        return;
                    }

                    ServerKernel.UserManager.SendToAllUser(
                        string.Format(Language.StrChangeNameSuccess, user.Name, msg.Name), Color.White, ChatTone.Talk);
                    msg.Mode = ChangeNameMode.CHANGED_SUCCESSFULY;
                    user.Send(msg);

                    user.Name = msg.Name;
                    break;
                }
            }
        }
    }
}
﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - 1056 - MsgTrade.cs
// Last Edit: 2019/11/24 19:03
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using FtwCore;
using FtwCore.Networking.Packets;
using GameServer.Structures;
using GameServer.Structures.Entities;

#endregion

namespace GameServer.Networking.GameServer
{
    public static partial class Handlers
    {
        public static void HandleMsgTrade(Character user, MsgTrade msg)
        {
            switch (msg.Type)
            {
                #region Request

                case TradeType.REQUEST:
                {
                    Character pAcceptRole = ServerKernel.UserManager.GetUser(msg.Target);
                    if (pAcceptRole == null)
                    {
                        return;
                    }

                    if (pAcceptRole.FetchTradeRequest(user.Identity))
                    {
                        user.Trade = pAcceptRole.Trade = new Trade(user, pAcceptRole);
                        user.Trade.ShowTable();
                        pAcceptRole.ClearTradeRequest();
                        return;
                    }
                    
                    if (pAcceptRole.Trade == null)
                    {
                        msg.Target = user.Identity;
                        pAcceptRole.Send(msg);
                        pAcceptRole.SendRelation(user);
                        user.SetTradeRequest(pAcceptRole.Identity);
                        user.SendSysMessage(Language.StrTradeRequestSent);
                    }
                    else
                    {
                        user.SendSysMessage(Language.StrTargetTrading);
                    }

                    break;
                }

                #endregion

                #region Time out

                case TradeType.TIME_OUT:
                {
                    GamePacketHandler.Report(msg);
                    //Console.WriteLine("Trade close due to timeout");
                    //pSender.Trade.CloseWindow(pMsg);
                    break;
                }

                #endregion

                #region Accept Trade

                case TradeType.ACCEPT:
                {
                    user.Trade?.AcceptTrade(user, msg);
                    break;
                }

                #endregion

                #region Add Item

                case TradeType.ADD_ITEM:
                {
                    user.Trade?.AddItem(msg.Target, user);
                    break;
                }

                #endregion

                #region Set Money

                case TradeType.SET_MONEY:
                {
                    user.Trade?.AddMoney(msg.Target, user, msg);
                    break;
                }

                #endregion

                #region Set Conquer Points

                case TradeType.SET_CONQUER_POINTS:
                {
                    user.Trade?.AddEmoney(msg.Target, user, msg);
                    break;
                }

                #endregion

                #region Close

                case TradeType.CLOSE:
                {
                    user.Trade?.CloseWindow(msg);
                    break;
                }

                #endregion
            }
        }
    }
}
﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - 1027 - MsgGemEmbed.cs
// Last Edit: 2019/11/24 19:03
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System;
using FtwCore;
using FtwCore.Common.Enums;
using FtwCore.Networking.Packets;
using GameServer.Structures.Entities;
using GameServer.Structures.Items;

#endregion

namespace GameServer.Networking.GameServer
{
    public static partial class Handlers
    {
        public static void HandleMsgGemEmbed(Character user, MsgGemEmbed msg)
        {
            if (!user.IsAlive)
            {
                user.SendSysMessage(Language.StrDie);
                return;
            }

            Item target = user.UserPackage.Find(msg.MainIdentity), source = user.UserPackage[msg.MinorIdentity];

            if (target == null
                || (source == null && msg.Mode == EmbedMode.GEM_ADD))
                return;

            switch (msg.Mode)
            {
                #region Gem Add
                case EmbedMode.GEM_ADD:
                    {
                        SocketGem gem = (SocketGem)(source.Type % 1000);

                        if (!Enum.IsDefined(typeof(SocketGem), (byte)gem) || source.GetItemSubtype() != 700)
                        {
                            user.SendSysMessage(Language.StrEmbedNotGem);
                            return;
                        }

                        if (target.GetItemSubtype() == 201)
                        {
                            switch (gem)
                            {
                                case SocketGem.NormalThunderGem:
                                case SocketGem.RefinedThunderGem:
                                case SocketGem.SuperThunderGem:
                                    break;
                                default:
                                    return;
                            }
                        }

                        if (target.GetItemSubtype() == 202)
                        {
                            switch (gem)
                            {
                                case SocketGem.NormalGloryGem:
                                case SocketGem.RefinedGloryGem:
                                case SocketGem.SuperGloryGem:
                                    break;
                                default:
                                    return;
                            }
                        }

                        if (target.GetItemSubtype() != 201 && target.GetItemSubtype() != 202)
                        {
                            switch (gem)
                            {
                                case SocketGem.NormalThunderGem:
                                case SocketGem.RefinedThunderGem:
                                case SocketGem.SuperThunderGem:
                                case SocketGem.NormalGloryGem:
                                case SocketGem.RefinedGloryGem:
                                case SocketGem.SuperGloryGem:
                                    return;
                            }
                        }

                        if (msg.HoleNum == 1 || (msg.HoleNum == 2 && target.SocketOne == SocketGem.EmptySocket))
                        {
                            if (target.SocketOne == SocketGem.NoSocket)
                            {
                                user.SendSysMessage(Language.StrEmbedTargetNoSocket);
                                return;
                            }

                            if (!user.UserPackage.RemoveFromInventory(source, RemovalType.Delete))
                            {
                                user.SendSysMessage(Language.StrEmbedNoRequiredItem);
                                return;
                            }

                            target.SocketOne = gem;
                            break;
                        }

                        if (msg.HoleNum == 2)
                        {
                            if (target.SocketOne > SocketGem.NoSocket
                                || target.SocketOne < SocketGem.EmptySocket)
                            {
                                if (target.SocketTwo == SocketGem.NoSocket)
                                {
                                    user.SendSysMessage(Language.StrEmbedNoSecondSocket);
                                    return;
                                }

                                if (target.SocketTwo != SocketGem.EmptySocket)
                                {
                                    user.SendSysMessage(Language.StrEmbedSocketAlreadyFilled);
                                    return;
                                }

                                if (!user.UserPackage.RemoveFromInventory(source, RemovalType.Delete))
                                {
                                    user.SendSysMessage(Language.StrEmbedNoRequiredItem);
                                    return;
                                }

                                target.SocketTwo = gem;
                            }
                        }
                        break;
                    }
                #endregion
                #region Gem Remove
                case EmbedMode.GEM_REMOVE:
                    {
                        if (msg.HoleNum == 1)
                        {
                            if (target.SocketOne == SocketGem.NoSocket)
                            {
                                user.SendSysMessage(Language.StrEmbedNoSocket);
                                return;
                            }
                            if (target.SocketOne == SocketGem.EmptySocket)
                            {
                                user.SendSysMessage(Language.StrEmbedNoGem);
                                return;
                            }

                            target.SocketOne = SocketGem.EmptySocket;

                            if (target.SocketTwo > SocketGem.NoSocket && target.SocketTwo < SocketGem.EmptySocket)
                            {
                                target.SocketOne = target.SocketTwo;
                                target.SocketTwo = SocketGem.EmptySocket;
                            }
                            break;
                        }

                        if (msg.HoleNum == 2)
                        {
                            if (target.SocketTwo == SocketGem.NoSocket)
                            {
                                user.SendSysMessage(Language.StrEmbedNoSocket2);
                                return;
                            }

                            if (target.SocketTwo == SocketGem.EmptySocket)
                            {
                                user.SendSysMessage(Language.StrEmbedNoGem);
                                return;
                            }

                            target.SocketTwo = SocketGem.EmptySocket;
                        }
                        break;
                    }
                    #endregion
            }

            user.Send(target.CreateMsgItemInfo(ItemMode.Update));
            user.Send(msg);
        }
    }
}
﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - 2046 - MsgTradeBuddy.cs
// Last Edit: 2019/11/24 19:03
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using FtwCore;
using FtwCore.Common;
using FtwCore.Database.Repositories;
using FtwCore.Networking.Packets;
using GameServer.Structures;
using GameServer.Structures.Entities;

#endregion

namespace GameServer.Networking.GameServer
{
    public static partial class Handlers
    {
        public static void HandleMsgTradeBuddy(Character user, MsgTradeBuddy msg)
        {
            switch (msg.Type)
            {
                #region Request Partnership

                case TradePartnerType.REQUEST_PARTNERSHIP:
                {
                    Character pRoleTarget = ServerKernel.UserManager.GetUser(msg.Identity);
                    if (pRoleTarget == null)
                    {
                        //pRole.Send(ServerString.STR_TRADE_BUDDY_NOT_FOUND);
                        return;
                    }

                    if (user.FetchTradeBuddyRequest(pRoleTarget.Identity))
                    {
                        if (pRoleTarget.TradePartners.ContainsKey(user.Identity)
                            || user.TradePartners.ContainsKey(pRoleTarget.Identity))
                        {
                            user.SendSysMessage(Language.StrTradeBuddyAlreadyAdded);
                            return;
                        }

                        var pSender = new TradePartner(user);
                        var pTarget = new TradePartner(pRoleTarget);
                        if (!pSender.Create(pRoleTarget.Identity)
                            || !pTarget.Create(user.Identity))
                        {
                            user.SendSysMessage(Language.StrTradeBuddySomethingWrong);
                            return;
                        }

                        user.Screen.Send(string.Format(Language.StrTradeBuddyAnnouncePartnership,
                            user.Name, pRoleTarget.Name), true);
                    }
                    else
                    {
                        pRoleTarget.SetTradeBuddyRequest(user.Identity);
                        msg.Identity = user.Identity;
                        msg.Name = user.Name;
                        pRoleTarget.Send(msg);
                        pRoleTarget.SendRelation(user);
                    }

                    break;
                }

                #endregion

                #region Reject Request

                case TradePartnerType.REJECT_REQUEST:
                {
                    Character pTarget = ServerKernel.UserManager.GetUser(msg.Identity);
                    if (pTarget != null)
                    {
                        msg.Identity = user.Identity;
                        msg.Name = user.Name;
                        msg.Online = true;
                        pTarget.Send(msg);
                    }

                    break;
                }

                #endregion

                #region Break partnership

                case TradePartnerType.BREAK_PARTNERSHIP:
                {
                    TradePartner pTarget;
                    if (user.TradePartners.TryRemove(msg.Identity, out pTarget))
                    {
                        pTarget.Delete();
                        if (pTarget.TargetOnline)
                        {
                            TradePartner trash;
                            pTarget.Owner.TradePartners.TryRemove(user.Identity, out trash);
                            trash.Delete();
                            pTarget.Owner.SendSysMessage(string.Format(Language.StrTradeBuddyBrokePartnership0,
                                user.Name));
                        }
                        else
                        {
                            new BusinessRepository().DeleteBusiness(user.Identity, pTarget.OwnerIdentity);
                        }

                        user.SendSysMessage(string.Format(Language.StrTradeBuddyBrokePartnership1, pTarget.Name));
                    }

                    break;
                }

                #endregion

                default:
                {
                    Program.WriteLog("Missing packet handler for type 2046:" + msg.Type, LogType.WARNING);
                    GamePacketHandler.Report(msg);
                    break;
                }
            }
        }
    }
}
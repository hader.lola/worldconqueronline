﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - 1015 - MsgName.cs
// Last Edit: 2019/11/24 19:03
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using FtwCore.Common;
using FtwCore.Networking.Packets;
using GameServer.Structures.Entities;

#endregion

namespace GameServer.Networking.GameServer
{
    public static partial class Handlers
    {
        public static void HandleMsgString(Character pUser, MsgName pMsg)
        {
            if (pUser == null) return;

            switch (pMsg.Action)
            {
                #region Query Mate

                case StringAction.QueryMate:
                {
                    Character target = ServerKernel.UserManager.GetUser(pMsg.Identity);
                    if (target != null)
                    {
                        var pStr = new MsgName
                        {
                            Identity = target.Identity,
                            Action = StringAction.QueryMate
                        };
                        pStr.Append(target.Mate);
                        pUser.Send(pStr);
                    }

                    break;
                }

                #endregion

                #region Whisper message

                case StringAction.WhisperWindowInfo:
                {
                    if (pMsg.Strings().Count <= 0) return;
                    Character pTarget = ServerKernel.UserManager.GetUser(pMsg.Strings()[0]);
                    if (pTarget != null)
                    {
                        MsgName newmsg = new MsgName {Action = StringAction.WhisperWindowInfo};
                        newmsg.Identity = pTarget.Identity;
                        newmsg.Append(pTarget.Name);
                        string msg = string.Format("{0} {1} {2} {3} {4} {5} {6} {7}",
                            pTarget.Identity,
                            pTarget.Level,
                            pTarget.BattlePower,
                            "#" + pTarget.SyndicateName,
                            " #None", // + pTarget.FamilyName,
                            pTarget.Mate,
                            (byte) pTarget.NobilityRank,
                            pTarget.Gender);
                        newmsg.Append(msg);
                        pUser.Send(newmsg);
                    }
                    break;
                }

                #endregion

                default:
                    ServerKernel.Log.SaveLog("String packet missing handler " + pMsg.Action, LogType.WARNING);
                    break;
            }
        }
    }
}
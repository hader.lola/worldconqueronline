﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - 1023 - MsgTeam.cs
// Last Edit: 2019/11/24 19:03
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using FtwCore;
using FtwCore.Common.Enums;
using FtwCore.Networking.Packets;
using GameServer.Structures;
using GameServer.Structures.Entities;

#endregion

namespace GameServer.Networking.GameServer
{
    public static partial class Handlers
    {
        public static void HandleMsgTeam(Character user, MsgTeam msg)
        {
            if (!user.IsAlive)
            {
                user.SendSysMessage(Language.StrDie);
                return;
            }

            switch (msg.Type)
            {
                #region Create

                case TeamActionType.CREATE:
                {
                    if (user.Team != null)
                    {
                        user.SendSysMessage(Language.StrTeamCannotCreate);
                        return; // already have a team
                    }

                    if (!user.AttachStatus(user, FlagInt.TEAM_LEADER, 0, int.MaxValue, int.MaxValue, 0))
                    {
                        user.SendSysMessage(Language.StrTeamCreateFailed);
                        return;
                    }

                    user.Team = new Team(user);
                    user.Send(msg);
                    msg.Type = TeamActionType.LEADER;
                    user.Send(msg);
                    break;
                }

                #endregion

                #region Dismiss

                case TeamActionType.DISMISS:
                {
                    if (user.Team == null)
                    {
                        user.SendSysMessage(Language.StrTeamFailedToDismiss);
                        return;
                    }

                    // user.Team.Destroy(user, msg);
                    user.Team.LeaveTeam(user, msg);
                    user.Team = null;
                    break;
                }

                #endregion

                #region Leave Team

                case TeamActionType.LEAVE_TEAM:
                {
                    if (user.Team == null)
                    {
                        user.SendSysMessage(Language.StrTeamFailedToLeave);
                        return;
                    }

                    user.Team.LeaveTeam(user, msg);
                    break;
                }

                #endregion

                #region Kick Member

                case TeamActionType.KICK:
                {
                    if (user.Team == null)
                    {
                        user.SendSysMessage(Language.StrTeamFailedToLeave);
                        return;
                    }

                    user.Team.KickMember(user, msg);
                    break;
                }

                #endregion

                #region Request Join (Applicant want to join)

                case TeamActionType.REQUEST_JOIN:
                {
                    if (user.Team != null)
                    {
                        user.SendSysMessage(Language.StrTeamAlreadyJoined);
                        return; // already has a team
                    }

                    Character pUserTarget = ServerKernel.UserManager.GetUser(msg.Target);
                    if (pUserTarget == null || pUserTarget.MapIdentity != user.MapIdentity)
                    {
                        user.SendSysMessage(Language.StrTeamNoCapitainClose);
                        return;
                    }

                    if (pUserTarget.Team == null)
                    {
                        user.SendSysMessage(Language.StrTeamTargetNoTeam);
                        return;
                    }

                    pUserTarget.Team.RequestMember(user, pUserTarget, msg);
                    break;
                }

                #endregion

                #region Request Invite (Applicant is inviting)

                case TeamActionType.REQUEST_INVITE:
                {
                    if (user.Team == null)
                    {
                        user.SendSysMessage(Language.StrTeamNoTeamToInvite);
                        return; // dont have a team, cant invite
                    }

                    Character pUserTarget = ServerKernel.UserManager.GetUser(msg.Target);
                    if (pUserTarget == null || pUserTarget.MapIdentity != user.MapIdentity)
                    {
                        user.SendSysMessage(Language.StrTeamApplicantNotFound);
                        return;
                    }

                    if (pUserTarget.Team != null)
                    {
                        user.SendSysMessage(Language.StrTeamHasInTeam);
                        return;
                    }

                    user.Team.InviteMember(pUserTarget, user, msg);
                    break;
                }

                #endregion

                #region Accept Team (Me accepting invitation)

                case TeamActionType.ACCEPT_INVITE:
                {
                    if (user.Team != null)
                    {
                        user.SendSysMessage(Language.StrTeamHaveJoinTeam);
                        return; // already has a team
                    }

                    Character pLeader = ServerKernel.UserManager.GetUser(msg.Target);
                    if (pLeader == null || pLeader.MapIdentity != user.MapIdentity)
                    {
                        user.SendSysMessage(Language.StrTeamNoCapitainClose);
                        return;
                    }

                    pLeader.Team.MemberAccept(pLeader, user, msg);
                    break;
                }

                #endregion

                #region Accept Join (Leader accepting join request)

                case TeamActionType.ACCEPT_JOIN:
                {
                    if (user.Team == null)
                    {
                        user.SendSysMessage(Language.StrTeamNoTeamToInvite);
                        return; // dont have a team, cant accept
                    }

                    Character pUserTarget = ServerKernel.UserManager.GetUser(msg.Target);
                    if (pUserTarget == null || pUserTarget.MapIdentity != user.MapIdentity)
                    {
                        user.SendSysMessage(Language.StrTeamApplicantNotFound);
                        return;
                    }

                    if (pUserTarget.Team != null)
                    {
                        user.SendSysMessage(Language.StrTeamHasInTeam);
                        return;
                    }

                    user.Team.AcceptMember(user, pUserTarget, msg);
                    break;
                }

                #endregion

                #region Forbid Join

                case TeamActionType.JOIN_DISABLE:
                {
                    if (user.Team == null || user.Team.Leader != user)
                        return;
                    user.Team.SetForbid(true);
                    user.Send(msg);
                    break;
                }

                #endregion

                #region Enable Join

                case TeamActionType.JOIN_ENABLE:
                {
                    if (user.Team == null || user.Team.Leader != user)
                        return;
                    user.Team.SetForbid(false);
                    user.Send(msg);
                    break;
                }

                #endregion

                #region Open Item

                case TeamActionType.ITEM_ENABLE:
                {
                    if (user.Team == null || user.Team.Leader != user)
                        return;
                    user.Team.SetCloseItem(true);
                    user.Send(msg);
                    break;
                }

                #endregion

                #region Close Item

                case TeamActionType.ITEM_DISABLE:
                {
                    if (user.Team == null || user.Team.Leader != user)
                        return;
                    user.Team.SetCloseItem(false);
                    user.Send(msg);
                    break;
                }

                #endregion

                #region Open Money

                case TeamActionType.MONEY_ENABLE:
                {
                    if (user.Team == null || user.Team.Leader != user)
                        return;
                    user.Team.SetCloseMoney(true);
                    user.Send(msg);
                    break;
                }

                #endregion

                #region Close Money

                case TeamActionType.MONEY_DISABLE:
                {
                    if (user.Team == null || user.Team.Leader != user)
                        return;
                    user.Team.SetCloseMoney(false);
                    user.Send(msg);
                    break;
                }

                #endregion

                #region Cancel Join or Invite Request

                case (TeamActionType) 14:
                {
                    Character pTarget = ServerKernel.UserManager.GetUser(msg.Target);
                    if (pTarget?.Team != null)
                    {
                        pTarget.SetTeamInvite(0);
                        pTarget.SetTeamJoin(0);
                    }

                    break;
                }

                #endregion
            }
        }
    }
}
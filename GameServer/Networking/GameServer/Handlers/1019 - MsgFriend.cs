﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - 1019 - MsgFriend.cs
// Last Edit: 2019/11/24 19:03
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using FtwCore;
using FtwCore.Common;
using FtwCore.Networking.Packets;
using GameServer.Structures.Entities;

#endregion

namespace GameServer.Networking.GameServer
{
    public static partial class Handlers
    {
        public static readonly uint[] FriendVipAmount = { 50, 60, 70, 80, 90, 100, 120 };

        public static void HandleMsgFriend(Character user, MsgFriend msg)
        {
            switch (msg.Mode)
            {
                #region Request Friend

                case RelationAction.REQUEST_FRIEND:
                    {
                        Character pRoleTarget = ServerKernel.UserManager.GetUser(msg.Identity);
                        if (pRoleTarget != null)
                        {
                            uint dwVipUsr = FriendVipAmount[user.Owner.VipLevel],
                                dwVipTgt = FriendVipAmount[pRoleTarget.Owner.VipLevel];

                            if (user.Friends.Count >= dwVipUsr)
                            {
                                user.SendSysMessage(Language.StrFriendListFull);
                                return;
                            }

                            if (pRoleTarget.Friends.Count >= dwVipTgt)
                            {
                                user.SendSysMessage(Language.StrTargetFrieldListFull);
                                return;
                            }

                            if (!pRoleTarget.FetchFriendRequest(user.Identity))
                            {
                                user.SetFriendRequest(pRoleTarget.Identity);
                                msg.Identity = user.Identity;
                                msg.Name = user.Name;
                                pRoleTarget.Send(msg);
                                pRoleTarget.SendRelation(user);
                                user.SendSysMessage(Language.StrMakeFriendSent);
                            }
                        }
                        break;
                    }

                #endregion
                #region AcceptFriend

                case RelationAction.NEW_FRIEND:
                {
                    Character pRoleTarget = ServerKernel.UserManager.GetUser(msg.Identity);
                    if (pRoleTarget.FetchFriendRequest(user.Identity))
                    {
                        user.CreateFriend(pRoleTarget);
                        pRoleTarget.ClearFriendRequest();
                    }
                    break;
                }
                #endregion
                #region Break

                case RelationAction.REMOVE_FRIEND:
                    {
                        user.DeleteFriend(msg.Identity);
                        break;
                    }

                #endregion
                #region Remove Enemy
                case RelationAction.REMOVE_ENEMY:
                    {
                        user.DeleteEnemy(msg.Identity);
                        break;
                    }
                #endregion
                default:
                    Program.WriteLog("Not handled 1019:" + msg.Mode, LogType.WARNING);
                    GamePacketHandler.Report(msg);
                    break;
            }
        }
    }
}
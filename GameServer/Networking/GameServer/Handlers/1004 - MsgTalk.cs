﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - 1004 - MsgTalk.cs
// Last Edit: 2019/11/24 19:03
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using FtwCore;
using FtwCore.Common;
using FtwCore.Common.Enums;
using FtwCore.Database.Entities;
using FtwCore.Database.Repositories;
using FtwCore.Networking.Packets;
using GameServer.Structures;
using GameServer.Structures.Entities;
using GameServer.Structures.Items;
using GameServer.World;

#endregion

namespace GameServer.Networking.GameServer
{
    public static partial class Handlers
    {
        public static void HandleMsgTalk(Character user, MsgTalk msg)
        {
            if (msg.Sender != user.Name)
            {
                Program.WriteLog($"{user.Name} != {msg.Sender} MsgTalk invalid msg to {msg.Recipient}: {msg.Message}");
                return;
            }

            if (ExecuteCommand(user, msg.Message))
            {
                ServerKernel.Log.GmLog("gm_command", $"{user.Name} => {msg.Message}");
                return;
            }

            switch (msg.Tone)
            {
                case ChatTone.Talk:
                    user.Screen.Send(msg, false);
                    break;
                case ChatTone.Whisper:
                    if (!user.IsAlive)
                    {
                        user.SendSysMessage(Language.StrDie);
                        return;
                    }

                    Character target = ServerKernel.UserManager.GetUser(msg.Recipient);
                    if (target != null)
                    {
                        msg.SenderMesh = user.Lookface;
                        msg.RecipientMesh = target.Lookface;
                        target.Send(msg);
                        user.Send(msg);
                    }
                    else
                        user.SendSysMessage(Language.StrUserNotOnline, Color.Red);

                    break;
                case ChatTone.Team:
                    user.Team?.Send(msg, user.Identity);
                    break;
                case ChatTone.Friend:
                    foreach (var friend in user.Friends.Values.Where(x => x.IsOnline))
                    {
                        friend.User.Send(msg);
                    }

                    break;
                case ChatTone.Guild:
                    if (!user.IsAlive)
                    {
                        user.SendSysMessage(Language.StrDie);
                        return;
                    }

                    user.Syndicate?.Send(msg, user.Identity);
                    break;
                case ChatTone.World:
                    if (!user.IsAlive)
                    {
                        user.SendSysMessage(Language.StrDie);
                        return;
                    }

                    ServerKernel.UserManager.SendToAllUser(msg);
                    break;
                case ChatTone.Ally:
                    if (user.Syndicate != null)
                    {
                        user.Syndicate.Send(msg);
                        foreach (var ally in user.Syndicate.Allies.Values)
                            ally.Send(msg);
                    }

                    break;
                case ChatTone.Ghost:
                    if (user.IsAlive)
                        return;
                    user.Screen.SendDeadMessage(msg);
                    break;
                case ChatTone.VendorHawk:
                    if (user.Booth?.Vending == true)
                    {
                        user.Booth.HawkMessage = msg.Message;
                        user.Screen.Send(msg, true);
                    }

                    break;
            }
        }

        private static bool ExecuteCommand(Character user, string command)
        {
            if (user.Gender == 2 && command.StartsWith("#"))
            {
                command = command.Substring(3, command.Length - 6);
            }

            string[] separator = {" "};
            string[] parsed = command.Split(separator, 3, StringSplitOptions.RemoveEmptyEntries);
            if (parsed.Length < 1)
                return false;

            if (!parsed[0].StartsWith("/"))
                return false;

            parsed[0] = parsed[0].ToLower();

            Character target = null;
            if (user.IsPm()) // all [PM] commands
            {
                switch (parsed[0])
                {
                    case "/startctf":
                    {
                        ServerKernel.CaptureTheFlag.ForceStartup();
                        break;
                    }
                    case "/spawnflag":
                    {
                        MsgWarFlag flag = new MsgWarFlag
                        {
                            Type = WarFlagType.Initialize
                        };
                        user.Send(flag);
                        flag = new MsgWarFlag
                        {
                            Type = WarFlagType.Location2XBonus,
                            IsWar = true,
                            Identity = 2,
                            MapX = 214,
                            MapY = 269
                        };
                        //flag.WriteInt(2, 8);
                        //flag.WriteInt(1, 28);
                        //flag.WriteUShort(214, 36);
                        //flag.WriteUShort(269, 38);
                        user.Send(flag);

                        return true;
                    }
                    case "/fakectfrank":
                    {
                        MsgWarFlag flag = new MsgWarFlag
                        {
                            Type = WarFlagType.Initialize
                        };
                        user.Send(flag);
                        flag = new MsgWarFlag
                        {
                            Type = WarFlagType.WarFlagTop4,
                            Identity = 2
                        };
                        flag.Append(1, 500, "Teste01");
                        flag.Append(2, 300, "Teste02");
                        flag.Append(3, 200, "Teste03");
                        user.Send(flag);
                        flag.Type = WarFlagType.WarFlagBaseRank;
                        flag.Append(11, 100, "TESTE");
                        user.Send(flag);
                        return true;
                    }
                    case "/light":
                    {
                        if (parsed.Length < 2)
                            return true;

                        user.Send(new MsgAction(1, uint.Parse(parsed[1]), 0, 0, GeneralActionType.MapArgb));
                        return true;
                    }
                    case "/weather":
                    {
                        parsed = command.Split(' ');
                        if (parsed.Length < 6)
                            return true;

                        int nType = int.Parse(parsed[1]), nIntensity = int.Parse(parsed[2]), nDir = int.Parse(parsed[3]);
                        uint dwColor = uint.Parse(parsed[4]), dwKeepSecs = uint.Parse(parsed[5]);

                        user.Map.Weather.SetNewWeather((Weather.WeatherType)nType, nIntensity, nDir, (int)dwColor,
                            (int)dwKeepSecs, 0);
                        return true;
                    }
                    case "/expmulti":
                    {
                        MsgUserAttrib attrib = new MsgUserAttrib
                        {
                            Identity = user.Identity
                        };
                        attrib.Append(ClientUpdateType.DoubleExpTimer, 400u, 3600u, 500u, 300u);
                        user.Send(attrib);
                        return true;
                    }
                    case "/dailyreset":
                    {
                        DailyUpdate.Do();
                        return true;
                    }
                    case "/xp":
                        user.SetXp(100);
                        user.BurstXp();
                        return true;
                    case "/attachstatus":
                    {
                        if (command.Length < 2) return true;
                        byte status = byte.Parse(parsed[1]);
                        user.AttachStatus(user, status, 0, 30, 0, 0);
                        break;
                    }

                    case "/msgmailnotify":
                    {
                        //for (ushort i = 0; i < 10; i++)
                        {
                            MsgMailNotify pMsg = new MsgMailNotify();
                            pMsg.WriteUShort(4, 4);
                            //pMsg.WriteUShort(i, 8);
                            user.Send(pMsg);
                        }
                        return true;
                    }

                    case "/msgmapitem":
                    {
                        MsgMapItem pMsg = new MsgMapItem((uint) new Random().Next(800000, 900000), 410057, user.MapX,
                            user.MapY, DropType.Drop);
                        //pMsg.WriteByte(1, 23);
                        //pMsg.WriteUInt(user.Identity, 28);
                        user.Send(pMsg);
                        return true;
                    }

                    case "/msgplayer":
                        Random temp = new Random();
                        int x = 0;
                        int y = 0;
                        for (int i = 0; i < 20; i++)
                        {
                            int offset = 120 + i;
                            //int startX = 80;
                            //int startY = startX + 2;
                            MsgPlayer pBuffer = new MsgPlayer((uint) temp.Next(10000000, 16000000));
                            pBuffer.Mesh = 11002;
                            //pBuffer.WriteUInt(25000000, 86);
                            //pBuffer.IsBoss = true;
                            //pBuffer.WriteBoolean(true, 177 + i);
                            //pBuffer.Hairstyle = 435;
                            //pBuffer.GuildIdentity = (ushort) user.SyndicateIdentity;
                            //pBuffer.GuildRank = SyndicateRank.Member;
                            //pBuffer.FlowerRanking = 30010001;
                            pBuffer.MapX = (ushort) (user.MapX - 9 + x++);
                            //pBuffer.WriteUShort((ushort)(user.MapX - 9 + x++ + x++), startX + i);
                            if (x > 16)
                            {
                                y++;
                                x = 0;
                            }

                            pBuffer.WriteByte(1, offset);
                            pBuffer.MapY = (ushort) (user.MapY - 9 + y);
                            //pBuffer.WriteUShort((ushort)(user.MapY - 9 + y++), startY + i);
                            //pBuffer.CountryFlag = 5;
                            //pBuffer.FamilyIdentity = 1111;
                            //pBuffer.FamilyRank = FamilyRank.ClanLeader;
                            //pBuffer.WriteUInt(1111, 165 + i);
                            //pBuffer.WriteUInt((uint) FamilyRank.ClanLeader, 169 + i);
                            pBuffer.StringCount = 1;
                            //pBuffer.Name = $"User[{pBuffer.MapX},{pBuffer.MapX}]";
                            pBuffer.Name = $"TeratoDragon{offset}";
                            //pBuffer.Spouse = "None";
                            //pBuffer.FamilyName = "Oppa~Oppa";
                            //pBuffer.FourthString = "Teste~Teste";
                            Program.WriteLog(
                                $"{pBuffer.Identity} {pBuffer.Name} {pBuffer.MapX}:{pBuffer.MapY} Offset: {i}",
                                LogType.DEBUG);
                            user.Send(pBuffer);
                        }

                        user.Send(new MsgTalk($"Initial pos: {user.MapX}:{user.MapY}", ChatTone.Talk));

                        return true;
                    case "/pos":
                        user.SendSysMessage($"Current Position: {user.MapIdentity} {user.MapX}, {user.MapY}", Color.Red);
                        return true;
                    case "/showid":
                        user.SendSysMessage($"AccountId={user.Owner.AccountIdentity}, UserId={user.Identity}",
                            Color.Red);
                        return true;
                    case "/openwindow":
                        if (parsed.Length < 2 || !uint.TryParse(parsed[1], out uint window))
                            return true;

                        user.Send(new MsgAction(user.Identity, window, user.MapX, user.MapY,
                            GeneralActionType.OpenWindow));
                        return true;
                    case "/openinterface":
                        if (parsed.Length < 2 || !uint.TryParse(parsed[1], out uint inter))
                            return true;

                        user.Send(new MsgAction(user.Identity, inter, user.MapX, user.MapY,
                            GeneralActionType.OpenCustom));
                        return true;
                    case "/pro":
                    {
                        if (parsed.Length < 2)
                            return true;

                        if (ushort.TryParse(parsed[1], out var prof))
                            user.Profession = (ProfessionType) prof;
                        return true;
                    }

                    case "/superman":
                        user.Force += 500;
                        user.Agility += 500;
                        user.Vitality += 500;
                        user.Spirit += 500;
                        return true;
                    case "/awarditem":
                        if (parsed.Length < 2)
                            return true;

                        if (uint.TryParse(parsed[1], out var awardItemType))
                        {
                            Item item = user.UserPackage.AwardItem(Item.CreateEntity(awardItemType));
                            ServerKernel.Log.GmLog("gmitem",
                                $"[{user.Identity}:{user.Name}] has created item [{item.Identity}:{item.Type}:{item.Name}]");
                            user.SendSysMessage($"[AwardItem] {awardItemType} created.");
                        }

                        return true;
                    case "/awardmoney":
                        if (!int.TryParse(parsed[1], out int awardmoney))
                            return true;

                        user.AwardMoney(awardmoney);
                        user.SendSysMessage($"[AwardMoney] {awardmoney} added.");
                        return true;
                    case "/awardemoney":
                        if (!int.TryParse(parsed[1], out int awardemoney))
                            return true;

                        user.AwardEmoney(awardemoney, EmoneySourceType.GmCommand, user);
                        user.SendSysMessage($"[AwardEMoney] {awardemoney} added.");
                        return true;
                    case "/awardboundemoney":
                        if (!int.TryParse(parsed[1], out int awardboundemoney))
                            return true;

                        user.AwardBoundEmoney(awardboundemoney);
                        return true;
                    case "/awardchipoints":
                        if (!int.TryParse(parsed[1], out int awardchipoints))
                            return true;

                        user.AwardChiPoints(awardchipoints);
                        return true;
                    case "/awardmagic":
                    {
                        if (command.Length < 2)
                            return true;
                        ushort magic = ushort.Parse(parsed[1]);
                        byte level = 0;
                        if (parsed.Length >= 3)
                            level = byte.Parse(parsed[2]);
                        user.Magics.Create(magic, level);
                        return true;
                    }

                    case "/life":
                        user.Life = user.MaxLife;
                        return true;
                    case "/mana":
                        user.Mana = user.MaxMana;
                        return true;
                    case "/restore":
                        user.Life = user.MaxLife;
                        user.Mana = user.MaxMana;
                        return true;
                    case "/action":
                        user.GameAction.ProcessAction(uint.Parse(parsed[1]), user, user, null, null);
                        return true;
                    case "/reloadaction":
                        if (uint.TryParse(parsed[1], out uint idAction))
                        {
                            ServerKernel.Actions.TryRemove(idAction, out GameActionEntity action);

                            GameActionEntity newAction = new GameActionRepository().GetById(idAction);
                            ServerKernel.Actions.TryAdd(idAction, newAction ?? action);
                            user.SendSysMessage($"[Action] {idAction} has been reloaded.");
                        }
                        else
                        {
                            if (parsed[1].ToLower() == "all")
                            {
                                Program.WriteLog($"Command /reloadaction all has been called by {user.Name}",
                                    LogType.WARNING);
                                user.SendMessage(
                                    "This command may break the server. Don't use it on release or live server.",
                                    ChatTone.Service);
                                ServerKernel.Actions.Clear();
                                IList<GameActionEntity> allActions = new GameActionRepository().FetchAll();
                                if (allActions != null)
                                {
                                    foreach (GameActionEntity action in allActions)
                                        ServerKernel.Actions.TryAdd(action.Identity, action);

                                    user.SendMessage(
                                        $"A total of {ServerKernel.Actions.Count} actions has been loaded to the server successfully.",
                                        ChatTone.Action);
                                }
                                else
                                {
                                    user.SendMessage("No actions has been loaded.", ChatTone.Action);
                                }
                            }
                        }

                        return true;

                    case "/uplev":
                    {
                        byte level;
                        if (!byte.TryParse(parsed[1], out level))
                            level = 1;
                        user.AwardLevel(level);
                        return true;
                    }

                    case "/reloadtask":
                        if (uint.TryParse(parsed[1], out uint idTask))
                        {
                            ServerKernel.Tasks.TryRemove(idTask, out GameTaskEntity action);

                            GameTaskEntity newAction = new GameTaskRepository().GetById(idTask);
                            ServerKernel.Tasks.TryAdd(idTask, newAction ?? action);
                            user.SendSysMessage($"[Action] Task {idTask} has been reloaded.");
                        }
                        else
                        {
                            if (parsed[1].ToLower() == "all")
                            {
                                Program.WriteLog($"Command /reloadtask all has been called by {user.Name}",
                                    LogType.WARNING);
                                user.SendMessage("This command may break the server. Don't use it on release or live server.",
                                    ChatTone.Service);
                                ServerKernel.Tasks.Clear();
                                IList<GameTaskEntity> allTasks = new GameTaskRepository().FetchAll();
                                if (allTasks != null)
                                {
                                    foreach (GameTaskEntity task in allTasks)
                                        ServerKernel.Tasks.TryAdd(task.Id, task);

                                    user.SendMessage(
                                        $"A total of {ServerKernel.Tasks.Count} tasks has been loaded to the server successfully.",
                                        ChatTone.Action);
                                }
                                else
                                {
                                    user.SendMessage("No tasks has been loaded.", ChatTone.Action);
                                }
                            }
                        }

                        return true;

                    case "/awardwskill":
                        if (command.Length < 2)
                            return true;
                        ushort wskill = ushort.Parse(parsed[1]);
                        byte wlevel = 0;
                        if (parsed.Length >= 3)
                            wlevel = Math.Min((byte) 20, byte.Parse(parsed[2]));
                        user.WeaponSkill.Create(wskill, wlevel);
                        return true;

                    case "/creategen":
                        user.SendMessage(
                            "Attention, use this command only on localhost tests or the generator thread may crash.");
                        // mobid mapid mapx mapy boundcx boundcy maxnpc rest maxpergen
                        string[] szComs = command.Split(' ');
                        if (szComs.Length < 10)
                        {
                            user.SendMessage("/creategen mobid mapid mapx mapy boundcx boundcy maxnpc rest maxpergen");
                            return true;
                        }

                        ushort idMob = ushort.Parse(szComs[1]);
                        uint idMap = uint.Parse(szComs[2]);
                        ushort mapX = ushort.Parse(szComs[3]);
                        ushort mapY = ushort.Parse(szComs[4]);
                        ushort boundcx = ushort.Parse(szComs[5]);
                        ushort boundcy = ushort.Parse(szComs[6]);
                        ushort maxNpc = ushort.Parse(szComs[7]);
                        ushort restSecs = ushort.Parse(szComs[8]);
                        ushort maxPerGen = ushort.Parse(szComs[9]);

                        GeneratorEntity newGen = new GeneratorEntity
                        {
                            Mapid = idMap,
                            Npctype = idMob,
                            BoundX = mapX,
                            BoundY = mapY,
                            BoundCx = boundcx,
                            BoundCy = boundcy,
                            MaxNpc = maxNpc,
                            RestSecs = restSecs,
                            MaxPerGen = maxPerGen,
                            BornX = 0,
                            BornY = 0,
                            TimerBegin = 0,
                            TimerEnd = 0
                        };

                        if (!new GeneratorRepository().Save(newGen))
                        {
                            user.SendMessage("Could not save generator.", ChatTone.Talk);
                            return true;
                        }

                        Generator pGen = new Generator(newGen);
                        pGen.Create();
                        ServerKernel.Generators.TryAdd(pGen.Identity, pGen);
                        pGen.FirstGeneration();

                        return true;

                    case "/findmob":
                        if (parsed.Length < 2)
                            return true;

                        foreach (var mob in user.Map.RoleSet.Values.Where(me => me is Monster).Cast<Monster>())
                        {
                            if (mob.Name == parsed[1])
                                user.SendMessage($"Name: {mob.Name}, MapX: {mob.MapX}, MapY: {mob.MapY}",
                                    ChatTone.Talk);
                        }

                        return true;

                    case "/testjiang":
                        user.KongFu.SendAllStars();
                        return true;
                    case "/jianghu":
                        parsed = command.Split(' ');
                        if (parsed.Length < 5)
                            return true;

                        int stage = int.Parse(parsed[1]);
                        int star = int.Parse(parsed[2]);
                        JiangHuAttrType type = (JiangHuAttrType) int.Parse(parsed[3]);
                        JiangHuQuality quality = (JiangHuQuality) int.Parse(parsed[4]);

                        if (!user.KongFu.HasStarted)
                            return true;

                        user.KongFu.SetAttribute(stage, star, type, quality);
                        return true;

                    case "/kongfustatus":
                        user.SendMessage($"MaxStage: {user.KongFu.CurrentStage} | InnerStrength: {user.KongFu.Score}");
                        return true;

                    case "/addtalent":
                        byte amount = 0;
                        if (parsed.Length < 2)
                        {
                            amount = 1;
                        }
                        else
                        {
                            amount = Math.Min((byte) 4, byte.Parse(parsed[1]));
                        }

                        user.KongFu.CurrentTalent += amount;
                        return true;
                }
            }

            if (user.IsGm()) // all [GM] commands
            {
                switch (parsed[0])
                {
                    case "/player":
                    {
                        if (parsed.Length < 2)
                            return true;

                        switch (parsed[1])
                        {
                            case "all":
                                user.SendMessage(
                                    $"There are currently {ServerKernel.UserManager.Count} users online (max: {Analytics.Statistic.MaxOnlinePlayers})",
                                    ChatTone.System);
                                break;
                            case "map":
                                user.SendMessage(
                                    $"There are currently {user.Map.PlayerSet.Count} users online on this map.",
                                    ChatTone.System);
                                break;
                        }
                        return true;
                    }

                    case "/find":
                    {
                        if (parsed.Length < 2)
                            return true;

                        target = ServerKernel.UserManager.GetUser(parsed[1]);
                        if (target == null)
                        {
                            user.SendSysMessage($"{parsed[1]} is not online or doesnt exist.", Color.Red);
                        }
                        else
                        {
                            user.FlyMap(target.MapIdentity, target.MapX, target.MapY);
                        }

                        return true;
                    }

                    case "/bring":
                    {
                        if (parsed.Length < 2)
                            return true;

                        target = ServerKernel.UserManager.GetUser(parsed[1]);
                        if (target == null)
                        {
                            user.SendSysMessage($"{parsed[1]} is not online or doesnt exist.", Color.Red);
                        }
                        else
                        {
                            target.FlyMap(user.MapIdentity, user.MapX, user.MapY);
                        }

                        return true;
                    }

                    case "/chgmap":
                    {
                        if (parsed.Length < 3)
                        {
                            user.SendMessage("/chgmap id_map x y", ChatTone.Talk);
                            return true;
                        }

                        string[] pos = parsed[2].Split(separator, 2, StringSplitOptions.None);
                        if (!uint.TryParse(parsed[1], out uint idMap)
                            || !ushort.TryParse(pos[0], out ushort x)
                            || !ushort.TryParse(pos[1], out ushort y))
                            return true;

                        if (!user.FlyMap(idMap, x, y))
                            user.SendSysMessage("Invalid position.", Color.Red);
                        return true;
                    }

                    case "/kickout":
                        if (parsed.Length < 2)
                        {
                            user.SendMessage("/kickout target_name [reason = optional]", ChatTone.System);
                            return true;
                        }

                        target = ServerKernel.UserManager.GetUser(parsed[1]);
                        if (target == null)
                            return true;

                        if (target.IsPm() && !user.IsAdministrator())
                        {
                            user.SendSysMessage("You cannot kickout another PM, unless you're the administrator.",
                                Color.Yellow);
                            return true;
                        }

                        string reason = "GM kickout";
                        if (parsed.Length == 3)
                            reason += $" reason[{parsed[2]}]";

                        ServerKernel.UserManager.KickoutSocket(target, reason);
                        return true;
                    case "/cmd":
                        if (parsed.Length < 3)
                            return true;

                        switch (parsed[1])
                        {
                            case "broadcast":
                                ServerKernel.UserManager.SendToAllUser(parsed[2], ChatTone.Center);
                                break;
                        }

                        return true;
                    case "/broadcast":
                        if (parsed.Length < 2)
                            return true;

                        string message = parsed[1];
                        if (parsed.Length == 3)
                            message += $" {parsed[2]}";

                        ServerKernel.UserManager.SendToAllUser($"{user.Name} says: {message}", ChatTone.Talk);
                        return true;
                }
            }

            switch (parsed[0])
            {
                case "/attachstatus":
                {
                    if (command.Length < 2 || user.Owner.Authority < 7) return true;
                    byte status = byte.Parse(parsed[1]);
                    user.AttachStatus(user, status, 0, 30, 0, 0);
                    return true;
                }

#if DEBUG
                case "/id":
                {
                    user.SendMessage($"AccountID: {user.Owner.AccountIdentity},UserID: {user.Identity}", Color.White,
                        ChatTone.Talk);
                    break;
                }

                case "/server_info":
                {
                    DateTime now = DateTime.Now;
                    var interval = now - ServerKernel.Analytics.StartTime;
                    user.SendMessage($"Server is running on version: {ServerKernel.Version}", Color.White,
                        ChatTone.Talk);
                    user.SendMessage(
                        $"There are currently {ServerKernel.UserManager.Count} (max: {Analytics.Statistic.MaxOnlinePlayers}) online players. Maximum server capacity is set to {ServerKernel.MaxOnlinePlayers}.",
                        Color.White, ChatTone.Talk);
                    user.SendMessage(
                        $"The server has been running for {interval.Days} days, {interval.Hours} hours, {interval.Minutes}, minutes and {interval.Seconds} seconds.",
                        Color.White, ChatTone.Talk);
                    break;
                }
#endif

                case "/report":
                {
                    if (parsed.Length < 2)
                        return true;

                    ReportEntity report = new ReportEntity
                    {
                        UserIdentity = user.Identity,
                        UserName = user.Name,
                        Message = parsed[1],
                        Timestamp = DateTime.Now
                    };
                    if (new ReportRepository().Save(report))
                    {
                        user.SendSysMessage("Your report has been sent successfully.");
                    }
                    else
                    {
                        user.SendSysMessage("Could not save your report.");
                    }

                    return true;
                }

                case "/revive":
                {
                    if (user.CanRevive())
                        user.Reborn(true);
                    return true;
                }

                case "/dc":
                case "/disconnect":
                    ServerKernel.UserManager.KickoutSocket(user, "Requested disconnection by command.");
                    return true;
            }

            return false;
        }
    }
}
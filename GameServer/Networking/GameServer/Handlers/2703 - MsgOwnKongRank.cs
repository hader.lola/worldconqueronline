﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - 2703 - MsgOwnKongRank.cs
// Last Edit: 2020/01/12 19:15
// Created: 2020/01/12 19:14
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System;
using FtwCore.Database.Entities;
using FtwCore.Networking.Packets;
using GameServer.Structures.Entities;

#endregion

namespace GameServer.Networking.GameServer
{
    public static partial class Handlers
    {
        public static void HandleOwnKongRank(Character user, MsgOwnKongRank msg)
        {
            const int MAX_PER_PAGE = 10;
            var list = user.KongFu.GetRanking();
            byte pos = 1;
            int from = Math.Max(0, msg.Page - 1) * MAX_PER_PAGE;
            int count = 0;
            for (int i = from; count < MAX_PER_PAGE && i < list.Count; i++)
            {
                CharacterEntity player = Database.CharacterRepository.SearchByIdentity(list[i].UserIdentity);
                if (player == null)
                    continue;
                msg.Append(pos++, list[i].Score, player.Level, player.Name, list[i].Name);
                count++;
            }

            byte ranking = 1;
            for (int i = 0; i < Math.Min(100, list.Count); i++)
            {
                if (list[i++].UserIdentity == user.Identity)
                {
                    break;
                }

                ranking++;
            }

            msg.UserRanking = ranking;
            msg.Amount = (byte) Math.Min(100, list.Count);
            user.Send(msg);
        }
    }
}
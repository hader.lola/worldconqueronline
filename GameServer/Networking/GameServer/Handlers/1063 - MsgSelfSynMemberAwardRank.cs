﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - 1063 - MsgSelfSynMemberAwardRank.cs
// Last Edit: 2020/01/16 00:17
// Created: 2020/01/15 19:40
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System.Drawing;
using System.Linq;
using FtwCore;
using FtwCore.Common;
using FtwCore.Common.Enums;
using FtwCore.Networking.Packets;
using GameServer.Structures.Entities;
using GameServer.Structures.Groups.Syndicates;

#endregion

namespace GameServer.Networking.GameServer
{
    public static partial class Handlers
    {
        public static void HandleMsgSelfSynMemAwardRank(Character user, MsgSelfSynMemAwardRank msg)
        {
            switch (msg.Type)
            {
                case SynMemAwardRankType.RewardsSetForEvent:
                {
                    var rank = ServerKernel.SyndicateManager.Syndicates.Values
                        .Where(x => x.MoneyPrize > 0 || x.EmoneyPrize > 0)
                        .OrderByDescending(x => x.MoneyPrize)
                        .ThenByDescending(x => x.EmoneyPrize)
                        .ToList();
                    int idx = 0;
                    for (; idx < 5; idx++)
                    {
                        if (idx >= rank.Count)
                            break;
                        msg.AddToRanking(rank[idx].Name, rank[idx].EmoneyPrize, rank[idx].MoneyPrize);
                    }

                    msg.EmoneyPrize = user.Syndicate.EmoneyPrize;
                    msg.MoneyPrize = user.Syndicate.MoneyPrize;
                    user.Send(msg);
                    break;
                }
                case SynMemAwardRankType.LastEventRewardsRanking:
                {
                    msg.EmoneyPrize = user.Syndicate.EmoneyPrize;
                    msg.MoneyPrize = user.Syndicate.MoneyPrize;

                    var rank = ServerKernel.CaptureTheFlag.GetSyndicateMembers(user.SyndicateIdentity)
                        .Where(x => x.Value3 > 0)
                        .OrderByDescending(x => x.Value3).ToList();
                    int nRank = 0;
                    for (; nRank < rank.Count; nRank++)
                    {
                        if (rank[nRank].PlayerIdentity == user.Identity)
                            break;
                    }

                    if (rank.Count - 1 >= nRank)
                    {
                        msg.AddToRanking((uint) (nRank + 1), user.Identity, user.Name, rank[nRank].Value5,
                            (uint) rank[nRank].Value6, (uint) rank[nRank].Value3);
                    }

                    int startIdx = (int) ((msg.Page - 1) * 4);
                    int idx = startIdx;
                    for (; idx < startIdx + 4; idx++)
                    {
                        if (idx >= rank.Count)
                            break;
                        var res = rank[idx];
                        msg.AddToRanking((uint) (idx + 1), res.PlayerIdentity, res.PlayerName, res.Value5,
                            (uint) res.Value6, (uint) res.Value3);
                    }

                    msg.ResultNum = (uint) rank.Count;
                    user.Send(msg);
                    break;
                }
                case SynMemAwardRankType.SetEmoney:
                {
                    Syndicate pSyn = user.Syndicate;
                    if (pSyn == null)
                        return;

                    if (user.SyndicatePosition != SyndicateRank.GuildLeader)
                        return;

                    if (pSyn.EmoneyPrize > 0 && msg.EmoneyPrize <= pSyn.EmoneyPrize)
                        return;

                    uint deductEmoney = msg.EmoneyPrize;
                    if (deductEmoney - pSyn.EmoneyPrize > pSyn.EmoneyDonation)
                    {
                        user.SendSysMessage(Language.StrSynRewardNotEnoughEmoney);
                        return;
                    }

                    deductEmoney -= pSyn.EmoneyPrize;

                    pSyn.EmoneyDonation -= deductEmoney;
                    pSyn.EmoneyPrize = msg.EmoneyPrize;

                    ServerKernel.UserManager.SendToAllUser(string.Format(Language.StrSynRewardRewardSet, user.Name,
                        pSyn.Name, msg.MoneyPrize,
                        msg.EmoneyPrize, ServerKernel.NextSynRewardEventName()), ChatTone.Talk);
                    break;
                }
                case SynMemAwardRankType.SetMoney:
                {
                    Syndicate pSyn = user.Syndicate;
                    if (pSyn == null)
                        return;

                    if (user.SyndicatePosition != SyndicateRank.GuildLeader)
                        return;

                    if (pSyn.MoneyPrize > 0 && msg.MoneyPrize <= pSyn.MoneyPrize)
                        return;

                    long deductMoney = msg.MoneyPrize;
                    if (deductMoney - pSyn.MoneyPrize > (long) pSyn.SilverDonation)
                    {
                        user.SendSysMessage(Language.StrSynRewardNotEnoughMoney);
                        return;
                    }

                    deductMoney -= pSyn.MoneyPrize;

                    pSyn.SilverDonation -= (ulong) deductMoney;
                    pSyn.MoneyPrize = msg.MoneyPrize;

                    ServerKernel.UserManager.SendToAllUser(string.Format(Language.StrSynRewardRewardSet, user.Name,
                        pSyn.Name, msg.MoneyPrize,
                        msg.EmoneyPrize, ServerKernel.NextSynRewardEventName()), ChatTone.Talk);
                    break;
                }
                case SynMemAwardRankType.SetMoneyEmoney:
                {
                    Syndicate pSyn = user.Syndicate;
                    if (pSyn == null)
                        return;

                    if (user.SyndicatePosition != SyndicateRank.GuildLeader)
                        return;

                    if (pSyn.MoneyPrize > 0 && msg.MoneyPrize <= pSyn.MoneyPrize)
                        return;

                    long deductMoney = msg.MoneyPrize;
                    if (deductMoney - pSyn.MoneyPrize > (long) pSyn.SilverDonation)
                    {
                        user.SendSysMessage(Language.StrSynRewardNotEnoughMoney);
                        return;
                    }

                    deductMoney -= pSyn.MoneyPrize;

                    uint deductEmoney = msg.EmoneyPrize;
                    if (deductEmoney - pSyn.EmoneyPrize > pSyn.EmoneyDonation)
                    {
                        user.SendSysMessage(Language.StrSynRewardNotEnoughEmoney);
                        return;
                    }

                    deductEmoney -= pSyn.EmoneyPrize;

                    pSyn.SilverDonation -= (ulong) deductMoney;
                    pSyn.MoneyPrize = msg.MoneyPrize;

                    pSyn.EmoneyDonation -= deductEmoney;
                    pSyn.EmoneyPrize = msg.EmoneyPrize;

                    ServerKernel.UserManager.SendToAllUser(string.Format(Language.StrSynRewardRewardSet, user.Name,
                        pSyn.Name, msg.MoneyPrize,
                        msg.EmoneyPrize, ServerKernel.NextSynRewardEventName()), ChatTone.Talk);
                    break;
                }
                case SynMemAwardRankType.CaptureTheFlagCurrentSynRank:
                {
                    var rank = ServerKernel.CaptureTheFlag.GetSyndicateMembers(user.SyndicateIdentity)
                        .Where(x => x.Value3 > 0)
                        .OrderByDescending(x => x.Value3)
                        .ToList();
                    int startIdx = (int) (msg.Page * 5);
                    int idx = startIdx;
                    for (; idx < startIdx + 5; idx++)
                    {
                        if (idx >= rank.Count)
                            break;
                        var res = rank[idx];
                        msg.AddToRanking(res.PlayerName, (uint) res.Value3);
                        if (res.PlayerIdentity == user.Identity)
                        {
                            msg.Exploits = (uint) res.Value3;
                        }
                    }

                    msg.ResultNum = (uint) rank.Count;
                    user.Send(msg);
                    break;
                }
                case SynMemAwardRankType.CaptureTheFlagTop8:
                    ServerKernel.CaptureTheFlag.SendInterfaceRanking(user, msg);
                    break;
                default:
                    Program.WriteLog($"MsgSelfSynMemAwardRank::{msg.Type} not handled {user.Identity}::{user.Name}",
                        LogType.DEBUG);
#if DEBUG
                    if (user.IsPm())
                        user.SendMessage($"MsgSelfSynMemAwardRank::{msg.Type} not handled", Color.White, ChatTone.Talk);
#endif
                    break;
            }
        }
    }
}
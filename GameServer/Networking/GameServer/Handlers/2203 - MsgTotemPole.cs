﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - 2203 - MsgTotemPole.cs
// Last Edit: 2020/01/16 23:53
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using FtwCore.Common.Enums;
using FtwCore.Networking.Packets;
using GameServer.Structures.Entities;
using GameServer.Structures.Groups.Syndicates;
using GameServer.Structures.Items;

#endregion

namespace GameServer.Networking.GameServer
{
    public static partial class Handlers
    {
        public static void HandleMsgTotemPole(Character pUser, MsgTotemPole pMsg)
        {
            if (pUser?.Syndicate == null || pUser.SyndicateMember == null)
                return;

            if (pUser.Syndicate.Arsenal == null)
                pUser.Syndicate.Arsenal = new Arsenal(pUser.Syndicate);

            switch (pMsg.Type)
            {
                case 0: // unlock arsenal
                {
                    pUser.Syndicate.Arsenal.UnlockArsenal((TotemPoleType) pMsg.BeginAt, pUser);
                    break;
                }

                case 1: // inscribe item
                {
                    Item pItem = null;
                    if ((pItem = pUser.UserPackage[pMsg.EndAt]) == null)
                        return;

                    pUser.Syndicate.Arsenal.InscribeItem(pItem, pUser);
                    break;
                }

                case 2: // remove item
                {
                    Item pItem;
                    if ((pItem = pUser.UserPackage.Find(pMsg.EndAt)) != null)
                    {
                        pUser.Syndicate.Arsenal.UninscribeItem(pItem, pUser);
                    }

                    break;
                }

                case 3: // enhance
                {
                    /**
                     * ArsenalType = The amount of enhancement
                     * BeginAt = The Arsenal ID
                     */
                    pUser.Syndicate.AddTotemPower(pUser, (TotemPoleType) pMsg.BeginAt, (byte) pMsg.ArsenalType);
                    pUser.Syndicate.Arsenal.SendArsenal(pUser);
                    break;
                }
                case 4: // refresh
                {
                    pUser.Syndicate.Arsenal.SendArsenal(pUser);
                    break;
                }

                default:
                {
                    ServerKernel.Log.SaveLog($"MsgTotemPole::{pMsg.Type}");
                    break;
                }
            }
        }
    }
}
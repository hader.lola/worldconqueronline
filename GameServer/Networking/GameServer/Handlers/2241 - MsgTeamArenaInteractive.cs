﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - 2241 - MsgTeamArenaInteractive.cs
// Last Edit: 2020/01/26 14:41
// Created: 2020/01/26 14:40
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using FtwCore.Networking.Packets;
using GameServer.Structures.Entities;
using GameServer.Structures.Qualifiers.Teams;

#endregion

namespace GameServer.Networking.GameServer
{
    public static partial class Handlers
    {
        public static void HandleMsgTeamArenaInteractive(Character user, MsgTeamArenaInteractive msg)
        {
            switch (msg.Type)
            {
                case ArenaType.ARENA_ICON_ON:
                {
                    if (user.Team.Leader.Identity != user.Identity)
                        return;
                    if (ServerKernel.TeamQualifier.Inscribe(user))
                    {
                        user.TeamArenaScreen();
                    }
                    else
                    {
                        ServerKernel.TeamQualifier.Uninscribe(user, TeamArenaQualifierManager.UninscribeReason.UserRequest);
                        user.TeamArenaScreen();
                    }
                    break;
                }
                case ArenaType.ARENA_ICON_OFF:
                {
                    if (user.Team.Leader.Identity != user.Identity)
                        return;

                    ServerKernel.TeamQualifier.Uninscribe(user, TeamArenaQualifierManager.UninscribeReason.UserRequest);
                    user.TeamArenaScreen();
                    break;
                }
            }
        }
    }
}
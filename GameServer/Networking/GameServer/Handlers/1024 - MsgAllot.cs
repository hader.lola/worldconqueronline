﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - 1024 - MsgAllot.cs
// Last Edit: 2019/11/24 19:03
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using FtwCore;
using FtwCore.Networking.Packets;
using GameServer.Structures.Entities;

#endregion

namespace GameServer.Networking.GameServer
{
    public static partial class Handlers
    {
        public static void HandleMsgAllot(Character pUser, MsgAllot pMsg)
        {
            if (!pUser.IsAlive)
            {
                pUser.SendSysMessage(Language.StrDie);
                return;
            }

            int nTotal = (int) (pMsg.Strength + pMsg.Agility + pMsg.Vitality + pMsg.Spirit);
            if (nTotal > pUser.AdditionalPoints)
            {
                pUser.SendSysMessage(Language.StrCheat);
                return;
            }

            pUser.AdditionalPoints -= (ushort) nTotal;
            pUser.Force += (ushort) pMsg.Strength;
            pUser.Agility += (ushort) pMsg.Agility;
            pUser.Vitality += (ushort) pMsg.Vitality;
            pUser.Spirit += (ushort) pMsg.Spirit;
        }
    }
}
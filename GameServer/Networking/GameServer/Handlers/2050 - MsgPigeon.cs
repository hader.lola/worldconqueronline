﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - 2050 - MsgPigeon.cs
// Last Edit: 2019/11/24 19:03
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using FtwCore.Networking.Packets;
using GameServer.Structures.Entities;

#endregion

namespace GameServer.Networking.GameServer
{
    public static partial class Handlers
    {
        public static void HandleMsgPigeon(Character user, MsgPigeon msg)
        {
            switch (msg.Type)
            {
                case 1: // next broadcasts list
                case 2: // next broadcast list (my)
                {
                    ServerKernel.Pigeon.RequestNextPage(user, msg);
                    break;
                }

                case 3: // send
                {
                    var list = msg.ToList();
                    if (list.Count <= 0)
                        return;

                    var szMessage = list[0];

                    ServerKernel.Pigeon.Push(user, szMessage, true);
                    ServerKernel.Pigeon.RequestNextPage(user, msg);
                    break;
                }

                case 4: // urgent 15 cps
                {
                    ServerKernel.Pigeon.Addition(user, msg.DwParam, 15);
                    ServerKernel.Pigeon.RequestNextPage(user, msg);
                    break;
                }

                case 5: // urgent 5 cps
                {
                    ServerKernel.Pigeon.Addition(user, msg.DwParam, 5);
                    ServerKernel.Pigeon.RequestNextPage(user, msg);
                    break;
                }
            }
        }
    }
}
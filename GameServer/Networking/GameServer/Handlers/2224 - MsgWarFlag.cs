﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - 2224 - MsgWarFlag.cs
// Last Edit: 2020/01/13 23:30
// Created: 2020/01/13 23:30
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using FtwCore.Networking.Packets;
using GameServer.Structures.Entities;

#endregion

namespace GameServer.Networking.GameServer
{
    public static partial class Handlers
    {
        public static void HandleMsgWarFlag(Character user, MsgWarFlag msg)
        {
            switch (msg.Type)
            {
                case WarFlagType.DeliverFlagEffect:
                {
                    ServerKernel.CaptureTheFlag.SendBaseRank(user);
                    ServerKernel.CaptureTheFlag.DeliverFlag(user);
                    break;
                }
                case WarFlagType.SyndicateRewardTab:
                {
                    msg.IsWar = ServerKernel.CaptureTheFlag.IsRunning;
                    user.Send(msg);
                    break;
                }
            }
        }
    }
}
﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - 1066 - MsgMeteSpecial.cs
// Last Edit: 2019/11/24 19:03
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using FtwCore;
using FtwCore.Common;
using FtwCore.Networking.Packets;
using GameServer.Structures.Entities;

#endregion

namespace GameServer.Networking.GameServer
{
    public static partial class Handlers
    {
        public static void HandleMsgMeteSpecial(Character user, MsgMeteSpecial msg)
        {
            if (user.Metempsychosis < 2)
            {
                user.SendSysMessage(Language.StrMeteSpecialMetempsychosis);
                return;
            }

            if (user.Level < 110)
            {
                user.SendSysMessage(Language.StrMeteSpecialLevel);
                return;
            }

            if (user.Gender == 1) // male
            {
                if (msg.Body < 3)
                {
                    user.SendSysMessage(Language.StrMeteSpecialWrongGender);
                    return;
                }
            }
            else if (user.Gender == 2) // female
            {
                if (msg.Body > 2)
                {
                    user.SendSysMessage(Language.StrMeteSpecialWrongGender);
                    return;
                }
            }

            if (!user.UserPackage.SpendItem(SpecialItem.OBLIVION_DEW))
            {
                user.SendSysMessage(Language.StrMeteSpecialNoOblivion);
                return;
            }

            user.Reincarnate((ushort) msg.Profession, (ushort) msg.Body);
        }
    }
}
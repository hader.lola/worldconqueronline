﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - 1107 - MsgSyndicate.cs
// Last Edit: 2019/11/24 19:03
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System;
using System.Linq;
using System.Text;
using FtwCore;
using FtwCore.Common.Enums;
using FtwCore.Database.Entities;
using FtwCore.Database.Repositories;
using FtwCore.Networking.Packets;
using GameServer.Structures;
using GameServer.Structures.Entities;
using GameServer.Structures.Groups.Syndicates;

#endregion

namespace GameServer.Networking.GameServer
{
    public static partial class Handlers
    {
        public static void HandleMsgSyndicate(Character pUser, MsgSyndicate pMsg)
        {
            if (pUser == null)
                return;

            switch (pMsg.Action)
            {
                #region Info / Refresh

                case SyndicateRequest.SYN_INFO:
                case SyndicateRequest.SYN_REFRESH:
                {
                    if (pUser.Syndicate == null
                        || pUser.SyndicateMember == null)
                        return;

                    pUser.SyndicateMember.SendSyndicate();

                    MsgDutyMinContri pNewMsg = new MsgDutyMinContri();
                    foreach (var pPos in m_pShowDuty)
                        pNewMsg.Append(pPos, pUser.Syndicate.MinimumDonation(pPos));
                    pUser.Send(pNewMsg);
                    pUser.Syndicate.SendRelation(pUser);
                    return;
                }

                #endregion

                #region Bulletin/Announcement

                case SyndicateRequest.SYN_BULLETIN:
                {
                    if (pUser.Syndicate == null
                        || pUser.SyndicateMember == null)
                        return;

                    if (pUser.SyndicateMember.Position != SyndicateRank.GuildLeader)
                        return;
                    string msg = Encoding.ASCII.GetString(pMsg, 26, pMsg[25]);
                    if (msg.Length > 127)
                        return;
                    pUser.Syndicate.SetAnnouncement(msg);
                    return;
                }

                #endregion

                #region Donate Silvers

                case SyndicateRequest.SYN_DONATE_SILVERS:
                {
                    if (pUser.Syndicate == null
                        || pUser.SyndicateMember == null)
                        return;

                    if (pMsg.Param < 10000) // should donate at least 10000 silvers
                        return;

                    if (!pUser.SpendMoney((int) pMsg.Param)) // not enough money
                        return;

                    pUser.Syndicate.ChangeFunds((int) pMsg.Param);
                    pUser.SyndicateMember.IncreaseMoney(pMsg.Param);
                    pUser.SyndicateMember.SendSyndicate();
                    pUser.SyndicateMember.SendCharacterInformation();
                    pUser.Syndicate.Send(string.Format(Language.StrSynDonate, pUser.SyndicateMember.GetRankName(),
                        pUser.Name, pMsg.Param));

                    return;
                }

                #endregion

                #region Donate CPs

                case SyndicateRequest.SYN_DONATE_CONQUER_POINTS:
                {
                    if (pUser.Syndicate == null
                        || pUser.SyndicateMember == null)
                        return;

                    if (!pUser.SpendEmoney((int) pMsg.Param, EmoneySourceType.Syndicate, pUser, true))
                        return;

                    pUser.Syndicate.ChangeEmoneyFunds((int) pMsg.Param);
                    pUser.SyndicateMember.IncreaseEmoney(pMsg.Param);
                    pUser.SyndicateMember.SendSyndicate();
                    pUser.SyndicateMember.SendCharacterInformation();
                    pUser.Syndicate.Send(string.Format(Language.StrSynDonateEmoney, pUser.SyndicateMember.GetRankName(),
                        pUser.Name, pMsg.Param));

                    return;
                }

                #endregion

                #region Promote

                case SyndicateRequest.SYN_PROMOTE:
                {
                    if (pUser.Syndicate == null
                        || pUser.SyndicateMember == null)
                        return;

                    pUser.SyndicateMember.SendPromotionList();
                    return;
                }

                #endregion

                #region Paid Promotion

                case SyndicateRequest.SYN_PAID_PROMOTE:
                {
                    if (pUser.Syndicate == null
                        || pUser.SyndicateMember == null)
                        return;

                    pUser.Syndicate.ProcessPaidPromotion(pMsg, pUser);
                    return;
                }

                #endregion

                #region Request Promotion

                case SyndicateRequest.SYN_SEND_REQUEST:

                    if (pUser.Syndicate == null
                        || pUser.SyndicateMember == null)
                        return;

                    if (!Enum.IsDefined(typeof(SyndicateRank), (ushort) pMsg.Param))
                        return;

                    var pos = (SyndicateRank) pMsg.Param;

                    SyndicateMember pMember = pUser.Syndicate.Members.Values.FirstOrDefault(x => x.Name == pMsg.Name);
                    if (pMember == null)
                        return;

                    pUser.Syndicate.PromoteMember(pUser, pMember, pos);
                    pUser.Syndicate.SendMembers(pUser, 0);
                    return;

                #endregion

                #region Join Request

                case SyndicateRequest.SYN_JOIN_REQUEST:
                {
                    // no syn join guild param: target
                    if (pUser.Syndicate != null || pUser.SyndicateMember != null)
                        return; // already joined

                    Character pTarget = ServerKernel.UserManager.GetUser(pMsg.Param);
                    if (pTarget == null)
                        return;

                    if (pUser.FetchSynInvite(pMsg.Param))
                    {
                        // do join
                        pTarget.Syndicate.AppendMember(pTarget, pUser.Identity, true);
                        pUser.ClearSynInvite();
                    }
                    else
                    {
                        // add join
                        pUser.SetSynJoin(pMsg.Param);
                        pMsg.Param = pUser.Identity;
                        pMsg.RequiredLevel = pUser.Level;
                        pMsg.RequiredMetempsychosis = pUser.Metempsychosis;
                        pMsg.RequiredProfession = (uint) pUser.Profession;
                        pTarget.Send(pMsg);
                        pTarget.SendRelation(pUser);
                    }

                    return;
                }

                #endregion

                #region Invite Request

                case SyndicateRequest.SYN_INVITE_REQUEST:
                {
                    // syn invite member param: target
                    if (pUser.Syndicate == null || pUser.SyndicateMember == null)
                        return; // no syn

                    Character pTarget = ServerKernel.UserManager.GetUser(pMsg.Param);
                    if (pTarget == null)
                        return;

                    if (pUser.FetchSynInvite(pMsg.Param))
                    {
                        // player waiting to join
                        return;
                    }

                    if (pTarget.FetchSynJoin(pUser.Identity))
                    {
                        // player requested to join
                        if (pUser.Syndicate.Members.Count >= 800)
                            return;

                        pUser.Syndicate.AppendMember(pUser, pTarget.Identity, false);
                        pTarget.ClearSynJoin();
                        return;
                    }

                    // inviting player to join
                    pTarget.SetSynInvite(pUser.Identity);
                    pMsg.Param = pUser.Identity;
                    pMsg.RequiredLevel = pUser.Level;
                    pMsg.RequiredMetempsychosis = pUser.Metempsychosis;
                    pMsg.RequiredProfession = (uint) pUser.Profession;
                    pTarget.Send(pMsg);
                    pTarget.SendRelation(pUser);
                    return;
                }

                #endregion

                #region Quit

                case SyndicateRequest.SYN_QUIT:
                {
                    if (pUser.Syndicate == null
                        || pUser.SyndicateMember == null)
                        return;

                    if (pUser.SyndicatePosition == SyndicateRank.GuildLeader)
                        return;

                    pUser.Syndicate.QuitSyndicate(pUser);
                    break;
                }

                #endregion

                #region Set Requirements

                case SyndicateRequest.SYN_SET_REQUIREMENTS:
                {
                    if (pUser.Syndicate == null
                        || pUser.SyndicateMember == null)
                        return;

                    if (pUser.SyndicatePosition != SyndicateRank.GuildLeader)
                        return;

                    if (pMsg.RequiredProfession > 127)
                        return; // max ^ 6
                    if (pMsg.RequiredMetempsychosis > 2)
                        return;
                    if (pMsg.RequiredLevel > ServerKernel.MaxLevel)
                        return;

                    pUser.Syndicate.ProfessionRequirement = (byte) pMsg.RequiredProfession;
                    pUser.Syndicate.MetempsychosisRequirement = (byte) pMsg.RequiredMetempsychosis;
                    pUser.Syndicate.Level = (byte) pMsg.RequiredLevel;
                    break;
                }

                #endregion

                #region Discharge

                case SyndicateRequest.SYN_DISCHARGE:
                case SyndicateRequest.SYN_DISCHARGE2:
                case SyndicateRequest.SYN_DISCHARGE3: // discharge paid promotion
                {
                    if (pUser.Syndicate == null || pUser.SyndicateMember == null)
                        return;

                    Character pClient = ServerKernel.UserManager.GetUser(pMsg.Param);
                    SyndicateMember pSynMember = null;
                    if (pClient == null)
                    {
                        CharacterEntity dbUser = new CharacterRepository().SearchByName(pMsg.Name);
                        if (dbUser == null)
                            return;

                        if (!pUser.Syndicate.Members.TryGetValue(dbUser.Identity, out pSynMember))
                            return;
                    }
                    else
                    {
                        pSynMember = pClient.SyndicateMember;
                    }

                    pUser.Syndicate.DischargeMember(pUser, pSynMember);
                    pUser.Syndicate.SendMembers(pUser, 0);
                    break;
                }

                #endregion

                #region Ally

                case SyndicateRequest.SYN_ALLIED:
                {
                    if (pUser.Syndicate == null)
                        return;

                    if (pUser.SyndicatePosition != SyndicateRank.GuildLeader)
                        return;

                    if (pUser.Syndicate.Allies.Count >= pUser.Syndicate.MaxAllies())
                    {
                        pUser.SendMessage(Language.StrSynAllyFull, ChatTone.System);
                        return;
                    }

                    Syndicate pTarget = ServerKernel.SyndicateManager.GetSyndicate(pMsg.Name);
                    if (pTarget == null || pTarget.IsDeleted)
                    {
                        pUser.SendMessage(Language.StrSynNoExist, ChatTone.System);
                        return;
                    }

                    if (!pTarget.LeaderIsOnline)
                    {
                        pUser.SendMessage(Language.StrSynLeaderNotOnline, ChatTone.System);
                        return;
                    }

                    Character pTargetClient = pTarget.LeaderRole;

                    RequestBox pBox = new RequestBox
                    {
                        OwnerIdentity = pUser.SyndicateIdentity,
                        OwnerName = pUser.SyndicateName,
                        ObjectIdentity = pTarget.Identity,
                        ObjectName = pTarget.Name,
                        Type = RequestBoxType.SyndicateAlly,
                        Message =
                            $"{pUser.Name} Guild Leader of {pUser.SyndicateName} wants to be your ally. Do you accept?"
                    };
                    pTargetClient.RequestBox = pUser.RequestBox = pBox;
                    pBox.Send(pTargetClient);
                    break;
                }

                #endregion

                #region Remove Ally

                case SyndicateRequest.SYN_NEUTRAL1:
                {
                    if (pUser.Syndicate == null)
                        return;

                    if (pUser.SyndicatePosition != SyndicateRank.GuildLeader)
                        return;

                    Syndicate pTarget = ServerKernel.SyndicateManager.GetSyndicate(pMsg.Name);
                    if (pTarget == null || pTarget.IsDeleted)
                    {
                        pUser.SendMessage(Language.StrSynNoExist, ChatTone.System);
                        return;
                    }

                    pUser.Syndicate.RemoveAlliance(pTarget.Identity);
                    break;
                }

                #endregion

                #region Enemy

                case SyndicateRequest.SYN_ENEMIED:
                {
                    if (pUser.Syndicate == null)
                        return;

                    if (pUser.SyndicatePosition != SyndicateRank.GuildLeader)
                        return;

                    if (pUser.Syndicate.Allies.Count >= pUser.Syndicate.MaxEnemies())
                    {
                        pUser.SendMessage(Language.StrSynEnemyFull, ChatTone.System);
                        return;
                    }

                    Syndicate pTarget = ServerKernel.SyndicateManager.GetSyndicate(pMsg.Name);
                    if (pTarget == null || pTarget.IsDeleted)
                    {
                        pUser.SendMessage(Language.StrSynNoExist, ChatTone.System);
                        return;
                    }

                    pUser.Syndicate.AntagonizeSyndicate(pTarget);
                    break;
                }

                #endregion

                #region Peace

                case SyndicateRequest.SYN_NEUTRAL2:
                {
                    if (pUser.Syndicate == null)
                        return;

                    if (pUser.SyndicatePosition != SyndicateRank.GuildLeader)
                        return;

                    Syndicate pTarget = ServerKernel.SyndicateManager.GetSyndicate(pMsg.Name);
                    if (pTarget == null || pTarget.IsDeleted)
                    {
                        pUser.SendMessage(Language.StrSynNoExist, ChatTone.System);
                        return;
                    }

                    pUser.Syndicate.RemoveEnemy(pTarget.Identity);
                    break;
                }

                #endregion

                default:
                    Program.WriteLog("Missing handler for 1107:" + pMsg.Action);
                    return;
            }
        }
    }
}
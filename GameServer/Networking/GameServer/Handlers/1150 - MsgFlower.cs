﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - 1150 - MsgFlower.cs
// Last Edit: 2019/11/24 19:03
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System;
using System.Drawing;
using FtwCore;
using FtwCore.Common;
using FtwCore.Common.Enums;
using FtwCore.Networking.Packets;
using GameServer.Structures.Entities;
using GameServer.Structures.Items;

#endregion

namespace GameServer.Networking.GameServer
{
    public static partial class Handlers
    {
        public static void HandleMsgFlower(Character user, MsgFlower msg)
        {
            switch (msg.Mode)
            {
                case 0: // give flower
                {
                    uint idTarget = msg.Identity;

                    Character target = ServerKernel.UserManager.GetUser(idTarget);
                    if (target == null)
                    {
                        user.SendSysMessage(Language.StrTargetNotOnline);
                        return;
                    }

                    if (!user.IsAlive)
                    {
                        user.SendSysMessage(Language.StrFlowerSenderNotAlive);
                        return;
                    }

                    if (user.Gender != 1)
                    {
                        user.SendSysMessage(Language.StrFlowerSenderNotMale);
                        return;
                    }

                    if (target.Gender != 2)
                    {
                        user.SendSysMessage(Language.StrFlowerReceiverNotFemale);
                        return;
                    }

                    if (user.Level < 40)
                    {
                        user.SendSysMessage(Language.StrFlowerLevelTooLow);
                        return;
                    }

                    ushort usAmount = 0;
                    FlowerType pType = FlowerType.RED_ROSE;
                    // daily flower
                    if (msg.ItemIdentity == 0)
                    {
                        if (user.RedRoses >= uint.Parse(DateTime.Now.ToString("yyyyMMdd")))
                        {
                            user.SendSysMessage(Language.StrFlowerHaveSentToday);
                            return;
                        }

                        switch (user.VipLevel)
                        {
                            case 0:
                                usAmount = 1;
                                break;
                            case 1:
                                usAmount = 2;
                                break;
                            case 2:
                                usAmount = 5;
                                break;
                            case 3:
                                usAmount = 7;
                                break;
                            case 4:
                                usAmount = 9;
                                break;
                            case 5:
                                usAmount = 12;
                                break;
                            default:
                                usAmount = 30;
                                break;
                        }

                        user.RedRoses = uint.Parse(DateTime.Now.ToString("yyyyMMdd"));
                    }
                    else
                    {
                        Item flower = user.UserPackage[msg.ItemIdentity];
                        if (flower == null)
                            return;

                        switch (flower.GetItemSubtype())
                        {
                            case 751:
                                pType = FlowerType.RED_ROSE;
                                break;
                            case 752:
                                pType = FlowerType.WHITE_ROSE;
                                break;
                            case 753:
                                pType = FlowerType.ORCHID;
                                break;
                            case 754:
                                pType = FlowerType.TULIP;
                                break;
                        }

                        usAmount = flower.Durability;
                        user.UserPackage.RemoveFromInventory(flower, RemovalType.Delete);
                    }

                    switch (pType)
                    {
                        case FlowerType.RED_ROSE:
                            target.RedRoses += usAmount;
                            break;
                        case FlowerType.WHITE_ROSE:
                            target.WhiteRoses += usAmount;
                            break;
                        case FlowerType.ORCHID:
                            target.Orchids += usAmount;
                            break;
                        case FlowerType.TULIP:
                            target.Tulips += usAmount;
                            break;
                    }

                    user.SendSysMessage(Language.StrFlowerSendSuccess);
                    switch (usAmount)
                    {
                        case 3:
                            switch (pType)
                            {
                                case FlowerType.RED_ROSE:
                                    target.SendSysMessage(string.Format(Language.StrFlowerGmRed3, user.Name), Color.White);
                                    break;
                                case FlowerType.WHITE_ROSE:
                                    target.SendSysMessage(string.Format(Language.StrFlowerGmWhite3, user.Name), Color.White);
                                    break;
                                case FlowerType.ORCHID:
                                    target.SendSysMessage(string.Format(Language.StrFlowerGmOrchid3, user.Name), Color.White);
                                    break;
                                case FlowerType.TULIP:
                                    target.SendSysMessage(string.Format(Language.StrFlowerGmTulip3, user.Name), Color.White);
                                    break;
                            }

                            break;
                        case 9:
                            switch (pType)
                            {
                                case FlowerType.RED_ROSE:
                                    target.SendSysMessage(string.Format(Language.StrFlowerGmRed9, user.Name), Color.White);
                                    break;
                                case FlowerType.WHITE_ROSE:
                                    target.SendSysMessage(string.Format(Language.StrFlowerGmWhite9, user.Name), Color.White);
                                    break;
                                case FlowerType.ORCHID:
                                    target.SendSysMessage(string.Format(Language.StrFlowerGmOrchid9, user.Name), Color.White);
                                    break;
                                case FlowerType.TULIP:
                                    target.SendSysMessage(string.Format(Language.StrFlowerGmTulip9, user.Name), Color.White);
                                    break;
                            }

                            break;
                        case 99:
                            switch (pType)
                            {
                                case FlowerType.RED_ROSE:
                                    ServerKernel.UserManager.SendToAllUser(string.Format(Language.StrFlowerGmRed99,
                                        user.Name, target.Name), Color.White, ChatTone.Center);
                                    break;
                                case FlowerType.WHITE_ROSE:
                                    ServerKernel.UserManager.SendToAllUser(string.Format(Language.StrFlowerGmWhite99,
                                        user.Name, target.Name), Color.White, ChatTone.Center);
                                    break;
                                case FlowerType.ORCHID:
                                    ServerKernel.UserManager.SendToAllUser(string.Format(Language.StrFlowerGmOrchid99,
                                        user.Name, target.Name), Color.White, ChatTone.Center);
                                    break;
                                case FlowerType.TULIP:
                                    ServerKernel.UserManager.SendToAllUser(string.Format(Language.StrFlowerGmTulip99,
                                        user.Name, target.Name), Color.White, ChatTone.Center);
                                    break;
                            }

                            break;
                        case 999:
                            switch (pType)
                            {
                                case FlowerType.RED_ROSE:
                                    ServerKernel.UserManager.SendToAllUser(string.Format(Language.StrFlowerGmRed999,
                                        user.Name, target.Name), Color.White, ChatTone.Center);
                                    break;
                                case FlowerType.WHITE_ROSE:
                                    ServerKernel.UserManager.SendToAllUser(string.Format(Language.StrFlowerGmWhite999,
                                        user.Name, target.Name), Color.White, ChatTone.Center);
                                    break;
                                case FlowerType.ORCHID:
                                    ServerKernel.UserManager.SendToAllUser(string.Format(Language.StrFlowerGmOrchid999,
                                        user.Name, target.Name), Color.White, ChatTone.Center);
                                    break;
                                case FlowerType.TULIP:
                                    ServerKernel.UserManager.SendToAllUser(string.Format(Language.StrFlowerGmTulip999,
                                        user.Name, target.Name), Color.White, ChatTone.Center);
                                    break;
                            }

                            break;
                        default:
                            target.SendSysMessage(string.Format(Language.StrFlowerReceiverPrompt, user.Name));
                            break;
                    }

                    ServerKernel.FlowerRanking.AddFlowers(pType, usAmount, idTarget);

                    user.Screen.Send(msg, true);
                    user.Send(new MsgFlower
                    {
                        Mode = 2,
                        Identity = user.Identity,
                        Amount = 0
                    });
                    target.Send(new MsgFlower
                    {
                        Sender = user.Name,
                        Receptor = target.Name,
                        SendAmount = usAmount,
                        SendFlowerType = pType
                    });
                    break;
                }

                default:
                {
                    Program.WriteLog($"MsgFlower.Mode({msg.Mode}) not handled", LogType.DEBUG);
                    break;
                }
            }
        }
    }
}
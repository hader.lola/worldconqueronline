﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - 1038 - MsgSolidify.cs
// Last Edit: 2019/11/24 19:03
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System.Collections.Generic;
using FtwCore;
using FtwCore.Common;
using FtwCore.Common.Enums;
using FtwCore.Networking.Packets;
using GameServer.Structures.Entities;
using GameServer.Structures.Items;

#endregion

namespace GameServer.Networking.GameServer
{
    public static partial class Handlers
    {
        public static void HandleMsgSolidify(Character user, MsgSolidify msg)
        {
            if (!user.IsAlive)
            {
                user.SendSysMessage(Language.StrDie);
                return;
            }

            Item target = user.UserPackage[msg.TargetItem];
            switch (msg.Mode)
            {
                case SolidifyMode.Refinery:
                {
                    if (target != null)
                    {
                        if (target.RefineryIsActive() && !target.RefineryIsPermanent)
                        {
                            List<uint> usableStones = new List<uint>();
                            List<uint> stones = msg.StoneItems;
                            uint sum = 0;
                            foreach (var id in stones)
                            {
                                Item item = user.UserPackage[id];
                                if (item != null)
                                {
                                    if (item.Type == SpecialItem.PERMANENT_STONE)
                                    {
                                        usableStones.Add(id);
                                        sum += 10;
                                    }

                                    if (item.Type == SpecialItem.BIGPERMANENT_STONE)
                                    {
                                        usableStones.Add(id);
                                        sum += 100;
                                    }
                                }
                            }

                            if (sum >= target.RefineryRemainingPoints())
                            {
                                foreach (uint item in usableStones)
                                    if (!user.UserPackage.RemoveFromInventory(item, RemovalType.Delete))
                                    {
                                        user.SendSysMessage(Language.StrSolidifyStoneError);
                                        ServerKernel.Log.GmLog(@"error_log",
                                            string.Format("Couldn't remove [{0}] from user [{1}]. Item inexistent.",
                                                item, user.Identity));
                                        return;
                                    }

                                target.RefineryExpire = 0;
                                target.RefineryStabilization += sum;
                                target.RefineryStart = 0;
                                user.Send(target.CreateMsgItemInfo(ItemMode.Update));
                                target.LoadArtifactAndRefinery();

                                MsgItemStatus status = new MsgItemStatus();
                                status.Append(target.Identity, (PurificationType)4, target.RefineryType, target.RefineryLevel,
                                     0, target.RefineryExpire - target.RefineryStart);
                                user.Send(status);

                                    target.SendPurification();
                                //user.Send(msg);
                                ServerKernel.Log.GmLog("refinery",
                                    $"User:[{user.Identity}] has stabilized item:[{target.Identity}] with {target.Type}");
                                foreach (uint item in usableStones)
                                    ServerKernel.Log.GmLog("refinery", $"Stone identity: {item}");
                            }
                        }
                    }

                    break;
                }

                case SolidifyMode.Artifact:
                {
                    if (target != null)
                    {
                        if (target.ArtifactIsActive()
                            && !target.ArtifactIsPermanent)
                        {
                            List<uint> usableStones = new List<uint>();
                            List<uint> stones = msg.StoneItems;
                            uint sum = 0;
                            foreach (var id in stones)
                            {
                                Item item = user.UserPackage[id];
                                if (item != null)
                                {
                                    if (item.Type == SpecialItem.PERMANENT_STONE)
                                    {
                                        usableStones.Add(id);
                                        sum += 10;
                                    }

                                    if (item.Type == SpecialItem.BIGPERMANENT_STONE)
                                    {
                                        usableStones.Add(id);
                                        sum += 100;
                                    }
                                }
                            }

                            if (sum >= target.ArtifactRemainingPoints())
                            {
                                foreach (uint item in usableStones)
                                    if (!user.UserPackage.RemoveFromInventory(item, RemovalType.Delete))
                                    {
                                        user.SendSysMessage(Language.StrSolidifyStoneError);
                                        ServerKernel.Log.GmLog(@"syslog\error_log",
                                            $"Couldn't remove [{item}] from user [{user.Identity}]. Item inexistent.");
                                        return;
                                    }

                                target.ArtifactExpire = 0;
                                target.ArtifactStabilization += sum;
                                target.ArtifactStart = 0;
                                user.Send(target.CreateMsgItemInfo(ItemMode.Update));
                                target.LoadArtifactAndRefinery();
                                target.SendPurification();

                                MsgItemStatus status = new MsgItemStatus();
                                status.Append(target.Identity, PurificationType.StabilizationEffectPurification, target.ArtifactType, target.ArtifactLevel, 
                                    target.ArtifactExpire - target.ArtifactStart);
                                user.Send(status);

                                ServerKernel.Log.GmLog("artifact",
                                    $"User:[{user.Identity}] has stabilized item:[{target.Identity}] with");
                                foreach (uint item in usableStones)
                                    ServerKernel.Log.GmLog("artifact", $"Stone identity: {item}");
                            }
                        }
                    }

                    break;
                }
            }
        }
    }
}
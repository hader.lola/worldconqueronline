﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - LoginPacketHandler.cs
// Last Edit: 2019/11/24 19:03
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System;
using FtwCore.Common;
using FtwCore.Common.Enums;
using FtwCore.Networking.Packets;
using GameServer.Structures.Login;

#endregion

namespace GameServer.Networking.LoginClient
{
    public class LoginPacketHandlers
    {
        #region 11 - Auth Response

        [PacketHandlerType(PacketType.MsgLoginAuthRequest)]
        public void AuthRequest(LoginUser pServer, byte[] pBuffer)
        {
            var pMsg = new MsgLoginSvAuthRequest(pBuffer);
            if (pMsg.Message != ServerKernel.HelloReplyString)
            {
                pServer.Disconnect();
                return;
            }

            pServer.ConnectionState = InterServerState.MeetingOk;
            pServer.Send(new MsgLoginSvAuthentication(ServerKernel.Username, ServerKernel.Password,
                ServerKernel.ServerName, (ushort) Analytics.Statistic.MaxOnlinePlayers));
            Program.WriteLog("Connected to the Account Server, waiting for authentication...");
        }

        #endregion

        #region 13 - Server State

        [PacketHandlerType(PacketType.MsgLoginCompleteAuthentication)]
        public void CompleteAuthentication(LoginUser pClient, byte[] pBuffer)
        {
            var pMsg = new LoginSvResponsePacket(pBuffer);
            switch (pMsg.Response)
            {
                case LoginServerResponse.LoginSuccessful:
                    // send server information packet
                    pClient.Send(new MsgServerInformation((ushort) Analytics.Statistic.MaxOnlinePlayers,
                        ServerKernel.UserManager.Count, ServerKernel.ServerName, ServerKernel.GamePort));
                    ServerKernel.UserManager.SendPlayersToLogin();
                    Program.WriteLog("Authentication completed! Server is ready to go...");
                    return;
                case LoginServerResponse.LoginDeniedDisconnected:
                    Program.WriteLog("Disconnected from the account server...", LogType.WARNING);
                    break;
                case LoginServerResponse.LoginDeniedServerNotExist:
                    Program.WriteLog("Access denied to the account server due to invalid server name...",
                        LogType.WARNING);
                    break;
                case LoginServerResponse.LoginDeniedIpaddress:
                    Program.WriteLog("Access denied to the account server due to invalid ip address...",
                        LogType.WARNING);
                    break;
                case LoginServerResponse.LoginDeniedLogin:
                    Program.WriteLog(
                        "Attempted to use an invalid username or password when connecting to the account server...",
                        LogType.WARNING);
                    break;
                default:
                    Program.WriteLog("Invalid LOGIN_COMPLETE_AUTHENTICATION response " + pMsg.Response,
                        LogType.DEBUG);
                    break;
            }

            try
            {
                pClient.Disconnect();
            }
            catch (Exception ex)
            {
            }
        }

        #endregion

        #region 26 - Get User Incoming Login Request

        [PacketHandlerType(PacketType.MsgLoginRequestUserSignin)]
        public void ProcessUserSignin(LoginUser pClient, byte[] pBuffer)
        {
            var pMsg = new MsgUsrLogin(pBuffer);
            ServerKernel.LoginQueue.TryRemove(pMsg.UserIdentity, out var _);
            ServerKernel.LoginQueue.TryAdd(pMsg.UserIdentity,
                new LoginRequest(pMsg.UserIdentity, pMsg.PacketHash, pMsg.RequestTime, pMsg.IpAddress,
                    pMsg.VipLevel,
                    pMsg.Authority, pMsg.MacAddress));
        }

        #endregion
    }
}
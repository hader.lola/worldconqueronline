﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - Jiang Hu.cs
// Last Edit: 2019/12/15 03:19
// Created: 2019/12/10 08:49
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using FtwCore;
using FtwCore.Common;
using FtwCore.Common.Enums;
using FtwCore.Database.Entities;
using FtwCore.Database.Repositories;
using FtwCore.Networking.Packets;
using GameServer.Networking.GameServer;
using GameServer.Structures.Entities;

#endregion

namespace GameServer.Structures
{
    public sealed class JiangHu
    {
        public const int QUALITY_AMOUNT = 6;
        private const int POINTS_TO_COURSE = 10000;
        private const int MAX_STAGES = 9;
        private const int MIN_SCORE_TO_NEXT_STAGE = 1350;
        private const int MAX_COURSES = 100;
        private const int MAX_FREE_COURSES_DAILY = 10;
        private const int MAX_TALENT = 4;
        private const int _WAIT_SECONDS_CHANGE_PK = 600;
        private const int STUDY_POINTS_COST = 10;
        private const int EMONEY_PRICE = 10;
        private const int EMONEY_RESTORE_PRICE = 20;

        private readonly uint[] m_dwOutTwinCityAddPoints =
        {
            10, 20, 26, 31, 52
        };

        private readonly uint[] m_dwTwinCityAddPoints =
        {
            312, 625, 781, 937, 1562
        };

        private readonly ConcurrentDictionary<int, JiangHuStage> m_myStages =
            new ConcurrentDictionary<int, JiangHuStage>();

        private readonly Character m_pUser;
        private readonly TimeOut m_tExitKongFu = new TimeOut(_WAIT_SECONDS_CHANGE_PK);

        public bool HasStarted;

        private KongFuEntity m_pEntity;

        private readonly TimeOut m_tAddPoints = new TimeOut(60);

        public JiangHu(Character user)
        {
            m_pUser = user ?? throw new NullReferenceException("User cannot be null when inititalizing JiangHu.");

            m_tExitKongFu.Clear();
        }

        public string Name
        {
            get => m_pEntity?.Name ?? Language.StrNone;
            set => m_pEntity.Name = value;
        }

        public bool IsJiangActive => m_pUser.PkMode == PkModeType.JiangHu ||
                                     m_tExitKongFu.IsActive() && !m_tExitKongFu.IsTimeOut();

        public int CurrentStage => m_myStages.Count;

        public byte CurrentTalent
        {
            get => m_pEntity?.CurrentTalent ?? 0;
            set
            {
                if (m_pEntity == null)
                    return;

                m_pUser.TalentPoints = m_pEntity.CurrentTalent = Math.Max((byte) 0, Math.Min((byte) MAX_TALENT, value));
                Send(KongfuBaseMode.UpdateTalent);
                Save();
            }
        }

        public uint TalentPoints
        {
            get => m_pEntity?.TalentPoints ?? 0;
            set
            {
                if (m_pEntity == null)
                    return;
                m_pEntity.TalentPoints = Math.Min(MAX_COURSES * POINTS_TO_COURSE, value);
                Save();
            }
        }

        public uint FreeCourses => TalentPoints / 10000;

        public int FreeCoursesUsedToday { get; set; }
        public int PaidCoursesUsedToday { get; set; }

        public byte LastUpdatedStage { get; set; }

        public byte LastUpdatedStar { get; set; }

        public int RemainingFreeCoursesToday => MAX_FREE_COURSES_DAILY - FreeCoursesUsedToday;

        public uint Score => m_pEntity.Score;

        public int SecondsTillNextCourse
        {
            get
            {
                int maxSeconds = 0;
                if (m_pUser.IsInTwinCity() && IsJiangActive)
                {
                    maxSeconds = (int) (10000d / m_dwTwinCityAddPoints[CurrentTalent] * 60) * 60;
                }
                else
                {
                    maxSeconds = (int) (10000d / m_dwOutTwinCityAddPoints[CurrentTalent] * 60) * 60;
                }

                int amount = (int) ((TalentPoints / (float) POINTS_TO_COURSE) * maxSeconds);
                return UnixTimestamp.Now() + (int) (maxSeconds - maxSeconds * (amount % 10000 / 10000d));
            }
        }

        public byte LastStage { get; set; }
        public byte LastStar { get; set; }

        public bool Create(string name)
        {
            if (m_pUser == null)
            {
                Program.WriteLog("Tried to start jiang hu for nulled Character class.", LogType.ERROR);
                return false;
            }

            if (m_pUser.Metempsychosis < 2)
            {
                m_pUser.SendSysMessage(Language.StrKongFuRequirementsNotMeet, Color.White);
                return false;
            }

            if (m_pUser.Level < 30 && m_pUser.Reincarnation == 0)
            {
                m_pUser.SendSysMessage(Language.StrKongFuRequirementsNotMeet, Color.White);
                return false;
            }

            KongFuRepository repository = new KongFuRepository();
            if (repository.HasJiangHu(m_pUser.Identity))
            {
                return Initialize();
            }

            if (!Handlers.CheckName(name))
            {
                m_pUser.SendSysMessage(Language.StrKongFuNameInvalidCharacters);
                return false;
            }

            if (repository.CheckNameExists(name))
            {
                m_pUser.SendSysMessage(Language.StrKongFuNameAlreadyExists);
                m_pUser.Send(new MsgAction(m_pUser.Identity, 617, 0, 0, GeneralActionType.OpenWindow));
                return false;
            }

            if (!repository.Save(m_pEntity = new KongFuEntity
            {
                UserIdentity = m_pUser.Identity,
                Name = name,
                CoursesAvailable = 5,
                CurrentTalent = 2,
                FreeCourses = 5,
                LastTraining = null,
                Score = 0,
                TalentPoints = 0,
                TrainingCount = 0,
                TrainingCountToday = 0,
                UnlockDate = DateTime.Now
            }))
            {
                m_pUser.SendSysMessage(Language.StrKongFuCreateError);
                return false;
            }

            HasStarted = true;

            m_myStages.TryAdd(1, new JiangHuStage(1, m_pUser, this));
            m_myStages[1].Generate(1, 0, false, false);

            Send(KongfuBaseMode.SendInfo);
            SendAllStars();
            SetStatus(true);
            m_pUser.AwardStudyPoints(100);
            Send(KongfuBaseMode.UpdateTime);
            Send(KongfuBaseMode.UpdateTalent);
            SetInnerPower();

            m_pUser.TalentPoints = CurrentTalent;
            m_pUser.JiangHuActive = true;

            m_tAddPoints.Startup(60);
            return true;
        }

        public bool Initialize()
        {
            if (m_pUser == null)
                return false;
            if (m_pUser.Metempsychosis < 2)
                return false;
            if (m_pUser.Level < 30 && m_pUser.Reincarnation == 0)
                return false;

            m_pEntity = new KongFuRepository().FetchByIdentity(m_pUser.Identity);
            if (m_pEntity != null)
            {
                HasStarted = true;
                List<KongFuAttrEntity> stars = new KongFuAttrRepository().FetchAll(m_pUser.Identity)
                    .OrderBy(x => x.Stage).ThenBy(x => x.Star).ToList();
                foreach (var star in stars)
                {
                    if (!m_myStages.ContainsKey(star.Stage))
                        m_myStages.TryAdd(star.Stage, new JiangHuStage(star.Stage, m_pUser, this));

                    m_myStages[star.Stage].AddStar(new JiangHuStar(star));
                }
            }
            else
            {
                return false;
            }

            FreeCoursesUsedToday = new KongFuAttrLogRepository().FreeCoursesToday(m_pUser.Identity);
            PaidCoursesUsedToday = new KongFuAttrLogRepository().PaidCoursesToday(m_pUser.Identity);

            m_pUser.TalentPoints = CurrentTalent;

            Send(KongfuBaseMode.SendInfo);
            SendAllStars();

            if (m_pEntity.RemainingToExit > 0)
            {
                m_tExitKongFu.Startup((int)m_pEntity.RemainingToExit);
                SetStatus(true, false);
            }
            else
            {
                SetStatus(false);
            }

            Send(KongfuBaseMode.UpdateTime);
            Send(KongfuBaseMode.UpdateTalent);
            SetInnerPower();

            m_tAddPoints.Startup(60);
            return HasStarted;
        }

        public bool IsStageUsable(int stage)
        {
            if (stage == 1)
                return true;
            if (m_myStages.ContainsKey(stage + 1))
                return true;
            if (m_myStages[stage - 1].GetInnerPower() < MIN_SCORE_TO_NEXT_STAGE || !m_myStages[stage - 1].HasAllStars)
                return false;
            return true;
        }

        public bool SpendFreeCourse()
        {
            if (FreeCourses > 0 && TalentPoints >= POINTS_TO_COURSE)
            {
                TalentPoints -= POINTS_TO_COURSE;
                return true;
            }

            return false;
        }

        public void Study(int stage, int star, int high, bool isFree, bool save)
        {
            /**
             * Check stages and if can start new one.
             */
            if (!m_myStages.ContainsKey(stage))
            {
                if (stage > CurrentStage && CurrentStage + 1 != stage)
                {
                    // cant learn this stage
                    return;
                }

                if (stage > 9)
                {
                    // invalid stage???
                    return;
                }

                if (m_myStages[CurrentStage].GetInnerPower() < MIN_SCORE_TO_NEXT_STAGE)
                {
                    // need 1350 score to upgrade to next stage
                    return;
                }

                if (!IsStageUsable(stage))
                {
                    // something made this stuck???
                    return;
                }

                m_myStages.TryAdd(stage, new JiangHuStage((byte) stage, m_pUser, this));
            }

            /**
             * Now we done, so we must check if we can use the required star, if it's an invalid star, we will get the latest one.
             */
            JiangHuStage pStage = m_myStages[stage];
            if (!pStage.Stars.ContainsKey(star))
            {
                if (star > pStage.CurrentStar && pStage.CurrentStar + 1 != star)
                {
                    // user is trying to open an invalid star?
                    star = pStage.CurrentStar + 1;
                }

                if (star > 9)
                {
                    // should not continue, may be a missclick or exploit. Let's just return.
                    return;
                }
            }

            if (isFree && FreeCoursesUsedToday >= MAX_FREE_COURSES_DAILY)
            {
                return;
            }

            if (m_pUser.StudyPoints < STUDY_POINTS_COST)
            {
                return;
            }

            if (/*isFree && */CurrentTalent == 0)
            {
                return;
            }

            if (isFree && FreeCourses == 0)
            {
                return;
            }

            int emoneyAmount = isFree ? 0 : EMONEY_PRICE + EMONEY_PRICE * Math.Min(49, PaidCoursesUsedToday);

            switch (high)
            {
                case 1:
                    emoneyAmount += 5;
                    break;
                case 2:
                    emoneyAmount += 50;
                    break;
            }

            if (!isFree && m_pUser.Emoney < emoneyAmount && m_pUser.BoundEmoney < emoneyAmount)
            {
                // not enough CPs
                return;
            }

            //if (isFree)
            CurrentTalent -= 1;

            if (!m_pUser.ReduceStudyPoints(STUDY_POINTS_COST))
            {
                // no study points
                return;
            }

            if (isFree && !SpendFreeCourse())
            {
                // no free courses???
                return;
            }

            if (!m_pUser.SpendEmoney(emoneyAmount, EmoneySourceType.System, null, true) && !m_pUser.SpendBoundEmoney(emoneyAmount))
            {
                return;
            }

            pStage.Generate(star, high, isFree, save);

            if (isFree)
            {
                FreeCoursesUsedToday++;
            }
            else
            {
                PaidCoursesUsedToday++;
            }

            m_pEntity.TrainingCount++;
            m_pEntity.TrainingCountToday++;
            m_pEntity.LastTraining = DateTime.Now;
            Save();

            m_pUser.Send(new MsgOwnKongfuImproveFeedback
            {
                FreeCourse = TalentPoints,
                Star = (byte) star,
                Stage = (byte) stage,
                FreeCourseUsedtoday = (byte) FreeCoursesUsedToday,
                PaidRounds = (uint) PaidCoursesUsedToday,
                Attribute = pStage.Stars[star].Identity
            });

            Update();
            SetInnerPower();
        }

        public void Restore(byte stage, byte star)
        {
            if (m_pUser.Emoney < EMONEY_RESTORE_PRICE)
            {
                // error message
                return;
            }

            if (!ContainsStar(stage, star))
                return;

            if (!m_pUser.SpendEmoney(EMONEY_RESTORE_PRICE, EmoneySourceType.System, null, true))
                return;

            foreach (var pStage in m_myStages.Values)
                if (pStage.Restore(stage, star))
                    break;

            Update();
            SetInnerPower();
        }

        public bool ContainsStar(byte stage, byte star)
        {
            return m_myStages.ContainsKey(stage) && m_myStages[stage].Stars.ContainsKey(star);
        }

        public JiangHuStar GetStar(byte stage, byte star)
        {
            return ContainsStar(stage, star) ? m_myStages[stage].Stars[star] : null;
        }

        public Dictionary<JiangHuAttrType, int> GetPowers()
        {
            Dictionary<JiangHuAttrType, int> result = new Dictionary<JiangHuAttrType, int>();
            foreach (var stage in m_myStages.Values)
            {
                foreach (var power in stage.GetPowers())
                {
                    if (result.ContainsKey(power.Key))
                        result[power.Key] += power.Value;
                    else result.Add(power.Key, power.Value);
                }
            }
            return result;
        }

        public void Update()
        {
            if (!m_myStages.ContainsKey(CurrentStage) || CurrentStage == 9)
                return;
            if (m_myStages[CurrentStage].GetInnerPower() < MIN_SCORE_TO_NEXT_STAGE)
                return;
            if (m_myStages.ContainsKey((byte) (CurrentStage + 1)))
                return;

            m_myStages.TryAdd((byte) (CurrentStage + 1), new JiangHuStage((byte) (CurrentStage + 1), m_pUser, this));
            SendAllStars();
        }

        public void SetInnerPower()
        {
            m_pEntity.Score = 0;
            foreach (var stage in m_myStages.Values)
                m_pEntity.Score += stage.GetInnerPower();
            Save();
        }

        public void Send(KongfuBaseMode mode, Character user = null)
        {
            MsgOwnKongfuBase msg = new MsgOwnKongfuBase
            {
                Mode = mode
            };

            switch (mode)
            {
                case KongfuBaseMode.UpdateStar:
                    msg.Append(m_pUser.Identity.ToString(), LastStage.ToString(), LastStar.ToString());
                    break;
                case KongfuBaseMode.UpdateTalent:
                    msg.Append(m_pUser.Identity.ToString(), (CurrentTalent + 1).ToString());
                    break;
                case KongfuBaseMode.UpdateTime:
                    msg.Append(TalentPoints.ToString(), SecondsTillNextCourse.ToString());
                    break;
                case KongfuBaseMode.SendInfo:
                    msg.Append(m_pUser.Identity.ToString(), LastStage.ToString(), LastStar.ToString());
                    break;
            }

            msg.Finish();

            if (user == null)
                m_pUser?.Screen.Send(msg, true);
            else
                user.Send(msg);
        }

        public void SetStatus(bool jiangOn, bool overridePk = true)
        {
            if (!HasStarted)
                return;

            if (jiangOn && overridePk)
            {
                m_tExitKongFu.Clear();
                m_pUser.ChangePkMode(PkModeType.JiangHu);
                m_pUser.JiangHuActive = true;
                m_pEntity.RemainingToExit = _WAIT_SECONDS_CHANGE_PK;
                Save();
            }

            if (!jiangOn)
            {
                m_pUser.JiangHuActive = false;
            }

            m_pUser.TalentPoints = CurrentTalent;

            MsgOwnKongfuBase msg = new MsgOwnKongfuBase
            {
                Mode = KongfuBaseMode.SendStatus
            };
            if (jiangOn)
            {
                msg.Append(m_pUser.Identity.ToString(), (CurrentTalent + 1).ToString(), IsJiangActive ? "1" : "2");
            }
            else
            {
                msg.Append(m_pUser.Identity.ToString(), "0", IsJiangActive ? "1" : "2");
            }

            msg.Finish();
            m_pUser.Send(msg);
        }

        public void LeaveJiangHu()
        {
            m_tExitKongFu.Startup(_WAIT_SECONDS_CHANGE_PK);
            m_pEntity.RemainingToExit = _WAIT_SECONDS_CHANGE_PK;
        }

        public void Logout()
        {
            if (m_tExitKongFu.IsActive() && !m_tExitKongFu.IsTimeOut())
            {
                m_pEntity.RemainingToExit = (uint) m_tExitKongFu.GetRemain();
            }
            else
            {
                m_pEntity.RemainingToExit = 0;
            }
            Save();
        }

        public void SendAllStars(Character user = null)
        {
            Update();
            MsgOwnKongfuImproveSummaryInfo msg = new MsgOwnKongfuImproveSummaryInfo
            {
                Name = Name,
                Stage = (byte) CurrentStage,
                FreeTalentToday = MAX_FREE_COURSES_DAILY,
                FreeTalentUsed = (byte) FreeCoursesUsedToday,
                Talent = (byte) (CurrentTalent + 1),
                Points = m_pUser.StudyPoints,
                //Timer = (uint) (user?.Identity != m_pUser.Identity ? 0xd1d401 : 0xec8600),
                BoughtTimes = (byte) PaidCoursesUsedToday
            };

            if (user == null || user.Identity == m_pUser.Identity)
                msg.Timer = 0xec8600;
            else
                msg.Timer = 0xd1d401;

            foreach (var stage in m_myStages.Values)
            {
                foreach (var star in stage.Stars.Values)
                {
                    msg.Append(star.Identity);
                }
            }

            if (user == null)
                m_pUser?.Send(msg);
            else user.Send(msg);
        }

        public void SetAttribute(int stage, int star, JiangHuAttrType type, JiangHuQuality quality)
        {
            if (!m_myStages.ContainsKey(stage))
                return;

            if (!m_myStages[stage].Stars.ContainsKey(star) && !m_pUser.IsPm())
                return;

            if (!m_myStages[stage].Stars.ContainsKey(star) && m_pUser.IsPm())
            {
                m_myStages[stage].Generate(star, 0, false, false);
            }

            m_myStages[stage].Stars[star].Quality = quality;
            m_myStages[stage].Stars[star].Type = type;
            m_myStages[stage].Stars[star].Save();
            SendAllStars();
            SetInnerPower();
        }

        public void OnTimer()
        {
            if (!HasStarted || CurrentStage == 0)
                return;

            if (m_tAddPoints.ToNextTime() && TalentPoints < MAX_COURSES * POINTS_TO_COURSE)
            {
                if (m_pUser.IsInTwinCity() && IsJiangActive)
                {
                    TalentPoints += m_dwTwinCityAddPoints[CurrentTalent];
                }
                else
                {
                    TalentPoints += m_dwOutTwinCityAddPoints[CurrentTalent];
                }

                Save();
                Send(KongfuBaseMode.UpdateTime);
            }

            if (m_tExitKongFu.IsActive() && m_tExitKongFu.IsTimeOut())
            {
                SetStatus(false);
                m_tExitKongFu.Clear();
            }
        }

        public List<KongFuEntity> GetRanking()
        {
            return new KongFuRepository().FetchRank(0, 1000).ToList();
        }

        public bool Save()
        {
            return new KongFuRepository().Save(m_pEntity);
        }
    }

    public sealed class JiangHuStage
    {
        private readonly uint[] m_attributePoints =
        {
            100, 120, 150, 200, 300, 500
        };

        private readonly Dictionary<int, List<JiangHuAttrType>> m_dicTypesPerLevel = new Dictionary<int, List<JiangHuAttrType>>
            {
                {
                    1, new List<JiangHuAttrType>
                    {
                        JiangHuAttrType.MAttack, JiangHuAttrType.SkillCriticalStrike, JiangHuAttrType.Breakthrough,
                        JiangHuAttrType.Counteraction, JiangHuAttrType.Mdefense,
                        JiangHuAttrType.MaxMana, JiangHuAttrType.FinalMagicAttack
                    }
                },
                {
                    2, new List<JiangHuAttrType>
                    {
                        JiangHuAttrType.SkillCriticalStrike, JiangHuAttrType.Counteraction, JiangHuAttrType.Immunity,
                        JiangHuAttrType.Mdefense, JiangHuAttrType.MaxMana,
                        JiangHuAttrType.FinalMagicDefense, JiangHuAttrType.FinalDefense
                    }
                },
                {
                    3, new List<JiangHuAttrType>
                    {
                        JiangHuAttrType.MAttack, JiangHuAttrType.SkillCriticalStrike, JiangHuAttrType.PAttack,
                        JiangHuAttrType.Counteraction, JiangHuAttrType.MaxMana,
                        JiangHuAttrType.FinalMagicAttack, JiangHuAttrType.FinalAttack
                    }
                },
                {
                    4, new List<JiangHuAttrType>
                    {
                        JiangHuAttrType.MAttack, JiangHuAttrType.SkillCriticalStrike, JiangHuAttrType.Breakthrough,
                        JiangHuAttrType.Counteraction, JiangHuAttrType.CriticalStrike,
                        JiangHuAttrType.MaxMana, JiangHuAttrType.FinalMagicAttack, JiangHuAttrType.FinalAttack
                    }
                },
                {
                    5, new List<JiangHuAttrType>
                    {
                        JiangHuAttrType.SkillCriticalStrike, JiangHuAttrType.Counteraction, JiangHuAttrType.Mdefense,
                        JiangHuAttrType.PDefense, JiangHuAttrType.MaxLife,
                        JiangHuAttrType.MaxMana, JiangHuAttrType.FinalMagicDefense, JiangHuAttrType.FinalDefense
                    }
                },
                {
                    6, new List<JiangHuAttrType>
                    {
                        JiangHuAttrType.SkillCriticalStrike, JiangHuAttrType.Counteraction, JiangHuAttrType.Immunity,
                        JiangHuAttrType.Mdefense, JiangHuAttrType.MaxMana,
                        JiangHuAttrType.FinalMagicDefense, JiangHuAttrType.FinalDefense
                    }
                },
                {
                    7, new List<JiangHuAttrType>
                    {
                        JiangHuAttrType.MAttack, JiangHuAttrType.SkillCriticalStrike, JiangHuAttrType.PAttack,
                        JiangHuAttrType.Counteraction, JiangHuAttrType.MaxMana,
                        JiangHuAttrType.FinalAttack
                    }
                },
                {
                    8, new List<JiangHuAttrType>
                    {
                        JiangHuAttrType.MAttack, JiangHuAttrType.SkillCriticalStrike, JiangHuAttrType.Breakthrough,
                        JiangHuAttrType.Counteraction, JiangHuAttrType.CriticalStrike,
                        JiangHuAttrType.MaxMana, JiangHuAttrType.FinalAttack
                    }
                },
                {
                    9, new List<JiangHuAttrType>
                    {
                        JiangHuAttrType.SkillCriticalStrike, JiangHuAttrType.Counteraction, JiangHuAttrType.PDefense,
                        JiangHuAttrType.MaxLife, JiangHuAttrType.MaxMana,
                        JiangHuAttrType.FinalMagicDefense, JiangHuAttrType.FinalDefense
                    }
                }
            };

        private readonly Dictionary<JiangHuAttrType, List<int>> m_jiangHuPowers =
            new Dictionary<JiangHuAttrType, List<int>>
            {
                {JiangHuAttrType.MaxLife, new List<int> {100, 150, 210, 280, 350, 450}},
                {JiangHuAttrType.PAttack, new List<int> {50, 75, 110, 140, 180, 240}},
                {JiangHuAttrType.MAttack, new List<int> {50, 70, 110, 160, 210, 320}},
                {JiangHuAttrType.PDefense, new List<int> {10, 50, 70, 100, 130, 180}},
                {JiangHuAttrType.Mdefense, new List<int> {5, 15, 25, 50, 70, 120}},
                {JiangHuAttrType.FinalAttack, new List<int> {5, 15, 25, 35, 45, 60}},
                {JiangHuAttrType.FinalMagicAttack, new List<int> {5, 15, 25, 35, 45, 60}},
                {JiangHuAttrType.FinalDefense, new List<int> {5, 15, 25, 35, 45, 60}},
                {JiangHuAttrType.FinalMagicDefense, new List<int> {5, 15, 25, 35, 45, 60}},
                {JiangHuAttrType.CriticalStrike, new List<int> {5, 8, 10, 13, 16, 20}},
                {JiangHuAttrType.SkillCriticalStrike, new List<int> {5, 6, 8, 10, 12, 15}},
                {JiangHuAttrType.Immunity, new List<int> {5, 8, 10, 13, 16, 20}},
                {JiangHuAttrType.Breakthrough, new List<int> {5, 8, 10, 13, 16, 20}},
                {JiangHuAttrType.Counteraction, new List<int> {5, 8, 10, 13, 16, 20}},
                {JiangHuAttrType.MaxMana, new List<int> {80, 100, 120, 160, 190, 230}}
            };

        private readonly JiangHu m_pParent;

        private readonly Random m_pRand = new Random();
        private readonly Character m_pUser;

        private readonly double[] m_sequenceBonus =
        {
            1.0d,
            1.0d,
            1.1d,
            1.13d,
            1.15d,
            1.18d,
            1.21d,
            1.25d,
            1.3d,
            1.5d
        };

        private readonly double[] m_sequenceInnerStrength =
        {
            1.0d,
            1.0d,
            1.1d,
            1.2d,
            1.3d,
            1.4d,
            1.5d,
            1.6d,
            1.8d,
            2.0d,
        };

        public readonly ConcurrentDictionary<int, JiangHuStar> Stars = new ConcurrentDictionary<int, JiangHuStar>();
        private JiangHuStar m_pLastSaveStar = null;

        public JiangHuStage(byte stage, Character user, JiangHu parent)
        {
            m_pParent = parent;
            CurrentStage = stage;
            m_pUser = user;
        }

        public byte CurrentStage { get; }

        public int CurrentStar => Stars.Count;
        
        public bool HasAllStars => Stars.Count == 9;

        public uint GetInnerPower()
        {
            JiangHuAttrType lastType = JiangHuAttrType.None;
            uint dwCurrentInnerPower = 0;
            uint dwInnerPower = 0;
            int nAlign = 1;
            foreach (var star in Stars.Values)
            {
                if (lastType != JiangHuAttrType.None && lastType != star.Type)
                {
                    dwInnerPower += (uint)(dwCurrentInnerPower * m_sequenceInnerStrength[nAlign]);
                    dwCurrentInnerPower = 0;
                    nAlign = 1;
                }
                dwCurrentInnerPower += m_attributePoints[(int)(star.Quality - 1)];
                if (lastType != JiangHuAttrType.None && lastType == star.Type)
                {
                    nAlign += 1;
                }
                lastType = star.Type;
            }
            dwInnerPower += (uint)(dwCurrentInnerPower * m_sequenceInnerStrength[nAlign]);
            return dwInnerPower;
        }

        /// <summary>
        ///     Add an existing star. MUST BE USED ONLY FOR LOADING.
        /// </summary>
        public void AddStar(JiangHuStar star)
        {
            if (star.Stage != CurrentStage || Stars.ContainsKey(star.Star))
                return;
            Stars.TryAdd(star.Star, star);
        }

        public Dictionary<JiangHuAttrType, int> GetPowers()
        {
            Dictionary<JiangHuAttrType, int> result = new Dictionary<JiangHuAttrType, int>();
            int nAlign = 1;
            List<int> lPower = new List<int>();
            JiangHuAttrType lastType = JiangHuAttrType.None;
            uint dwCurrentInnerPower = 0;
            uint dwInnerPower = 0;
            foreach (var star in Stars.Values)
            {
                if (lastType != JiangHuAttrType.None && lastType != star.Type)
                {
                    int nPower = 0;
                    foreach (var power in lPower)
                    {
                        nPower += (int) (power * m_sequenceBonus[nAlign]);
                    }

                    dwInnerPower += (uint) (dwCurrentInnerPower * m_sequenceInnerStrength[nAlign]);
                    
                    if (result.ContainsKey(lastType))
                        result[lastType] += nPower;
                    else result.Add(lastType, nPower);
                    
                    lPower = new List<int>();
                    dwCurrentInnerPower = 0;
                    nAlign = 1;
                }

                lPower.Add(m_jiangHuPowers[star.Type][(int)star.Quality-1]);
                dwCurrentInnerPower += m_attributePoints[(int) (star.Quality - 1)];

                if (lastType != JiangHuAttrType.None && lastType == star.Type)
                {
                    nAlign += 1;
                }

                lastType = star.Type;
            }

            int nLastPower = 0;
            foreach (var power in lPower)
                nLastPower += (int)(power * m_sequenceBonus[nAlign]);

            dwInnerPower += (uint)(dwCurrentInnerPower * m_sequenceInnerStrength[nAlign]);

            if (result.ContainsKey(lastType))
                result[lastType] += nLastPower;
            else result.Add(lastType, nLastPower);

            return result;
        }

        /// <summary>
        ///     Generates the value for a new star or existing one.
        /// </summary>
        /// <param name="nStar">The star that must be created.</param>
        /// <param name="bFreeCourse">If the user is using a free course.</param>
        /// <param name="bSaveValue">If we should save the value for further rollback.</param>
        public void Generate(int nStar, int high, bool bFreeCourse, bool bSaveValue)
        {
            float[] fChances = {70f, 55f, 45f, 30f, 20f};

            bool bGenerateRandomQuality = true;
            byte newQuality = 1;
            JiangHuQuality quality = JiangHuQuality.Common;

            switch (high)
            {
                case 1:
                    newQuality = 2;
                    break;
                case 2:
                    newQuality = 3;
                    break;
            }

            JiangHuStar currentValue = Stars.FirstOrDefault(x => x.Key == nStar).Value;
            if (currentValue != null && currentValue.Quality >= JiangHuQuality.Ultra && Calculations.ChanceCalc(2.5f))
            {
                bGenerateRandomQuality = false;
                quality = JiangHuQuality.Epic;
            }

            if (bGenerateRandomQuality)
            {
                for (int i = 0; i < 5 - newQuality; i++)
                {
                    if (Calculations.ChanceCalc(fChances[i]))
                        newQuality++;
                    else break;
                }

                quality = (JiangHuQuality) Math.Min((byte) 5, newQuality);
            }

            byte pType = (byte) m_dicTypesPerLevel[CurrentStage][m_pRand.Next() % m_dicTypesPerLevel[CurrentStage].Count];

            if (currentValue == null)
            {
                currentValue = new JiangHuStar(new KongFuAttrEntity
                {
                    UserIdentity = m_pUser.Identity,
                    Quality = (byte) quality,
                    Star = (byte) nStar,
                    Stage = CurrentStage,
                    LearnTime = DateTime.Now,
                    Attribute = pType
                });
                Stars.TryAdd(nStar, currentValue);
            }
            else
            {
                m_pLastSaveStar = new JiangHuStar(new KongFuAttrEntity
                {
                    Attribute = (byte) currentValue.Type,
                    Quality = (byte) currentValue.Quality,
                    Stage = currentValue.Stage,
                    Star = currentValue.Star
                    
                });

                currentValue.Quality = quality;
                currentValue.Type = (JiangHuAttrType) pType;
            }
            currentValue.Save();

            m_pParent.LastStage = CurrentStage;
            m_pParent.LastStar = (byte) nStar;

            new KongFuAttrLogRepository().Save(new KongFuAttrLogEntity
            {
                UserIdentity = m_pUser.Identity,
                Quality = (byte) quality,
                Star = (byte) nStar,
                Stage = CurrentStage,
                Attribute = pType,
                Usage = DateTime.Now,
                FreeCourse = (byte) (bFreeCourse ? 1 : 0)
            });
        }

        public bool Restore(byte stage, byte star)
        {
            if (m_pLastSaveStar == null)
                return false;

            if (m_pLastSaveStar.Stage != stage || m_pLastSaveStar.Star != star)
                return false;

            if (!Stars.ContainsKey(star))
                return false;

            Stars[star].Quality = m_pLastSaveStar.Quality;
            Stars[star].Type = m_pLastSaveStar.Type;
            Stars[star].Save();

            m_pLastSaveStar = null;
            return true;
        }
    }

    public sealed class JiangHuStar
    {
        private readonly KongFuAttrEntity m_pAttr;

        public JiangHuStar(KongFuAttrEntity entity)
        {
            m_pAttr = entity;
        }

        public ushort Identity => (ushort) ((ushort) Type + (ushort) Quality * 256);

        public uint UserIdentity => m_pAttr.UserIdentity;

        public JiangHuQuality Quality
        {
            get => (JiangHuQuality) m_pAttr.Quality;
            set => m_pAttr.Quality = (byte) value;
        }

        public byte Stage => m_pAttr.Stage;
        public byte Star => m_pAttr.Star;

        public JiangHuAttrType Type
        {
            get => (JiangHuAttrType) m_pAttr.Attribute;
            set => m_pAttr.Attribute = (byte) value;
        }

        public bool Save()
        {
            return new KongFuAttrRepository().Save(m_pAttr);
        }
    }
}
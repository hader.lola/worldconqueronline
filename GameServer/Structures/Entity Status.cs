﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - Entity Status.cs
// Last Edit: 2019/11/24 19:04
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System;
using System.Collections.Concurrent;
using System.Runtime.InteropServices;
using FtwCore.Common;
using FtwCore.Common.Enums;
using FtwCore.Networking.Packets;
using GameServer.Structures.Entities;

#endregion

namespace GameServer.Structures
{
    [StructLayout(LayoutKind.Sequential, Pack = 4, CharSet = CharSet.Ansi, Size = 16)]
    public struct StatusInfoStruct
    {
        public int Status;
        public int Power;
        public int Seconds;
        public int Times;

        public StatusInfoStruct(int nStatus, int nPower, int nSecs, int nTimes)
            : this()
        {
            Status = nStatus;
            Power = nPower;
            Seconds = nSecs;
            Times = nTimes;
        }
    }

    public sealed class StatusOnce : IStatus
    {
        private Role m_pOwner;
        private TimeOutMS m_tKeep;
        private long m_dAutoFlash;
        private int m_nData;
        private int m_nStatus;
        private TimeOutMS m_tInterval;
        private uint m_dwCaster;
        private byte m_pLevel;

        public StatusOnce()
        {
            m_pOwner = null;
            m_nStatus = 0;
        }

        public StatusOnce(Role pOwner)
        {
            m_pOwner = pOwner;
            m_nStatus = 0;
        }

        public bool Create(Role pRole, int nStatus, int nPower, int nSecs, int nTimes, uint caster = 0, byte level = 0)
        {
            m_pOwner = pRole;
            m_dwCaster = caster;
            m_nStatus = nStatus;
            m_nData = nPower;
            m_tKeep = new TimeOutMS(nSecs * 1000);
            m_tKeep.Startup((int) Math.Min((long) nSecs * 1000, int.MaxValue));
            m_tKeep.Update();
            m_tInterval = new TimeOutMS(1000);
            m_tInterval.Update();
            m_pLevel = level;
            return true;
        }

        public int Identity => m_nStatus;

        public bool IsValid => m_tKeep.IsActive() && !m_tKeep.IsTimeOut();

        public int Power
        {
            get => m_nData;
            set => m_nData = value;
        }

        public byte Level => m_pLevel;

        public int Time => m_tKeep.GetInterval();

        public int RemainingTime => m_tKeep.GetRemain() / 1000;

        public bool GetInfo(ref StatusInfoStruct pInfo)
        {
            pInfo.Power = m_nData;
            pInfo.Seconds = m_tKeep.GetRemain() / 1000;
            pInfo.Status = m_nStatus;
            pInfo.Times = 0;

            return IsValid;
        }

        public bool ChangeData(int nPower, int nSecs, int nTimes = 0, uint wCaster = 0)
        {
            try
            {
                m_nData = nPower;
                m_tKeep.SetInterval(nSecs * 1000);
                m_tKeep.Update();

                if (wCaster != 0)
                    m_dwCaster = wCaster;

                if (m_pOwner is Character pUser)
                {
                    if (Identity == FlagInt.AZURE_SHIELD)
                    {
                        pUser.UpdateAzureShield(nSecs, nPower, Level);
                    }

                    pUser.UpdateAttributes();
                }

                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool IncTime(int nMilliSecs, int nLimit)
        {
            int nInterval = Math.Min(nMilliSecs + m_tKeep.GetRemain(), nLimit);
            m_tKeep.SetInterval(nInterval);
            return m_tKeep.Update();
        }

        public bool ToFlash()
        {
            if (!IsValid)
                return false;

            if (m_dAutoFlash == 0 && m_tKeep.GetRemain() <= 5000)
            {
                m_dAutoFlash = 1;
                return true;
            }

            return false;
        }

        public uint CasterId => m_dwCaster;

        public bool IsUserCast => m_dwCaster == m_pOwner.Identity || m_dwCaster == 0;

        public void OnTimer()
        {
        }
    }

    public sealed class StatusMore : IStatus
    {
        private Role m_pOwner;
        private TimeOut m_tKeep;
        private long m_dAutoFlash;
        private int m_nData;
        private int m_nStatus;
        private int m_nTimes;
        private uint m_dwCaster;
        private byte m_pLevel;

        public StatusMore()
        {
            m_pOwner = null;
            m_nStatus = 0;
        }

        public StatusMore(Role pOwner)
        {
            m_pOwner = pOwner;
            m_nStatus = 0;
        }

        ~StatusMore()
        {
            // todo destroy and detach status
        }

        public bool Create(Role pRole, int nStatus, int nPower, int nSecs, int nTimes, uint wCaster = 0, byte level = 0)
        {
            m_pOwner = pRole;
            m_nStatus = nStatus;
            m_nData = nPower;
            m_tKeep = new TimeOut(nSecs);
            m_tKeep.Update(); // no instant start
            m_nTimes = nTimes;
            m_dwCaster = wCaster;
            m_pLevel = level;
            return true;
        }

        public int Identity => m_nStatus;

        public bool IsValid => m_nTimes > 0;

        public int Power
        {
            get => m_nData;
            set => m_nData = value;
        }

        public byte Level => m_pLevel;

        public int RemainingTime => m_tKeep.GetRemain();

        public int Time => m_tKeep.GetInterval();

        public bool GetInfo(ref StatusInfoStruct pInfo)
        {
            pInfo.Power = m_nData;
            pInfo.Seconds = m_tKeep.GetRemain();
            pInfo.Status = m_nStatus;
            pInfo.Times = m_nTimes;

            return IsValid;
        }

        public bool ChangeData(int nPower, int nSecs, int nTimes = 0, uint wCaster = 0)
        {
            try
            {
                m_nData = nPower;
                m_tKeep.SetInterval(nSecs);
                m_tKeep.Update();

                m_dwCaster = wCaster;
                if (m_pOwner is Character user)
                {
                    user.UpdateAttributes();
                }

                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool IncTime(int nMilliSecs, int nLimit)
        {
            int nInterval = Math.Min(nMilliSecs + m_tKeep.GetRemain(), nLimit);
            m_tKeep.SetInterval(nInterval);
            return m_tKeep.Update();
        }

        public bool ToFlash()
        {
            if (!IsValid)
                return false;

            if (m_dAutoFlash == 0 && m_tKeep.GetRemain() <= 5000)
            {
                m_dAutoFlash = 1;
                return true;
            }

            return false;
        }

        public uint CasterId => m_dwCaster;

        public bool IsUserCast => m_dwCaster == m_pOwner.Identity || m_dwCaster == 0;

        public void OnTimer()
        {
            try
            {
                if (!IsValid || !m_tKeep.ToNextTime())
                    return;

                if (m_pOwner != null)
                {
                    int nLoseLife;

                    switch (m_nStatus)
                    {
                        case FlagInt.POISONED: // poison
                            if (!m_pOwner.IsAlive)
                                return;

                            nLoseLife = (int) Calculations.CutOverflow(m_nData, m_pOwner.Life - 1);
                            m_pOwner.AddAttrib(ClientUpdateType.Hitpoints, -1 * nLoseLife);

                            var msg2 = new MsgMagicEffect
                            {
                                Identity = m_pOwner.Identity,
                                SkillIdentity = 10010
                            };
                            msg2.AppendTarget(m_pOwner.Identity, (uint) nLoseLife, true, 0, 0);
                            m_pOwner.Map.SendToRegion(msg2, m_pOwner.MapX, m_pOwner.MapY);

                            if (!m_pOwner.IsAlive)
                                m_pOwner.BeKill(null);
                            break;
                        case FlagInt.VORTEX: // shuriken vortex
                            if (!m_pOwner.IsAlive)
                                return;

                            if (m_pOwner is Character owner)
                                owner.ProcessMagicAttack(6010, 0, owner.MapX, owner.MapY);
                            break;
                        case FlagInt.TOXIC_FOG: // toxic fog
                            if (!m_pOwner.IsAlive || m_pOwner.Life <= 1)
                                return;

                            var power = m_nData > 30000 ? (m_nData - 30000) / 100f : m_nData;
                            nLoseLife = (int) Calculations.CutOverflow((int) (m_pOwner.Life * power),
                                m_pOwner.Life - 1);

                            if (m_pOwner.Detoxication > 0)
                            {
                                int detox = m_pOwner.Detoxication;
                                if (m_pOwner.Detoxication > 100)
                                    detox = 100;
                                nLoseLife = Calculations.MulDiv(nLoseLife, Math.Min(100 - detox, 100), 100);
                            }

                            m_pOwner.BeAttack(1, m_pOwner, nLoseLife, false);

                            var msg = new MsgMagicEffect
                            {
                                Identity = m_pOwner.Identity,
                                SkillIdentity = 10010
                            };
                            msg.AppendTarget(m_pOwner.Identity, (uint) nLoseLife, true, 0, 0);
                            m_pOwner.Map.SendToRegion(msg, m_pOwner.MapX, m_pOwner.MapY);
                            break;
                    }

                    m_nTimes--;
                }
            }
            catch (Exception ex)
            {
                Program.WriteLog("StatusOnce::OnTimer() error!", LogType.ERROR);
                Program.WriteLog(ex.ToString(), LogType.EXCEPTION);
            }
        }
    }

    public sealed class StatusSet
    {
        private Role m_pOwner;
        private TimeOut m_pDeadMark = new TimeOut(8);
        public ConcurrentDictionary<int, IStatus> Status;

        private ulong Status0
        {
            get => m_pOwner.Flag1;
            set => m_pOwner.Flag1 = value;
        }

        private ulong Status1
        {
            get => m_pOwner.Flag2;
            set => m_pOwner.Flag2 = value;
        }

        private ulong Status2
        {
            get => m_pOwner.Flag3;
            set => m_pOwner.Flag3 = (uint) value;
        }

        public StatusSet(Role pRole)
        {
            if (pRole == null)
                return;
            m_pOwner = pRole;
            Status = new ConcurrentDictionary<int, IStatus>(5, 128);
        }

        public int GetAmount()
        {
            return Status.Count;
        }

        public IStatus GetObjByIndex(int nKey)
        {
            IStatus ret;
            return Status.TryGetValue(nKey, out ret) ? ret : null;
        }

        public IStatus GetObj(ulong nKey, bool b64 = false)
        {
            IStatus ret;
            return Status.TryGetValue(InvertFlag(nKey, b64), out ret) ? ret : null;
        }

        public bool AddObj(IStatus pStatus)
        {
            var pInfo = new StatusInfoStruct();
            pStatus.GetInfo(ref pInfo);
            if (Status.ContainsKey(pInfo.Status))
                return false; // status already exists

            if (pInfo.Status < 65)
            {
                ulong flag = 1UL << (pInfo.Status - 1);
                Status.TryAdd(pInfo.Status, pStatus);
                Status0 |= flag;
            }
            else if (pInfo.Status < 129)
            {
                ulong flag = 1UL << (pInfo.Status - 1);
                Status.TryAdd(pInfo.Status, pStatus);
                Status1 |= flag;
            }
            else if (pInfo.Status < 193)
            {
                ulong flag = 1UL << (pInfo.Status - 1);
                Status.TryAdd(pInfo.Status, pStatus);
                Status2 |= flag;
            }

            switch (pStatus.Identity)
            {
                case FlagInt.TYRANT_AURA:
                case FlagInt.FEND_AURA:
                case FlagInt.WATER_AURA:
                case FlagInt.FIRE_AURA:
                case FlagInt.METAL_AURA:
                case FlagInt.WOOD_AURA:
                case FlagInt.EARTH_AURA:
                {
                    break;
                }
            }

            uint dwAura = 0;
            switch (pInfo.Status)
            {
                case FlagInt.TYRANT_AURA:
                    dwAura = (uint) AuraType.TYRANT_AURA;
                    break;
                case FlagInt.FEND_AURA:
                    dwAura = (uint) AuraType.FEND_AURA;
                    break;
                case FlagInt.WATER_AURA:
                    dwAura = (uint) AuraType.WATER_AURA;
                    break;
                case FlagInt.FIRE_AURA:
                    dwAura = (uint) AuraType.FIRE_AURA;
                    break;
                case FlagInt.METAL_AURA:
                    dwAura = (uint) AuraType.METAL_AURA;
                    break;
                case FlagInt.WOOD_AURA:
                    dwAura = (uint) AuraType.WOOD_AURA;
                    break;
                case FlagInt.EARTH_AURA:
                    dwAura = (uint) AuraType.EARTH_AURA;
                    break;
                case FlagInt.SUPER_SHIELD_HALO:
                    dwAura = (uint) AuraType.MAGIC_DEFENDER;
                    break;
            }

            if (dwAura > 0)
            {
                var pMsg = new MsgAura
                {
                    Action = IconAction.ADD,
                    EntityIdentity = m_pOwner.Identity,
                    Level = pStatus.Level,
                    Power1 = (uint) pInfo.Power,
                    Type = dwAura
                };
                m_pOwner.Send(pMsg);
            }

            if (pStatus.Identity == FlagInt.SHACKLED && m_pOwner is Character character)
            {
                character.UpdateSoulShackle(pInfo.Seconds);
            }
            else if (pStatus.Identity == FlagInt.AZURE_SHIELD && m_pOwner is Character role)
            {
                role.UpdateAzureShield(pInfo.Seconds, pInfo.Power, pStatus.Level);
            }

            var pUser = m_pOwner as Character;
            pUser?.UpdateAttributes();

            return true;
        }

        public bool DelObj(int nFlag)
        {
            if (nFlag > 192)
                return false;

            IStatus trash;
            if (!Status.TryRemove(nFlag, out trash))
                return false;

            ulong uFlag = 1UL << (nFlag - 1);
            if (nFlag < 65)
                Status0 &= ~uFlag;
            else if (nFlag < 129)
                Status1 &= ~uFlag;
            else
                Status2 &= ~uFlag;

            switch (nFlag)
            {
                case FlagInt.AZURE_SHIELD:
                {
                    var pStts = new StatusOnce(m_pOwner);
                    pStts.Create(m_pOwner, FlagInt.AZURE_SHIELD_FADE, 0, 3, 0, m_pOwner.Identity);
                    AddObj(pStts);
                    break;
                }

                case FlagInt.TYRANT_AURA:
                case FlagInt.FEND_AURA:
                case FlagInt.WATER_AURA:
                case FlagInt.FIRE_AURA:
                case FlagInt.METAL_AURA:
                case FlagInt.WOOD_AURA:
                case FlagInt.EARTH_AURA:
                {
                    break;
                }

                case FlagInt.SHACKLED:
                {
                    (m_pOwner as Character)?.UpdateSoulShackle(0);
                    break;
                }
            }

            var pUser = m_pOwner as Character;
            pUser?.UpdateAttributes();
            return true;
        }

        public bool DelObj(Effect0 pFlag)
        {
            return DelObj(InvertFlag((ulong) pFlag));
        }

        public bool DelObj(Effect1 pFlag)
        {
            return DelObj(InvertFlag((ulong) pFlag, true));
        }

        public IStatus this[int nKey]
        {
            get
            {
                try
                {
                    return Status.TryGetValue(nKey, out var ret) ? ret : null;
                }
                catch
                {
                    return null;
                }
            }
        }

        public void SetDeadMark(int nSeconds)
        {
            if (!HasDeadMark())
            {
                SendDeadMark();
            }

            m_pDeadMark.Startup(nSeconds);
        }

        public bool HasDeadMark()
        {
            return m_pDeadMark.IsActive() && !m_pDeadMark.IsTimeOut();
        }

        public void CheckDeadMark()
        {
            if (m_pDeadMark.TimeOver())
                RemoveDeadMark();
        }

        public void RemoveDeadMark()
        {
            m_pDeadMark.Clear();
            SendDeadMark(true);
        }

        public void SendDeadMark(bool remove = false)
        {
            m_pOwner.Map.SendToRegion(new MsgDeadMark(remove, m_pOwner.Identity), m_pOwner.MapX, m_pOwner.MapY);
        }

        /// <summary>
        /// Gotta check if there is a faster way to do this.
        /// </summary>
        /// <param name="flag">The flag that will be checked.</param>
        /// <param name="b64">If it's a effect 2 flag, you should set this true.</param>
        /// <returns></returns>
        public static int InvertFlag(ulong flag, bool b64 = false, bool b128 = false)
        {
            var inv = flag >> 0;
            int ret = -1;
            for (int i = 0; inv > 1; i++)
            {
                inv = flag >> i;
                ret++;
            }

            return !b64 ? ret : !b128 ? ret + 64 : ret + 128;
        }

        public void SendAllStatus()
        {
            Character pUsr = m_pOwner as Character;
            if (pUsr == null || !pUsr.LoginComplete) return;
            pUsr.UpdateClient(ClientUpdateType.StatusFlag, Status0, Status1, Status2, true);
        }
    }

    public interface IStatus
    {
        /// <summary>
        /// This method will get the status id.
        /// </summary>
        int Identity { get; }

        /// <summary>
        /// This method will check if the status still valid and running.
        /// </summary>
        bool IsValid { get; }

        /// <summary>
        /// This method will return the power of the status. This wont make percentage checks. The value is a short.
        /// </summary>
        int Power { get; set; }

        int Time { get; }

        int RemainingTime { get; }

        byte Level { get; }

        /// <summary>
        /// This method will get the status information into another param.
        /// </summary>
        /// <param name="pInfo">The structure that will be filled with the information.</param>
        bool GetInfo(ref StatusInfoStruct pInfo);

        /// <summary>
        /// This method will override the old values from the status.
        /// </summary>
        /// <param name="nPower">The new power of the status.</param>
        /// <param name="nSecs">The remaining time to the status.</param>
        /// <param name="nTimes">How many times the status will appear. If StatusMore.</param>
        /// <param name="wCaster">The identity of the caster.</param>
        bool ChangeData(int nPower, int nSecs, int nTimes = 0, uint wCaster = 0);

        bool IncTime(int nMilliSecs, int nLimit);
        bool ToFlash();
        uint CasterId { get; }
        bool IsUserCast { get; }
        void OnTimer();
    }
}
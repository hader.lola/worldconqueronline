﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - Syndicate Recruitment.cs
// Last Edit: 2019/11/24 19:04
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using FtwCore;
using FtwCore.Common;
using FtwCore.Common.Enums;
using FtwCore.Database.Entities;
using FtwCore.Database.Repositories;
using FtwCore.Networking.Packets;
using GameServer.Structures.Entities;

#endregion

namespace GameServer.Structures.Groups.Syndicates
{
    public sealed class SyndicateRecruitment
    {
        private const int _MAX_PER_PAGE_I = 4;
        private const int _MIN_MONEY_AMOUNT = 500000;
        private SyndicateAdvertisingRepository m_pRepo;

        private ConcurrentDictionary<uint, SyndicateAdvertisingEntity> m_pSyndicateAdvertisings =
            new ConcurrentDictionary<uint, SyndicateAdvertisingEntity>();

        public bool Create()
        {
            m_pRepo = new SyndicateAdvertisingRepository();
            var allSyn = m_pRepo.FetchAll();
            if (allSyn != null)
            {
                foreach (var syn in allSyn)
                {
                    m_pSyndicateAdvertisings.TryAdd(syn.SyndicateIdentity, syn);
                }
            }

            return true;
        }

        public void AddSyndicate(Character pSender, MsgSynRecuitAdvertising pMsg)
        {
            if (m_pRepo == null && !Create())
                return;

            if (pSender?.Syndicate == null)
                return;

            string szMessage = pMsg.Description;
            if (pSender.SyndicatePosition != SyndicateRank.GuildLeader)
            {
                return;
            }

            if (szMessage.Length > 255)
                szMessage = szMessage.Substring(0, 255);

            if (m_pSyndicateAdvertisings.Values.FirstOrDefault(x => x.SyndicateIdentity == pSender.SyndicateIdentity) !=
                null)
            {
                EditSyndicate(pSender, pMsg);
                return;
            }

            if (pMsg.LevelRequirement <= 0)
                pMsg.LevelRequirement = 1;

            if (pSender.Syndicate.SilverDonation < pMsg.Amount)
            {
                pSender.SendSysMessage(Language.StrSynRecruitNotEnoughMoney, Color.Red);
                return;
            }

            if (pMsg.Amount < _MIN_MONEY_AMOUNT)
            {
                pSender.SendSysMessage(Language.StrSynRecruitNotEnoughDonation, Color.Red);
                return;
            }

            pSender.Syndicate.ChangeFunds((int) pMsg.Amount * -1);

            SyndicateAdvertisingEntity dbSyn = new SyndicateAdvertisingEntity
            {
                AutoRecruit = (byte) (pMsg.IsAutoRecruit ? 1 : 0),
                Donation = (uint) pMsg.Amount,
                Message = szMessage,
                RequiredLevel = pMsg.LevelRequirement,
                RequiredMetempsychosis = pMsg.RebornRequirement,
                RequiredProfession = pMsg.ProfessionForbid,
                SyndicateIdentity = pSender.SyndicateIdentity,
                SyndicateName = pSender.SyndicateName,
                Timestamp = (uint) UnixTimestamp.Now()
            };
            if (m_pRepo.Save(dbSyn))
            {
                m_pSyndicateAdvertisings.TryAdd(dbSyn.SyndicateIdentity, dbSyn);
            }
            else
            {
                pSender.SendSysMessage("Oops! Something went wrong.", Color.Red);
            }
        }

        public void EditSyndicate(Character pSender, MsgSynRecuitAdvertising pMsg)
        {
            if (pSender == null || pSender.Syndicate == null)
                return;

            string szMessage = pMsg.Description;
            if (pSender.SyndicatePosition != SyndicateRank.GuildLeader)
            {
                return;
            }

            if (szMessage.Length > 255)
                szMessage = szMessage.Substring(0, 255);

            SyndicateAdvertisingEntity dbSyn;
            if (!m_pSyndicateAdvertisings.TryGetValue(pSender.SyndicateIdentity, out dbSyn))
            {
                return;
            }

            if (pMsg.LevelRequirement <= 0)
                pMsg.LevelRequirement = 1;

            if (pSender.Syndicate.SilverDonation < pMsg.Amount)
            {
                pSender.SendSysMessage(Language.StrSynRecruitNotEnoughMoney, Color.Red);
                return;
            }

            if (pMsg.Amount < _MIN_MONEY_AMOUNT)
            {
                pSender.SendSysMessage(Language.StrSynRecruitNotEnoughDonation, Color.Red);
                return;
            }

            pSender.Syndicate.ChangeFunds((int) pMsg.Amount * -1);

            {
                dbSyn.AutoRecruit = (byte) (pMsg.IsAutoRecruit ? 1 : 0);
                dbSyn.Donation = (uint) pMsg.Amount;
                dbSyn.Message = szMessage;
                dbSyn.RequiredLevel = pMsg.LevelRequirement;
                dbSyn.RequiredMetempsychosis = pMsg.RebornRequirement;
                dbSyn.RequiredProfession = pMsg.ProfessionForbid;
                dbSyn.SyndicateIdentity = pSender.SyndicateIdentity;
                dbSyn.SyndicateName = pSender.SyndicateName;
                dbSyn.Timestamp = (uint) UnixTimestamp.Now();
            }
            if (m_pRepo.Save(dbSyn))
            {
                // m_pSyndicateAdvertisings.TryAdd(dbSyn.SyndicateIdentity, dbSyn);
            }
            else
            {
                pSender.SendSysMessage("Oops! Something went wrong.", Color.Red);
            }
        }

        public void SendEditScreen(Character pSender)
        {
            if (pSender == null || pSender.Syndicate == null || pSender.SyndicatePosition != SyndicateRank.GuildLeader)
                return;

            SyndicateAdvertisingEntity dbSyn;
            if (!m_pSyndicateAdvertisings.TryGetValue(pSender.SyndicateIdentity, out dbSyn))
            {
                return;
            }

            MsgSynRecuitAdvertising pMsg = new MsgSynRecuitAdvertising
            {
                IsAutoRecruit = dbSyn.AutoRecruit > 0,
                Amount = dbSyn.Donation,
                Description = dbSyn.Message,
                GenderForbid = 0,
                Identity = dbSyn.SyndicateIdentity,
                LevelRequirement = (ushort) dbSyn.RequiredLevel,
                ProfessionForbid = (ushort) dbSyn.RequiredProfession,
                RebornRequirement = (ushort) dbSyn.RequiredMetempsychosis
            };
            pSender.Send(pMsg);
        }

        public void JoinSyndicate(Character pUser, uint idSyn)
        {
            Syndicate pSyn = ServerKernel.SyndicateManager.GetSyndicate(idSyn);
            if (pSyn == null)
            {
                pUser.SendSysMessage(Language.StrSynRecruitUnexistentSyn, Color.Red);
                return;
            }

            SyndicateAdvertisingEntity dbSyn;
            if (!m_pSyndicateAdvertisings.TryGetValue(idSyn, out dbSyn))
            {
                return;
            }

            if (!IsValid(dbSyn))
                return;

            if (pSyn.IsDeleted)
            {
                pUser.SendSysMessage(Language.StrSynRecruitUnexistentSyn, Color.Red);
                return;
            }

            if (!pSyn.AppendMember(pUser))
            {
                pUser.SendSysMessage(Language.StrSynRecruitNotMeetRequirements, Color.Red);
            }
        }

        private bool IsValid(SyndicateAdvertisingEntity pSyn)
        {
            return pSyn.Timestamp + 60 * 60 * 24 * 7 > UnixTimestamp.Now();
        }

        public void CheckSyndicates()
        {
            List<SyndicateAdvertisingEntity> pRemove = new List<SyndicateAdvertisingEntity>();
            foreach (var syn in m_pSyndicateAdvertisings.Values)
            {
                if (!IsValid(syn))
                {
                    pRemove.Add(syn);
                }
            }

            foreach (var syn in pRemove)
            {
                m_pSyndicateAdvertisings.TryRemove(syn.SyndicateIdentity, out _);
                m_pRepo.Delete(syn);
            }
        }

        public bool IsAdvertising(uint idSyn)
        {
            return m_pSyndicateAdvertisings.ContainsKey(idSyn);
        }

        public void SendSyndicates(uint nPage, MsgSynRecruitAdvertisingList pMsg, Character pUser)
        {
            uint beginAt = pMsg.StartIndex;
            pMsg.FirstMatch = 1;
            pMsg.MaxCount = (uint) m_pSyndicateAdvertisings.Count;

            int nAmount = 0;
            foreach (var syn in m_pSyndicateAdvertisings.Values.OrderByDescending(x => x.Donation)
                .ThenBy(x => x.Identity))
            {
                if (nAmount < beginAt)
                {
                    nAmount++;
                    continue;
                }

                if (nAmount >= beginAt + _MAX_PER_PAGE_I)
                    break;

                Syndicate pSyn = ServerKernel.SyndicateManager.GetSyndicate(syn.SyndicateIdentity);
                if (pSyn == null || pSyn.IsDeleted)
                    continue;
                if (pMsg.Count >= 2)
                {
                    pUser.Send(pMsg);
                    pMsg = new MsgSynRecruitAdvertisingList
                    {
                        StartIndex = nPage,
                        MaxCount = (uint) m_pSyndicateAdvertisings.Count
                    };
                }

                bool canJoin = pUser.SyndicateIdentity != pSyn.Identity;

                if (pUser.Level < syn.RequiredLevel
                    || pUser.Metempsychosis < syn.RequiredMetempsychosis)
                    canJoin = false;

                // todo make the corrections to check from the announce
                switch (pUser.ProfessionSort)
                {
                    case 1:
                        if (!pSyn.TrojanEnabled)
                            canJoin = false;
                        break;
                    case 2:
                        if (!pSyn.WarriorEnabled)
                            canJoin = false;
                        break;
                    case 4:
                        if (!pSyn.ArcherEnabled)
                            canJoin = false;
                        break;
                    case 5:
                        if (!pSyn.NinjaEnabled)
                            canJoin = false;
                        break;
                    case 6:
                        if (!pSyn.MonkEnabled)
                            canJoin = false;
                        break;
                    case 7:
                        if (!pSyn.PirateEnabled)
                            canJoin = false;
                        break;
                    case 10:
                    case 13:
                    case 14:
                        if (!pSyn.TaoistEnabled)
                            canJoin = false;
                        break;
                }

                pMsg.Append(pSyn.Identity, syn.Message, pSyn.Name, pSyn.LeaderName, pSyn.Level,
                    pSyn.MemberCount, pSyn.SilverDonation, canJoin);
                nAmount++;
            }

            pUser.Send(pMsg);
        }
    }
}
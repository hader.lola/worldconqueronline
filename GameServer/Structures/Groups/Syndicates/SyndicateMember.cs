﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - SyndicateMember.cs
// Last Edit: 2019/11/24 19:04
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System;
using System.Drawing;
using FtwCore.Common;
using FtwCore.Common.Enums;
using FtwCore.Database.Entities;
using FtwCore.Database.Repositories;
using FtwCore.Networking.Packets;
using GameServer.Networking.GameServer;
using GameServer.Structures.Entities;

#endregion

namespace GameServer.Structures.Groups.Syndicates
{
    public sealed class SyndicateMember
    {
        private SyndicateMemberEntity m_dbEntity;
        private Syndicate m_pSyndicate;
        public Syndicate Syndicate => m_pSyndicate;

        public SyndicateMember(Syndicate pSyn)
        {
            m_pSyndicate = pSyn;
        }

        public bool Create(Character pUser)
        {
            if (m_pSyndicate == null)
                return false;

            m_dbEntity = new SyndicateMemberEntity
            {
                JoinDate = DateTime.Now,
                Id = pUser.Identity,
                SynId = m_pSyndicate.Identity,
                Rank = (ushort)(SyndicateRank.Member)
            };
            Name = pUser.Name;
            Mate = pUser.Mate;
            Lookface = pUser.Lookface;
            Level = pUser.Level;
            Profession = (ushort) pUser.Profession;
            return Save();
        }

        public bool Create(SyndicateMemberEntity dbSynAttr, CharacterEntity pUser)
        {
            if (m_pSyndicate == null)
                return false;

            m_dbEntity = dbSynAttr;
            Name = pUser.Name;
            Mate = pUser.Mate;
            Lookface = pUser.Lookface;
            Level = pUser.Level;
            Profession = pUser.Profession;
            return true;
        }

        public uint Identity => m_dbEntity.Id;

        public ushort SyndicateIdentity => (ushort) m_dbEntity.SynId;

        public uint Lookface { get; set; }
        public string Name { get; set; }
        public string Mate { get; set; }
        public byte Level { get; set; }
        public ushort Profession { get; set; }

        public uint Nobility
        {
            get
            {
                if (!IsOnline)
                {
                    return (uint) Handlers.GetRanking(Identity);
                }
                return (uint) User.NobilityRank;
            }
        }

        public SyndicateRank Position
        {
            get => (SyndicateRank) m_dbEntity.Rank;
            set
            {
                if (!Enum.IsDefined(typeof(SyndicateRank), value))
                    return;
                if (IsOnline)
                    User.SyndicatePosition = value;
                m_dbEntity.Rank = (ushort) value;
                Save();
            }
        }

        public uint SilverDonation => (uint) (m_dbEntity.Proffer / 10000);

        public uint TotalSilverDonation => (uint) m_dbEntity.ProfferTotal / 10000;

        public uint EmoneyDonation => m_dbEntity.Emoney * 20;

        public uint TotalEmoneyDonation => m_dbEntity.EmoneyTotal * 20;

        public int PkDonation => m_dbEntity.Pk;

        public uint TotalPkDonation => m_dbEntity.PkTotal;

        public uint GuideDonation => m_dbEntity.Guide;

        public uint GuideTotal => m_dbEntity.GuideTotal;

        public uint ArsenalDonation
        {
            get => m_dbEntity.Arsenal;
            set
            {
                m_dbEntity.Arsenal = value;
                Save();
            }
        }

        public uint TotalDonation => (uint) (SilverDonation + EmoneyDonation + PkDonation + GuideDonation + ArsenalDonation);

        public uint RedRoseDonation { get; set; }
        public uint WhiteRoseDonation { get; set; }
        public uint OrchidDonation { get; set; }
        public uint TulipDonation { get; set; }

        public uint Exploit => m_dbEntity.Exploit;

        public uint PositionExpire
        {
            get => m_dbEntity.Expiration;
            set => m_dbEntity.Expiration = value;
        }

        public bool PositionExpired => DateTime.Now > UnixTimestamp.ToDateTime(PositionExpire);

        public DateTime JoinDate => m_dbEntity.JoinDate;

        public int DaysOnSyndicate => (int) (DateTime.Now - JoinDate).TotalDays;

        public bool IsOnline => ServerKernel.UserManager.GetUser(Identity) != null;

        public Character User => ServerKernel.UserManager.GetUser(Identity);

        public bool Save()
        {
            return m_dbEntity != null && new SyndicateMemberRepository().Save(m_dbEntity);
        }

        public bool Delete()
        {
            return m_dbEntity != null && new SyndicateMemberRepository().Delete(m_dbEntity);
        }

        #region Donation

        public bool IncreaseMoney(uint dwAmount)
        {
            if (m_dbEntity.Proffer >= int.MaxValue)
                return true;

            if (m_dbEntity.Proffer + dwAmount >= int.MaxValue)
                m_dbEntity.Proffer = int.MaxValue;
            else if (m_dbEntity.Proffer + dwAmount < int.MaxValue)
                m_dbEntity.Proffer += dwAmount;

            if (m_dbEntity.ProfferTotal < int.MaxValue)
            {
                if (m_dbEntity.ProfferTotal + dwAmount >= int.MaxValue)
                    m_dbEntity.ProfferTotal = int.MaxValue;
                else if (m_dbEntity.ProfferTotal + dwAmount < int.MaxValue)
                    m_dbEntity.ProfferTotal += dwAmount;
            }
            return Save();
        }

        /// <summary>
        /// This method will decrease the user donation. It doesn't check if the user has enough Silver Donation.
        /// </summary>
        public bool SpendMoney(int dwAmount)
        {
            if (dwAmount > 0)
            {
                if (m_dbEntity.Proffer + dwAmount >= int.MaxValue)
                    m_dbEntity.Proffer = int.MaxValue;
                else
                    m_dbEntity.Proffer += dwAmount;
            }
            else
            {
                if (m_dbEntity.Proffer + dwAmount <= int.MinValue)
                    m_dbEntity.Proffer = int.MinValue;
                else
                    m_dbEntity.Proffer += dwAmount;
            }
            return Save();
        }

        public bool IncreaseEmoney(uint dwAmount)
        {
            if (m_dbEntity.Emoney >= uint.MaxValue)
                return true;

            if (m_dbEntity.Emoney + dwAmount >= uint.MaxValue)
                m_dbEntity.Emoney = uint.MaxValue;
            else if (m_dbEntity.Emoney + dwAmount < uint.MaxValue)
                m_dbEntity.Emoney += dwAmount;

            if (m_dbEntity.EmoneyTotal < uint.MaxValue)
            {
                if (m_dbEntity.EmoneyTotal + dwAmount >= uint.MaxValue)
                    m_dbEntity.EmoneyTotal = uint.MaxValue;
                else if (m_dbEntity.EmoneyTotal + dwAmount < uint.MaxValue)
                    m_dbEntity.EmoneyTotal += dwAmount;
            }
            return Save();
        }

        /// <summary>
        /// This method does check if the user has enough cps to expend.
        /// </summary>
        public bool DecreaseEmoney(uint dwAmount)
        {
            if (dwAmount > m_dbEntity.Emoney)
                return false;

            if (m_dbEntity.Emoney + (dwAmount * -1) < 0)
                m_dbEntity.Emoney = 0;
            else
                m_dbEntity.Emoney -= dwAmount;

            return Save();
        }

        public bool IncreasePkDonation(uint dwAmount)
        {
            if (m_dbEntity.Pk >= int.MaxValue)
                return true;

            if (m_dbEntity.Pk + dwAmount >= int.MaxValue)
                m_dbEntity.Pk = int.MaxValue;
            else if (m_dbEntity.Pk + dwAmount < int.MaxValue)
                m_dbEntity.Pk += (int)dwAmount;

            if (m_dbEntity.PkTotal < uint.MaxValue)
            {
                if (m_dbEntity.PkTotal + dwAmount >= uint.MaxValue)
                    m_dbEntity.PkTotal = uint.MaxValue;
                else if (m_dbEntity.PkTotal + dwAmount < uint.MaxValue)
                    m_dbEntity.PkTotal += dwAmount;
            }
            return Save();
        }

        public bool DecreasePkDonation(int dwAmount)
        {
            if (dwAmount > 0)
            {
                if (m_dbEntity.Pk + dwAmount >= int.MaxValue)
                    m_dbEntity.Pk = int.MaxValue;
                else
                    m_dbEntity.Pk += dwAmount;
            }
            else
            {
                if (m_dbEntity.Pk + dwAmount <= int.MinValue)
                    m_dbEntity.Pk = int.MinValue;
                else
                    m_dbEntity.Pk += dwAmount;
            }
            return Save();
        }

        public bool IncreaseGuideDonation(uint dwAmount)
        {
            if (m_dbEntity.Guide >= int.MaxValue)
                return true;

            if (m_dbEntity.Guide + dwAmount >= int.MaxValue)
                m_dbEntity.Guide = int.MaxValue;
            else if (m_dbEntity.Guide + dwAmount < int.MaxValue)
                m_dbEntity.Guide += dwAmount;

            if (m_dbEntity.GuideTotal < uint.MaxValue)
            {
                if (m_dbEntity.GuideTotal + dwAmount >= uint.MaxValue)
                    m_dbEntity.GuideTotal = uint.MaxValue;
                else if (m_dbEntity.GuideTotal + dwAmount < uint.MaxValue)
                    m_dbEntity.GuideTotal += dwAmount;
            }
            return Save();
        }

        public bool DecreaseGuideDonation(int dwAmount)
        {
            if (dwAmount > 0)
            {
                if (m_dbEntity.Guide + dwAmount >= int.MaxValue)
                    m_dbEntity.Guide = int.MaxValue;
                else
                    m_dbEntity.Guide += (uint)dwAmount;
            }
            else
            {
                if (m_dbEntity.Guide + dwAmount <= int.MinValue)
                    m_dbEntity.Guide = uint.MinValue;
                else
                    m_dbEntity.Guide -= (uint)(dwAmount * -1);
            }
            return Save();
        }

        #endregion

        #region Socket

        public void Send(byte[] pBuffer)
        {
            User?.Send(pBuffer);
        }

        public void SendSyndicate()
        {
            Character pTarget = ServerKernel.UserManager.GetUser(Identity);

            if (m_pSyndicate == null)
            {
                m_pSyndicate = ServerKernel.SyndicateManager.GetSyndicate(m_dbEntity.SynId);
            }

            if (pTarget != null && m_pSyndicate != null)
            {
                try
                {
                    pTarget.Send(new MsgTalk(m_pSyndicate.Announcement, ChatTone.GuildAnnouncement, Color.White)
                    {
                        Identity = m_pSyndicate.Identity
                    });

                    MsgSyndicateAttributeInfo pInfo = new MsgSyndicateAttributeInfo
                    {
                        EMoneyFund = m_pSyndicate.EmoneyDonation,
                        EnrollmentDate = uint.Parse(JoinDate.ToString("yyyyMMdd")),
                        LeaderName = m_pSyndicate.LeaderName,
                        MemberAmount = m_pSyndicate.MemberCount,
                        MoneyFund = m_pSyndicate.SilverDonation,
                        Position = Position,
                        RequiredLevel = m_pSyndicate.LevelRequirement,
                        RequiredMetempsychosis = m_pSyndicate.MetempsychosisRequirement,
                        RequiredProfession = m_pSyndicate.ProfessionRequirement,
                        SyndicateIdentity = m_pSyndicate.Identity,
                        SyndicateLevel = m_pSyndicate.Level
                    };
                    uint expire = 0;
                    uint time = (uint) UnixTimestamp.Now();
                    if (time < PositionExpire)
                        expire = uint.Parse(UnixTimestamp.ToDateTime(PositionExpire).ToString("yyyyMMdd"));
                    pInfo.PositionExpire = expire;
                    Send(pInfo);

                    pTarget.UpdateClient(ClientUpdateType.GuildBattlepower,
                        Syndicate.Arsenal.SharedBattlePower(pTarget));

                    m_pSyndicate.SendName(pTarget, true);
                }
                catch (Exception ex)
                {
                    Program.WriteLog(ex.ToString());
                }
            }
        }

        public void SendPromotionList()
        {
            if (m_pSyndicate == null || !IsOnline)
                return;

            byte synLevel = m_pSyndicate.Level;

            var msg = new MsgSyndicate();
            msg.Action = SyndicateRequest.SYN_PROMOTE;

            if (Position == SyndicateRank.GuildLeader)
            {
                int maxDl = synLevel < 4 ? 2 : synLevel < 7 ? 3 : 4;
                int maxHdl = synLevel < 6 ? 1 : 2;
                int maxHMan = synLevel < 5 ? 1 : synLevel < 7 ? 2 : synLevel < 9 ? 4 : 6;
                int maxHSuper = synLevel < 5 ? 1 : synLevel < 7 ? 2 : synLevel < 9 ? 4 : 6;
                int maxHStew = synLevel < 3 ? 1 : synLevel < 5 ? 2 : synLevel < 7 ? 4 : synLevel < 9 ? 6 : 8;
                int maxAide = synLevel < 4 ? 2 : synLevel < 6 ? 4 : 6;

                msg.Positions.Add((ushort) SyndicateRank.GuildLeader + " 1 1 " + m_pSyndicate.Arsenal.SharedBattlePower(SyndicateRank.GuildLeader, User) + " " + 0);
                msg.Positions.Add((ushort) SyndicateRank.DeputyLeader + " " + m_pSyndicate.DeputyLeaderCount + " " + maxDl + " " + m_pSyndicate.Arsenal.SharedBattlePower(SyndicateRank.DeputyLeader, User) + " " + 0);
                msg.Positions.Add((ushort) SyndicateRank.HonoraryDeputyLeader + " " + m_pSyndicate.HonoraryDeputyLeaderCount + " " + maxHdl + " " + m_pSyndicate.Arsenal.SharedBattlePower(SyndicateRank.HonoraryDeputyLeader, User) + " " + Syndicate.HONORARY_DEPUTY_LEADER_PRICE * -1);
                msg.Positions.Add((ushort) SyndicateRank.HonoraryManager + " " + m_pSyndicate.HonoraryManagerCount + " " + maxHMan + " " + m_pSyndicate.Arsenal.SharedBattlePower(SyndicateRank.HonoraryManager, User) + " " + Syndicate.HONORARY_MANAGER_PRICE * -1);
                msg.Positions.Add((ushort) SyndicateRank.HonorarySupervisor + " " + m_pSyndicate.HonorarySupervisorCount + " " + maxHSuper + " " + m_pSyndicate.Arsenal.SharedBattlePower(SyndicateRank.HonorarySupervisor, User) + " " +Syndicate.HONORARY_SUPERVISOR_PRICE * -1);
                msg.Positions.Add((ushort) SyndicateRank.HonorarySteward + " " + m_pSyndicate.HonoraryStewardCount + " " + maxHStew + " " + m_pSyndicate.Arsenal.SharedBattlePower(SyndicateRank.HonorarySteward, User) +" " + Syndicate.HONORARY_STEWARD_PRICE * -1);
                msg.Positions.Add((ushort) SyndicateRank.Aide + " " + m_pSyndicate.AideCount + " " + maxAide + " " + m_pSyndicate.Arsenal.SharedBattlePower(SyndicateRank.Aide, User) + " " + 0);
            }
            else if (Position == SyndicateRank.LeaderSpouse)
            {
                msg.Positions.Add((ushort) SyndicateRank.LeaderSpouseAide + " " + m_pSyndicate.LeaderSpouseAideCount + " 1 " + m_pSyndicate.Arsenal.SharedBattlePower(SyndicateRank.LeaderSpouseAide, User) + " " + 0);
            }
            else if (Position == SyndicateRank.DeputyLeader || Position == SyndicateRank.HonoraryDeputyLeader)
            {
                msg.Positions.Add((ushort) SyndicateRank.DeputyLeaderAide + " " + m_pSyndicate.DeputyLeaderAideCount + " 1 " + m_pSyndicate.Arsenal.SharedBattlePower(SyndicateRank.DeputyLeaderAide, User) + " " + 0);
            }
            else if (Position == SyndicateRank.Manager || Position == SyndicateRank.HonoraryManager)
            {
                msg.Positions.Add((ushort) SyndicateRank.ManagerAide + " " + m_pSyndicate.ManagerAideCount + " 1 " + m_pSyndicate.Arsenal.SharedBattlePower(SyndicateRank.ManagerAide, User) + " " + 0);
            }
            else if (Position == SyndicateRank.Supervisor
                     || Position == SyndicateRank.ArsenalSupervisor
                     || Position == SyndicateRank.CpSupervisor
                     || Position == SyndicateRank.GuideSupervisor
                     || Position == SyndicateRank.HonorarySupervisor
                     || Position == SyndicateRank.LilySupervisor
                     || Position == SyndicateRank.OrchidSupervisor
                     || Position == SyndicateRank.PkSupervisor
                     || Position == SyndicateRank.RoseSupervisor
                     || Position == SyndicateRank.SilverSupervisor
                     || Position == SyndicateRank.TulipSupervisor)
            {
                msg.Positions.Add((ushort) SyndicateRank.SupervisorAide + " " + m_pSyndicate.SupervisorAideCount + " 1 " +
                                  m_pSyndicate.Arsenal.SharedBattlePower(SyndicateRank.SupervisorAide, User) + " " + 0);
            }
            else
            {
                return;
            }

            msg.SetList();
            Send(msg);
        }

        public void SendCharacterInformation()
        {
            Character pTarget = ServerKernel.UserManager.GetUser(Identity);
            if (pTarget != null)
            {
                var sPacket = new MsgSynpOffer
                {
                    Flag = Identity,
                    MoneyDonation = (int) SilverDonation,
                    EmoneyDonation = EmoneyDonation,
                    GuideDonation = GuideDonation,
                    PkDonation = PkDonation,
                    Exploits = Exploit,
                    TotemDonation = ArsenalDonation,
                    RedRoseDonation = pTarget.RedRoses,
                    WhiteRoseDonation = pTarget.WhiteRoses,
                    OrchidDonation = pTarget.Orchids,
                    TulipDonation = pTarget.Tulips
                };
                pTarget.Send(sPacket);
            }
        }

        #endregion

        #region Promotion

        public bool IsUserSetPosition()
        {
            return IsUserSetPosition(Position);
        }

        public static bool IsUserSetPosition(SyndicateRank pos)
        {
            switch (pos)
            {
                case SyndicateRank.GuildLeader:
                case SyndicateRank.DeputyLeader:
                case SyndicateRank.DeputyLeaderAide:
                case SyndicateRank.HonoraryDeputyLeader:
                case SyndicateRank.HonoraryManager:
                case SyndicateRank.HonorarySteward:
                case SyndicateRank.SupervisorAide:
                case SyndicateRank.HonorarySupervisor:
                case SyndicateRank.ManagerAide:
                case SyndicateRank.Aide:
                case SyndicateRank.LeaderSpouseAide:
                    return true;
                default:
                    return false;
            }
        }

        public string GetRankName()
        {
            switch (Position)
            {
                case SyndicateRank.GuildLeader:
                    return "Guild Leader";
                case SyndicateRank.LeaderSpouse:
                    return "Leader Spouse";
                case SyndicateRank.LeaderSpouseAide:
                    return "Leader Spouse Aide";
                case SyndicateRank.DeputyLeader:
                    return "Deputy Leader";
                case SyndicateRank.DeputyLeaderAide:
                    return "Deputy Leader Aide";
                case SyndicateRank.DeputyLeaderSpouse:
                    return "Deputy Leader Spouse";
                case SyndicateRank.HonoraryDeputyLeader:
                    return "Honorary Deputy Leader";
                case SyndicateRank.Manager:
                    return "Manager";
                case SyndicateRank.ManagerAide:
                    return "Manager Aide";
                case SyndicateRank.ManagerSpouse:
                    return "Manager Spouse";
                case SyndicateRank.HonoraryManager:
                    return "Honorary Manager";
                case SyndicateRank.Supervisor:
                    return "Supervisor";
                case SyndicateRank.SupervisorAide:
                    return "Supervisor Aide";
                case SyndicateRank.SupervisorSpouse:
                    return "Supervisor Spouse";
                case SyndicateRank.TulipSupervisor:
                    return "Tulip Supervisor";
                case SyndicateRank.ArsenalSupervisor:
                    return "Arsenal Supervisor";
                case SyndicateRank.CpSupervisor:
                    return "CP Supervisor";
                case SyndicateRank.GuideSupervisor:
                    return "Guide Supervisor";
                case SyndicateRank.LilySupervisor:
                    return "Lily Supervisor";
                case SyndicateRank.OrchidSupervisor:
                    return "Orchid Supervisor";
                case SyndicateRank.SilverSupervisor:
                    return "Silver Supervisor";
                case SyndicateRank.RoseSupervisor:
                    return "Rose Supervisor";
                case SyndicateRank.PkSupervisor:
                    return "PK Supervisor";
                case SyndicateRank.HonorarySupervisor:
                    return "Honorary Supervisor";
                case SyndicateRank.Steward:
                    return "Steward";
                case SyndicateRank.StewardSpouse:
                    return "Steward Spouse";
                case SyndicateRank.DeputySteward:
                    return "Deputy Steward";
                case SyndicateRank.HonorarySteward:
                    return "Honorary Steward";
                case SyndicateRank.Aide:
                    return "Aide";
                case SyndicateRank.TulipAgent:
                    return "Tulip Agent";
                case SyndicateRank.OrchidAgent:
                    return "Orchid Agent";
                case SyndicateRank.CpAgent:
                    return "CP Agent";
                case SyndicateRank.ArsenalAgent:
                    return "Arsenal Agent";
                case SyndicateRank.SilverAgent:
                    return "Silver Agent";
                case SyndicateRank.GuideAgent:
                    return "Guide Agent";
                case SyndicateRank.PkAgent:
                    return "PK Agent";
                case SyndicateRank.RoseAgent:
                    return "Rose Agent";
                case SyndicateRank.LilyAgent:
                    return "Lily Agent";
                case SyndicateRank.Agent:
                    return "Agent Follower";
                case SyndicateRank.TulipFollower:
                    return "Tulip Follower";
                case SyndicateRank.OrchidFollower:
                    return "Orchid Follower";
                case SyndicateRank.CpFollower:
                    return "CP Follower";
                case SyndicateRank.ArsenalFollower:
                    return "Arsenal Follower";
                case SyndicateRank.SilverFollower:
                    return "Silver Follower";
                case SyndicateRank.GuideFollower:
                    return "Guide Follower";
                case SyndicateRank.PkFollower:
                    return "PK Follower";
                case SyndicateRank.RoseFollower:
                    return "Rose Follower";
                case SyndicateRank.LilyFollower:
                    return "Lily Follower";
                case SyndicateRank.Follower:
                    return "Follower";
                case SyndicateRank.SeniorMember:
                    return "Member";
                case SyndicateRank.Member:
                    return "Member";
                default:
                    return "ERROR";
            }
        }

        #endregion
    }
}
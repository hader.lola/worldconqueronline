﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - Player Booth.cs
// Last Edit: 2019/11/24 19:04
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System.Collections.Concurrent;
using System.Linq;
using FtwCore.Common.Enums;
using FtwCore.Database.Entities;
using FtwCore.Networking.Packets;
using GameServer.Structures.Entities;
using GameServer.Structures.Items;

#endregion

namespace GameServer.Structures
{
    public class PlayerBooth : ScreenObject
    {
        private Npc m_pNpc;
        private DynamicNpc m_pShop;
        private Character m_pOwner;

        public ConcurrentDictionary<uint, BoothItem> Items;
        private bool m_vending;

        private string m_szWords = "";

        public PlayerBooth(Character owner)
            : base(owner.Identity)
        {
            m_pOwner = owner;
            m_pMap = owner.Map;
        }

        public bool Create()
        {
            m_pMap = m_pOwner.Map;
            Items = new ConcurrentDictionary<uint, BoothItem>();
            m_pNpc =
                m_pMap.RoleSet.Values.FirstOrDefault(x =>
                    x.MapX == m_pOwner.MapX - 2 && x.MapY == m_pOwner.MapY) as Npc;
            if (m_pNpc == null) return false;
            if (m_pNpc.Vending) return false;

            Vending = true;

            m_szWords = "";

            m_pOwner.Direction = FacingDirection.SouthEast;
            m_pOwner.Action = EntityAction.Sit;

            var tempDb = new DynamicNpcEntity
            {
                Id = m_pOwner.Identity % 1000000 + m_pOwner.Identity / 1000000 * 100000,
                Name = m_pOwner.Name,
                Cellx = (ushort) (m_pNpc.MapX + 3),
                Celly = m_pNpc.MapY,
                Type = BaseNpc.BOOTH_NPC,
                Lookface = 406,
                Mapid = m_pOwner.MapIdentity,
                Datastr = $"{m_pOwner.Name}Stash"
            };
            m_pShop = new DynamicNpc(tempDb) {Name = m_pOwner.Name};
            m_idObj = m_pShop.Identity;
            m_pShop.Name = tempDb.Datastr;
            m_usMapX = tempDb.Cellx;
            m_usMapY = tempDb.Celly;
            ServerKernel.RoleManager.AddRole(m_pShop);

            return true;
        }

        public bool Destroy()
        {
            m_szWords = "";

            if (m_pShop != null && Vending)
            {
                ServerKernel.RoleManager.RemoveRole(Identity);

                m_vending = false;
                m_pNpc.Vending = false;
                m_pNpc.Name = "";
                //LeaveMap();
                m_pOwner.Action = EntityAction.Stand;
                m_pNpc = null;
                m_pShop = null;
            }

            Items.Clear();
            return true;
        }

        public override string Name
        {
            get => m_pOwner.Name;
            set { }
        }

        public bool Vending
        {
            get => m_vending;
            set
            {
                m_vending = value;
                m_pNpc.Vending = value;
            }
        }

        #region IScreen Object Properties

        public override ushort MapX
        {
            get => m_usMapX;
            set
            {
                m_usMapX = value;
                m_pNpc.MapX = value;
            }
        }

        public override ushort MapY
        {
            get => m_usMapY;
            set
            {
                m_usMapY = value;
                m_pNpc.MapY = value;
            }
        }

        public string HawkMessage
        {
            get => m_szWords;
            set => m_szWords = value;
        }

        public override void EnterMap()
        {
            if (ServerKernel.Maps.TryGetValue(m_pOwner.MapIdentity, out m_pMap))
            {
                Map.EnterRoom(this);
            }
        }

        public override void LeaveMap()
        {
            Map?.LeaveRoom(Identity);
        }

        public override void SendSpawnTo(Character pObj)
        {
            m_pNpc.SendSpawnTo(pObj);
        }

        #endregion

        public void SendHawkMessage(Character pTarget)
        {
            if (string.IsNullOrEmpty(m_szWords))
                return;

            pTarget.Send(new MsgTalk(m_szWords, ChatTone.VendorHawk)
            {
                Identity = m_pOwner.Identity,
                Sender = m_pOwner.Name,
                SenderMesh = m_pOwner.Lookface
            });
        }
    }
}
﻿#region ExecPlan - Cabeçalho e Copyright

// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5729 (auto hunt feature)
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: felipe - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - Item.cs
// Last Edit: 2019/12/12 18:03
// Created: 2019/12/02 17:22
// ////////////////////////////////////////////////////////////////////////////////////

#endregion

#region References

using System;
using System.Linq;
using CO2_CORE_DLL.IO;
using FtwCore;
using FtwCore.Common;
using FtwCore.Common.Enums;
using FtwCore.Database.Entities;
using FtwCore.Networking.Packets;
using GameServer.Structures.Entities;
using Newtonsoft.Json;

#endregion

namespace GameServer.Structures.Items
{
    public sealed class Item
    {
        #region Constants

        private readonly int[] ARTIFACT_STABILIZATION_POINTS = {0, 10, 30, 50, 100, 150, 200, 300};
        private readonly int[] REFINERY_STABILIZATION_POINTS = {0, 10, 30, 70, 150, 270, 500};

        private readonly uint[] TALISMAN_SOCKET_QUALITY_ADDITION = {0, 0, 0, 0, 0, 0, 5, 10, 40, 1000};

        private readonly uint[] TALISMAN_SOCKET_PLUS_ADDITION =
        {
            0, 6, 30, 80, 240, 740, 2220, 6660, 20000, 60000, 62000,
            66000, 72000
        };

        private readonly uint[] TALISMAN_SOCKET_HOLE_ADDITION0 = {0, 160, 960};
        private readonly uint[] TALISMAN_SOCKET_HOLE_ADDITION1 = {0, 2000, 8000};

        private const int _MAX_UPGRADE_CHECK = 10;

        // Single hand weapon define 400000
        public const int SWEAPON_NONE = 00000;
        public const int SWEAPON_BLADE = 10000;
        public const int SWEAPON_SWORD = 20000;
        public const int SWEAPON_BACKSWORD = 21000;
        public const int SWEAPON_HOOK = 30000;
        public const int SWEAPON_WHIP = 40000;
        public const int SWEAPON_AXE = 50000;
        public const int SWEAPON_HAMMER = 60000;
        public const int SWEAPON_CLUB = 80000;
        public const int SWEAPON_SCEPTER = 81000;
        public const int SWEAPON_DAGGER = 90000;

        // Double hand weapon define 500000
        public const int DWEAPON_BOW = 00000;
        public const int DWEAPON_GLAVE = 10000;
        public const int DWEAPON_POLEAXE = 30000;
        public const int DWEAPON_LONGHAMMER = 40000;
        public const int DWEAPON_SPEAR = 60000;
        public const int DWEAPON_WANT = 61000;
        public const int DWEAPON_HALBERT = 80000;

        // Other weapon define 600000
        public const int DWEAPON_KATANA = 01000;
        public const int DWEAPON_PRAYER_BEADS = 10000;

        #endregion

        private ItemEntity m_dbItem;
        private ItemtypeEntity m_itemtypeEntity;
        private ItemAdditionEntity m_dbItemAddition;
        private Character m_pOwner;

        private bool m_bCreated;

        private Artifact m_artifact;
        private Refinery m_refinery;
        private Carry m_pCarry;

        public bool Create(Character player, ItemEntity item)
        {
            if (item == null)
                return false;
            m_dbItem = item;

            if (player == null)
                return false;
            m_pOwner = player;

            if (Addition > 0)
                m_dbItemAddition = GetAddition();

            if (!ServerKernel.Itemtype.TryGetValue(item.Type, out m_itemtypeEntity))
            {
                if (player.IsGm())
                    player.SendSysMessage($"Could not load itemtype {item.Type}");
                Program.WriteLog($"Could not load itemtype {item.Type}");
                return false;
            }

            LoadArtifactAndRefinery();

            m_bCreated = true;
            if (m_dbItem.Id == 0)
                Save();
            return true;
        }

        #region Ownership

        public Character Owner => m_pOwner;

        public void ChangeOwner(Character user)
        {
            if (OwnerIdentity == 0)
                OwnerIdentity = PlayerIdentity;

            if (user == null)
            {
                PlayerIdentity = 0;
                m_pOwner = null;
            }
            else
            {
                PlayerIdentity = user.Identity;
                m_pOwner = user;
            }
        }

        #endregion

        #region Attributes

        public uint Identity => m_dbItem.Id;

        /// <summary>
        /// To change this value, use the function ChangeType(uint type)
        /// </summary>
        public uint Type => m_dbItem.Type;

        public ItemtypeEntity Itemtype => m_itemtypeEntity;

        public string Name => m_itemtypeEntity?.Name ?? "";

        /// <summary>
        /// Identidade do primeiro dono do item.
        /// </summary>
        public uint OwnerIdentity
        {
            get => m_dbItem.OwnerId;
            set
            {
                m_dbItem.OwnerId = value;
                Save();
            }
        }

        /// <summary>
        /// Identidade do usuário que é DONO do item no momento.
        /// </summary>
        public uint PlayerIdentity
        {
            get => m_dbItem.PlayerId;
            set
            {
                m_dbItem.PlayerId = value;
                Save();
            }
        }

        public uint Action => m_itemtypeEntity?.IdAction ?? 0u;

        public ItemPosition Position
        {
            get => (ItemPosition) m_dbItem.Position;
            set
            {
                m_dbItem.Position = (byte) value;
                Save();
            }
        }

        public ushort Durability
        {
            get => m_dbItem.Amount;
            set
            {
                m_dbItem.Amount = value;
                Save();
            }
        }

        public ushort MaxDurability
        {
            get => m_dbItem.AmountLimit;
            set
            {
                m_dbItem.AmountLimit = value;
                Save();
            }
        }

        public SocketGem SocketOne
        {
            get => (SocketGem) m_dbItem.Gem1;
            set
            {
                m_dbItem.Gem1 = (byte) value;
                if (IsInscribed)
                {
                    m_pOwner?.Syndicate?.Arsenal.UpdatePoles();
                }

                Save();
            }
        }

        public SocketGem SocketTwo
        {
            get => (SocketGem) m_dbItem.Gem2;
            set
            {
                m_dbItem.Gem2 = (byte) value;
                if (IsInscribed)
                {
                    m_pOwner?.Syndicate?.Arsenal.UpdatePoles();
                }

                Save();
            }
        }

        public ItemEffect Effect
        {
            get => (ItemEffect) m_dbItem.Magic1;
            set
            {
                m_dbItem.Magic1 = (byte) value;
                Save();
            }
        }

        public byte Addition
        {
            get => m_dbItem.Magic3;
            set
            {
                m_dbItem.Magic3 = value;
                m_dbItemAddition = GetAddition();
                if (IsInscribed)
                {
                    m_pOwner?.Syndicate?.Arsenal.UpdatePoles();
                }

                Save();
            }
        }

        public uint SocketProgress
        {
            get => m_dbItem.Data;
            set
            {
                m_dbItem.Data = value;
                Save();
            }
        }

        public byte ReduceDamage
        {
            get => m_dbItem.ReduceDmg;
            set
            {
                m_dbItem.ReduceDmg = Math.Min((byte) 7, value);
                Save();
            }
        }

        public byte Enchantment
        {
            get => m_dbItem.AddLife;
            set
            {
                m_dbItem.AddLife = value;
                Save();
            }
        }

        public bool IsLocked => m_dbItem.Plunder != 0;

        public bool IsForbbiden => (m_dbItem.Specialflag & 0x1) != 0;

        public ItemColor Color
        {
            get => (ItemColor) m_dbItem.Color;
            set
            {
                m_dbItem.Color = (byte) value;
                Save();
            }
        }

        public uint AdditionProgress
        {
            get => m_dbItem.AddlevelExp;
            set
            {
                m_dbItem.AddlevelExp = value;
                Save();
            }
        }

        public bool IsBound
        {
            get => (m_dbItem.Monopoly & ItemType.ITEM_MONOPOLY_MASK) != 0
                   && (m_dbItem.Monopoly & ItemType.ITEM_STORAGE_MASK) != 0;
            set
            {
                if (value)
                    m_dbItem.Monopoly |= ItemType.ITEM_MONOPOLY_MASK | ItemType.ITEM_STORAGE_MASK;
                else
                {
                    int temp = m_dbItem.Monopoly;
                    temp &= ~(ItemType.ITEM_MONOPOLY_MASK | ItemType.ITEM_STORAGE_MASK);
                    m_dbItem.Monopoly = (byte) temp;
                }

                Save();
            }
        }

        public bool IsInscribed
        {
            get => m_dbItem.Inscribed != 0;
            set
            {
                m_dbItem.Inscribed = (byte) (value ? 1 : 0);
                Save(true);
            }
        }

        public uint ArtifactType
        {
            get => m_dbItem.ArtifactType;
            set
            {
                m_artifact.ArtifactType = m_dbItem.ArtifactType = value;
                Save();
            }
        }

        public uint ArtifactStart
        {
            get => m_dbItem.ArtifactStart;
            set
            {
                m_artifact.ArtifactStartTime = m_dbItem.ArtifactStart = value;
                Save();
            }
        }

        public uint ArtifactExpire
        {
            get => m_dbItem.ArtifactExpire;
            set
            {
                m_artifact.ArtifactExpireTime = m_dbItem.ArtifactExpire = value;
                Save();
            }
        }

        public uint ArtifactStabilization
        {
            get => m_dbItem.ArtifactStabilization;
            set
            {
                m_artifact.StabilizationPoints = m_dbItem.ArtifactStabilization = value;
                Save();
            }
        }

        public byte ArtifactLevel
        {
            get => m_artifact.ArtifactLevel;
            set => m_artifact.ArtifactLevel = value;
        }

        public uint RefineryType
        {
            get => m_dbItem.RefineryType;
            set
            {
                m_refinery.RefineryType = m_dbItem.RefineryType = value;
                Save();
            }
        }

        public byte RefineryLevel
        {
            get => m_dbItem.RefineryLevel;
            set
            {
                m_refinery.RefineryLevel = m_dbItem.RefineryLevel = value;
                Save();
            }
        }

        public uint RefineryStart
        {
            get => m_dbItem.RefineryStart;
            set
            {
                m_refinery.RefineryStartTime = m_dbItem.RefineryStart = value;
                Save();
            }
        }

        public uint RefineryExpire
        {
            get => m_dbItem.RefineryExpire;
            set
            {
                m_refinery.RefineryExpireTime = m_dbItem.RefineryExpire = value;
                Save();
            }
        }

        public uint RefineryStabilization
        {
            get => m_dbItem.RefineryStabilization;
            set
            {
                m_refinery.StabilizationPoints = m_dbItem.RefineryStabilization = value;
                Save();
            }
        }

        public ushort StackAmount
        {
            get => m_dbItem.StackAmount;
            set
            {
                m_dbItem.StackAmount = value;
                Save();
            }
        }

        public ushort StackLimit => Math.Max((ushort) 1, m_itemtypeEntity?.StackLimit ?? 1);

        public uint RemainingTime
        {
            get => m_dbItem.RemainingTime;
            set
            {
                m_dbItem.RemainingTime = value;
                Save();
            }
        }

        public int BattlePower
        {
            get
            {
                if (!IsEquipment() || IsAccessory() || IsGarment() || IsMountArmor() || IsMount() || IsGourd())
                    return 0;

                int ret = (int) Type % 5;
                if (Owner != null && Owner.Map.IsSkillMap())
                {
                    ret += Math.Max(5, (int) Addition);
                    ret += SocketOne != SocketGem.NoSocket ? 1 : 0;
                    ret += (int) SocketOne % 10 == 3 ? 1 : 0;
                }
                else
                {
                    ret += Addition;
                    ret += SocketOne != SocketGem.NoSocket ? 1 : 0;
                    ret += (int) SocketOne % 10 == 3 ? 1 : 0;
                    ret += SocketTwo != SocketGem.NoSocket ? 1 : 0;
                    ret += (int) SocketTwo % 10 == 3 ? 1 : 0;
                }

                if ((IsBackswordType() || IsWeaponTwoHand()) && m_pOwner.UserPackage[ItemPosition.LeftHand] == null)
                    ret *= 2;

                //Program.WriteLog($"{Name}(+{Addition}) {SocketOne} {SocketTwo} -> BP: {ret}");
                return ret;
            }
        }

        #endregion

        #region Battle Status

        public byte RequiredLevel => m_itemtypeEntity?.ReqLevel ?? 0;

        public byte RequiredGender => m_itemtypeEntity?.ReqSex ?? 0;

        public byte RequiredProfession => m_itemtypeEntity?.ReqProfession ?? 0;

        public ushort RequiredForce => m_itemtypeEntity?.ReqForce ?? 0;
        public ushort RequiredAgility => m_itemtypeEntity?.ReqSpeed ?? 0;
        public ushort RequiredVitality => m_itemtypeEntity?.ReqHealth ?? 0;
        public ushort RequiredSpirit => m_itemtypeEntity?.ReqSoul ?? 0;

        public int MinAttack
        {
            get
            {
                int dmg = m_itemtypeEntity?.AttackMin ?? 0;
                dmg += m_dbItemAddition?.AttackMin ?? 0;
                if (m_pOwner?.Map?.IsSkillMap() != true && m_artifact.ArtifactItemtype != null
                                                        && (ArtifactIsPermanent ||
                                                            (!ArtifactIsPermanent &&
                                                             m_artifact.ArtifactItemtype.ReqLevel <=
                                                             Itemtype.ReqLevel)))
                    dmg += m_artifact.ArtifactItemtype?.AttackMin ?? 0;

                return dmg;
            }
        }

        public int MaxAttack
        {
            get
            {
                int dmg = m_itemtypeEntity?.AttackMax ?? 0;
                dmg += m_dbItemAddition?.AttackMax ?? 0;
                if (m_pOwner?.Map?.IsSkillMap() != true && m_artifact.ArtifactItemtype != null
                                                        && (ArtifactIsPermanent ||
                                                            (!ArtifactIsPermanent &&
                                                             m_artifact.ArtifactItemtype.ReqLevel <=
                                                             Itemtype.ReqLevel)))
                    dmg += m_artifact.ArtifactItemtype?.AttackMax ?? 0;
                return dmg;
            }
        }

        public int MagicAttack
        {
            get
            {
                int dmg = m_itemtypeEntity?.MagicAtk ?? 0;
                dmg += m_dbItemAddition?.MagicAtk ?? 0;
                if (m_pOwner?.Map?.IsSkillMap() != true && m_artifact.ArtifactItemtype != null
                                                        && (ArtifactIsPermanent ||
                                                            (!ArtifactIsPermanent &&
                                                             m_artifact.ArtifactItemtype.ReqLevel <=
                                                             Itemtype.ReqLevel)))
                    dmg += m_artifact.ArtifactItemtype?.MagicAtk ?? 0;
                return dmg;
            }
        }

        public int Defense
        {
            get
            {
                int defense = m_itemtypeEntity?.Defense ?? 0;
                defense += m_dbItemAddition?.Defense ?? 0;
                if (m_pOwner?.Map?.IsSkillMap() != true && m_artifact.ArtifactItemtype != null
                                                        && (ArtifactIsPermanent ||
                                                            (!ArtifactIsPermanent &&
                                                             m_artifact.ArtifactItemtype.ReqLevel <=
                                                             Itemtype.ReqLevel)))
                    defense += m_artifact.ArtifactItemtype?.Defense ?? 0;
                return defense;
            }
        }

        public int MagicDefense
        {
            get
            {
                int defense = m_itemtypeEntity?.MagicDef ?? 0;
                defense += m_dbItemAddition?.MagicDef ?? 0;

                if (m_pOwner?.Map?.IsSkillMap() != true && m_artifact.ArtifactItemtype != null
                                                        && (ArtifactIsPermanent ||
                                                            (!ArtifactIsPermanent &&
                                                             m_artifact.ArtifactItemtype.ReqLevel <=
                                                             Itemtype.ReqLevel)))
                    defense += m_artifact.ArtifactItemtype?.MagicDef ?? 0;

                if (m_refinery.Mode == RefineryMode.RefMdefense && m_pOwner?.Map?.IsSkillMap() != true)
                    defense += (int) m_refinery.RefineryPercent;
                return defense;
            }
        }

        public int MagicDefenseBonus
        {
            get
            {
                if (IsArmor() || IsHelmet())
                    return m_itemtypeEntity?.MagicDef ?? 0;
                return 0;
            }
        }

        public int Dexterity
        {
            get
            {
                if (IsMount())
                    return 0;

                int dex = m_itemtypeEntity?.Dexterity ?? 0;
                dex += m_dbItemAddition?.Dexterity ?? 0;
                return dex;
            }
        }

        public int Dodge
        {
            get
            {
                if (IsMount())
                    return 0;

                int dodge = m_itemtypeEntity?.Dodge ?? 0;
                dodge += m_dbItemAddition?.Dodge ?? 0;
                return dodge;
            }
        }

        public int Life
        {
            get
            {
                int life = m_itemtypeEntity?.Life ?? 0;
                life += m_dbItemAddition?.Life ?? 0;

                if (m_pOwner?.Map?.IsSkillMap() != true && m_artifact.ArtifactItemtype != null
                                                        && (ArtifactIsPermanent ||
                                                            (!ArtifactIsPermanent &&
                                                             m_artifact.ArtifactItemtype.ReqLevel <=
                                                             Itemtype.ReqLevel)))
                    life += m_artifact.ArtifactItemtype?.Life ?? 0;

                if (m_refinery.Mode == RefineryMode.RefIntensification && m_pOwner?.Map?.IsSkillMap() != true)
                    life += (int) m_refinery.RefineryPercent;

                return life;
            }
        }

        public int Mana
        {
            get
            {
                int mana = m_itemtypeEntity?.Mana ?? 0;
                mana += m_artifact.ArtifactItemtype?.Mana ?? 0;
                return mana;
            }
        }

        public uint Vigor
        {
            get
            {
                if (IsMount())
                {
                    uint vigor = (uint) (m_itemtypeEntity?.Dexterity ?? 0);
                    vigor += m_dbItemAddition?.Dexterity ?? 0u;
                    return vigor;
                }

                return 0;
            }
        }

        public int AttackSpeed => m_itemtypeEntity?.AtkSpeed ?? 0;

        public int AttackRange => m_itemtypeEntity?.AtkRange ?? 0;

        public uint CriticalStrike
        {
            get
            {
                uint crit = m_itemtypeEntity?.CritStrike ?? 0;

                if (m_artifact.ArtifactItemtype != null &&
                    (ArtifactIsPermanent ||
                     (!ArtifactIsPermanent && m_artifact.ArtifactItemtype.ReqLevel <= Itemtype.ReqLevel)))
                    crit += m_artifact.ArtifactItemtype?.CritStrike ?? 0;

                if (m_refinery.Mode == RefineryMode.RefCriticalStrike)
                    crit += m_refinery.RefineryPercent * 100;
                return crit;
            }
        }

        public uint SkillCriticalStrike
        {
            get
            {
                uint crit = m_itemtypeEntity?.SkillCritStrike ?? 0;
                if (m_artifact.ArtifactItemtype != null &&
                    (ArtifactIsPermanent ||
                     (!ArtifactIsPermanent && m_artifact.ArtifactItemtype.ReqLevel <= Itemtype.ReqLevel)))
                    crit += m_artifact.ArtifactItemtype?.SkillCritStrike ?? 0;
                if (m_refinery.Mode == RefineryMode.RefScriticalStrike)
                    crit += m_refinery.RefineryPercent * 100;
                return crit;
            }
        }

        public uint Immunity
        {
            get
            {
                uint immunity = m_itemtypeEntity?.Immunity ?? 0;
                if (m_artifact.ArtifactItemtype != null &&
                    (ArtifactIsPermanent ||
                     (!ArtifactIsPermanent && m_artifact.ArtifactItemtype.ReqLevel <= Itemtype.ReqLevel)))
                    immunity += m_artifact.ArtifactItemtype?.Immunity ?? 0;
                if (m_refinery.Mode == RefineryMode.RefImmunity)
                    immunity += m_refinery.RefineryPercent * 100;
                return immunity;
            }
        }

        public uint Breakthrough
        {
            get
            {
                uint breakthrough = m_itemtypeEntity?.Breakthrough ?? 0;
                if (m_artifact.ArtifactItemtype != null &&
                    (ArtifactIsPermanent ||
                     (!ArtifactIsPermanent && m_artifact.ArtifactItemtype.ReqLevel <= Itemtype.ReqLevel)))
                    breakthrough += m_artifact.ArtifactItemtype?.Breakthrough ?? 0;
                if (m_refinery.Mode == RefineryMode.RefBreakthrough)
                    breakthrough += m_refinery.RefineryPercent * 10;
                return breakthrough;
            }
        }

        public uint Penetration
        {
            get
            {
                uint penetration = m_itemtypeEntity?.Penetration ?? 0;
                if (m_artifact.ArtifactItemtype != null &&
                    (ArtifactIsPermanent ||
                     (!ArtifactIsPermanent && m_artifact.ArtifactItemtype.ReqLevel <= Itemtype.ReqLevel)))
                    penetration += m_artifact.ArtifactItemtype?.Penetration ?? 0;
                if (m_refinery.Mode == RefineryMode.RefPenetration)
                    penetration += m_refinery.RefineryPercent * 100;
                return penetration;
            }
        }

        public uint Counteraction
        {
            get
            {
                uint counteraction = m_itemtypeEntity?.Counteraction ?? 0;
                if (m_artifact.ArtifactItemtype != null &&
                    (ArtifactIsPermanent ||
                     (!ArtifactIsPermanent && m_artifact.ArtifactItemtype.ReqLevel <= Itemtype.ReqLevel)))
                    counteraction += m_artifact.ArtifactItemtype?.Counteraction ?? 0;
                if (m_refinery.Mode == RefineryMode.RefCounteraction)
                    counteraction += m_refinery.RefineryPercent * 10;
                return counteraction;
            }
        }

        public uint Block
        {
            get
            {
                uint block = m_itemtypeEntity?.Block ?? 0;
                if (m_artifact.ArtifactItemtype != null &&
                    (ArtifactIsPermanent ||
                     (!ArtifactIsPermanent && m_artifact.ArtifactItemtype.ReqLevel <= Itemtype.ReqLevel)))
                    block += m_artifact.ArtifactItemtype?.Block ?? 0;
                if (m_refinery.Mode == RefineryMode.RefBlock)
                    block += m_refinery.RefineryPercent * 100;
                return block;
            }
        }

        public uint Detoxication
        {
            get
            {
                uint detoxication = m_itemtypeEntity?.Detoxication ?? 0;
                if (m_artifact.ArtifactItemtype != null &&
                    (ArtifactIsPermanent ||
                     (!ArtifactIsPermanent && m_artifact.ArtifactItemtype.ReqLevel <= Itemtype.ReqLevel)))
                    detoxication += m_artifact.ArtifactItemtype?.Detoxication ?? 0;
                if (m_refinery.Mode == RefineryMode.RefDetoxication)
                    detoxication += m_refinery.RefineryPercent;
                return detoxication;
            }
        }

        public uint MetalResistance
        {
            get
            {
                uint metalResistance = m_itemtypeEntity?.ResistMetal ?? 0;
                if (m_artifact.ArtifactItemtype != null &&
                    (ArtifactIsPermanent ||
                     (!ArtifactIsPermanent && m_artifact.ArtifactItemtype.ReqLevel <= Itemtype.ReqLevel)))
                    metalResistance += m_artifact.ArtifactItemtype?.ResistMetal ?? 0;
                if (m_refinery.Mode == RefineryMode.RefMetalResist)
                    metalResistance += m_refinery.RefineryPercent;
                return metalResistance;
            }
        }

        public uint WoodResistance
        {
            get
            {
                uint woodResistance = m_itemtypeEntity?.ResistWood ?? 0;
                if (m_artifact.ArtifactItemtype != null &&
                    (ArtifactIsPermanent ||
                     (!ArtifactIsPermanent && m_artifact.ArtifactItemtype.ReqLevel <= Itemtype.ReqLevel)))
                    woodResistance += m_artifact.ArtifactItemtype?.ResistWood ?? 0;
                if (m_refinery.Mode == RefineryMode.RefWoodResist)
                    woodResistance += m_refinery.RefineryPercent;
                return woodResistance;
            }
        }

        public uint WaterResistance
        {
            get
            {
                uint waterResistance = m_itemtypeEntity?.ResistWater ?? 0;
                if (m_artifact.ArtifactItemtype != null &&
                    (ArtifactIsPermanent ||
                     (!ArtifactIsPermanent && m_artifact.ArtifactItemtype.ReqLevel <= Itemtype.ReqLevel)))
                    waterResistance += m_artifact.ArtifactItemtype?.ResistWater ?? 0;
                if (m_refinery.Mode == RefineryMode.RefWaterResist)
                    waterResistance += m_refinery.RefineryPercent;
                return waterResistance;
            }
        }

        public uint FireResistance
        {
            get
            {
                uint fireResistance = m_itemtypeEntity?.ResistFire ?? 0;
                if (m_artifact.ArtifactItemtype != null &&
                    (ArtifactIsPermanent ||
                     (!ArtifactIsPermanent && m_artifact.ArtifactItemtype.ReqLevel <= Itemtype.ReqLevel)))
                    fireResistance += m_artifact.ArtifactItemtype?.ResistFire ?? 0;
                if (m_refinery.Mode == RefineryMode.RefFireResist)
                    fireResistance += m_refinery.RefineryPercent;
                return fireResistance;
            }
        }

        public uint EarthResistance
        {
            get
            {
                uint earthResistance = m_itemtypeEntity?.ResistEarth ?? 0;
                if (m_artifact.ArtifactItemtype != null &&
                    (ArtifactIsPermanent ||
                     (!ArtifactIsPermanent && m_artifact.ArtifactItemtype.ReqLevel <= Itemtype.ReqLevel)))
                    earthResistance += m_artifact.ArtifactItemtype?.ResistEarth ?? 0;
                if (m_refinery.Mode == RefineryMode.RefEarthResist)
                    earthResistance += m_refinery.RefineryPercent;
                return earthResistance;
            }
        }

        #endregion

        #region Refinery and Artifact

        public bool ArtifactIsPermanent => ArtifactType == 0 ||
                                           ArtifactStabilization >= ARTIFACT_STABILIZATION_POINTS[ArtifactLevel];

        public bool RefineryIsPermanent =>
            RefineryType == 0 || RefineryStabilization >= REFINERY_STABILIZATION_POINTS[RefineryLevel];

        public uint ArtifactRemainingPoints()
        {
            if (ArtifactIsPermanent)
                return 0;
            return (uint) (ARTIFACT_STABILIZATION_POINTS[ArtifactLevel] - ArtifactStabilization);
        }

        public uint RefineryRemainingPoints()
        {
            if (RefineryIsPermanent)
                return 0;
            return (uint) (REFINERY_STABILIZATION_POINTS[RefineryLevel] - RefineryStabilization);
        }

        public void LoadArtifactAndRefinery()
        {
            if (ArtifactType > 0 && ServerKernel.Itemtype.TryGetValue(ArtifactType, out ItemtypeEntity artifact))
            {
                m_artifact.ArtifactItemtype = artifact;
                m_artifact.ArtifactType = artifact.Type;
                m_artifact.ArtifactExpireTime = ArtifactExpire;
                m_artifact.ArtifactLevel = artifact.Phase;
                m_artifact.Avaiable = ArtifactIsPermanent || ArtifactExpire > UnixTimestamp.Now();
                m_artifact.ItemIdentity = Identity;
                m_artifact.StabilizationPoints = ArtifactStabilization;
                m_artifact.ArtifactStartTime = ArtifactStart;
            }
            else if (ArtifactType > 0)
            {
                ResetArtifact();
            }

            if (RefineryType > 0 && ServerKernel.Refinery.TryGetValue(RefineryType, out RefineryEntity refinery))
            {
                switch (refinery.Itemtype.ToString().Length)
                {
                    // Position
                    case 1:
                    {
                        if (Calculations.GetItemPosition(Type) == (ItemPosition) refinery.Itemtype)
                            break;
                        if (Calculations.GetItemPosition(Type) == ItemPosition.RightHand
                            && refinery.Itemtype == 5
                            && GetSort() == ItemSort.ItemsortWeaponDoubleHand)
                            break;
                        return;
                    }

                    // Type
                    case 2:
                    {
                        if (Type / 10000 == refinery.Itemtype)
                            break;
                        return;
                    }

                    // Specific item
                    case 3:
                    {
                        if (Type / 1000 == refinery.Itemtype)
                            break;
                        // Ring/HeavyRing Exception :)
                        if (refinery.Itemtype == 150
                            && Type / 1000 == 151)
                            break;
                        return;
                    }

                    default:
                        return;
                }

                if (!Enum.IsDefined(typeof(RefineryMode), refinery.Type))
                    return;

                if (RefineryIsPermanent
                    || RefineryType > 0
                    && RefineryExpire > UnixTimestamp.Now())
                {
                    m_refinery.RefineryLevel = RefineryLevel;
                    m_refinery.RefineryExpireTime = RefineryExpire;
                    m_refinery.RefineryStartTime = RefineryStart;
                    m_refinery.RefineryType = RefineryType;
                    m_refinery.Avaiable = true;
                    m_refinery.ItemIdentity = Identity;
                    m_refinery.StabilizationPoints = RefineryStabilization;
                    m_refinery.RefineryPercent = refinery.Power;
                    m_refinery.Mode = (RefineryMode) refinery.Type;
                }
                else
                    ResetRefinery();
            }
            else if (RefineryType > 0)
            {
                ResetRefinery();
            }
        }

        public void SendArtifact(PurificationType mode)
        {
            if (ArtifactType == 0)
                return;

            int now = UnixTimestamp.Now();
            var msg = new MsgItemStatus();
            uint remainingArtifact = 0;
            if (ArtifactIsPermanent)
                ArtifactExpire = 0;
            else if (now < ArtifactExpire && !ArtifactIsPermanent)
                remainingArtifact = (uint) (ArtifactExpire - now);
            msg.Append(Identity, mode, ArtifactType, ArtifactLevel, remainingArtifact);
            m_pOwner?.Send(msg);
        }

        public void SendPurification(Character pTarget = null)
        {
            if (ArtifactType == 0 && RefineryType == 0 || m_pOwner == null)
                return;

            int now = UnixTimestamp.Now();

            var artifact = new MsgItemStatus();
            uint remainingArtifact = 0;
            if (ArtifactIsPermanent)
                ArtifactExpire = 0;
            else if (now < ArtifactExpire && !ArtifactIsPermanent)
                remainingArtifact = (uint) (ArtifactExpire - now);

            if (ArtifactIsActive() && ArtifactIsPermanent)
                artifact.Append(Identity, PurificationType.PermanentArtifact, ArtifactType, ArtifactLevel, 0);
            else if (ArtifactIsActive() && remainingArtifact > 0)
                artifact.Append(Identity, PurificationType.AddDragonSoul, ArtifactType, ArtifactLevel,
                    remainingArtifact);

            uint remainingRefinery = 0;
            if (RefineryIsPermanent)
                RefineryExpire = 0;
            else if (now < RefineryExpire && !RefineryIsPermanent)
                remainingRefinery = (uint) (RefineryExpire - now);

            uint dwType = RefineryType;
            if (RefineryType >= 724440 && RefineryType <= 724444)
                dwType = 301;

            if (RefineryIsActive() && RefineryIsPermanent)
                artifact.Append(Identity, PurificationType.PermanentRefinery, dwType, RefineryLevel,
                    m_refinery.RefineryPercent, 0);
            else if (RefineryIsActive() && remainingRefinery > 0)
                artifact.Append(Identity, PurificationType.AddRefinery, dwType, RefineryLevel,
                    m_refinery.RefineryPercent, remainingRefinery);

            if (pTarget != null)
                pTarget.Send(artifact);
            else
                m_pOwner.Send(artifact);
        }

        public void CheckForPurificationExpired()
        {
            var packet = new MsgItemStatus();
            if (!ArtifactIsActive())
            {
                packet.Append(Identity, PurificationType.ArtifactExpired, 0, 0, 0);
                ResetArtifact();
            }

            if (!RefineryIsActive())
            {
                packet.Append(Identity, PurificationType.RefineryExpired, 0, 0, 0, 0);
                ResetRefinery();
            }

            m_pOwner.Send(packet);
        }

        public bool RefineryIsActive()
        {
            int time = UnixTimestamp.Now();
            return time < RefineryExpire || RefineryIsPermanent;
        }

        public bool ArtifactIsActive()
        {
            int time = UnixTimestamp.Now();
            return time < ArtifactExpire || ArtifactIsPermanent;
        }

        public void ResetRefinery()
        {
            RefineryType = 0;
            RefineryLevel = 0;
            RefineryStart = 0;
            RefineryExpire = 0;
        }

        public void ResetArtifact()
        {
            ArtifactExpire = 0;
            ArtifactStart = 0;
            ArtifactType = 0;
            ArtifactLevel = 0;
        }

        #endregion

        #region Carry

        public void SaveLocation()
        {
            if (Type == SpecialItem.MEMORY_AGATE)
            {
                if (m_pCarry == null)
                {
                    m_pCarry = new Carry(m_pOwner, this);
                }

                m_pCarry.AddLocation(m_pOwner.MapIdentity, m_pOwner.MapX, m_pOwner.MapY);
            }
        }

        public void UpdateLocation(int idx)
        {
            if (Type == SpecialItem.MEMORY_AGATE)
            {
                if (m_pCarry == null)
                {
                    SaveLocation();
                    return;
                }

                m_pCarry.UpdateLocation(idx);
            }
        }

        public void CarrySetName(string name, int idx)
        {
            if (Type == SpecialItem.MEMORY_AGATE)
            {
                if (m_pCarry == null)
                {
                    m_pCarry = new Carry(m_pOwner, this);
                }

                m_pCarry.SaveName(name, idx);
            }
        }

        public void SendCarry()
        {
            if (Type == SpecialItem.MEMORY_AGATE)
            {
                if (m_pCarry == null)
                {
                    m_pCarry = new Carry(m_pOwner, this);
                }

                m_pCarry.Send();
            }
        }

        public bool GetTeleport(uint idCarry, out CarryEntity ret)
        {
            if (SpecialItem.MEMORY_AGATE != Type)
            {
                ret = null;
                return false;
            }

            if (m_pCarry == null)
                m_pCarry = new Carry(m_pOwner, this);

            ret = m_pCarry?.Fetch(idCarry);
            return ret != null;
        }

        public int CarryCount
        {
            get
            {
                if (SpecialItem.MEMORY_AGATE != Type)
                    return 0;
                if (m_pCarry == null)
                    m_pCarry = new Carry(m_pOwner, this);
                return m_pCarry.Count;
            }
        }

        #endregion

        #region Item Lock

        public bool TryUnlock()
        {
            if (IsLocked && m_dbItem.Plunder > 1 && m_dbItem.Plunder <= UnixTimestamp.Now())
            {
                m_dbItem.Plunder = 0;

                MsgEquipLock unlock = new MsgEquipLock
                {
                    Identity = Identity,
                    Mode = LockMode.UnlockedItem
                };
                m_pOwner.Send(unlock);
                unlock.Mode = LockMode.RequestUnlock;
                m_pOwner.Send(unlock);
                Save();
                return true;
            }

            if (IsLocked && m_dbItem.Plunder > 1)
            {
                DateTime unlock = UnixTimestamp.ToDateTime(m_dbItem.Plunder);
                MsgEquipLock packet = new MsgEquipLock
                {
                    Identity = Identity,
                    Mode = LockMode.RequestUnlock,
                    Unknown = 3,
                    Param = (uint) (unlock.Year * 10000 + unlock.Day * 100 + unlock.Month)
                };
                m_pOwner.Send(packet);
                return false;
            }

            return true;
        }

        public bool IsUnlocking => m_dbItem.Plunder > 1 || m_dbItem.Plunder > UnixTimestamp.Now();
        public bool IsPick => GetItemSubtype() == 562;

        public void SetLockTime()
        {
            m_dbItem.Plunder = 1;
            Save();
        }

        public void SetUnlockTime()
        {
            m_dbItem.Plunder = (uint) (UnixTimestamp.Now() + UnixTimestamp.TIME_SECONDS_DAY * 3);
            Save();
        }

        public void SystemInstantUnlock()
        {
            m_dbItem.Plunder = 0;
            Save();
        }

        #endregion

        #region Item durability

        public bool IsBroken()
        {
            return Durability == 0;
        }

        #endregion

        #region Equipment Improvements

        public bool ChangeType(uint type, bool keepDurability = false)
        {
            if (ServerKernel.Itemtype.TryGetValue(type, out m_itemtypeEntity))
            {
                m_dbItem.Type = m_itemtypeEntity.Type;
                m_dbItemAddition = GetAddition();

                if (!keepDurability)
                    Durability = m_itemtypeEntity.Amount;

                MaxDurability = m_itemtypeEntity.AmountLimit;
                m_pOwner.Send(CreateMsgItemInfo(ItemMode.Update));
                m_pOwner.Screen.RefreshSpawnForObservers();
                m_pOwner.Send(CreateMsgItemInfo(ItemMode.Update));

                Save();
                return true;
            }

            return false;
        }

        public bool GetUpLevelChance(int itemtype, out double nChance, out int nNextId)
        {
            nNextId = 0;
            nChance = 0;
            int sort = itemtype / 10000, subtype = itemtype / 1000;

            if (NextItemLevel(itemtype).Type == itemtype)
                return false;

            ItemtypeEntity info = NextItemLevel(itemtype);
            nNextId = (int) info.Type;

            if (info.ReqLevel >= 120)
                return false;

            nChance = 100.00;
            if (sort == 11 || sort == 14 || sort == 13 || sort == 90 || subtype == 123) //Head || Armor || Shield
            {
                switch ((Int32) (info.Type % 100 / 10))
                {
                    //case 5:
                    //    nChance = 50.00;
                    //    break;
                    case 6:
                        nChance = 40.00;
                        break;
                    case 7:
                        nChance = 30.00;
                        break;
                    case 8:
                        nChance = 20.00;
                        break;
                    case 9:
                        nChance = 15.00;
                        break;
                    default:
                        nChance = 500.00;
                        break;
                }

                switch (info.Type % 10)
                {
                    case 6:
                        nChance = nChance * 0.90;
                        break;
                    case 7:
                        nChance = nChance * 0.70;
                        break;
                    case 8:
                        nChance = nChance * 0.30;
                        break;
                    case 9:
                        nChance = nChance * 0.10;
                        break;
                }
            }
            else
            {
                switch ((Int32) (info.Type % 1000 / 10))
                {
                    //case 11:
                    //    nChance = 95.00;
                    //    break;
                    case 12:
                        nChance = 90.00;
                        break;
                    case 13:
                        nChance = 85.00;
                        break;
                    case 14:
                        nChance = 80.00;
                        break;
                    case 15:
                        nChance = 75.00;
                        break;
                    case 16:
                        nChance = 70.00;
                        break;
                    case 17:
                        nChance = 65.00;
                        break;
                    case 18:
                        nChance = 60.00;
                        break;
                    case 19:
                        nChance = 55.00;
                        break;
                    case 20:
                        nChance = 50.00;
                        break;
                    case 21:
                        nChance = 45.00;
                        break;
                    case 22:
                        nChance = 40.00;
                        break;
                    default:
                        nChance = 500.00;
                        break;
                }

                switch (info.Type % 10)
                {
                    case 6:
                        nChance = nChance * 0.90;
                        break;
                    case 7:
                        nChance = nChance * 0.70;
                        break;
                    case 8:
                        nChance = nChance * 0.30;
                        break;
                    case 9:
                        nChance = nChance * 0.10;
                        break;
                }
            }

            return true;
        }

        public ItemtypeEntity NextItemLevel()
        {
            return NextItemLevel((int) Type);
        }

        public ItemtypeEntity NextItemLevel(Int32 id)
        {
            // By CptSky
            Int32 NextId = id;

            var Sort = (Byte) (id / 100000);
            var Type = (Byte) (id / 10000);
            var SubType = (Int16) (id / 1000);

            if (Sort == 1) //!Weapon
            {
                if (Type == 12 && (SubType == 120 || SubType == 121) || Type == 15 || Type == 16
                ) //Necklace || Ring || Boots
                {
                    var Level = (Byte) ((id % 1000 - id % 10) / 10);
                    if (Type == 12 && Level < 8 || Type == 15 && SubType != 152 && Level > 0 && Level < 21 ||
                        Type == 15 && SubType == 152 && Level >= 4 && Level < 22 ||
                        Type == 16 && Level > 0 && Level < 21)
                    {
                        //Check if it's still the same type of item...
                        if ((Int16) ((NextId + 20) / 1000) == SubType)
                            NextId += 20;
                    }
                    else if (Type == 12 && Level == 8 || Type == 12 && Level >= 21 ||
                             Type == 15 && SubType != 152 && Level == 0
                             || Type == 15 && SubType != 152 && Level >= 21 ||
                             Type == 15 && SubType == 152 && Level >= 22 || Type == 16 && Level >= 21)
                    {
                        //Check if it's still the same type of item...
                        if ((Int16) ((NextId + 10) / 1000) == SubType)
                            NextId += 10;
                    }
                    else if (Type == 12 && Level >= 9 && Level < 21 || Type == 15 && SubType == 152 && Level == 1)
                    {
                        //Check if it's still the same type of item...
                        if ((Int16) ((NextId + 30) / 1000) == SubType)
                            NextId += 30;
                    }
                }
                else if (Type == 11 || Type == 14 || Type == 13 || SubType == 123) //Head || Armor
                {
                    var Level = (Byte) ((id % 100 - id % 10) / 10);
                    var Quality = (Byte) (id % 10);

                    //Check if it's still the same type of item...
                    if ((Int16) ((NextId + 10) / 1000) == SubType)
                        NextId += 10;
                }
            }
            else if (Sort == 4 || Sort == 5 || Sort == 6) //Weapon
            {
                //Check if it's still the same type of item...
                if ((Int16) ((NextId + 10) / 1000) == SubType)
                    NextId += 10;

                //Invalid Backsword ID
                if (NextId / 10 == 42103 || NextId / 10 == 42105 || NextId / 10 == 42109 || NextId / 10 == 42111)
                    NextId += 10;
            }
            else if (Sort == 9)
            {
                var Level = (Byte) ((id % 100 - id % 10) / 10);
                if (Level != 30) //!Max...
                {
                    //Check if it's still the same type of item...
                    if ((Int16) ((NextId + 10) / 1000) == SubType)
                        NextId += 10;
                }
            }

            if (ServerKernel.Itemtype.TryGetValue((uint) NextId, out var itemtype))
                return itemtype;
            return null;
        }

        public uint ChkUpEqQuality(uint type)
        {
            if (!UpgradableItems(type) || type == SpecialItem.TYPE_MOUNT_ID)
                return 0;

            uint nQuality = type % 10;

            if (nQuality < 3 || nQuality >= 9)
                return 0;

            nQuality++;
            if (nQuality < 5)
                nQuality = nQuality + (5 - nQuality) + 1;

            type = type - type % 10 + nQuality;

            if (!ServerKernel.Itemtype.ContainsKey(type))
                return 0;
            return type;
        }

        public bool GetUpEpQualityInfo(out double nChance, out uint idNewType)
        {
            nChance = 0;
            idNewType = 0;

            if (Type == 150000 || Type == 150310 || Type == 150320 || Type == 410301 || Type == 421301 ||
                Type == 500301)
                return false;

            idNewType = ChkUpEqQuality(Type);
            nChance = 100;

            switch (Type % 10)
            {
                case 6:
                    nChance = 50;
                    break;
                case 7:
                    nChance = 33;
                    break;
                case 8:
                    nChance = 20;
                    break;
                default:
                    nChance = 100;
                    break;
            }

            if (!ServerKernel.Itemtype.TryGetValue(Type, out var itemtype))
                return false;
            uint nFactor = itemtype.ReqLevel;

            if (nFactor > 70)
                nChance = (uint) (nChance * (100 - (nFactor - 70) * 1.0) / 100);

            nChance = Math.Max(1, nChance);
            return true;
        }

        public bool RecoverDurability()
        {
            MaxDurability = Itemtype.AmountLimit;
            m_pOwner.Send(CreateMsgItemInfo(ItemMode.Update));
            Save();
            return true;
        }

        public bool UpgradableItems(uint type)
        {
            return Enum.IsDefined(typeof(ItemPosition), Calculations.GetItemPosition(type));
        }

        public uint GetFirstId()
        {
            uint firstId = Type;

            var sort = (byte) (Type / 100000);
            var type = (byte) (Type / 10000);
            var subType = (short) (Type / 1000);

            if (Type == 150000 || Type == 150310 || Type == 150320 || Type == 410301 || Type == 421301 || Type == 500301
                || Type == 601301 || Type == 610301)
                return Type;

            if (Type >= 120310 && Type <= 120319)
                return Type;

            if (sort == 1) //!Weapon
            {
                if (subType == 120 || subType == 121) //Necklace
                    firstId = Type - Type % 1000 + Type % 10;
                else if (type == 15 || type == 16) //Ring || Boots
                    firstId = Type - Type % 1000 + 10 + Type % 10;
                else if (type == 11 || subType == 114 || subType == 123 || type == 14) //Head
                {
                    if (subType != 112 && subType != 115 && subType != 116)
                        firstId = Type - Type % 1000 + Type % 10;
                    else
                    {
                        firstId = Type - Type % 1000 + Type % 10;
                    }
                }
                else if (type == 14)
                {
                    firstId = Type - Type % 1000 + Type % 10;
                }
                else if (type == 13) //Armor
                {
                    firstId = Type - Type % 1000 + Type % 10;
                }
            }
            else if (sort == 4 || sort == 5 || sort == 6) //Weapon
                firstId = Type - Type % 1000 + 20 + Type % 10;
            else if (sort == 9)
                firstId = Type - Type % 1000 + Type % 10;

            if (ServerKernel.Itemtype.ContainsKey(firstId))
                return firstId;
            return Type;
        }

        public uint GetUpQualityGemAmount()
        {
            if (!GetUpEpQualityInfo(out var nChance, out _))
                return 0;
            return (uint) (100 / nChance + 1) * 12 / 10;
        }

        public uint GetUpgradeGemAmount()
        {
            if (!GetUpLevelChance((int) Type, out var nChance, out _))
                return 0;
            return (uint) (100 / nChance + 1) * 12 / 10;
        }

        public bool DegradeItem(bool bCheckDura = true)
        {
            if (!IsEquipment())
                return false;
            if (bCheckDura)
                if (Durability / 100 < MaxDurability / 100)
                {
                    m_pOwner.SendSysMessage(Language.StrItemErrRepairItem);
                    return false;
                }

            uint newId = GetFirstId();
            ItemtypeEntity newType = ServerKernel.Itemtype.Values.FirstOrDefault(x => x.Type == newId);
            if (newType == null || newType.Type == Type)
                return false;
            return ChangeType(newType.Type);
        }

        public bool UpItemQuality()
        {
            if (Durability / 100 < MaxDurability / 100)
            {
                m_pOwner.SendSysMessage(Language.StrItemErrRepairItem);
                return false;
            }

            if (!GetUpEpQualityInfo(out var nChance, out var newId))
            {
                m_pOwner.SendSysMessage(Language.StrItemErrMaxQuality);
                return false;
            }

            if (!ServerKernel.Itemtype.TryGetValue(newId, out var newType))
            {
                m_pOwner.SendSysMessage(Language.StrItemErrMaxLevel);
                return false;
            }

            int gemCost = (int) (100 / nChance + 1) * 12 / 10;

            if (!m_pOwner.UserPackage.SpendDragonBalls(gemCost, IsBound))
            {
                m_pOwner.SendSysMessage(string.Format(Language.StrItemErrNotEnoughDragonBalls, gemCost));
                return false;
            }

            return ChangeType(newType.Type);
        }

        /// <summary>
        /// This method will upgrade an equipment level using meteors.
        /// </summary>
        /// <returns></returns>
        public bool UpEquipmentLevel()
        {
            if (Durability / 100 < MaxDurability / 100)
            {
                m_pOwner.SendSysMessage(Language.StrItemErrRepairItem);
                return false;
            }

            if (!GetUpLevelChance((int) Type, out var nChance, out var newId))
            {
                m_pOwner.SendSysMessage(Language.StrItemErrMaxLevel);
                return false;
            }

            if (!ServerKernel.Itemtype.TryGetValue((uint) newId, out var newType))
            {
                m_pOwner.SendSysMessage(Language.StrItemErrMaxLevel);
                return false;
            }

            if (newType.ReqLevel > m_pOwner.Level)
            {
                m_pOwner.SendSysMessage(Language.StrItemErrNotEnoughLevel);
                return false;
            }

            int gemCost = (int) (100 / nChance + 1) * 12 / 10;
            if (!m_pOwner.UserPackage.SpendMeteors(gemCost))
            {
                m_pOwner.SendSysMessage(string.Format(Language.StrItemErrNotEnoughMeteors, gemCost));
                return false;
            }

            Durability = newType.AmountLimit;
            MaxDurability = newType.AmountLimit;
            return ChangeType(newType.Type);
        }

        public bool UpUltraEquipmentLevel()
        {
            if (Durability / 100 < MaxDurability / 100)
            {
                m_pOwner.SendSysMessage(Language.StrItemErrRepairItem);
                return false;
            }

            ItemtypeEntity newType = NextItemLevel((int) Type);

            if (newType == null || newType.Type == Type)
                return false;

            ItemtypeEntity itemtype;
            if (!ServerKernel.Itemtype.TryGetValue(newType.Type, out itemtype))
            {
                m_pOwner.SendSysMessage(Language.StrItemErrMaxLevel);
                return false;
            }

            if (itemtype.ReqLevel > m_pOwner.Level)
            {
                m_pOwner.SendSysMessage(Language.StrItemErrNotEnoughLevel);
                return false;
            }

            if (!m_pOwner.UserPackage.SpendDragonBalls(1, IsBound))
            {
                m_pOwner.SendSysMessage(Language.StrItemErrNoDragonBall);
                return false;
            }

            return ChangeType(newType.Type);
        }

        public void RepairItem()
        {
            if (m_pOwner == null)
                return;

            if (!IsEquipment((int) Type) && !IsWeapon())
                return;

            if (IsBroken())
            {
                if (!m_pOwner.UserPackage.SpendMeteors(5))
                {
                    m_pOwner.SendSysMessage(Language.StrItemErrRepairMeteor);
                    return;
                }

                Durability = MaxDurability;
                Save();
                m_pOwner.Send(CreateMsgItemInfo(ItemMode.Update));
                ServerKernel.Log.GmLog("Repair",
                    string.Format("User [{2}] repaired broken [{0}][{1}] with 5 meteors.", Type, Identity,
                        PlayerIdentity));
                return;
            }

            var nRecoverDurability = (ushort) (Math.Max(0u, MaxDurability) - Durability);
            if (nRecoverDurability == 0)
                return;

            int nRepairCost = GetRecoverDurCost();

            if (!m_pOwner.SpendMoney(Math.Max(1, nRepairCost), true))
            {
                return;
            }

            Durability = MaxDurability;
            Save();
            m_pOwner.Send(CreateMsgItemInfo(ItemMode.Update));
            ServerKernel.Log.GmLog("Repair",
                string.Format("User [{2}] repaired broken [{0}][{1}] with {3} silvers.", Type, Identity, PlayerIdentity,
                    nRepairCost));
        }

        public int GetRecoverDurCost()
        {
            if (Durability > 0 && Durability < MaxDurability)
            {
                var price = (int) m_itemtypeEntity.Price;
                double qualityMultiplier = 0;

                switch (Type % 10)
                {
                    case 9:
                        qualityMultiplier = 1.125;
                        break;
                    case 8:
                        qualityMultiplier = 0.975;
                        break;
                    case 7:
                        qualityMultiplier = 0.9;
                        break;
                    case 6:
                        qualityMultiplier = 0.825;
                        break;
                    default:
                        qualityMultiplier = 0.75;
                        break;
                }

                return (int) Math.Ceiling(price * ((MaxDurability - Durability) / MaxDurability) * qualityMultiplier);
            }

            return 0;
        }

        public uint CalculateSocketingProgress()
        {
            uint total = 0;
            total += TALISMAN_SOCKET_QUALITY_ADDITION[Type % 10];
            total += TALISMAN_SOCKET_PLUS_ADDITION[Addition];
            if (IsWeapon())
            {
                if (SocketTwo > 0)
                    total += TALISMAN_SOCKET_HOLE_ADDITION0[2];
                else if (SocketOne > 0)
                    total += TALISMAN_SOCKET_HOLE_ADDITION0[1];
            }
            else
            {
                if (SocketTwo > 0)
                    total += TALISMAN_SOCKET_HOLE_ADDITION1[2];
                else if (SocketOne > 0)
                    total += TALISMAN_SOCKET_HOLE_ADDITION1[1];
            }

            return total;
        }

        #endregion

        #region Common Checks

        public bool ItemActive()
        {
            return RemainingTime > 0;
        }

        public bool ItemExpired()
        {
            return ItemActive() && RemainingTime < UnixTimestamp.Now();
        }

        public bool IsNonsuchItem()
        {
            switch (Type)
            {
                case SpecialItem.TYPE_DRAGONBALL:
                case SpecialItem.TYPE_METEOR:
                case SpecialItem.TYPE_METEORTEAR:
                    return true;
            }

            // precious gem
            if (IsGem() && Type % 10 >= 2)
                return true;

            // todo handle chests inside of inventory

            // other type
            if (GetSort() == ItemSort.ItemsortUsable || GetSort() == ItemSort.ItemsortUsable2 ||
                GetSort() == ItemSort.ItemsortUsable3)
                return false;

            // high quality
            if (GetQuality() >= 8)
                return true;

            int nGem1 = (int) SocketOne % 10;
            int nGem2 = (int) SocketTwo % 10;

            bool bIsnonsuch = false;

            if (IsWeapon())
            {
                if (SocketOne != SocketGem.EmptySocket && nGem1 >= 2
                    || SocketTwo != SocketGem.EmptySocket && nGem2 >= 2)
                    bIsnonsuch = true;
            }
            else if (IsShield())
            {
                if (SocketOne != SocketGem.NoSocket || SocketTwo != SocketGem.NoSocket)
                    bIsnonsuch = true;
            }

            return bIsnonsuch;
        }

        public bool IsGem()
        {
            return GetItemSubtype() == 700;
        }

        public bool IsNeverDropWhenDead()
        {
            return m_dbItem.Monopoly == 3 || Itemtype.Monopoly == 3 || Itemtype.Monopoly == 9 || Addition > 5 ||
                   IsBound || IsLocked;
        }

        public int GetSellPrice()
        {
            if (IsBroken() || IsArrow())
                return 0;
            int price = (int) (m_itemtypeEntity?.Price ?? 0);
            int amount = Math.Min(MaxDurability, Durability);
            price = price + Calculations.MulDiv(price, GetQuality() * 5, 100);
            price = Calculations.MulDiv(price / 3, amount, m_dbItem.AmountLimit);
            return price;
        }

        public bool IsTaskItem()
        {
            return (m_itemtypeEntity?.IdAction ?? 0) > 0;
        }

        public bool IsPileEnable()
        {
            return IsExpend() && StackLimit > 1;
        }

        public int GetLevel()
        {
            return (int) (Type % 1000 / 10);
        }

        public static int GetLevel(int nType)
        {
            return nType % 1000 / 10;
        }

        public int GetQuality()
        {
            return (int) (Type % 10);
        }

        public static int GetQuality(int nType)
        {
            return nType % 10;
        }

        public int GetItemSubtype()
        {
            return (int) (Type / 1000);
        }

        public static int GetItemSubtype(int nType)
        {
            return nType / 1000;
        }

        public bool IsArrowSort()
        {
            return Type / 1000 == 1050;
        }

        public static bool IsArrowSort(int nType)
        {
            return nType / 1000 == 1050;
        }

        public bool IsArrow()
        {
            return IsArrowSort();
        }

        public static bool IsArrow(int nType)
        {
            return IsArrowSort(nType);
        }

        public bool IsBow()
        {
            return Type / 1000 == 500;
        }

        public static bool IsBow(int nType)
        {
            return nType / 1000 == 500;
        }

        public bool IsCountable()
        {
            return IsArrowSort();
        }

        public static bool IsCountable(int nType)
        {
            return IsArrowSort(nType);
        }

        public bool IsMount()
        {
            return Type == 300000;
        }

        public static bool IsMount(int nType)
        {
            return nType == 300000;
        }

        public bool IsEquipment()
        {
            return !IsArrowSort() && (int) GetSort() < 7 || IsShield() || GetItemSubtype() == 2100;
        }

        public bool IsPkLossEnable()
        {
            switch (Position)
            {
                case ItemPosition.Headwear:
                case ItemPosition.Necklace:
                case ItemPosition.Ring:
                case ItemPosition.RightHand:
                case ItemPosition.Armor:
                case ItemPosition.Boots:
                case ItemPosition.AttackTalisman:
                case ItemPosition.DefenceTalisman:
                case ItemPosition.Crop:
                    return true;
                case ItemPosition.LeftHand:
                    return !IsArrow();
                default: return false;
            }
        }

        public static bool IsEquipment(int nType)
        {
            return !IsArrowSort(nType) && (int) GetSort(nType) < 7 || IsShield(nType);
        }

        public bool IsArmor()
        {
            return Type / 10000 == 13;
        }

        public static bool IsArmor(int nType)
        {
            return nType / 10000 == 13;
        }

        public bool IsShield()
        {
            return Type / 1000 == 900;
        }

        public static bool IsShield(int nType)
        {
            return nType / 1000 == 900;
        }

        public ItemSort GetSort()
        {
            return (ItemSort) (Type / 100000);
        }

        public static ItemSort GetSort(int nType)
        {
            return (ItemSort) (nType / 100000);
        }

        public bool IsMountArmor()
        {
            return Type / 1000 == 200;
        }

        public bool IsAccessory()
        {
            uint nType = Type / 1000;
            return nType == 350 || nType == 360 || nType == 370 || nType == 380;
        }

        public uint GetItemType()
        {
            return GetItemType((int) Type);
        }

        public static uint GetItemType(int type)
        {
            if (GetSort(type) == ItemSort.ItemsortWeaponSingleHand)
                return (uint) (444000 + type % 1000 - type % 10);
            if (GetSort(type) == ItemSort.ItemsortWeaponDoubleHand)
                return (uint) (555000 + type % 1000 - type % 10);
            return (uint) (type % 100000 / 10000 * 10000);
        }

        public bool IsNormal()
        {
            return GetSort() == ItemSort.ItemsortUsable2;
        }

        public bool IsWeaponOneHand()
        {
            return GetSort() == ItemSort.ItemsortWeaponSingleHand;
        } // single hand use

        public bool IsWeaponTwoHand()
        {
            return GetSort() == ItemSort.ItemsortWeaponDoubleHand;
        } // two hand use

        public bool IsWeaponProBased()
        {
            return GetSort() == ItemSort.ItemsortWeaponProfBased;
        } // professional hand use

        public bool IsRapier()
        {
            return GetItemSubtype() == 611;
        }

        public bool IsPistol()
        {
            return GetItemSubtype() == 612;
        }

        public bool IsWeapon()
        {
            return IsWeaponOneHand() || IsWeaponTwoHand() || IsWeaponProBased();
        }

        public bool IsPrayerBeads()
        {
            return GetItemSubtype() == 610;
        }

        public bool IsOther()
        {
            return GetSort() == ItemSort.ItemsortUsable;
        }

        public bool IsFinery()
        {
            return !IsArrowSort() && GetSort() == ItemSort.ItemsortFinery;
        }

        public bool IsExpend()
        {
            return IsArrowSort()
                   || Type < 50000
                   || GetSort() == ItemSort.ItemsortUsable
                   || GetSort() == ItemSort.ItemsortUsable2
                   || GetSort() == ItemSort.ItemsortUsable3;
        }

        public bool IsHelmet()
        {
            return Type >= 110000 && Type < 120000 || Type >= 140000 && Type < 150000;
        }

        public bool IsNecklace()
        {
            return Type >= 120000 && Type < 123000;
        }

        public bool IsRing()
        {
            return Type >= 150000 && Type < 152000;
        }

        public bool IsBangle()
        {
            return Type >= 152000 && Type < 153000;
        }

        public bool IsShoes()
        {
            return Type >= 160000 && Type < 161000;
        }

        public bool IsEquipEnable()
        {
            return IsEquipment() || IsArrowSort();
        }

        public bool IsMedicine()
        {
            return (Type >= 1000000 && Type <= 1049999) || Type == 725065 || Type == 725066;
        }

        public bool IsGourd()
        {
            return Type >= 2100000 && Type < 2200000;
        }

        public bool IsGarment()
        {
            return Type >= 170000 && Type < 200000;
        }

        public bool IsAttackTalisman()
        {
            return Type >= 201000 && Type < 202000;
        }

        public bool IsDefenseTalisman()
        {
            return Type >= 202000 && Type < 203000;
        }

        public bool IsCrop()
        {
            return Type >= 203000 && Type < 204000;
        }

        public bool IsTwoHandAccessory()
        {
            return Type >= 350000 && Type < 360000;
        }

        public bool IsOneHandAccessory()
        {
            return Type >= 360000 && Type < 370000;
        }

        public bool IsBowAccessory()
        {
            return Type >= 370000 && Type < 380000;
        }

        public bool IsShieldAccessory()
        {
            return Type >= 380000 && Type < 390000;
        }

        public bool IsBackswordType()
        {
            return Type / 1000 == 421;
        }

        public bool IsHoldEnable()
        {
            return IsWeaponOneHand() || IsWeaponTwoHand() || IsWeaponProBased() || IsBow() || IsShield() || IsArrow();
        }

        public ItemPosition GetPosition()
        {
            if (IsHelmet())
                return ItemPosition.Headwear;
            if (IsNecklace())
                return ItemPosition.Necklace;
            if (IsRing())
                return ItemPosition.Ring;
            if (IsBangle())
                return ItemPosition.Ring;
            if (IsPistol())
                return ItemPosition.LeftHand;
            if (IsWeapon())
                return ItemPosition.RightHand;
            if (IsShield())
                return ItemPosition.LeftHand;
            if (IsArrow())
                return ItemPosition.LeftHand;
            if (IsArmor())
                return ItemPosition.Armor;
            if (IsShoes())
                return ItemPosition.Boots;
            if (IsMount())
                return ItemPosition.Steed;
            if (IsMountArmor())
                return ItemPosition.SteedArmor;
            if (IsGourd())
                return ItemPosition.Gourd;
            if (IsGarment())
                return ItemPosition.Garment;
            if (IsAttackTalisman())
                return ItemPosition.AttackTalisman;
            if (IsDefenseTalisman())
                return ItemPosition.DefenceTalisman;
            if (IsCrop())
                return ItemPosition.Crop;
            if (IsOneHandAccessory() || IsTwoHandAccessory() || IsBowAccessory())
                return ItemPosition.RightHandAccessory;
            if (IsShieldAccessory())
                return ItemPosition.LeftHand;
            return ItemPosition.Inventory;
        }

        #endregion

        #region Map

        public void UserChangeMap()
        {
            // makes no sense
            if (Owner == null || !IsEquipment())
                return;

            // to avoid much overhead, let's limit this to be updated sometimes
            // I know it's almost the same as running everytime
            // but it's better to ask and do only 2 if's than doing all the search through the dictionary every time.
            if (Owner.Map.IsSkillMap())
            {
                m_dbItemAddition = GetAddition();
            }
            else if (Addition != (m_dbItemAddition?.Level ?? 0))
            {
                m_dbItemAddition = GetAddition();
            }
        }

        #endregion

        #region Split and Merge

        public Item Split(int nNum)
        {
            if (!IsPileEnable())
                return null;

            if (nNum <= 0 || nNum >= StackAmount)
                return null;

            try
            {
                Item item = Clone();
                item.StackAmount = (ushort) nNum;
                StackAmount -= (ushort) nNum;
                return item;
            }
            catch
            {
                Program.WriteLog("Could not clone item", LogType.ERROR);
            }

            return null;
        }

        public Item Clone()
        {
            ItemEntity entity = new ItemEntity
            {
                AddlevelExp = m_dbItem.AddlevelExp,
                AddLife = m_dbItem.AddLife,
                Amount = m_dbItem.Amount,
                AmountLimit = m_dbItem.AmountLimit,
                AntiMonster = m_dbItem.AntiMonster,
                ArtifactType = m_dbItem.ArtifactType,
                ArtifactStabilization = m_dbItem.ArtifactStabilization,
                ArtifactExpire = m_dbItem.ArtifactExpire,
                ArtifactStart = m_dbItem.ArtifactStart,
                ChkSum = m_dbItem.ChkSum,
                Type = m_dbItem.Type,
                StackAmount = m_dbItem.StackAmount,
                Position = m_dbItem.Position,
                Plunder = m_dbItem.Plunder,
                RefineryType = m_dbItem.RefineryType,
                Color = m_dbItem.Color,
                Data = m_dbItem.Data,
                Monopoly = m_dbItem.Monopoly,
                RefineryStabilization = m_dbItem.RefineryStabilization,
                Gem1 = m_dbItem.Gem1,
                Gem2 = m_dbItem.Gem2,
                Ident = m_dbItem.Ident,
                Magic1 = m_dbItem.Magic1,
                Magic2 = m_dbItem.Magic2,
                Magic3 = m_dbItem.Magic3,
                OwnerId = m_dbItem.OwnerId,
                PlayerId = m_dbItem.PlayerId,
                ReduceDmg = m_dbItem.ReduceDmg,
                RefineryExpire = m_dbItem.RefineryExpire,
                RefineryLevel = m_dbItem.RefineryLevel,
                RefineryStart = m_dbItem.RefineryStart,
                RemainingTime = m_dbItem.RemainingTime,
                Specialflag = m_dbItem.Specialflag
            };

            Item item = new Item();
            if (item.Create(m_pOwner, entity))
                return item;
            return null;
        }

        #endregion

        #region Drop

        public bool CanBeDropped
        {
            get
            {
                if (IsGourd() || IsLocked || IsForbbiden || Addition > 5 ||
                    (m_itemtypeEntity.Monopoly & ItemType.ITEM_DROP_HINT_MASK) != 0)
                    return false;
                return true;
            }
        }

        public bool DisappearWhenDropped
        {
            get
            {
                if ((m_itemtypeEntity.Monopoly & ItemType.ITEM_MONOPOLY_MASK) != 0
                    || (m_itemtypeEntity.Monopoly & ItemType.ITEM_DROP_HINT_MASK) != 0
                    || IsBound)
                    return true;
                return false;
            }
        }

        public bool CanBeSold => !IsBound && !IsForbbiden && !IsInscribed && !IsLocked;

        #endregion

        #region Packets

        public MsgItemInfoEx CreateMsgItemInfoEx(ushort viewType)
        {
            return new MsgItemInfoEx
            {
                Bless = ReduceDamage,
                Bound = IsBound,
                Color = Color,
                Composition = AdditionProgress,
                Durability = Durability,
                MaximumDurability = MaxDurability,
                Enchant = Enchantment,
                Identity = Identity,
                Itemtype = Type,
                Locked = IsLocked,
                SocketOne = SocketOne,
                SocketTwo = SocketTwo,
                Position = Position,
                ViewType = viewType,
                TargetIdentity = m_pOwner?.Identity ?? 0u,
                Plus = Addition,
                //Purification = ArtifactType,
                RemainingTime = RemainingTime,
                SocketProgress = SocketProgress,
                StackAmount = StackAmount,
                Suspicious = IsForbbiden
            };
        }

        public MsgItem CreateMsgItem(ItemAction action, uint param1 = 0, uint param2 = 0, uint param3 = 0,
            uint param4 = 0)
        {
            return new MsgItem
            {
                Identity = Identity,
                Action = action,
                Param1 = param1,
                Param2 = param2,
                Param3 = param3,
                Param4 = param4
            };
        }

        public MsgItemInfo CreateMsgItemInfo(ItemMode mode = ItemMode.Default)
        {
            return new MsgItemInfo
            {
                Identity = Identity,
                ItemMode = mode,
                Itemtype = Type,
                Durability = Durability,
                MaximumDurability = MaxDurability,
                Position = Position,
                SocketProgress = SocketProgress,
                SocketOne = SocketOne,
                SocketTwo = SocketTwo,
                Effect = Effect,
                Plus = Addition,
                Bless = ReduceDamage,
                Bound = IsBound,
                Enchantment = Enchantment,
                Suspicious = IsForbbiden,
                Locked = IsLocked, // todo item lock
                Color = Color,
                Composition = AdditionProgress,
                Inscribed = IsInscribed,
                RemainingTime = (uint) (RemainingTime - UnixTimestamp.Now()),
                PackageAmount = StackAmount
            };
        }

        #endregion

        #region Database

        public bool Save(bool bForce = false)
        {
            if (!bForce && !m_bCreated)
                return false;
            return m_dbItem != null && Database.ItemRepository.Save(m_dbItem);
        }

        public bool Delete()
        {
            ServerKernel.Log.GmLog($"{m_pOwner.Identity}_item_delete", ToJson());
            return m_dbItem != null && Database.ItemRepository.Delete(m_dbItem);
        }

        public void GenerateDefault()
        {
            m_dbItem = new ItemEntity
            {
                AddLife = 0,
                AddlevelExp = 0,
                Amount = 1,
                AmountLimit = 1,
                AntiMonster = 0,
                ArtifactExpire = 0,
                ArtifactStabilization = 0,
                ArtifactStart = 0,
                ArtifactType = 0,
                RefineryExpire = 0,
                RefineryLevel = 0,
                RefineryStabilization = 0,
                RefineryStart = 0,
                RefineryType = 0,
                RemainingTime = 0,
                ReduceDmg = 0,
                PlayerId = 0,
                Plunder = 0,
                Position = 0,
                ChkSum = 0,
                Color = 3,
                Data = 0,
                Gem1 = 0,
                Gem2 = 0,
                Ident = 0,
                Inscribed = 0,
                OwnerId = 0,
                Magic1 = 0,
                Magic2 = 0,
                Magic3 = 0,
                Monopoly = 0,
                Specialflag = 0,
                StackAmount = 0,
                Type = 0
            };
        }

        public ItemAdditionEntity GetAddition()
        {
            if (Addition == 0)
                return null;

            int level = Addition;
            if (Owner != null && Owner.Map.IsSkillMap())
            {
                level = Math.Max(5, (int) Addition);
            }

            uint idItem = 0;
            if (GetSort() == ItemSort.ItemsortWeaponSingleHand)
                idItem = (uint) (444000 + Type % 1000 - GetQuality());
            else if (GetSort() == ItemSort.ItemsortWeaponDoubleHand)
                idItem = (uint) (555000 + Type % 1000 - GetQuality());
            else
                idItem = (uint) (Type - GetQuality());
            return ServerKernel.ItemAddition.FirstOrDefault(x => x.TypeId == idItem && x.Level == level);
        }

        #endregion

        #region Static

        public static ItemEntity CreateEntity(uint type, bool bound = false)
        {
            if (!ServerKernel.Itemtype.TryGetValue(type, out var itemtype))
                return null;

            ItemEntity entity = new ItemEntity
            {
                Magic1 = itemtype.Magic1,
                Magic2 = itemtype.Magic2,
                Magic3 = itemtype.Magic3,
                Type = type,
                Amount = itemtype.Amount,
                AmountLimit = itemtype.AmountLimit,
                StackAmount = 1,
                Gem1 = itemtype.Gem1,
                Gem2 = itemtype.Gem2,
                Monopoly = (byte) (bound ? 3 : itemtype.Monopoly),
                Color = (byte) ItemColor.Orange
            };
            return entity;
        }

        #endregion

        #region Json

        public string ToJson()
        {
            return JsonConvert.SerializeObject(m_dbItem);
        }

        #endregion
    }
}
﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - Trade.cs
// Last Edit: 2019/11/24 19:04
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System;
using System.Collections.Generic;
using System.Linq;
using FtwCore;
using FtwCore.Common.Enums;
using FtwCore.Database.Entities;
using FtwCore.Database.Repositories;
using FtwCore.Networking.Packets;
using GameServer.Structures.Entities;
using GameServer.Structures.Items;

#endregion

namespace GameServer.Structures
{
    public class Trade
    {
        private bool m_bAccepted;
        private bool m_bOwnerConfirmed;
        private bool m_bTargetConfirmed;

        private readonly Dictionary<uint, Item> m_dicOwner = new Dictionary<uint, Item>(20);
        private readonly Dictionary<uint, Item> m_dicTarget = new Dictionary<uint, Item>(20);
        private uint m_dwOwnerEmoney;

        private uint m_dwOwnerMoney;
        private uint m_dwTargetEmoney;
        private uint m_dwTargetMoney;
        private readonly Character m_pOwner;
        private readonly Character m_pTarget;

        public Trade(Character owner, Character target)
        {
            m_pOwner = owner;
            m_pTarget = target;
            m_pOwner.Trade = this;
            m_pTarget.Trade = this;
        }

        public bool AddItem(uint id, Character pSender)
        {
            Item item = pSender.UserPackage[id];
            if (item != null)
            {
                if (item.IsBound)
                {
                    var msg = new MsgTrade
                    {
                        Target = id,
                        Type = TradeType.REMOVE_ITEM
                    };
                    pSender.Send(msg);
                    pSender.SendSysMessage(Language.StrNotForTrade);
                    return false;
                }

                if (item.Itemtype.Monopoly == 3 || item.Itemtype.Monopoly == 9)
                {
                    var msg = new MsgTrade
                    {
                        Target = id,
                        Type = TradeType.REMOVE_ITEM
                    };
                    pSender.Send(msg);
                    pSender.SendSysMessage(Language.StrNotForTrade);
                    return false;
                }

                TradePartner partner;

                if (pSender == m_pOwner)
                {
                    if (m_pTarget.UserPackage.IsPackFull(item))
                    {
                        var pmsg = new MsgTrade
                        {
                            Target = id,
                            Type = TradeType.REMOVE_ITEM
                        };
                        pSender.Send(pmsg);

                        pSender.SendSysMessage(Language.StrTradeTargetBagIsFull);
                        m_pTarget.SendSysMessage(Language.StrTradeYourBagIsFull);
                        return false;
                    }

                    if (item.IsLocked
                        && (!pSender.TradePartners.TryGetValue(m_pTarget.Identity, out partner)
                            || !partner.IsActive))
                    {
                        var pmsg = new MsgTrade
                        {
                            Target = id,
                            Type = TradeType.REMOVE_ITEM
                        };
                        pSender.Send(pmsg);
                        pSender.SendSysMessage(Language.StrNotForTrade);
                        return false;
                    }

                    if (!item.CanBeSold)
                    {
                        var pmsg = new MsgTrade
                        {
                            Target = id,
                            Type = TradeType.REMOVE_ITEM
                        };
                        pSender.Send(pmsg);
                        pSender.SendSysMessage(Language.StrNotForTrade);
                        return false;
                    }

                    if (m_dicOwner.ContainsKey(id))
                    {
                        var pmsg = new MsgTrade
                        {
                            Target = id,
                            Type = TradeType.REMOVE_ITEM
                        };
                        pSender.Send(pmsg);
                        // item already exists
                        return false;
                    }

                    m_dicOwner.Add(id, item);
                    m_pTarget.Send(item.CreateMsgItemInfo(ItemMode.Trade));
                }
                else if (pSender == m_pTarget)
                {
                    if (m_pOwner.UserPackage.IsPackFull(item))
                    {
                        var pmsg = new MsgTrade
                        {
                            Target = id,
                            Type = TradeType.REMOVE_ITEM
                        };
                        pSender.Send(pmsg);

                        pSender.SendSysMessage(Language.StrTradeYourBagIsFull);
                        m_pTarget.SendSysMessage(Language.StrTradeTargetBagIsFull);
                        return false;
                    }

                    if (item.IsLocked
                        && (!pSender.TradePartners.TryGetValue(m_pOwner.Identity, out partner)
                            || !partner.IsActive))
                    {
                        var pmsg = new MsgTrade
                        {
                            Target = id,
                            Type = TradeType.REMOVE_ITEM
                        };
                        pSender.Send(pmsg);
                        pSender.SendSysMessage(Language.StrNotForTrade);
                        return false;
                    }

                    if (!item.CanBeSold)
                    {
                        var pmsg = new MsgTrade
                        {
                            Target = id,
                            Type = TradeType.REMOVE_ITEM
                        };
                        pSender.Send(pmsg);
                        pSender.SendSysMessage(Language.StrNotForTrade);
                        return false;
                    }

                    if (m_dicTarget.ContainsKey(id))
                    {
                        // item already exists
                        var pmsg = new MsgTrade
                        {
                            Target = id,
                            Type = TradeType.REMOVE_ITEM
                        };
                        pSender.Send(pmsg);
                        return false;
                    }

                    m_dicTarget.Add(id, item);

                    m_pOwner.Send(item.CreateMsgItemInfo(ItemMode.Trade));
                }
                else
                {
                    return false; // who the fuck?
                }

                return true;
            }

            pSender.SendSysMessage(Language.StrNotForTrade);
            return false;
        }

        public bool AddMoney(uint dwAmount, Character pSender, MsgTrade pMsg)
        {
            if (dwAmount > pSender.Silver)
            {
                pSender.SendSysMessage(Language.StrNotEnoughMoney);
                return false;
            }

            if (pSender == m_pOwner)
            {
                if (m_pTarget.Silver + dwAmount > int.MaxValue)
                {
                    CloseWindow();
                    return false;
                }

                m_dwOwnerMoney = dwAmount;
                pMsg.Type = TradeType.SHOW_MONEY;
                m_pTarget.Send(pMsg);
            }
            else if (pSender == m_pTarget)
            {
                if (m_pOwner.Silver + dwAmount > int.MaxValue)
                {
                    CloseWindow();
                    return false;
                }

                m_dwTargetMoney = dwAmount;
                pMsg.Type = TradeType.SHOW_MONEY;
                m_pOwner.Send(pMsg);
            }
            else
            {
                // dude?
                return false;
            }

            return true;
        }

        public bool AddEmoney(uint dwAmount, Character pSender, MsgTrade pMsg)
        {
            if (dwAmount > pSender.Emoney)
            {
                pSender.SendSysMessage(Language.StrNotEnoughMoney);
                return false;
            }

            if (pSender == m_pOwner)
            {
                if (m_pTarget.Emoney + dwAmount > int.MaxValue)
                {
                    CloseWindow();
                    return false;
                }

                m_dwOwnerEmoney = dwAmount;
                pMsg.Type = TradeType.SHOW_CONQUER_POINTS;
                m_pTarget.Send(pMsg);
            }
            else if (pSender == m_pTarget)
            {
                if (m_pOwner.Emoney + dwAmount > int.MaxValue)
                {
                    CloseWindow();
                    return false;
                }

                m_dwTargetEmoney = dwAmount;
                pMsg.Type = TradeType.SHOW_CONQUER_POINTS;
                m_pOwner.Send(pMsg);
            }
            else
            {
                return false;
            }

            return true;
        }

        public bool AcceptTrade(Character pSender, MsgTrade pMsg)
        {
            if (!m_bAccepted)
            {
                if (pSender == m_pOwner)
                {
                    pMsg.Target = pSender.Identity;
                    m_pTarget.Send(pMsg);
                    m_bOwnerConfirmed = true;
                    if (!m_bTargetConfirmed)
                        return true;
                }
                else if (pSender == m_pTarget)
                {
                    pMsg.Target = pSender.Identity;
                    m_pOwner.Send(pMsg);
                    m_bTargetConfirmed = true;
                    if (!m_bOwnerConfirmed)
                        return true;
                }
                else
                {
                    return false;
                }

                m_bAccepted = true;
            }

            /*  
             Changed the steps a little bit from the old version. In the old version if p1 inserts items and
             send the acceptance packet twice it would confirm the trade :) 2016-04-19
             */
            if (!m_bAccepted) return false; // should not reach here if both didn't accept

            // checks if both accounts still has the items on the inventory
            bool success = m_dicOwner.Values.All(item => m_pOwner.UserPackage[item.Identity] != null);

            if (!success)
            {
                m_pOwner.SendSysMessage(Language.StrTradeFail);
                m_pTarget.SendSysMessage(Language.StrTradeFail);
                CloseWindow();
                return false;
            }

            success = m_dicTarget.Values.All(item => m_pTarget.UserPackage[item.Identity] != null);

            // checks if both accounts still have space in the inventory
            if (!m_pOwner.UserPackage.IsPackSpare(m_dicTarget.Count))
                success = false;
            if (!m_pTarget.UserPackage.IsPackSpare(m_dicOwner.Count))
                success = false;

            // checks for money and emoney
            if (m_dwOwnerMoney > m_pOwner.Silver)
                success = false;
            if (m_dwOwnerEmoney > m_pOwner.Emoney)
                success = false;
            if (m_dwTargetMoney > m_pTarget.Silver)
                success = false;
            if (m_dwTargetEmoney > m_pTarget.Emoney)
                success = false;

            if (!success)
            {
                m_pOwner.SendSysMessage(Language.StrTradeFail);
                m_pTarget.SendSysMessage(Language.StrTradeFail);
                CloseWindow();
                return false;
            }

            ServerKernel.Log.GmLog("trade_item",
                $"user1_id[{m_pOwner.Identity}] user1[{m_pOwner.Name}] lev1[{m_pOwner.Level}] money1[{m_dwOwnerMoney}] emoney1[{m_dwOwnerEmoney}]");
            ServerKernel.Log.GmLog("trade_item",
                $"user2_id[{m_pTarget.Identity}] user2[{m_pTarget.Name}] lev2[{m_pTarget.Level}] money2[{m_dwTargetMoney}] emoney2[{m_dwTargetEmoney}]");

            TradeEntity trade = new TradeEntity
            {
                UserIpAddress = m_pOwner.Owner.IpAddress,
                UserMacAddress = m_pOwner.Owner.MacAddress,
                TargetIpAddress = m_pTarget.Owner.IpAddress,
                TargetMacAddress = m_pTarget.Owner.MacAddress,
                MapIdentity = m_pOwner.MapIdentity,
                TargetEmoney = m_dwTargetEmoney,
                TargetMoney = m_dwTargetMoney,
                UserEmoney = m_dwOwnerEmoney,
                UserMoney = m_dwOwnerMoney,
                TargetIdentity = m_pTarget.Identity,
                UserIdentity = m_pOwner.Identity,
                TargetX = m_pTarget.MapX,
                TargetY = m_pTarget.MapY,
                UserX = m_pOwner.MapX,
                UserY = m_pOwner.MapY,
                Timestamp = DateTime.Now
            };
            new TradeRepository().Save(trade);

            CloseWindow();

            List<TradeItemEntity> allEntities = new List<TradeItemEntity>();
            foreach (var item in m_dicOwner.Values)
            {
                m_pOwner.UserPackage.RemoveFromInventory(item.Identity, RemovalType.RemoveAndDisappear);
                //item.OwnerIdentity = m_pOwner.Identity;
                //item.PlayerIdentity = m_pTarget.Identity;
                item.ChangeOwner(m_pTarget);
                m_pTarget.UserPackage.AddItem(item);

                ServerKernel.Log.GmLog("trade_item",
                    string.Format(
                        "user_id[{0}] name[{1}]| item[{2}][{3}] type[{4}] gem1[{5}] gem2[{6}] magic3[{7}] dur[{8}] max_dur[{9}] data[{10}], dec_dmg[{11}] add_life[{12}] color[{13}] Addlevel_exp[{14}] artifact_type[{15}] artifact_expire[{16}] artifact_stabilization[{17}] refinery_type[{18}] refinery_level[{19}] refinery_stabilization[{20}] stack_amount[{21}] remaining_time[{22}]",
                        m_pOwner.Identity, m_pOwner.Name, item.Itemtype.Name, item.Identity, item.Type, item.SocketOne,
                        item.SocketTwo, item.Addition, item.Durability, item.MaxDurability, item.SocketProgress,
                        item.ReduceDamage, item.Enchantment, item.Color, item.AdditionProgress,
                        item.ArtifactType, item.ArtifactExpire, item.ArtifactStabilization,
                        item.RefineryType, item.RefineryLevel, item.RefineryStabilization, item.StackAmount,
                        item.RemainingTime));

                allEntities.Add(new TradeItemEntity
                {
                    Chksum = (uint) item.GetHashCode(),
                    ItemIdentity = item.Identity,
                    Itemtype = item.Type,
                    SenderIdentity = m_pOwner.Identity,
                    TradeIdentity = trade.Identity,
                    JsonData = item.ToJson()
                });
            }

            foreach (var item in m_dicTarget.Values)
            {
                m_pTarget.UserPackage.RemoveFromInventory(item.Identity, RemovalType.RemoveAndDisappear);
                //item.OwnerIdentity = m_pTarget.Identity;
                //item.PlayerIdentity = m_pOwner.Identity;
                item.ChangeOwner(m_pOwner);
                m_pOwner.UserPackage.AddItem(item);

                ServerKernel.Log.GmLog("trade_item",
                    string.Format(
                        "user_id[{0}] name[{1}]| item[{2}][{3}] type[{4}] gem1[{5}] gem2[{6}] magic3[{7}] dur[{8}] max_dur[{9}] data[{10}], dec_dmg[{11}] add_life[{12}] color[{13}] Addlevel_exp[{14}] artifact_type[{15}] artifact_expire[{16}] artifact_stabilization[{17}] refinery_type[{18}] refinery_level[{19}] refinery_stabilization[{20}] stack_amount[{21}] remaining_time[{22}]",
                        m_pTarget.Identity, m_pTarget.Name, item.Itemtype.Name, item.Identity, item.Type,
                        item.SocketOne, item.SocketTwo, item.Addition, item.Durability, item.MaxDurability,
                        item.SocketProgress, item.ReduceDamage, item.Enchantment, item.Color, item.AdditionProgress,
                        item.ArtifactType, item.ArtifactExpire, item.ArtifactStabilization,
                        item.RefineryType, item.RefineryLevel, item.RefineryStabilization, item.StackAmount,
                        item.RemainingTime));

                allEntities.Add(new TradeItemEntity
                {
                    Chksum = (uint) item.GetHashCode(),
                    ItemIdentity = item.Identity,
                    Itemtype = item.Type,
                    SenderIdentity = m_pTarget.Identity,
                    TradeIdentity = trade.Identity,
                    JsonData = item.ToJson()
                });
            }

            new TradeItemRepository().Save(allEntities.ToArray());

            m_pOwner.SpendMoney((int) m_dwOwnerMoney);
            m_pOwner.SpendEmoney((int) m_dwOwnerEmoney, EmoneySourceType.Trade, m_pTarget);
            m_pTarget.SpendMoney((int) m_dwTargetMoney);
            m_pTarget.SpendEmoney((int) m_dwTargetEmoney, EmoneySourceType.Trade, m_pOwner);

            m_pOwner.AwardMoney((int) m_dwTargetMoney);
            m_pOwner.AwardEmoney((int) m_dwTargetEmoney, EmoneySourceType.Trade, m_pTarget);
            m_pTarget.AwardMoney((int) m_dwOwnerMoney);
            m_pTarget.AwardEmoney((int) m_dwOwnerEmoney, EmoneySourceType.Trade, m_pOwner);

            m_pOwner.SendSysMessage(Language.StrTradeSucceed);
            m_pTarget.SendSysMessage(Language.StrTradeSucceed);
            return true;
        }

        public bool IsTrading(Character user1, Character user2)
        {
            return (m_pOwner == user1 || m_pTarget == user1) && (m_pOwner == user2 || m_pTarget == user2);
        }

        public void CloseWindow()
        {
            var msg = new MsgTrade {Type = TradeType.HIDE_TABLE, Target = m_pTarget.Identity};
            m_pOwner.Send(msg);
            msg.Target = m_pOwner.Identity;
            m_pTarget.Send(msg);

            m_pTarget.Trade = null;
            m_pOwner.Trade = null;
        }

        public void CloseWindow(MsgTrade msg)
        {
            m_pOwner.Trade = null;
            if (m_pTarget != null)
            {
                msg.Type = TradeType.HIDE_TABLE;
                msg.Target = m_pTarget.Identity;
                m_pOwner.Send(msg);
                msg.Target = m_pOwner.Identity;
                m_pTarget.Owner.Send(msg);
                m_pTarget.Trade = null;
            }
        }

        public void ShowTable()
        {
            var msg = new MsgTrade
            {
                Type = TradeType.SHOW_TABLE,
                Target = m_pOwner.Identity
            };
            m_pTarget.Send(msg);
            msg.Target = m_pTarget.Identity;
            m_pOwner.Send(msg);
        }
    }
}
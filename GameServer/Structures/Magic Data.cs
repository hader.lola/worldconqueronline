﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - Magic Data.cs
// Last Edit: 2019/11/24 19:04
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using FtwCore;
using FtwCore.Common;
using FtwCore.Common.Enums;
using FtwCore.Database.Entities;
using FtwCore.Database.Repositories;
using FtwCore.Networking.Packets;
using GameServer.Structures.Entities;
using GameServer.Structures.Items;
using GameServer.Structures.Managers.GameEvents;
using GameServer.World;

#endregion

namespace GameServer.Structures
{
    public sealed class MagicData
    {
        private const int _MAX_TARGET_NUM = 25;

        public const int MAGICSTATE_NONE = 0, // ÎÞ×´Ì¬
            MAGICSTATE_INTONE = 1, // Ò÷³ª×´Ì¬
            MAGICSTATE_LAUNCH = 2, // Ê©Õ¹×´Ì¬
            MAGICSTATE_DELAY = 3; // ÑÓ³Ù×´Ì¬

        public const int MAGICSORT_ATTACK = 1,
            MAGICSORT_RECRUIT = 2, // support auto active.
            MAGICSORT_CROSS = 3,
            MAGICSORT_FAN = 4, // support auto active(random).
            MAGICSORT_BOMB = 5,
            MAGICSORT_ATTACHSTATUS = 6,
            MAGICSORT_DETACHSTATUS = 7,
            MAGICSORT_SQUARE = 8,
            MAGICSORT_JUMPATTACK = 9, // move, a-lock
            MAGICSORT_RANDOMTRANS = 10, // move, a-lock
            MAGICSORT_DISPATCHXP = 11,
            MAGICSORT_COLLIDE = 12, // move, a-lock & b-synchro
            MAGICSORT_SERIALCUT = 13, // auto active only.
            MAGICSORT_LINE = 14, // support auto active(random).
            MAGICSORT_ATKRANGE = 15, // auto active only, forever active.
            MAGICSORT_ATKSTATUS = 16, // support auto active, random active.
            MAGICSORT_CALLTEAMMEMBER = 17,
            MAGICSORT_RECORDTRANSSPELL = 18,
            MAGICSORT_TRANSFORM = 19,
            MAGICSORT_ADDMANA = 20, // support self target only.
            MAGICSORT_LAYTRAP = 21,
            MAGICSORT_DANCE = 22, // ÌøÎè(only use for client)
            MAGICSORT_CALLPET = 23, // ÕÙ»½ÊÞ
            MAGICSORT_VAMPIRE = 24, // ÎüÑª£¬power is percent award. use for call pet
            MAGICSORT_INSTEAD = 25, // ÌæÉí. use for call pet
            MAGICSORT_DECLIFE = 26, // ¿ÛÑª(µ±Ç°ÑªµÄ±ÈÀý)
            MAGICSORT_GROUNDSTING = 27, // µØ´Ì,
            MAGICSORT_VORTEX = 28,
            MAGICSORT_ACTIVATESWITCH = 29,
            MAGICSORT_SPOOK = 30,
            MAGICSORT_WARCRY = 31,
            MAGICSORT_RIDING = 32,
            MAGICSORT_ATTACHSTATUS_AREA = 34,
            MAGICSORT_REMOTEBOMB = 35, // fuck tq i dont know what name to use _|_
            MAGICSORT_KNOCKBACK = 38,
            MAGICSORT_DASHWHIRL = 40,
            MAGICSORT_PERSEVERANCE = 41,
            MAGICSORT_SELFDETACH = 46,
            MAGICSORT_DETACHBADSTATUS = 47,
            MAGICSORT_CLOSE_LINE = 48,
            MAGICSORT_COMPASSION = 50,
            MAGICSORT_TEAMFLAG = 51,
            MAGICSORT_INCREASEBLOCK = 52,
            MAGICSORT_OBLIVION = 53,
            MAGICSORT_STUNBOMB = 54,
            MAGICSORT_TRIPLEATTACK = 55,
            MAGICSORT_DASHDEADMARK = 61,
            MAGICSORT_MOUNTWHIRL = 64,
            MAGICSORT_TARGETDRAG = 65,
            MAGICSORT_CLOSESCATTER = 67,
            MAGICSORT_ASSASSINVORTEX = 68,
            MAGICSORT_BLISTERINGWAVE = 69; // gotta find better names

        public const int PURE_TROJAN_ID = 10315;
        public const int PURE_WARRIOR_ID = 10311;
        public const int PURE_ARCHER_ID = 10313;
        public const int PURE_NINJA_ID = 6003;
        public const int PURE_MONK_ID = 10405;
        public const int PURE_PIRATE_ID = 11040;
        public const int PURE_WATER_ID = 30000;
        public const int PURE_FIRE_ID = 10310;

        public const int MAGICDAMAGE_ALT = 26; // ·¨ÊõÐ§¹ûµÄ¸ß²îÏÞÖÆ
        public const int AUTOLEVELUP_EXP = -1; // ×Ô¶¯Éý¼¶µÄ±êÖ¾
        public const int DISABLELEVELUP_EXP = 0; // ²»Éý¼¶µÄ±êÖ¾
        public const int AUTOMAGICLEVEL_PER_USERLEVEL = 10; // Ã¿10¼¶£¬·¨ÊõµÈ¼¶¼ÓÒ»¼¶
        public const int USERLEVELS_PER_MAGICLEVEL = 10; // Íæ¼ÒµÈ¼¶±ØÐëÊÇ·¨ÊõµÈ¼¶µÄ10±¶

        public const int KILLBONUS_PERCENT = 5; // É±ËÀ¹ÖÎïµÄ¶îÍâEXP½±Àø
        public const int HAVETUTORBONUS_PERCENT = 10; // ÓÐµ¼Ê¦µÄÇé¿öÏÂ¶îÍâEXP½±Àø
        public const int WITHTUTORBONUS_PERCENT = 20; // ºÍµ¼Ê¦Ò»ÆðÁ·µÄÇé¿öÏÂEXP½±Àø

        public const int MAGIC_DELAY = 1000; // Ä§·¨DELAY
        public const int MAGIC_DECDELAY_PER_LEVEL = 100; // Ã¿¸ö¡°·¨ÊõµÈ¼¶¡±¼õÉÙµÄÄ§·¨DELAY
        public const int RANDOMTRANS_TRY_TIMES = 10; // Ëæ»úË²ÒÆµÄ³¢ÊÔ´ÎÊý
        public const int DISPATCHXP_NUMBER = 20; // ¼ÓXPµÄÊýÁ¿
        public const int COLLIDE_POWER_PERCENT = 80; // ³å×²Ôö¼Ó¹¥»÷Á¦µÄ°Ù·Ö±È
        public const int COLLIDE_SHIELD_DURABILITY = 3; // ³å×²Òª¼õÉÙµÄ¶ÜÅÆÊÙÃü
        public const int LINE_WEAPON_DURABILITY = 2; // Ö±Ïß¹¥»÷Òª¼õÉÙµÄÎäÆ÷ÊÙÃü
        public const int MAX_SERIALCUTSIZE = 10; // Ë³ÊÆÕ¶µÄÊýÁ¿
        public const int AWARDEXP_BY_TIMES = 1; // °´´ÎÊý¼Ó¾­ÑéÖµ
        public const int AUTO_MAGIC_DELAY_PERCENT = 150; // Á¬ÐøÄ§·¨¹¥»÷Ê±Ôö¼ÓµÄDELAY
        public const int BOW_SUBTYPE = 500; // ¹­µÄSUBTYPE
        public const int POISON_MAGIC_TYPE = 1501; // use for more status
        public const int DEFAULT_MAGIC_FAN = 120; // 
        public const int STUDENTBONUS_PERCENT = 5; // µ¼Ê¦É±ËÀÒ»Ö»¹ÖÎïÍ½µÜµÃµ½µÄ¾­Ñé°Ù·Ö±È

        public const int MAGIC_KO_LIFE_PERCENT = 15; // ±ØÉ±¼¼ÄÜÈ¥ÑªÉÏÏÞ
        public const int MAGIC_ESCAPE_LIFE_PERCENT = 15; // ÌÓÅÜ¼¼ÄÜÓÐÐ§µÄÉúÃüÉÏÏÞ

        private Role m_pOwner;

        private bool m_bAutoAttack;
        private int m_nMagicState = MAGICSTATE_NONE;
        private uint m_idTarget;
        private int m_nDelay;
        private int m_nData;
        private int m_nApplyTimes;
        private Point m_pPos = new Point();

        private Magic m_pMagic;

        /// <summary>
        /// This delay is a general one, to avoid people from spamming skills in low ping scenarios.
        /// </summary>
        private TimeOutMS m_tDelay = new TimeOutMS(700);

        private TimeOutMS m_tIntone = new TimeOutMS(0);
        private TimeOutMS m_tApply = new TimeOutMS(0);

        public ConcurrentDictionary<uint, Magic> Magics = new ConcurrentDictionary<uint, Magic>();

        public MagicData(Role owner)
        {
            m_pOwner = owner;

            if (m_pOwner is Character user)
            {
                var allMagics = new MagicRepository().FetchByUser(m_pOwner.Identity);
                if (allMagics != null)
                {
                    foreach (var mgc in allMagics)
                    {
                        var newMgc = new Magic(m_pOwner);
                        if (!newMgc.Create(mgc))
                        {
                            ServerKernel.Log.SaveLog(
                                $"Could not load magic (id:{mgc.Id}) for (targetId:{m_pOwner.Identity})",
                                "magic_error");
                            continue;
                        }

                        //newMgc.SendSkill(SkillAction.AddExsisting);
                        Magics.TryAdd(newMgc.Type, newMgc);
                    }
                }
            }
            else if (m_pOwner is Monster monster)
            {
                var mgc = ServerKernel.MonsterMagics.Where(x => x.OwnerIdentity == monster.Type);
                foreach (var magic in mgc)
                {
                    var newMgc = new Magic(m_pOwner);
                    if (!newMgc.Create(new MagicEntity
                    {
                        Level = magic.MagicLevel,
                        Type = magic.MagicIdentity,
                        OwnerId = magic.OwnerIdentity
                    }))
                    {
                        ServerKernel.Log.SaveLog(
                            $"Could not load magic (id:{magic.Identity}) for (targetId:{magic.OwnerIdentity})",
                            "monstermagic_error");
                        continue;
                    }

                    Magics.TryAdd(newMgc.Type, newMgc);
                }
            }
        }

        public bool CheckCondition(Magic magic, uint idTarget, ref ushort x, ref ushort y)
        {
            // cant use any spell while watching matchs.
            if (m_pOwner is Character watcher && watcher.IsArenaWitness)
                return false;

            if (!m_tDelay.IsTimeOut(MAGIC_DELAY-magic.Level* MAGIC_DECDELAY_PER_LEVEL) && MAGICSORT_COLLIDE != magic.Sort)
                return false;

            if (!magic.IsReady())
                return false;

            if (m_pOwner.Map.IsLineSkillMap() && magic.Sort != MAGICSORT_LINE)
                return false;

            if (!((magic.AutoActive & 1) == 1
                  || (magic.AutoActive & 4) == 4) && magic.Type != 6001)
            {
                if (!Calculations.ChanceCalc(magic.Percent))
                    return false;
            }

            if (m_pOwner.Map.QueryRegion(Map.REGION_PK_PROTECTED, m_pOwner.MapX, m_pOwner.MapY) && m_pOwner is Character)
            {
                if (magic.Ground > 0)
                {
                    if (magic.Crime > 0)
                    {
                        return false;
                    }
                }
                else
                {
                    Role pTarget = m_pOwner.BattleSystem.FindRole(idTarget);
                    if (pTarget != null && pTarget is Character && magic.Crime > 0)
                        return false;
                }
            }

            if (!m_pOwner.Map.IsTrainingMap() && m_pOwner is Character)
            {
                if (m_pOwner.Mana < magic.UseMana)
                    return false;
                if (m_pOwner.Stamina < magic.UseStamina)
                    return false;

                if (magic.UseItem > 0)
                {
                    if (!m_pOwner.CheckWeaponSubType(magic.UseItem, magic.UseItemNum))
                        return false;
                }
            }

            if (magic.UseXp == 1)
            {
                IStatus pStatus = m_pOwner.QueryStatus(FlagInt.START_XP);
                if (pStatus == null && (magic.Status == FlagInt.VORTEX && m_pOwner.QueryStatus(FlagInt.VORTEX) == null))
                    return false;
            }

            if (magic.WeaponSubtype > 0 && m_pOwner is Character)
            {
                if (!m_pOwner.CheckWeaponSubType(magic.WeaponSubtype))
                    return false;
            }

            uint nSort = magic.Sort;
            if ((nSort == MAGICSORT_CALLTEAMMEMBER || nSort == MAGICSORT_RECORDTRANSSPELL)
                && m_pOwner.Map.IsChgMapDisable())
            {
                return false;
            }

            if (nSort == MAGICSORT_RECORDTRANSSPELL &&
                m_pOwner.Map.QueryRegion(Map.REGION_CITY, m_pOwner.MapX, m_pOwner.MapY))
            {
                return false;
            }

            if (m_pOwner is Character
                && (m_pOwner as Character).QueryTransformation != null
                && (m_pOwner as Character).QueryTransformation.Lookface > 0)
            {
                return false;
            }

            if (m_pOwner.IsWing && nSort == MAGICSORT_TRANSFORM)
            {
                return false;
            }

            if (m_pOwner.Map.IsWingDisable() && nSort == MAGICSORT_ATTACHSTATUS && magic.Status == FlagInt.FLY)
            {
                return false;
            }

            Role pRole = null;
            if (magic.Ground <= 0
                && nSort != MAGICSORT_GROUNDSTING
                && nSort != MAGICSORT_VORTEX
                && nSort != MAGICSORT_DASHWHIRL
                && nSort != MAGICSORT_DASHDEADMARK
                && nSort != MAGICSORT_MOUNTWHIRL)
            {
                pRole = m_pOwner.BattleSystem.FindRole(idTarget);
                if (pRole == null)
                    return false;

                if (!pRole.IsAlive
                    && nSort != MAGICSORT_DETACHSTATUS
                    && nSort != MAGICSORT_ATTACHSTATUS
                    && nSort != MAGICSORT_DETACHBADSTATUS)
                    return false;

                if (pRole.Map.IsPkDisable() && pRole is Character user && nSort == MAGICSORT_CLOSE_LINE)
                    return false;

                if (nSort == MAGICSORT_DECLIFE)
                {
                    if (pRole.Life * 100 / pRole.MaxLife >= 15)
                        return false;
                }

                x = pRole.MapX;
                y = pRole.MapY;
            }

            if (HitByMagic() != 0 && !HitByWeapon() && m_pOwner.GetDistance(x, y) > magic.Distance + 1)
            {
                return false;
            }

            if (m_pOwner.GetDistance(x, y) > m_pOwner.GetAttackRange(0) + magic.Distance + 1)
            {
                return false;
            }

            DynamicNpc pNpc = null;
            if (pRole != null && pRole is DynamicNpc)
            {
                pNpc = pRole as DynamicNpc;
            }

            if (pNpc != null)
            {
                if (!pNpc.IsAttackable(m_pOwner))
                    return false;

                if (pNpc.IsGoal() && m_pOwner.Level < pNpc.Level)
                {
                    return false;
                }
            }

            return true;
        }

        public bool MagicAttack(ushort usMagicType, uint idTarget, ushort x, ushort y, byte ucAutoActive = 0)
        {
            if (m_pOwner is Monster)
                m_nMagicState = 0;

            switch (m_nMagicState)
            {
                case MAGICSTATE_INTONE:
                {
                    AbortMagic(true);
                    break;
                }
                case MAGICSTATE_DELAY:
                {
                    return false;
                }
                case MAGICSTATE_LAUNCH:
                {
                    return false;
                }
            }

            if (!(Magics.TryGetValue(usMagicType, out m_pMagic) &&
                  (ucAutoActive == 0 || (m_pMagic.AutoActive & ucAutoActive) != 0)))
            {
                ServerKernel.Log.GmLog("cheat", $"invalid magic type: {usMagicType}, user[{m_pOwner.Name}][{m_pOwner.Identity}]");
                AbortMagic(true);
                return false;
            }

            if (!CheckCondition(m_pMagic, idTarget, ref x, ref y))
            {
                if (m_pMagic.Sort == MAGICSORT_COLLIDE)
                {
                    ProcessCollideFail(x, y, (int)idTarget);		// idTarget: dir
                }

                AbortMagic(true);
                return false;
            }

            if (!m_pMagic.Delay())
                return false;

            m_idTarget = idTarget;
            if (m_pMagic.Ground > 0 && m_pMagic.Sort != MAGICSORT_ATKSTATUS)
                m_idTarget = 0;

            m_bAutoAttack = true;
            if (MAGICSORT_COLLIDE == m_pMagic.Sort)
            {
                m_nData = (int)idTarget;
            }

            m_pPos = new Point(x, y);

            if ((!m_pOwner.Map.IsTrainingMap() && m_pOwner.MapIdentity != 1005) && m_pOwner is Character)
            {
                if (m_pMagic.UseMana > 0)
                    m_pOwner.AddAttrib(ClientUpdateType.Mana, -1 * m_pMagic.UseMana);
                if (m_pMagic.UseStamina > 0)
                    m_pOwner.AddAttrib(ClientUpdateType.Stamina, -1 * m_pMagic.UseStamina);
                if (m_pMagic.UseItem > 0 && m_pMagic.UseItemNum > 0)
                    m_pOwner.SpendEquipItem(m_pMagic.UseItem, m_pMagic.UseItemNum, true);
            }

            if (m_pMagic.UseXp == 1 && m_pOwner is Character)
            {
                IStatus pStatus = m_pOwner.QueryStatus(FlagInt.START_XP);
                if (pStatus == null && (m_pOwner.QueryStatus(FlagInt.VORTEX) == null && m_pOwner.QueryStatus(FlagInt.BLADE_FLURRY) == null))
                    return false;
                m_pOwner.DetachStatus(FlagInt.START_XP);
                (m_pOwner as Character).ClsXpVal();
            }

            if (!IsWeaponMagic(m_pMagic.Type))
            {
                MsgInteract pMsg = new MsgInteract
                {
                    EntityIdentity = m_pOwner.Identity,
                    TargetIdentity = idTarget,
                    CellX = x,
                    CellY = y,
                    MagicType = m_pMagic.Type,
                    MagicLevel = m_pMagic.Level,
                    Action = InteractionType.ACT_ITR_MAGIC_ATTACK
                };
                m_pOwner.Map.SendToRegion(pMsg, m_pOwner.MapX, m_pOwner.MapY);
            }

            if (m_pMagic.UseMana != 0)
            {
                if (!m_pOwner.Map.IsTrainingMap())
                    m_pOwner.DecEquipmentDurability(false, HitByMagic(), (ushort)m_pMagic.UseItemNum);

                if (Calculations.ChanceCalc(7) && m_pOwner is Character user)
                    user.SendGemEffect();
            }

            if (m_pMagic.IntoneSpeed <= 0)
            {
                if (!Launch())
                {
                    m_tApply.Clear();
                    ResetDelay();
                }
                else
                {
                    //if (m_idTarget != 0 && m_pOwner.BattleSystem.FindRole(m_idTarget)?.IsPlayer() == false)
                    if (m_pOwner.Map.IsTrainingMap() && m_pMagic.Sort != MAGICSORT_TRIPLEATTACK)
                    {
                        m_bAutoAttack = true;
                        m_tDelay = new TimeOutMS(Math.Max(2000, m_pMagic.DelayMs));
                        m_tDelay.Update();
                        m_nMagicState = MAGICSTATE_DELAY;
                        return true;
                    }

                    if (m_tApply == null)
                        m_tApply = new TimeOutMS(0);
                    if (m_pMagic == null)
                        return false;

                    m_tApply.Startup((int) m_pMagic.StepSeconds);
                    m_nMagicState = MAGICSTATE_LAUNCH;
                }

                if (m_pMagic.Status != FlagInt.LUCKY_DIFFUSE)
                    m_pOwner.ProcessOnAttack();
            }
            else
            {
                m_nMagicState = MAGICSTATE_INTONE;
                m_tIntone.Startup((int) m_pMagic.IntoneSpeed);
            }

            return true;
        }

        #region Handlers

        public bool Launch()
        {
            bool bRet = false;

            if (m_pMagic == null)
                return false;

            if (!m_pOwner.IsAlive)
            {
                ShowMiss();
                return false;
            }
            
            switch (m_pMagic.Sort)
            {
                case MAGICSORT_ATTACK: bRet = ProcessAttack(); break;
                case MAGICSORT_RECRUIT: bRet = ProcessRecruit(); break;
                case MAGICSORT_FAN: bRet = ProcessFan(); break;
                case MAGICSORT_BOMB: bRet = ProcessBomb(); break;
                case MAGICSORT_ATTACHSTATUS: bRet = ProcessAttach(); break;
                case MAGICSORT_DETACHSTATUS: bRet = ProcessDetach(); break;
                case MAGICSORT_DISPATCHXP: bRet = ProcessDispatchXp(); break;
                case MAGICSORT_COLLIDE: bRet = ProcessCollide(); break;
                case MAGICSORT_LINE: bRet = ProcessLine(); break;
                case MAGICSORT_ATKSTATUS: bRet = ProcessAtkStatus(); break;
                case MAGICSORT_TRANSFORM: bRet = ProcessTransform(); break;
                case MAGICSORT_ADDMANA: bRet = ProcessAddMana(); break;
                case MAGICSORT_GROUNDSTING: bRet = ProcessGroundSting(); break;
                case MAGICSORT_VORTEX: bRet = ProcessVortex(); break;
                case MAGICSORT_ACTIVATESWITCH: bRet = ProcessActivateSwitch(); break;
                case MAGICSORT_RIDING: bRet = ProcessMount(); break;
                case MAGICSORT_ATTACHSTATUS_AREA: bRet = ProcessAttachAreaStatus(); break;
                case MAGICSORT_REMOTEBOMB: bRet = ProcessRemoteBomb(); break;
                case MAGICSORT_KNOCKBACK: bRet = ProcessKnockback(); break;
                case MAGICSORT_DASHDEADMARK:
                case MAGICSORT_MOUNTWHIRL:
                case MAGICSORT_DASHWHIRL: bRet = ProcessDashWhirl(); break;
                case MAGICSORT_SELFDETACH: bRet = ProcessSelfDetach(); break;
                case MAGICSORT_DETACHBADSTATUS: bRet = ProcessDetachBadStatus(); break;
                case MAGICSORT_CLOSE_LINE: bRet = ProcessCloseLine(); break;
                case MAGICSORT_COMPASSION: bRet = ProcessDetachTeam(); break;
                case MAGICSORT_TEAMFLAG: bRet = ProcessTeamFlag(); break;
                case MAGICSORT_INCREASEBLOCK: ProcessIncreaseBlock(); break;
                case MAGICSORT_OBLIVION: bRet = ProcessOblivion(); break;
                case MAGICSORT_STUNBOMB: bRet = ProcessStunBomb(); break;
                case MAGICSORT_TRIPLEATTACK: bRet = ProcessTripleAttack(); break;
                //case MAGICSORT_ASSASSINVORTEX: bRet = ProcessBladeFlurry(); break;
                case MAGICSORT_BLISTERINGWAVE: bRet = ProcessBlisteringWave(); break;
            }

            return bRet;
        }

        #region 1 - Attack

        private bool ProcessAttack()
        {
            if (m_pMagic == null || m_pOwner == null || m_idTarget == 0)
                return false;

            var pTarget = m_pOwner.BattleSystem.FindRole(m_idTarget);
            if (pTarget == null
                || !Calculations.InScreen(m_pOwner.MapX, m_pOwner.MapY, pTarget.MapX, pTarget.MapY)
                || (!pTarget.IsAlive && !pTarget.IsAttackable(m_pOwner)))
            {
                return false;
            }

            if (m_pOwner.IsImmunity(pTarget) || !pTarget.IsAttackable(m_pOwner))
                return false;

            if (m_pMagic.FloorAttr > 0)
            {
                int nAttr = pTarget.Map[pTarget.MapX, pTarget.MapY].Elevation;
                if (nAttr != m_pMagic.FloorAttr)
                    return false;
            }

            if ((m_pOwner.IsWing || pTarget.IsWing) && m_pMagic.Type == 6000)
                return false;

            int nTotalExp = 0;
            InteractionEffect pSpecial = InteractionEffect.None;
            int nPower = m_pOwner.BattleSystem.CalcPower(HitByMagic(), m_pOwner, pTarget, ref pSpecial);

            if (pTarget is Character role)
            {
                if (role.CheckScapegoat(m_pOwner))
                    return true;
            }

            var pMsg = new MsgMagicEffect
            {
                Identity = m_pOwner.Identity,
                SkillIdentity = m_pMagic.Type,
                SkillLevel = m_pMagic.Level,
                CellX = m_pOwner.MapX,
                CellY = m_pOwner.MapY
            };
            pMsg.AppendTarget(pTarget.Identity, (uint)pTarget.BattleSystem.TestAzureShield(nPower), true, (uint)pSpecial, (uint)m_pMagic.GetElementPower(pTarget));
            m_pOwner.Map.SendToRegion(pMsg, m_pOwner.MapX, m_pOwner.MapY);

            CheckCrime(pTarget);

            if (nPower > 0)
            {
                int nLifeLost = (int)Math.Min(pTarget.Life, pTarget.BattleSystem.CheckAzureShield(nPower));
                pTarget.BeAttack(HitByMagic(), m_pOwner, nLifeLost, true);

                if (pTarget is DynamicNpc pNpc && pNpc.IsAwardScore() && m_pOwner is Character character)
                {
                    if (pNpc.IsSynFlag())
                        character.AwardSynWarScore(pNpc, nLifeLost);
                }

                nTotalExp += nLifeLost;
            }

            if (m_pOwner is Character pOwner)
            {
                pOwner.SendWeaponMagic2(pTarget);
            }

            if (nTotalExp > 0)
            {
                AwardExpOfLife(pTarget, nTotalExp);
                AwardExp(nTotalExp, nTotalExp, false);
            }

            if (!pTarget.IsAlive)
                m_pOwner.Kill(pTarget, (uint) GetDieMode());

            return true;
        }

        #endregion
        #region 2 - Recruit
        private bool ProcessRecruit()
        {
            if (m_pMagic == null) return false;

            var setRole = new Dictionary<uint, Role>();
            var setPower = new Dictionary<uint, int>();

            var pRoleOwner = m_pOwner as Character;
            if (pRoleOwner?.Team != null && m_pMagic.Multi > 0)
            {
                foreach (var target in pRoleOwner.Team.Members.Values)
                    if (pRoleOwner.Map.IsInScreen(pRoleOwner, target))
                        setRole.Add(target.Identity, target);
            }
            else
            {
                var pRole = m_pOwner.BattleSystem.FindRole(m_idTarget);
                if (pRole == null || !pRole.IsAlive)
                    return false;

                if (!m_pOwner.Map.IsInScreen(pRole, m_pOwner))
                    return false;

                setRole.Add(pRole.Identity, pRole);
            }

            int nExp = 0;
            var msg = new MsgMagicEffect
            {
                Identity = m_pOwner.Identity,
                SkillIdentity = m_pMagic.Type,
                SkillLevel = m_pMagic.Level
            };

            foreach (var obj in setRole.Values)
            {
                if (!obj.IsAlive) continue;

                var nPower = m_pMagic.Power;
                if (nPower == -32768)
                    nPower = (int)(obj.MaxLife - obj.Life);

                setPower.Add(obj.Identity, nPower);
                msg.AppendTarget(obj.Identity, (uint)nPower, false, 0, 0);
            }
            m_pOwner.Map.SendToRegion(msg, m_pOwner.MapX, m_pOwner.MapY);

            foreach (var obj in setRole.Values)
            {
                if (!obj.IsAlive)
                    continue;

                var nAddLife = Calculations.CutOverflow(setPower[obj.Identity], Calculations.CutTrail(0, obj.MaxLife - obj.Life));
                if (nAddLife > 0)
                {
                    obj.AddAttrib(ClientUpdateType.Hitpoints, nAddLife);
                    // todo broadcast team life
                }

                var pNpc = obj as DynamicNpc;
                if (pNpc != null && pNpc.IsGoal())
                    nAddLife = nAddLife * (10 / 100);

                nExp += (int)nAddLife;
            }

            AwardExp(0, 0, (m_pMagic.Power % 1000));
            return true;
        }
        #endregion
        #region 4 - Fan
        private bool ProcessFan()
        {
            if (m_pMagic == null)
                return false;

            Magic pMagic = m_pMagic;
            var setRole = new Dictionary<uint, Role>();
            var setPower = new Dictionary<uint, int>();
            var pos = new Point();
            int nRange = (int)m_pMagic.Distance + 2;
            int nWidth = DEFAULT_MAGIC_FAN;
            long nExp = 0;
            int nPowerSum = 0;

            if (m_pMagic.Ground != 0)
            {
                pos.X = m_pOwner.MapX;
                pos.Y = m_pOwner.MapY;
            }
            else
            {
                Role pTarget = m_pOwner.FindAroundRole(m_idTarget) as Role;
                if (pTarget == null || !pTarget.IsAlive) return false;

                pos.X = pTarget.MapX;
                pos.Y = pTarget.MapY;
                setRole.Add(pTarget.Identity, pTarget);
            }

            var setTarget = m_pOwner.Map.CollectMapThing(nRange, pos);

            foreach (var pScrnObj in setTarget)
            {
                var posThis = new Point(pScrnObj.MapX, pScrnObj.MapY);
                if (pScrnObj.Identity == m_pOwner.Identity || pScrnObj.Identity == m_idTarget || !Calculations.IsInFan(posThis, pos, nRange, nWidth, m_pPos))
                    continue;

                var pRole = pScrnObj;
                if (pRole.IsAttackable(m_pOwner)
                    && !IsImmunity(pRole))
                    setRole.Add(pRole.Identity, pRole);
            }

            var msg = new MsgMagicEffect
            {
                Identity = m_pOwner.Identity,
                SkillIdentity = m_pMagic.Type,
                SkillLevel = m_pMagic.Level,
                CellX = (ushort)pos.X,
                CellY = (ushort)pos.Y
            };
            var setRemove = new List<uint>();
            foreach (var pRole in setRole.Values)
            {
                if (pRole is Character)
                {
                    if ((pRole as Character).CheckScapegoat(m_pOwner))
                    {
                        setRemove.Add(pRole.Identity);
                        continue;
                    }
                }

                var special = InteractionEffect.None;
                int nPower = m_pOwner.BattleSystem.CalcPower(HitByMagic(), m_pOwner, pRole, ref special);

                setPower.Add(pRole.Identity, nPower);

                if (msg.TargetCount > 29)
                {
                    m_pOwner.Map.SendToRegion(msg, m_pOwner.MapX, m_pOwner.MapY);
                    msg = new MsgMagicEffect
                    {
                        Identity = m_pOwner.Identity,
                        SkillIdentity = m_pMagic.Type,
                        SkillLevel = m_pMagic.Level,
                        CellX = (ushort)pos.X,
                        CellY = (ushort)pos.Y
                    };
                }

                msg.AppendTarget(pRole.Identity, (uint)pRole.BattleSystem.TestAzureShield(nPower), true, (uint)special, (uint)m_pMagic.GetElementPower(pRole));
            }
            m_pOwner.Map.SendToRegion(msg, m_pOwner.MapX, m_pOwner.MapY);

            foreach (var i in setRemove)
                setRole.Remove(i);

            CheckCrime(setRole);

            Character owner = m_pOwner as Character;
            bool bMgc2Dealt = false;
            foreach (var pRole in setRole.Values)
            {
                nPowerSum += setPower[pRole.Identity];

                var pNpc = pRole as DynamicNpc;
                int nLifeLost = (int)Math.Min(pRole.Life, pRole.BattleSystem.CheckAzureShield(setPower[pRole.Identity]));
                pRole.BeAttack(HitByMagic(), m_pOwner, nLifeLost, true);

                if (pNpc != null && pNpc.IsAwardScore())
                {
                    owner?.AwardSynWarScore(pNpc, nLifeLost);
                }

                if (owner != null && (pRole.IsMonster() || pNpc != null && pNpc.IsGoal() && m_pOwner.Level >= pNpc.Level))
                {
                    nExp += owner.AdjustExperience(pRole, nLifeLost, false);
                    if (!pRole.IsAlive)
                    {
                        int nBonusExp = (int)(pRole.MaxLife * 20 / 100);
                        owner.BattleSystem.OtherMemberAwardExp(pRole, nBonusExp);
                        nExp += owner.AdjustExperience(pRole, nBonusExp, false);
                    }
                }

                if (!pRole.IsAlive)
                    m_pOwner.Kill(pRole, GetDieMode());

                if (!bMgc2Dealt && Calculations.ChanceCalc(10f) && m_pOwner is Character)
                {
                    (m_pOwner as Character).SendWeaponMagic2(pRole);
                    bMgc2Dealt = true;
                }
            }

            AwardExp(nExp, nExp, false, pMagic);
            return true;
        }
        #endregion
        #region 5 - Bomb

        private bool ProcessBomb()
        {
            if (m_pMagic == null) return false;
            var setRole = new Dictionary<uint, Role>();
            var setPower = new Dictionary<uint, int>();
            var setRemove = new List<uint>();

            var pos = m_pPos;

            long nExp = 0;
            int nPowerSum = 0;

            var targetLocked = CollectTargetSet_Bomb(ref pos, 0, (int)m_pMagic.Range + 1);

            var msg = new MsgMagicEffect
            {
                CellX = (ushort)pos.X,
                CellY = (ushort)pos.Y,
                Identity = m_pOwner.Identity,
                SkillIdentity = m_pMagic.Type,
                SkillLevel = m_pMagic.Level
            };

            int targetNum = 0;
            foreach (var obj in targetLocked)
            {
                if (obj.Identity != m_pOwner.Identity && obj.IsAttackable(m_pOwner) && !IsImmunity(obj))
                {
                    var special = InteractionEffect.None;
                    int nPower = m_pOwner.BattleSystem.CalcPower(HitByMagic(), m_pOwner, obj, ref special);

                    if (obj is Character)
                    {
                        if ((obj as Character).CheckScapegoat(m_pOwner))
                        {
                            nPower = 0;
                        }
                    }

                    setPower.Add(obj.Identity, nPower);

                    if (targetNum++ < _MAX_TARGET_NUM)
                    {
                        msg.AppendTarget(obj.Identity, (uint)obj.BattleSystem.TestAzureShield(nPower), true, (uint)special, (uint)m_pMagic.GetElementPower(obj));
                    }
                    else
                    {
                        m_pOwner.Map.SendToRegion(msg, (ushort)pos.X, (ushort)pos.Y);
                        msg = new MsgMagicEffect
                        {
                            CellX = (ushort)pos.X,
                            CellY = (ushort)pos.Y,
                            Identity = m_pOwner.Identity,
                            SkillIdentity = m_pMagic.Type,
                            SkillLevel = m_pMagic.Level
                        };
                        msg.AppendTarget(obj.Identity, (uint)obj.BattleSystem.TestAzureShield(nPower), true, (uint)special, (uint)m_pMagic.GetElementPower(obj));
                        targetNum = 0;
                    }

                    setRole.Add(obj.Identity, obj);

                    CheckCrime(obj);
                }
                else
                    setRemove.Add(obj.Identity);
            }

            m_pOwner.Map.SendToRegion(msg, (ushort)pos.X, (ushort)pos.Y);

            foreach (var rem in setRemove)
                setRole.Remove(rem);

            foreach (var obj in setRole.Values)
            {
                nPowerSum += setPower[obj.Identity];

                var pNpc = obj as DynamicNpc;
                int nLifeLost = (int)Math.Min(obj.Life, obj.BattleSystem.CheckAzureShield(setPower[obj.Identity]));

                Character user = m_pOwner as Character;
                if (pNpc != null && pNpc.IsAwardScore() && user != null)
                {
                    (m_pOwner as Character).AwardSynWarScore(pNpc, nLifeLost);
                }

                obj.BeAttack(HitByMagic(), m_pOwner, nLifeLost, true);

                if (user != null && obj.IsMonster()
                    || (pNpc != null && pNpc.IsGoal() && pNpc.Level < m_pOwner.Level))
                {
                    nExp += user.AdjustExperience(obj, nLifeLost, false);

                    if (!obj.IsAlive)
                    {
                        int nBonusExp = (int)(obj.MaxLife * (5 / 100));

                        m_pOwner.BattleSystem.OtherMemberAwardExp(obj, nExp);

                        nExp += user.AdjustExperience(obj, nBonusExp, true);
                    }
                }

                if (!obj.IsAlive)
                    m_pOwner.Kill(obj, GetDieMode());
            }
            
            AwardExp(0, nExp, nExp);
            return true;
        }

        #endregion
        #region 6 - Attach Status

        private bool ProcessAttach()
        {
            if (m_pMagic == null) return false;

            var pRoleUser = m_pOwner.BattleSystem.FindRole(m_idTarget);

            if (pRoleUser == null && m_idTarget == m_pOwner.Identity)
                pRoleUser = m_pOwner;
            if (pRoleUser == null)
                return false;

            if (!pRoleUser.IsAlive && m_pMagic.Target != 64)
                return false;

            Character user = m_pOwner as Character;
            if (m_pMagic.Target == 64)
            {
                if (pRoleUser.IsAlive || !(pRoleUser is Character))
                    return false;

                if (m_pMagic.Status == FlagInt.SHACKLED && pRoleUser.QueryStatus(FlagInt.SHACKLED) != null && user != null)
                {
                    user.SendSysMessage(string.Format(Language.StrTargetAlreadyShacked));
                    return false;
                }
            }

            int nPower = m_pMagic.Power;
            int nSecs = (int)m_pMagic.StepSeconds;
            int nTimes = (int)m_pMagic.ActiveTimes;
            int nStatus = (int)m_pMagic.Status;
            byte pLevel = (byte)m_pMagic.Level;

            if (nPower < 0)
            {
                Program.WriteLog($"ERROR: magic type [{m_pMagic.Type}] status [{nStatus}] invalid power");
                return false;
            }

            if (nSecs == 0)
            {
                nSecs = int.MaxValue;
            }

            uint nDmg = 1;
            switch (nStatus)
            {
                case FlagInt.FLY:
                    {
                        if (pRoleUser.Identity != m_pOwner.Identity)
                            return false;
                        if (!pRoleUser.IsBowman || !pRoleUser.IsAlive)
                            return false; // cant fly
                        if (pRoleUser.Map.IsWingDisable())
                            return false;
                        if (pRoleUser.QueryStatus(FlagInt.RIDING) != null)
                            pRoleUser.DetachStatus(FlagInt.RIDING);
                        break;
                    }
                case FlagInt.SHACKLED:
                case FlagInt.POISON_STAR:
                    {
                        if (m_pOwner.IsImmunity(pRoleUser))
                            return false;
                        float nChance = 100f;
                        if (m_pOwner.BattlePower < pRoleUser.BattlePower)
                        {
                            int nDeltaLev = pRoleUser.BattlePower - m_pOwner.BattlePower;
                            if (nDeltaLev < 20)
                                nChance = 100 - (nDeltaLev * 5);
                            else
                                nChance = 0;
                        }
                        if (nChance < 0 || !Calculations.ChanceCalc(nChance))
                            nDmg = 0;
                        break;
                    }
                case FlagInt.LUCKY_ABSORB:
                    {
                        nStatus = FlagInt.LUCKY_DIFFUSE;

                        if (m_pOwner is Character)
                        {
                            Character pUser = m_pOwner as Character;
                            if (pUser.Booth != null && pUser.Booth.Vending)
                                nDmg = 0;
                        }
                        break;
                    }

                case FlagInt.AZURE_SHIELD:
                {
                    nSecs = 60;
                    break;
                }
                case FlagInt.DEFENSIVE_INSTANCE:
                    {
                        if (m_pOwner.DetachStatus(nStatus))
                        {
                            nDmg = 0;
                            return true;
                        }
                        break;
                    }
                case FlagInt.MAGIC_DEFENDER:
                    {
                        if (m_pOwner.DetachStatus(nStatus))
                        {
                            nDmg = 0;
                        }
                        break;
                    }
            }

            if (nDmg == 0 && (nStatus == FlagInt.LUCKY_DIFFUSE || nStatus == FlagInt.DEFENSIVE_INSTANCE))
                return false;

            var msg = new MsgMagicEffect
            {
                Identity = m_pOwner.Identity,
                SkillIdentity = m_pMagic.Type,
                SkillLevel = m_pMagic.Level
            };
            msg.AppendTarget(pRoleUser.Identity, nDmg, nDmg != 0, 0, 0);
            m_pOwner.Map.SendToRegion(msg, m_pOwner.MapX, m_pOwner.MapY);

            CheckCrime(pRoleUser);

            if (nDmg == 0)
                return false;

            pRoleUser.AttachStatus(pRoleUser, nStatus, nPower, nSecs, nTimes, pLevel);

            int nExp = 3;
            if (m_pOwner.Map.IsTrainingMap())
                nExp = 1;

            if (pRoleUser is Character pRoleChar)
            {
                if (nPower >= 30000)
                {
                    var nPowerTimes = (nPower - 30000) - 100;
                    switch (nStatus)
                    {
                        case FlagInt.STIG: // stigma
                            pRoleChar.SendSysMessage(string.Format(Language.StrStigmaActiveP,
                                    nSecs, nPowerTimes));
                            break;
                        case FlagInt.STAR_OF_ACCURACY: // accuracy
                            pRoleChar.SendSysMessage(string.Format(Language.StrAccuracyActiveP,
                                    nSecs, nPowerTimes));
                            break;
                        case FlagInt.SHIELD: // shield
                            pRoleChar.SendSysMessage(string.Format(Language.StrShieldActiveP,
                                    nSecs, nPowerTimes));
                            break;
                        case FlagInt.AZURE_SHIELD: // azure shield
                            break;
                        case FlagInt.DODGE: // dodge
                            pRoleChar.SendSysMessage(string.Format(Language.StrDodgeActiveP,
                                    nSecs, nPowerTimes));
                            break;
                    }
                }
                else
                {
                    int nPowerTimes = nPower;
                    switch (nStatus)
                    {
                        case FlagInt.STIG: // stigma
                            pRoleChar.SendSysMessage(string.Format(Language.StrStigmaActiveT,
                                    nSecs, nPowerTimes));
                            break;
                        case FlagInt.STAR_OF_ACCURACY: // accuracy
                            pRoleChar.SendSysMessage(string.Format(Language.StrAccuracyActiveT,
                                    nSecs, nPowerTimes));
                            break;
                        case FlagInt.SHIELD: // shield
                            pRoleChar.SendSysMessage(string.Format(Language.StrShieldActiveT,
                                    nSecs, nPowerTimes));
                            break;
                        case FlagInt.AZURE_SHIELD: // azure shield
                            break;
                        case FlagInt.DODGE: // dodge
                            pRoleChar.SendSysMessage(string.Format(Language.StrDodgeActiveT,
                                    nSecs, nPowerTimes));
                            break;
                    }
                }

                AwardExp(0, nExp, false);
            }

            return true;
        }
        #endregion
        #region 7 - Detach Status

        private bool ProcessDetach()
        {
            if (m_pMagic == null)
                return false;
            
            var pRole = m_pOwner.BattleSystem.FindRole(m_idTarget) as Role;
            if (pRole == null) return false;

            int nPower = 0;
            int nSecs = (int)m_pMagic.StepSeconds;
            int nTimes = (int)m_pMagic.ActiveTimes;
            int nStatus = (int)m_pMagic.Status;

            if (!pRole.IsAlive)
            {
                if (nStatus != 0)
                    return false;

                if (!(pRole is Character))
                    return false;
            }

            if (nStatus == 0)
                if (pRole.Map.IsPkField())
                    return false;

            if (nStatus == 0 && pRole is Character)
            {
                // reborn
                (pRole as Character).Reborn(false, true);
                pRole.Send(new MsgMapInfo(pRole.Map.Identity,
                    pRole.Map.MapDoc, pRole.Map.Flag));
            }
            else
            {
                switch (m_pMagic.Status)
                {
                    case FlagInt.FLY:
                        if (pRole is Character pUser) // if is character
                        {
                            if (!pUser.IsWing) // the target is flying?
                                break;//return false;

                            if (pUser.BattlePower <= m_pOwner.BattlePower) // caster does have more bp than victim
                            {
                                pUser.DetachStatus(FlagInt.FLY);
                                nPower = (int)(pUser.MaxLife * ((m_pMagic.Power % 10000) / 100f));
                            }
                            else
                            {
                                var diff = m_pOwner.BattlePower - pUser.BattlePower; // get the diff
                                float pct = 100;
                                if (diff > 19) // if diff >= 20
                                    pct = 5; // 5 pct chance only
                                else
                                    pct -= diff * 5; // oooor
                                if (Calculations.ChanceCalc(pct)) // if success, remove fly of target
                                {
                                    pUser.DetachStatus(FlagInt.FLY);
                                    nPower = (int)(pUser.MaxLife * ((m_pMagic.Power % 10000) / 100f));
                                }
                            }
                        }
                        else // isnt character, return
                            return false;
                        break;
                }
            }

            var msg = new MsgMagicEffect
            {
                Identity = m_pOwner.Identity,
                SkillIdentity = m_pMagic.Type,
                SkillLevel = m_pMagic.Level
            };
            msg.AppendTarget(pRole.Identity, (uint)nPower, true, 0, 0);
            pRole.Map.SendToRegion(msg, pRole.MapX, pRole.MapY);

            if (nPower > 0)
            {
                int nLifeLost = (int)Math.Min(pRole.Life, nPower);
                pRole.BeAttack(HitByMagic(), m_pOwner, nLifeLost, true);
            }

            return true;
        }

        #endregion
        #region 11 - Dispatch XP

        private bool ProcessDispatchXp()
        {
            if (m_pMagic == null) return false;

            Dictionary<uint, Character> setUser = new Dictionary<uint, Character>();
            Dictionary<uint, int> setPower = new Dictionary<uint, int>();

            Character pOwner = null;

            if (m_pOwner is Character owner)
                pOwner = owner;

            if (pOwner?.Team != null)
            {
                var pTeam = pOwner.Team;
                string szMessage = string.Format(Language.StrDispatchXp, pOwner.Name);

                foreach (var member in pTeam.Members.Values.Where(x => x.Identity != pOwner.Identity && x.IsAlive))
                {
                    if (member.GetDistance(pOwner.MapX, pOwner.MapY) > Calculations.SCREEN_DISTANCE)
                        continue;

                    member.AddXp(20);
                    setUser.Add(member.Identity, member);
                    setPower.Add(member.Identity, 20);
                    member.SendMessage(szMessage);
                }
            }

            var msg = new MsgMagicEffect
            {
                Identity = m_pOwner.Identity,
                SkillIdentity = m_pMagic.Type,
                SkillLevel = m_pMagic.Level,
                CellX = (ushort)m_pPos.X,
                CellY = (ushort)m_pPos.Y
            };

            foreach (var tgt in setUser.Values)
                msg.AppendTarget(tgt.Identity, (uint)setPower[tgt.Identity], true, 0, 0);

            m_pOwner.Map.SendToRegion(msg, m_pOwner.MapX, m_pOwner.MapY);
            AwardExp(0, 0, 0, m_pMagic);
            return true;
        }

        #endregion
        #region 12 - Collide

        private bool ProcessCollide()
        {
            if (m_pMagic == null || m_pOwner == null)
                return false;

            Character user = m_pOwner as Character;
            if (user != null && !m_pOwner.SynchroPosition(m_pPos.X, m_pPos.Y))
            {
                ServerKernel.UserManager.KickoutSocket(user, "ProcessCollide.SynchroPosition");
                return false;
            }

            int nDir = m_nData % 8;
            ushort nTargetX = (ushort)(m_pPos.X + ScreenObject.DeltaX[nDir]);
            ushort nTargetY = (ushort)(m_pPos.Y + ScreenObject.DeltaY[nDir]);

            if (!m_pOwner.Map.IsStandEnable(nTargetX, nTargetY))
            {
                m_pOwner.Send(new MsgTalk(Language.StrInvalidMsg, ChatTone.TopLeft));
                if (user != null)
                    ServerKernel.UserManager.KickoutSocket(user, "ProcessCollide Invalid msg");
                return false;
            }

            // search the target
            bool bSuc = true;

            Role pTarget = m_pOwner.Map.QueryRole(nTargetX, nTargetY) as Role;
            if (pTarget == null || !pTarget.IsAlive)
                bSuc = false;

            if (pTarget == null || IsImmunity(pTarget))
                bSuc = false;

            int nPower = 0;
            bool bBackEnable = false;
            uint idTarget = 0;

            DynamicNpc pNpc = null;
            InteractionEffect pSpecial = InteractionEffect.None;
            if (pTarget != null)
            {
                pNpc = pTarget as DynamicNpc;

                if (bSuc)
                {
                    bBackEnable = m_pOwner.Map.IsStandEnable((ushort)(nTargetX + ScreenObject.DeltaX[nDir]),
                        (ushort)(nTargetY + ScreenObject.DeltaY[nDir]));

                    if (pNpc != null)
                        bBackEnable = false;

                    if (!bBackEnable)
                        nDir = 0;

                    if (HitByWeapon())
                    {
                        nPower += m_pOwner.BattleSystem.CalcAttackPower(m_pOwner, pTarget, ref pSpecial);

                        if (bBackEnable || pNpc != null)
                            nPower = Calculations.MulDiv(nPower, 80, 100);
                        else
                            nPower = Calculations.AdjustDataEx(nPower, m_pMagic.Power, 0);
                    }

                    if (!m_pOwner.Map.IsTrainingMap() && m_pOwner is Character)
                        (m_pOwner as Character).AddEquipmentDurability(ItemPosition.LeftHand, -1 * 3);
                }

                idTarget = pTarget.Identity;
            }

            uint dwData = (uint)(nDir * 0x01000000 + pTarget.BattleSystem.TestAzureShield(nPower));
            if (!bBackEnable)
                dwData += 1 * 0x10000000;

            MsgInteract pMsg = new MsgInteract
            {
                EntityIdentity = m_pOwner.Identity,
                TargetIdentity = idTarget,
                CellX = nTargetX,
                CellY = nTargetY,
                Data = dwData,
                ActivationType = pSpecial
            };
            m_pOwner.Map.SendToRegion(pMsg, m_pOwner.MapX, m_pOwner.MapY);

            if (user != null && user.IsPm())
                user.Send(new MsgTalk($"bump move:({m_pOwner.MapX},{m_pOwner.MapY})->({nTargetX},{nTargetY})", ChatTone.Talk));

            if (bBackEnable && user != null)
            {
                user.ProcessOnMove(MovementType.COLLIDE);
                user.MoveToward((FacingDirection)nDir, true);
            }

            if (pTarget != null && bBackEnable && bSuc && user != null)
            {
                user.ProcessOnMove(MovementType.COLLIDE);
                user.MoveToward((FacingDirection)nDir, true);
                // not synchro yet
            }

            if (pTarget != null)
                CheckCrime(pTarget);

            if (nPower > 0 && pTarget != null)
            {
                int nLifeLost = (int)Math.Min(pTarget.BattleSystem.CheckAzureShield(nPower), pTarget.Life);

                if (pNpc != null && pNpc.IsAwardScore() && m_pOwner is Character character)
                    character.AwardSynWarScore(pNpc, nLifeLost);

                pTarget.BeAttack(HitByMagic(), m_pOwner, nLifeLost, true);

                if (user != null && pTarget.IsMonster() || pNpc != null && pNpc.IsGoal() && m_pOwner.Level >= pNpc.Level)
                {
                    long nExp = user.AdjustExperience(pTarget, nLifeLost, false);

                    if (!pTarget.IsAlive)
                    {
                        int nBonusExp = (int)(pTarget.MaxLife * 0.05f);
                        user.BattleSystem.OtherMemberAwardExp(pTarget, nBonusExp);

                        nExp += user.AdjustExperience(pTarget, nBonusExp, true);
                    }

                    AwardExp(0, nExp, nExp, m_pMagic);
                }

                if (!pTarget.IsAlive)
                    m_pOwner.Kill(pTarget, GetDieMode());
            }

            return true;
        }

        #endregion
        #region 14 - Line

        private bool ProcessLine()
        {
            if (m_pMagic == null)
                return false;

            var setTarget = new Dictionary<uint, Role>();
            var setPower = new Dictionary<uint, int>();
            var setPoint = new List<Point>();

            long nExp = 0;
            int nPowerSum = 0;
            
            var pos = new Point(m_pOwner.MapX, m_pOwner.MapY);

            Calculations.DDALine(pos.X, pos.Y, m_pPos.X, m_pPos.Y, (int)m_pMagic.Range, ref setPoint);

            foreach (var point in setPoint)
            {
                try
                {
                    if (m_pOwner.Map[point.X, point.Y].Access < TileType.Npc)
                        continue;

                    if (m_pOwner.Map[point.X, point.Y].Elevation - m_pOwner.Map[m_pOwner.MapX, m_pOwner.MapY].Elevation > 26)
                        continue;
                }
                catch { continue; }

                var pTarget = m_pOwner.Map.QueryRole((ushort)point.X, (ushort)point.Y) as Role;
                if (pTarget == null
                    || IsImmunity(pTarget)
                    || !pTarget.IsAttackable(m_pOwner))
                    continue;

                if (pTarget.Identity != m_pOwner.Identity)
                    setTarget.Add(pTarget.Identity, pTarget);
            }

            var msg = new MsgMagicEffect
            {
                Identity = m_pOwner.Identity,
                SkillIdentity = m_pMagic.Type,
                SkillLevel = m_pMagic.Level,
                CellX = (ushort)m_pPos.X,
                CellY = (ushort)m_pPos.Y
            };

            var setRemove = new List<Role>();
            int nTargetNum = 0;
            foreach (var pTarget in setTarget.Values)
            {
                if (pTarget.IsAttackable(m_pOwner) && !IsImmunity(pTarget))
                {
                    var special = InteractionEffect.None;
                    int nPower = m_pOwner.BattleSystem.CalcPower(HitByMagic(), m_pOwner, pTarget, ref special);

                    if (pTarget is Character)
                    {
                        if ((pTarget as Character).CheckScapegoat(m_pOwner))
                        {
                            nPower = 0;
                        }
                    }

                    if (++nTargetNum < _MAX_TARGET_NUM)
                    {
                        msg.AppendTarget(pTarget.Identity, (uint)pTarget.BattleSystem.TestAzureShield(nPower), true
                            , (byte)special, (uint)m_pMagic.GetElementPower(pTarget));
                    }
                    else
                    {
                        m_pOwner.Map.SendToRegion(msg, (ushort)pos.X, (ushort)pos.Y);
                        msg = new MsgMagicEffect
                        {
                            CellX = (ushort)pos.X,
                            CellY = (ushort)pos.Y,
                            Identity = m_pOwner.Identity,
                            SkillIdentity = m_pMagic.Type,
                            SkillLevel = m_pMagic.Level
                        };
                        msg.AppendTarget(pTarget.Identity, (uint) pTarget.BattleSystem.TestAzureShield(nPower), true, 
                            (byte)special, (uint)m_pMagic.GetElementPower(pTarget));
                        nTargetNum = 0;
                    }
                    setPower.Add(pTarget.Identity, nPower);
                    CheckCrime(pTarget);

                }
                else
                    setRemove.Add(pTarget);
            }

            foreach (var remove in setRemove)
                setTarget.Remove(remove.Identity);

            m_pOwner.Map.SendToRegion(msg, m_pOwner.MapX, m_pOwner.MapY);

            Character user = m_pOwner as Character;
            foreach (var pTarget in setTarget.Values)
            {
                DynamicNpc pNpc = null;
                if (pTarget is DynamicNpc)
                    pNpc = pTarget as DynamicNpc;

                int nLifeLost = (int)Math.Min(pTarget.Life, pTarget.BattleSystem.CheckAzureShield(setPower[pTarget.Identity]));

                nPowerSum += nLifeLost;

                if (pNpc != null && pNpc.IsAwardScore() && m_pOwner is Character)
                {
                    (m_pOwner as Character).AwardSynWarScore(pNpc, nLifeLost);
                }

                pTarget.BeAttack(HitByMagic(), m_pOwner, nLifeLost, true);

                if (user != null && (pTarget.IsMonster() || pNpc != null && pNpc.IsGoal() && pTarget.Level < m_pOwner.Level))
                {
                    nExp += user.AdjustExperience(pTarget, nLifeLost, false);
                    if (!pTarget.IsAlive)
                    {
                        int nBonusExp = (int)(pTarget.MaxLife * (5 / 100));

                        user.BattleSystem.OtherMemberAwardExp(pTarget, nBonusExp);

                        nExp += user.AdjustExperience(m_pOwner, nBonusExp, true);
                    }
                }

                if (user != null && pTarget is Character target)
                {
                    if (user.MapIdentity == LineSkillPkTournament.MAP_ID_U)
                    {
                        ServerKernel.LineSkillPk.Hit(user, target);
                    }
                }

                if (!pTarget.IsAlive)
                    m_pOwner.Kill(pTarget, GetDieMode());
            }

            AwardExp(0, 0, nExp);
            return true;
        }

        #endregion
        #region 16 - Attack Status
        private bool ProcessAtkStatus()
        {
            if (m_pMagic == null) return false;

            var pTarget = m_pOwner.BattleSystem.FindRole(m_idTarget) as Role;
            if (pTarget == null)
                return false;

            if (!pTarget.IsAttackable(m_pOwner) && !pTarget.IsAlive)
                return false;

            if (IsImmunity(pTarget))
                return false;

            int nPower = 0;
            bool bAttachStatus = false;
            var special = InteractionEffect.None;

            if (HitByWeapon())
            {
                switch (m_pMagic.Status)
                {
                    case 0:
                        break;
                    default:
                        {
                            nPower = m_pOwner.BattleSystem.CalcAttackPower(m_pOwner, pTarget, ref special);
                            break;
                        }
                }
            }

            var msg = new MsgMagicEffect
            {
                Identity = m_pOwner.Identity,
                SkillIdentity = m_pMagic.Type,
                SkillLevel = m_pMagic.Level
            };
            msg.AppendTarget(pTarget.Identity, (uint)pTarget.BattleSystem.TestAzureShield(nPower), false, (byte)special, (uint)m_pMagic.GetElementPower(pTarget));
            m_pOwner.Map.SendToRegion(msg, pTarget.MapX, pTarget.MapY);

            CheckCrime(pTarget);

            var pNpc = pTarget as DynamicNpc;
            Character user = m_pOwner as Character;
            long nExp = 0;
            if (nPower > 0)
            {
                int nLifeLost = (int) Math.Min(pTarget.Life, pTarget.BattleSystem.CheckAzureShield(nPower));
                if (pNpc != null && pNpc.IsAwardScore())
                {
                    user?.AwardSynWarScore(pNpc, nLifeLost);
                }

                pTarget.BeAttack(HitByMagic(), m_pOwner, nLifeLost, true);

                if (user != null && (pTarget.IsMonster()
                    || (pNpc != null && pNpc.IsGoal() && pNpc.Level < m_pOwner.Level)))
                {
                    nExp += user.AdjustExperience(pTarget, nLifeLost, false);
                    if (!pTarget.IsAlive)
                    {
                        int nBonusExp = (int)(pTarget.MaxLife * (5 / 100));
                        user.BattleSystem.OtherMemberAwardExp(pTarget, nBonusExp);
                        nExp += user.AdjustExperience(pTarget, nBonusExp, true);
                    }
                }
            }

            AwardExp(0, nExp, m_pMagic.Power % 100);

            if (!pTarget.IsAlive)
                pTarget.BeKill(m_pOwner);

            return true;
        }
        #endregion
        #region 19 - Transform

        private bool ProcessTransform()
        {
            if (m_pMagic == null || !(m_pOwner is Character))
                return false;

            var msg = new MsgMagicEffect
            {
                Identity = m_pOwner.Identity,
                SkillIdentity = m_pMagic.Type,
                SkillLevel = m_pMagic.Level,
                CellX = (ushort)m_pPos.X,
                CellY = (ushort)m_pPos.Y
            };
            m_pOwner.Map.SendToRegion(msg, m_pOwner.MapX, m_pOwner.MapY);
            ((Character) m_pOwner).Transform((uint)m_pMagic.Power, (int)m_pMagic.StepSeconds, true);
            return true;
        }

        #endregion
        #region 20 - Add Mana

        private bool ProcessAddMana()
        {
            if (m_pMagic == null) return false;

            int nAddMana = m_pMagic.Power;
            nAddMana = (int) Calculations.CutOverflow(nAddMana, m_pOwner.MaxMana - m_pOwner.Mana);

            MsgMagicEffect pMsg = new MsgMagicEffect
            {
                Identity = m_pOwner.Identity,
                CellX = m_pOwner.MapX,
                CellY = m_pOwner.MapY,
                SkillIdentity = m_pMagic.Type,
                SkillLevel = m_pMagic.Level
            };
            pMsg.AppendTarget(m_pOwner.Identity, (uint)nAddMana, true, 0, 0);
            m_pOwner.Map.SendToRegion(pMsg, m_pOwner.MapX, m_pOwner.MapY);

            m_pOwner.AddAttrib(ClientUpdateType.Mana, nAddMana);

            int nExp = m_pMagic.Power;
            if (m_pOwner.Map.IsTrainingMap())
                nExp /= 10;

            AwardExp(0, 0, nExp);
            return true;
        }

        #endregion
        #region 27 - Ground Sting
        private bool ProcessGroundSting()
        {
            if (m_pMagic == null) return false;
            if (m_pMagic.Status <= 0) return false;

            var setRole = new Dictionary<uint, Role>();
            var pos = new Point(m_pPos.X, m_pPos.Y);

            var targetLocked = CollectTargetSet_Bomb(ref pos, 0, (int)m_pMagic.Range);

            var msg = new MsgMagicEffect
            {
                CellX = (ushort)pos.X,
                CellY = (ushort)pos.Y,
                Identity = m_pOwner.Identity,
                SkillIdentity = m_pMagic.Type,
                SkillLevel = m_pMagic.Level
            };

            int targetNum = 0;
            var remove = new List<Role>();
            foreach (var obj in targetLocked)
            {
                Monster mob = obj as Monster;
                if (obj.IsAttackable(m_pOwner)
                    && obj != m_pOwner)
                {
                    if (obj.GetDistance(pos.X, pos.Y) > m_pMagic.Range || !(obj is Character) && mob == null)
                    {
                        remove.Add(obj);
                        continue;
                    }

                    if (mob != null && (mob.IsGuard() || mob.IsDynaMonster()))
                    {
                        remove.Add(obj);
                        continue;
                    }

                    if (mob != null && mob.IsBoss)
                    {
                        msg.AppendTarget(obj.Identity, 0, true, 0, 0);
                        remove.Add(obj);
                        continue;
                    }

                    if (IsImmunity(obj))
                    {
                        remove.Add(obj);
                        continue;
                    }

                    int nDeltaBp = 0;
                    float nChance = 0;
                    if (m_pOwner.BattlePower >= obj.BattlePower || !(m_pOwner is Character))
                        nChance = 100f;
                    else
                    {
                        nDeltaBp = obj.BattlePower - m_pOwner.BattlePower;
                        nChance = nDeltaBp >= 20 ? 0 : 100 - (nDeltaBp * 5);
                    }

                    uint nDmg = 1;
                    if (!Calculations.ChanceCalc(nChance))
                        nDmg = 0;

                    if (nDmg <= 0)
                        remove.Add(obj);

                    if (++targetNum < _MAX_TARGET_NUM)
                    {
                        msg.AppendTarget(obj.Identity, nDmg, true, 0, 0);
                    }
                    else
                    {
                        m_pOwner.Map.SendToRegion(msg, (ushort)pos.X, (ushort)pos.Y);
                        msg = new MsgMagicEffect
                        {
                            CellX = (ushort)pos.X,
                            CellY = (ushort)pos.Y,
                            Identity = m_pOwner.Identity,
                            SkillIdentity = m_pMagic.Type,
                            SkillLevel = m_pMagic.Level
                        };
                        msg.AppendTarget(obj.Identity, nDmg, true, 0, 0);
                        targetNum = 0;
                    }

                    if (nDmg > 0)
                        CheckCrime(obj);
                }
                else
                {
                    remove.Add(obj);
                }
            }
            m_pOwner.Map.SendToRegion(msg, (ushort)pos.X, (ushort)pos.Y);

            foreach (var rem in remove)
                targetLocked.Remove(rem);

            foreach (var obj in targetLocked)
            {
                obj.AttachStatus(obj, (int)m_pMagic.Status, m_pMagic.Power,
                    (int)m_pMagic.StepSeconds, (int)m_pMagic.ActiveTimes, 0);
            }

            int nExp = m_pMagic.Power % 100;
            if (m_pOwner.Map.IsTrainingMap())
                nExp /= 5;

            AwardExp(0, nExp, false);

            return true;
        }
        #endregion
        #region 28 - Vortex

        private bool ProcessVortex()
        {
            if (m_pMagic == null)
                return false;

            if (m_pOwner.QueryStatus(FlagInt.VORTEX) != null)// && m_pMagic.IsReady()) // vortex active
            {
                m_pMagic.Delay();

                var pos = new Point();

                long nExp = 0;
                int nPowerSum = 0;

                var targetLocked = CollectTargetSet_Bomb(ref pos, 0, (int)m_pMagic.Range);

                var msg = new MsgMagicEffect
                {
                    CellX = m_pOwner.MapX,
                    CellY = m_pOwner.MapY,
                    Identity = m_pOwner.Identity,
                    SkillIdentity = m_pMagic.Type,
                    SkillLevel = m_pMagic.Level
                };

                Character user = m_pOwner as Character;
                int targetNum = 0, nCount = 0;
                foreach (var obj in targetLocked)
                {
                    if (obj.Identity != m_pOwner.Identity
                        && obj.IsAttackable(m_pOwner))
                    {
                        if (IsImmunity(obj))
                            continue;

                        var special = InteractionEffect.None;
                        int nPower = m_pOwner.BattleSystem.CalcPower(HitByMagic(), m_pOwner, obj, ref special);//, ref special);

                        if (user != null)
                        {
                            if (user.CheckScapegoat(m_pOwner))
                            {
                                nPower = 0;
                            }
                        }

                        if (++targetNum < _MAX_TARGET_NUM)
                        {
                            msg.AppendTarget(obj.Identity, (uint)obj.BattleSystem.TestAzureShield(nPower), true, (byte)special, (uint)m_pMagic.GetElementPower(obj));
                        }
                        else
                        {
                            m_pOwner.Map.SendToRegion(msg, (ushort)m_pOwner.MapX, (ushort)m_pOwner.MapY);
                            msg = new MsgMagicEffect
                            {
                                CellX = (ushort)pos.X,
                                CellY = (ushort)pos.Y,
                                Identity = m_pOwner.Identity,
                                SkillIdentity = m_pMagic.Type,
                                SkillLevel = m_pMagic.Level
                            };
                            msg.AppendTarget(obj.Identity, (uint)obj.BattleSystem.TestAzureShield(nPower), true, (byte)special, (uint)m_pMagic.GetElementPower(obj));
                            targetNum = 0;
                            nCount++;
                        }

                        CheckCrime(obj);

                        var pNpc = obj as DynamicNpc;
                        int nLifeLost = (int)Math.Min(obj.BattleSystem.CheckAzureShield(nPower), obj.Life);
                        nPowerSum += nLifeLost;
                        if (pNpc != null && pNpc.IsAwardScore())
                        {
                            user?.AwardSynWarScore(pNpc, nLifeLost);
                        }

                        obj.BeAttack(HitByMagic(), m_pOwner, nLifeLost, true);

                        if (user != null && (obj.IsMonster()
                            || pNpc != null && pNpc.IsGoal() && pNpc.Level < m_pOwner.Level))
                        {
                            nExp += user.AdjustExperience(obj, nLifeLost, false);

                            if (!obj.IsAlive)
                            {
                                int nBonusExp = (int)(obj.MaxLife * (5 / 100));
                                user.BattleSystem.OtherMemberAwardExp(obj, nExp);
                                nExp += user.AdjustExperience(obj, nBonusExp, true);
                                m_pOwner.Kill(obj, GetDieMode());
                            }
                        }
                    }
                }

                m_pOwner.Map.SendToRegion(msg, (ushort)m_pOwner.MapX, (ushort)m_pOwner.MapY);
                AwardExp(0, nExp, 0);
                m_nMagicState = MAGICSTATE_NONE;
            }
            else
            {
                int status = (int)m_pMagic.Status;
                int power = m_pMagic.Power;
                int step = (int)m_pMagic.StepSeconds;
                int times = (int)m_pMagic.ActiveTimes;

                AbortMagic(true);

                if (!m_pOwner.IsAlive)
                    return false;

                if (m_pOwner.IsWing)
                    return false;

                m_pOwner.AttachStatus(m_pOwner, status, power, step, times, 0);
            }

            return true;
        }

        #endregion
        #region 29 - Activate Switch

        private bool CheckActivateSwitch()
        {
            if (m_idTarget == 0) return false;

            Role pRoleTarget = m_pOwner.BattleSystem.FindRole(m_idTarget);
            if (!(pRoleTarget is Character))
                return false;

            Character pTarget = pRoleTarget as Character;
            return pTarget.CheckScapegoat(m_pOwner);
        }

        private bool ProcessActivateSwitch()
        {
            if (m_idTarget == 0) return false;

            Role pRoleTarget = m_pOwner.BattleSystem.FindRole(m_idTarget);
            if (!pRoleTarget.IsAlive)
                return false;

            InteractionEffect pSpecial = InteractionEffect.None;
            int nDamage = m_pOwner.BattleSystem.CalcPower(HitByMagic(), m_pOwner, pRoleTarget, ref pSpecial, 0, false);
            int nTotalExp = 0;

            var pMsg = new MsgMagicEffect
            {
                Identity = m_pOwner.Identity,
                SkillIdentity = m_pMagic.Type,
                SkillLevel = m_pMagic.Level
            };
            pMsg.AppendTarget(pRoleTarget.Identity, (uint)nDamage, true, (byte)pSpecial, 0);
            m_pOwner.Map.SendToRegion(pMsg, pRoleTarget.MapX, pRoleTarget.MapY);

            if (nDamage > 0)
            {
                int nLifeLost = (int)Math.Min(pRoleTarget.Life, pRoleTarget.BattleSystem.CheckAzureShield(nDamage));
                pRoleTarget.BeAttack(HitByMagic(), m_pOwner, nLifeLost, true);
                nTotalExp += nLifeLost;
            }

            if (nTotalExp > 0)
            {
                AwardExpOfLife(pRoleTarget, nTotalExp);
            }

            if (!pRoleTarget.IsAlive)
                m_pOwner.Kill(pRoleTarget, GetDieMode());
            return true;
        }

        #endregion
        #region 32 - Mount
        private bool ProcessMount()
        {
            if (m_pOwner == null)
                return false;

            if (!(m_pOwner is Character))
            {
                Program.WriteLog($"ERROR: [{m_pOwner.Identity}] {m_pOwner.Name} tried to use ProcessMount().");
                return false;
            }

            Character pUser = m_pOwner as Character;
            
            Item mount = pUser.UserPackage[ItemPosition.Steed];
            if (mount == null)
                return false;

            if (pUser.QueryStatus(FlagInt.FLY) != null)
            {
                return false;
            }

            if (pUser.QueryStatus(FlagInt.RIDING) != null)
            {
                pUser.DetachStatus(FlagInt.RIDING);
                return true;
            }

            if (pUser.Map.Identity == 1039 || (m_pOwner.Map.Identity == 1036 && mount.Addition < 6))
                return false;

            pUser.Vigor = pUser.MaxVigor;
            pUser.AttachStatus(m_pOwner, FlagInt.RIDING, 0, (int)m_pMagic.StepSeconds, 0, 0);
            return true;
        }
        #endregion
        #region 34 - Attach Area Status

        private bool ProcessAttachAreaStatus()
        {
            if (m_pMagic == null) return false;
            var setRole = new Dictionary<uint, Role>();
            var setPower = new Dictionary<uint, int>();

            var pos = m_pPos;

            int nExp = 0;
            int nPowerSum = 0;

            var lockedTarget = CollectTargetSet_Bomb(ref pos, 0, (int)m_pMagic.Range + 1);

            var msg = new MsgMagicEffect
            {
                CellX = (ushort)pos.X,
                CellY = (ushort)pos.Y,
                Identity = m_pOwner.Identity,
                SkillIdentity = m_pMagic.Type,
                SkillLevel = m_pMagic.Level
            };

            int nPower = m_pMagic.Power;
            int nSecs = (int)m_pMagic.StepSeconds;
            int nTimes = (int)m_pMagic.ActiveTimes;
            int nStatus = (int)m_pMagic.Status;

            if (nPower < 0)
            {
                Program.WriteLog($"ERROR: magic type [{m_pMagic.Type}] status [{nStatus}] invalid power");
                return false;
            }

            int targetNum = 0;
            foreach (var obj in lockedTarget)
            {
                if (obj.Identity == m_pOwner.Identity)
                    continue;

                if (!obj.IsAlive && m_pMagic.Target != 64)
                    continue;

                if (m_pMagic.Target == 64)
                {
                    if (obj.IsAlive || !(obj is Character))
                        continue;

                    if (m_pMagic.Status == FlagInt.SHACKLED && obj.QueryStatus(FlagInt.SHACKLED) != null && m_pOwner is Character character)
                    {
                        character.SendSysMessage(Language.StrTargetAlreadyShacked);
                        continue;
                    }
                }

                if (++targetNum < _MAX_TARGET_NUM)
                {
                    msg.AppendTarget(obj.Identity, (uint)(nPower), nPower != 0, 0, 0);
                }
                else
                {
                    m_pOwner.Map.SendToRegion(msg, (ushort)pos.X, (ushort)pos.Y);
                    msg = new MsgMagicEffect
                    {
                        CellX = (ushort)pos.X,
                        CellY = (ushort)pos.Y,
                        Identity = m_pOwner.Identity,
                        SkillIdentity = m_pMagic.Type,
                        SkillLevel = m_pMagic.Level
                    };
                    msg.AppendTarget(obj.Identity, (uint)(nPower), nPower != 0, 0, 0);
                }

                CheckCrime(obj);

                obj.AttachStatus(obj, nStatus, nPower, nSecs, nTimes, 0);
            }

            m_pOwner.Map.SendToRegion(msg, (ushort)pos.X, (ushort)pos.Y);

            AwardExp(0, nExp, nExp);
            return true;
        }

        #endregion
        #region 35 - Remote Bomb

        private bool ProcessRemoteBomb()
        {
            if (m_pMagic == null) return false;
            var setRole = new Dictionary<uint, Role>();
            var setPower = new Dictionary<uint, int>();
            var setRemove = new List<uint>();

            var pos = m_pPos;

            long nExp = 0;
            int nPowerSum = 0;

            var targetLocked = CollectTargetSet_Bomb(ref pos, 0, (int)m_pMagic.Range + 1);

            var msg = new MsgMagicEffect
            {
                CellX = (ushort)pos.X,
                CellY = (ushort)pos.Y,
                Identity = m_pOwner.Identity,
                SkillIdentity = m_pMagic.Type,
                SkillLevel = m_pMagic.Level
            };

            int targetNum = 0;
            foreach (var obj in targetLocked)
            {
                if (obj.Identity != m_pOwner.Identity && obj.IsAttackable(m_pOwner) && !IsImmunity(obj))
                {
                    var special = InteractionEffect.None;
                    int nPower = m_pOwner.BattleSystem.CalcPower(HitByMagic(), m_pOwner, obj, ref special);

                    if (obj is Character)
                    {
                        if ((obj as Character).CheckScapegoat(m_pOwner))
                        {
                            nPower = 0;
                        }
                    }

                    setPower.Add(obj.Identity, nPower);

                    if (targetNum < _MAX_TARGET_NUM)
                    {
                        msg.AppendTarget(obj.Identity, (uint)obj.BattleSystem.TestAzureShield(nPower), true, (uint)special, (uint)m_pMagic.GetElementPower(obj));
                    }
                    else
                    {
                        m_pOwner.Map.SendToRegion(msg, (ushort)pos.X, (ushort)pos.Y);
                        msg = new MsgMagicEffect
                        {
                            CellX = (ushort)pos.X,
                            CellY = (ushort)pos.Y,
                            Identity = m_pOwner.Identity,
                            SkillIdentity = m_pMagic.Type,
                            SkillLevel = m_pMagic.Level
                        };
                        msg.AppendTarget(obj.Identity, (uint)obj.BattleSystem.TestAzureShield(nPower), true, (uint)special, (uint)m_pMagic.GetElementPower(obj));
                        targetNum = 0;
                    }

                    setRole.Add(obj.Identity, obj);
                    CheckCrime(obj);
                }
                else
                    setRemove.Add(obj.Identity);
            }

            foreach (var rem in setRemove)
                setRole.Remove(rem);

            m_pOwner.Map.SendToRegion(msg, (ushort)pos.X, (ushort)pos.Y);

            Character user = m_pOwner as Character;
            foreach (var obj in setRole.Values)
            {
                var pNpc = obj as DynamicNpc;
                int nLifeLost = (int)Math.Min(obj.Life, obj.BattleSystem.CheckAzureShield(setPower[obj.Identity]));
                nPowerSum += nLifeLost;

                if (pNpc != null && pNpc.IsAwardScore())
                    user?.AwardSynWarScore(pNpc, nLifeLost);

                obj.BeAttack(HitByMagic(), m_pOwner, nLifeLost, true);

                if (user != null && (obj.IsMonster()
                    || (pNpc != null && pNpc.IsGoal() && pNpc.Level < m_pOwner.Level)))
                {
                    nExp += user.AdjustExperience(obj, nLifeLost, false);

                    if (!obj.IsAlive)
                    {
                        int nBonusExp = (int)(obj.MaxLife * (5 / 100));

                        user.BattleSystem.OtherMemberAwardExp(obj, nExp);

                        nExp += user.AdjustExperience(obj, nBonusExp, true);
                    }
                }

                if (!obj.IsAlive)
                    m_pOwner.Kill(obj, GetDieMode());
            }

            AwardExp(0, nExp, nExp);
            return true;
        }

        #endregion
        #region 38 - Knockback

        private bool ProcessKnockback()
        {
            if (m_pMagic == null || m_pOwner == null)
                return false;

            var setPoint = new List<Point>();

            // search the target
            var pTarget = m_pOwner.BattleSystem.FindRole(m_idTarget);
            if (pTarget == null
                || !Calculations.InScreen(m_pOwner.MapX, m_pOwner.MapY, pTarget.MapX, pTarget.MapY)
                || (!pTarget.IsAlive && !pTarget.IsAttackable(m_pOwner)))
            {
                return false;
            }

            if (IsImmunity(pTarget))
                return false;

            int nPower = 0;

            DynamicNpc pNpc = null;
            InteractionEffect pSpecial = InteractionEffect.None;
            pNpc = pTarget as DynamicNpc;

            FacingDirection nDir = (FacingDirection)(Calculations.GetDirection(m_pOwner.MapX, m_pOwner.MapY, pTarget.MapX, pTarget.MapY) % 8);

            Calculations.DDALine(m_pOwner.MapX, m_pOwner.MapY, m_pPos.X, m_pPos.Y, (int)m_pMagic.Range, ref setPoint);

            ushort nTargetX = pTarget.MapX;
            ushort nTargetY = pTarget.MapY;

            foreach (var point in setPoint)
            {
                try
                {
                    Tile pPos = m_pOwner.Map[point.X, point.Y];
                    if (pPos.Access < TileType.Npc
                        || pPos.Elevation - m_pOwner.Map[m_pOwner.MapX, m_pOwner.MapY].Elevation > 26)
                    {
                        break;
                    }
                    nTargetX = (ushort)point.X;
                    nTargetY = (ushort)point.Y;
                }
                catch
                {
                    continue;
                }
            }

            if (HitByWeapon())
            {
                nPower += m_pOwner.BattleSystem.CalcAttackPower(m_pOwner, pTarget, ref pSpecial);

                if (pNpc != null)
                    nPower = Calculations.MulDiv(nPower, 80, 100);
                else
                    nPower = Calculations.AdjustDataEx(nPower, m_pMagic.Power, 0);
            }

            Character user = m_pOwner as Character;
            if (!m_pOwner.Map.IsTrainingMap())
                user?.AddEquipmentDurability(ItemPosition.LeftHand, -3);

            MsgMagicEffect pMsg = new MsgMagicEffect
            {
                Identity = m_pOwner.Identity,
                SkillIdentity = m_pMagic.Type,
                SkillLevel = m_pMagic.Level,
                CellX = nTargetX,
                CellY = nTargetY
            };
            pMsg.AppendTarget(pTarget.Identity, (uint)pTarget.BattleSystem.TestAzureShield(nPower), true, (uint)pSpecial, 0);
            m_pOwner.Map.SendToRegion(pMsg, nTargetX, nTargetY);

            if (user?.IsPm() == true)
                m_pOwner.Send(new MsgTalk($"bump move:({m_pOwner.MapX},{m_pOwner.MapY})->({nTargetX},{nTargetY})", ChatTone.Talk));

            pTarget.ProcessOnMove(MovementType.COLLIDE);
            CheckCrime(pTarget);

            if (nPower > 0)
            {
                int nLifeLost = (int)Math.Min(pTarget.BattleSystem.CheckAzureShield(nPower), pTarget.Life);

                if (pNpc != null && pNpc.IsAwardScore())
                {
                    user?.AwardSynWarScore(pNpc, nLifeLost);
                }

                pTarget.BeAttack(HitByMagic(), m_pOwner, nLifeLost, true);
                
                if (user != null && (pTarget.IsMonster() || pNpc != null && pNpc.IsGoal() && m_pOwner.Level >= pNpc.Level))
                {
                    long nExp = user.AdjustExperience(pTarget, nLifeLost, false);

                    if (!pTarget.IsAlive)
                    {
                        int nBonusExp = (int)(pTarget.MaxLife * 0.05f);
                        user.BattleSystem.OtherMemberAwardExp(pTarget, nBonusExp);

                        nExp += user.AdjustExperience(pTarget, nBonusExp, true);
                    }

                    AwardExp(0, nExp, nExp, m_pMagic);
                }

                if (!pTarget.IsAlive)
                    m_pOwner.Kill(pTarget, GetDieMode());
            }

            return true;
        }

        #endregion
        #region 40 - Dash Whirl

        private bool ProcessDashWhirl()
        {
            if (m_pMagic == null || m_pOwner == null)
                return false;

            if (m_pMagic.Type == 11190 && m_pOwner.QueryStatus(FlagInt.RIDING) == null)
                return false;

            var setPoint = new List<Point>();
            var setTarget = new Dictionary<uint, Role>();
            var setPower = new Dictionary<uint, uint>();
            var setRemove = new List<Role>();

            int nDistance = (int)Math.Min(Calculations.GetDistance(m_pOwner.MapX, m_pOwner.MapY, (ushort)m_pPos.X, (ushort)m_pPos.Y), m_pMagic.DashRange);

            Calculations.DDALine(m_pOwner.MapX, m_pOwner.MapY, m_pPos.X, m_pPos.Y, nDistance, ref setPoint);

            ushort nTargetX = m_pOwner.MapX;
            ushort nTargetY = m_pOwner.MapY;

            var roles = m_pOwner.Map.CollectMapThing((int) (m_pMagic.DashRange + 1), m_pPos);
            int nTiles = 0;
            foreach (var point in setPoint)
            {
                try
                {
                    Tile pPos = m_pOwner.Map[point.X, point.Y];
                    if (pPos.Access < TileType.Npc
                        || pPos.Elevation - m_pOwner.Map[m_pOwner.MapX, m_pOwner.MapY].Elevation > 26)
                    {
                        break;
                    }

                    nTargetX = (ushort)point.X;
                    nTargetY = (ushort)point.Y;
                    ushort testX = nTargetX;
                    ushort testY = nTargetY;

                    var pList = roles.Where(x => Calculations.GetDistance(testX, testY, x.MapX, x.MapY) < m_pMagic.Range + 1);
                    foreach (var role in pList)
                        if (!setTarget.ContainsKey(role.Identity))
                            setTarget.Add(role.Identity, role);
                    nTiles++;
                }
                catch
                {
                    continue;
                }
            }

            if (m_pOwner is Character player)
            {
                int deltaX = nTargetX - player.MapX;
                int deltaY = nTargetY - player.MapY;
                if (!m_pOwner.Map.SampleElevation((int)m_pMagic.DashRange, m_pOwner.MapX, m_pOwner.MapY, deltaX, deltaY,
                    player.Elevation))
                {
                    nTargetX = player.MapX;
                    nTargetY = player.MapY;
                    setTarget.Clear();
                }
            }

            var pMsg = new MsgMagicEffect
            {
                Identity = m_pOwner.Identity,
                SkillIdentity = m_pMagic.Type,
                SkillLevel = m_pMagic.Level,
                CellX = nTargetX,
                CellY = nTargetY
            };

            int nTargetNum = 0, nCount = 0;
            foreach (var pTarget in setTarget.Values)
            {
                if (pTarget.IsAttackable(m_pOwner) && !IsImmunity(pTarget))
                {
                    var special = InteractionEffect.None;
                    int nPower = m_pOwner.BattleSystem.CalcPower(HitByMagic(), m_pOwner, pTarget, ref special);

                    if (pTarget is Character character)
                    {
                        if (character.CheckScapegoat(m_pOwner))
                        {
                            nPower = 0;
                        }
                    }

                    if (++nTargetNum >= 30)
                    {
                        m_pOwner.Map.SendToRegion(pMsg, (ushort)m_pOwner.MapX, (ushort)m_pOwner.MapY);
                        pMsg = new MsgMagicEffect
                        {
                            CellX = nTargetX,
                            CellY = nTargetY,
                            Identity = m_pOwner.Identity,
                            SkillIdentity = m_pMagic.Type,
                            SkillLevel = m_pMagic.Level
                        };
                        nTargetNum = 0;
                    }
                    pMsg.AppendTarget(pTarget.Identity, (uint)pTarget.BattleSystem.TestAzureShield(nPower), true, (byte)special, (uint)m_pMagic.GetElementPower(pTarget));
                    setPower.Add(pTarget.Identity, (uint)nPower);
                    CheckCrime(pTarget);
                    nCount++;
                }
                else
                    setRemove.Add(pTarget);
            }

            foreach (var role in setRemove)
                if (setTarget.ContainsKey(role.Identity))
                    setTarget.Remove(role.Identity);

            MsgInteract pInteract = new MsgInteract
            {
                Timestamp = Time.Now,
                EntityIdentity = m_pOwner.Identity,
                TargetIdentity = m_pOwner.Identity,
                CellX = nTargetX,
                CellY = nTargetY,
                Action = (InteractionType)53,
                MagicType = m_pMagic.Type,
                MagicLevel = m_pMagic.Level
            };
            m_pOwner.Map.SendToRegion(pInteract, m_pOwner.MapX, m_pOwner.MapY);

            Character user = m_pOwner as Character;
            if (user?.IsPm() == true)
                m_pOwner.Send(new MsgTalk(
                    $"bump move:({m_pOwner.MapX},{m_pOwner.MapY})->({nTargetX},{nTargetY}) distance({Calculations.GetDistance(m_pOwner.MapX, m_pOwner.MapY, nTargetX, nTargetY)})",
                    ChatTone.Talk));

            m_pOwner.MapX = nTargetX;
            m_pOwner.MapY = nTargetY;

            user?.Send(pMsg);
            user?.Screen.SendMovement(pMsg);
            user?.ProcessOnMove(MovementType.SHIFT);

            uint nPowerSum = 0u;
            long nExp = 0;
            foreach (var pTarget in setTarget.Values)
            {
                nPowerSum += setPower[pTarget.Identity];

                DynamicNpc pNpc = null;
                if (pTarget is DynamicNpc)
                    pNpc = pTarget as DynamicNpc;

                int nLifeLost = (int)Math.Min(pTarget.Life, pTarget.BattleSystem.CheckAzureShield((int) setPower[pTarget.Identity]));
                if (pNpc != null && pNpc.IsAwardScore())
                {
                    user?.AwardSynWarScore(pNpc, nLifeLost);
                }

                pTarget.BeAttack(HitByMagic(), m_pOwner, nLifeLost, true);

                if (user != null && (pTarget.IsMonster() || pNpc != null && pNpc.IsGoal() && pTarget.Level < m_pOwner.Level))
                {
                    nExp += user.AdjustExperience(pTarget, nLifeLost, false);
                    if (!pTarget.IsAlive)
                    {
                        int nBonusExp = (int)(pTarget.MaxLife * .05f);
                        user.BattleSystem.OtherMemberAwardExp(pTarget, nBonusExp);
                        nExp += user.AdjustExperience(m_pOwner, nBonusExp, true);
                    }
                }

                if (!pTarget.IsAlive)
                    m_pOwner.Kill(pTarget, GetDieMode());
                else
                {
                    if (m_pMagic.Type == 11110)
                    {
                        Magic pBlackSpot = this[11120];
                        if (pBlackSpot != null)
                        {
                            pTarget.Status?.SetDeadMark((int)pBlackSpot.StepSeconds);
                        }
                    }
                }
            }

            AwardExp(nExp, nExp, true);
            return true;
        }

        #endregion
        #region 46 - Self Detach

        private bool ProcessSelfDetach()
        {
            if (m_pMagic == null)
                return false;

            var pRole = m_pOwner as Character;
            if (pRole == null) return false;

            if (!pRole.IsAlive)
                return false;

            var msg = new MsgMagicEffect
            {
                Identity = m_pOwner.Identity,
                SkillIdentity = m_pMagic.Type,
                SkillLevel = m_pMagic.Level
            };
            msg.AppendTarget(pRole.Identity, 0, false, 0, 0);
            pRole.Map.SendToRegion(msg, pRole.MapX, pRole.MapY);

            foreach (var detach in m_pTranquilityDetach)
                pRole.DetachStatus(detach);

            return true;
        }

        #endregion
        #region 47 - Detach Bad Status

        private int[] m_pTranquilityDetach =
        {
            FlagInt.DAZED,
            FlagInt.HUGE_DAZED,
            FlagInt.POISONED,
            FlagInt.POISON_STAR,
            FlagInt.NO_POTION,
            FlagInt.CONFUSED,
            FlagInt.TOXIC_FOG,
            FlagInt.SHACKLED
        };

        private bool ProcessDetachBadStatus()
        {
            if (m_pMagic == null)
                return false;
            
            var pRole = m_pOwner.BattleSystem.FindRole(m_idTarget) as Role;
            if (pRole == null) return false;

            var msg = new MsgMagicEffect
            {
                Identity = m_pOwner.Identity,
                SkillIdentity = m_pMagic.Type,
                SkillLevel = m_pMagic.Level
            };
            msg.AppendTarget(pRole.Identity, 0, false, 0, 0);
            pRole.Map.SendToRegion(msg, pRole.MapX, pRole.MapY);

            for (int i = 0; i < m_pTranquilityDetach.Length; i++)
                pRole.DetachStatus(m_pTranquilityDetach[i]);

            return true;
        }

        #endregion
        #region 48 - Close Line

        private bool ProcessCloseLine()
        {
            if (m_pMagic == null)
                return false;

            var setTarget = new Dictionary<uint, Role>();
            var setPower = new Dictionary<uint, int>();
            var setPoint = new List<Point>();

            long nExp = 0;
            int nPowerSum = 0;

            setTarget.Clear();
            setPower.Clear();
            setPoint.Clear();

            if (m_pPos.X == m_pOwner.MapX && m_pPos.Y == m_pOwner.MapY)
                return false;

            var pos = new Point(m_pOwner.MapX, m_pOwner.MapY);

            Calculations.DDALine(pos.X, pos.Y, m_pPos.X, m_pPos.Y, (int)m_pMagic.Distance, ref setPoint);

            foreach (var point in setPoint)
            {
                var pTarget = m_pOwner.Map.QueryRole((ushort)point.X, (ushort)point.Y) as Role;
                if (pTarget == null
                    || !pTarget.IsAttackable(m_pOwner)
                    || IsImmunity(pTarget))
                    continue;

                if (pTarget is Character || pTarget.IsMonster())
                {
                    try
                    {
                        if (m_pOwner.Map[point.X, point.Y].Elevation > 26)
                            continue;
                    }
                    catch { continue; }
                }

                if (pTarget.Identity == m_pOwner.Identity || !pTarget.IsAttackable(m_pOwner)) continue;

                if (!m_pOwner.IsWing && !pTarget.IsWing)
                    setTarget.Add(pTarget.Identity, pTarget);
            }

            var clickTarget = m_pOwner.BattleSystem.FindRole(m_idTarget);
            if (clickTarget != null && !setTarget.ContainsKey(clickTarget.Identity) && !IsImmunity(clickTarget))
                setTarget.Add(clickTarget.Identity, clickTarget);

            if (setTarget.Count <= 0)
            {
                return false;
            }

            var msg = new MsgMagicEffect
            {
                Identity = m_pOwner.Identity,
                SkillIdentity = m_pMagic.Type,
                SkillLevel = m_pMagic.Level,
                CellX = (ushort)m_pPos.X,
                CellY = (ushort)m_pPos.Y
            };

            foreach (var pTarget in setTarget.Values)
            {
                if (pTarget.IsAttackable(m_pOwner))
                {
                    var special = InteractionEffect.None;
                    int nPower = m_pOwner.BattleSystem.CalcPower(HitByMagic(), m_pOwner, pTarget, ref special);
                    msg.AppendTarget(pTarget.Identity, (uint)pTarget.BattleSystem.TestAzureShield(nPower), true, (byte)special, 0);
                    setPower.Add(pTarget.Identity, nPower);
                    CheckCrime(pTarget);
                }
            }

            m_pOwner.Map.SendToRegion(msg, m_pOwner.MapX, m_pOwner.MapY);

            Character user = m_pOwner as Character;
            foreach (var pTarget in setTarget.Values)
            {

                DynamicNpc pNpc = null;
                if (pTarget is DynamicNpc)
                    pNpc = pTarget as DynamicNpc;

                int nLifeLost = (int)Math.Min(pTarget.Life, pTarget.BattleSystem.CheckAzureShield(setPower[pTarget.Identity]));

                nPowerSum += nLifeLost;
                if (pNpc != null && pNpc.IsAwardScore())
                {
                    user?.AwardSynWarScore(pNpc, nLifeLost);
                }

                pTarget.BeAttack(HitByMagic(), m_pOwner, nLifeLost, true);

                if (user != null && (pTarget.IsMonster() || pNpc != null && pNpc.IsGoal() && pTarget.Level < m_pOwner.Level))
                {
                    nExp += user.AdjustExperience(pTarget, nLifeLost, false);
                    if (!pTarget.IsAlive)
                    {
                        int nBonusExp = (int)(pTarget.MaxLife * (5 / 100));
                        user.BattleSystem.OtherMemberAwardExp(pTarget, nExp);
                        nExp += user.AdjustExperience(m_pOwner, nBonusExp, true);
                    }
                }

                if (!pTarget.IsAlive)
                    m_pOwner.Kill(pTarget, GetDieMode());
            }
            
            AwardExp(0, 0, nExp);
            return true;
        }

        #endregion
        #region 50 - Compassion

        private int[] m_pCompassionDetach =
        {
            FlagInt.DAZED,
            FlagInt.HUGE_DAZED,
            FlagInt.POISONED,
            FlagInt.POISON_STAR,
            FlagInt.NO_POTION,
            FlagInt.CONFUSED,
            FlagInt.TOXIC_FOG
        };

        private bool ProcessDetachTeam()
        {
            if (!(m_pOwner is Character))
                return false;

            Character pCaster = m_pOwner as Character;

            List<Character> pTargetList = new List<Character>();
            if (pCaster.Team == null)
            {
                pTargetList.Add(pCaster);
            }
            else
            {
                pTargetList = new List<Character>(pCaster.Team.Members.Values.ToList());
            }

            MsgMagicEffect pMsg = new MsgMagicEffect
            {
                Identity = m_pOwner.Identity,
                CellX = m_pOwner.MapX,
                CellY = m_pOwner.MapY,
                SkillIdentity = m_pMagic.Type,
                SkillLevel = m_pMagic.Level
            };
            foreach (var member in pTargetList)
            {
                if (!member.IsAlive)
                    continue;
                for (int i = 0; i < m_pCompassionDetach.Length; i++)
                    member.DetachStatus(m_pCompassionDetach[i]);
                pMsg.AppendTarget(member.Identity, 0, false, 0, 0);
            }
            m_pOwner.Map.SendToRegion(pMsg, m_pOwner.MapX, m_pOwner.MapY);
            return true;
        }

        #endregion
        #region 51 - Team DeliverFlag

        private bool ProcessTeamFlag()
        {
            if (m_pOwner == null || m_pMagic == null) return false;
            
            int nPower = m_pMagic.Power;
            int nSecs = (int)m_pMagic.StepSeconds;
            int nTimes = (int)m_pMagic.ActiveTimes;
            int nStatus = (int)m_pMagic.Status;
            byte pLevel = (byte)m_pMagic.Level;

            if (nPower < 0)
            {
                Program.WriteLog($"ERROR: magic type [{m_pMagic.Type}] status [{nStatus}] invalid power");
                return false;
            }

            var msg = new MsgMagicEffect
            {
                Identity = m_pOwner.Identity,
                SkillIdentity = m_pMagic.Type,
                SkillLevel = m_pMagic.Level
            };
            msg.AppendTarget(m_pOwner.Identity, 0, true, 0, 0);
            m_pOwner.Map.SendToRegion(msg, m_pOwner.MapX, m_pOwner.MapY);

            Character pSender = m_pOwner as Character;
            if (pSender != null)
            {
                for (int i = 98; i <= 111; i++)
                {
                    var sts = pSender.QueryStatus(i);
                    if (sts != null && sts.IsUserCast)
                        pSender.DetachStatus(i);
                }

                pSender.AttachStatus(pSender, nStatus, nPower, nSecs, nTimes, pLevel);

                pSender.Team?.Send($"{m_pOwner.Name} has used {m_pMagic.Name} at {m_pOwner.Map.Name}.");
                pSender.ProcessAura();
            }

            int nExp = 3;
            if (m_pOwner.Map.IsTrainingMap())
                nExp = 1;

            AwardExp(0, 0, nExp);
            return true;
        }

        #endregion
        #region 52 - Increase Block

        private bool ProcessIncreaseBlock()
        {
            if (m_pMagic == null) return false;

            var pRoleUser = m_pOwner.BattleSystem.FindRole(m_idTarget);

            if (pRoleUser == null && m_idTarget == m_pOwner.Identity)
                pRoleUser = m_pOwner;
            if (pRoleUser == null)
                return false;

            int nPower = m_pMagic.Power;
            int nSecs = (int)m_pMagic.StepSeconds;
            int nTimes = (int)m_pMagic.ActiveTimes;
            int nStatus = (int)m_pMagic.Status;

            if (nPower < 0)
            {
                ServerKernel.Log.SaveLog(string.Format("ERROR: magic type [{0}] status [{1}] invalid power", m_pMagic.Type, nStatus));
                return false;
            }

            uint nDmg = 1;

            var msg = new MsgMagicEffect
            {
                Identity = m_pOwner.Identity,
                SkillIdentity = m_pMagic.Type,
                SkillLevel = m_pMagic.Level
            };
            msg.AppendTarget(pRoleUser.Identity, nDmg, nDmg != 0, 0, 0);
            m_pOwner.Map.SendToRegion(msg, m_pOwner.MapX, m_pOwner.MapY);

            CheckCrime(pRoleUser);

            pRoleUser.AttachStatus(pRoleUser, nStatus, nPower, nSecs, nTimes, (byte)m_pMagic.Level);
            return true;
        }

        #endregion
        #region 53 - Oblivion

        private bool ProcessOblivion()
        {
            if (m_pMagic == null || !(m_pOwner is Character)) return false;

            var pRoleUser = m_pOwner as Character;

            if (!pRoleUser.IsAlive)
                return false;

            int nPower = m_pMagic.Power;
            int nSecs = (int)m_pMagic.StepSeconds;
            int nTimes = (int)m_pMagic.ActiveTimes;
            int nStatus = FlagInt.OBLIVION;

            if (nPower < 0)
            {
                Program.WriteLog($"ERROR: magic type [{m_pMagic.Type}] status [{nStatus}] invalid power");
                return false;
            }

            pRoleUser.ResetOblivion();

            var msg = new MsgMagicEffect
            {
                Identity = m_pOwner.Identity,
                SkillIdentity = m_pMagic.Type,
                SkillLevel = m_pMagic.Level
            };
            msg.AppendTarget(pRoleUser.Identity, 1, true, 0, 0);
            m_pOwner.Map.SendToRegion(msg, m_pOwner.MapX, m_pOwner.MapY);

            CheckCrime(pRoleUser);

            pRoleUser.AttachStatus(pRoleUser, nStatus, nPower, nSecs, nTimes, 0);
            return true;
        }

        #endregion
        #region 54 - Stun Bomb Magic
        private bool ProcessStunBomb()
        {
            if (m_pMagic == null) return false;
            var setRemove = new List<Role>();
            var setPower = new Dictionary<uint, int>();

            var pos = new Point(m_pOwner.MapX, m_pOwner.MapY);

            long nExp = 0;
            int nPowerSum = 0;

            var lockedTarget = CollectTargetSet_Bomb(ref pos, 0, (int)m_pMagic.Range);

            var msg = new MsgMagicEffect
            {
                Identity = m_pOwner.Identity,
                CellX = (ushort)m_pMagic.ActiveTimes,
                CellY = 0,
                SkillIdentity = m_pMagic.Type,
                SkillLevel = m_pMagic.Level
            };

            foreach (var obj in lockedTarget)
            {
                if (obj.Identity != m_pOwner.Identity
                    && obj.IsAttackable(m_pOwner)
                    && !IsImmunity(obj))
                {
                    if (msg.TargetCount >= _MAX_TARGET_NUM)
                    {
                        m_pOwner.Map.SendToRegion(msg, (ushort)pos.X, (ushort)pos.Y);
                        msg = new MsgMagicEffect
                        {
                            Identity = m_pOwner.Identity,
                            CellX = (ushort)m_pMagic.ActiveTimes,
                            CellY = 0,
                            SkillIdentity = m_pMagic.Type,
                            SkillLevel = m_pMagic.Level
                        };
                    }
                    var special = InteractionEffect.None;
                    int nPower = m_pOwner.BattleSystem.CalcPower(HitByMagic(), m_pOwner, obj, ref special);
                    setPower.Add(obj.Identity, nPower);
                    msg.AppendTarget(obj.Identity, (uint)obj.BattleSystem.TestAzureShield(nPower), true, (byte)special, 0);
                    CheckCrime(obj);

                    if (obj.IsWing)
                        obj.DetachStatus(FlagInt.FLY);
                }
                else
                {
                    setRemove.Add(obj);
                }
            }

            foreach (var remove in setRemove)
                lockedTarget.Remove(remove);

            m_pOwner.Map.SendToRegion(msg, (ushort)pos.X, (ushort)pos.Y);

            Character user = m_pOwner as Character;
            foreach (var obj in lockedTarget)
            {
                if (!obj.IsAttackable(m_pOwner))
                    continue;
                
                var pNpc = obj as DynamicNpc;
                int nLifeLost = (int)Math.Min(obj.BattleSystem.CheckAzureShield(setPower[obj.Identity]), obj.Life);
                nPowerSum += nLifeLost;

                if (pNpc != null && pNpc.IsAwardScore())
                {
                    user?.AwardSynWarScore(pNpc, nLifeLost);
                }

                obj.BeAttack(HitByMagic(), m_pOwner, nLifeLost, true);

                if (obj is Character targetUser)
                    targetUser.ProcessOnMove(MovementType.SHIFT);

                if (user != null && 
                    (obj.IsMonster() || (pNpc != null && pNpc.IsGoal() && pNpc.Level < m_pOwner.Level)))
                {
                    nExp += user.AdjustExperience(obj, nLifeLost, false);

                    if (!obj.IsAlive)
                    {
                        int nBonusExp = (int)(obj.MaxLife * (5 / 100));
                        user.BattleSystem.OtherMemberAwardExp(obj, nExp);
                        nExp += user.AdjustExperience(obj, nBonusExp, true);
                    }
                }

                if (!obj.IsAlive)
                    m_pOwner.Kill(obj, GetDieMode());
            }

            AwardExp(0, nExp, nExp);
            return true;
        }
        #endregion
        #region 55 - Triple Attack
        private bool ProcessTripleAttack()
        {
            if (m_pMagic == null || m_pOwner == null)
                return false;

            var pTarget = m_pOwner.BattleSystem.FindRole(m_idTarget);
            if (pTarget == null
                || !Calculations.InScreen(m_pOwner.MapX, m_pOwner.MapY, pTarget.MapX, pTarget.MapY)
                || !pTarget.IsAttackable(m_pOwner))
                return false;

            if (m_pOwner.IsImmunity(pTarget))
                return false;

            long nTotalExp = 0;
            int nTotalDamage = 0;

            var msg = new MsgMagicEffect
            {
                Identity = m_pOwner.Identity,
                SkillIdentity = m_pMagic.Type,
                SkillLevel = m_pMagic.Level
            };
            var special = InteractionEffect.None;
            int nPower = 0;
            for (int i = 0; i < 3; i++)
            {
                if (!m_pOwner.BattleSystem.IsTargetDodged(m_pOwner, pTarget))
                    nPower = m_pOwner.BattleSystem.CalcPower(0, m_pOwner, pTarget, ref special, m_pMagic.Power);

                int temp = pTarget.BattleSystem.TestAzureShield(nPower);
                msg.AppendTarget(pTarget.Identity, (uint)Math.Max(temp, nPower), true, (byte)special, 0);

                special = InteractionEffect.None;
                nTotalDamage += nPower;
                nPower = 0;
            }
            m_pOwner.Map.SendToRegion(msg, pTarget.MapX, pTarget.MapY);

            CheckCrime(pTarget);

            Character user = m_pOwner as Character;
            if (nTotalDamage > 0)
            {
                int nLifeLost = (int)Math.Min(pTarget.Life, pTarget.BattleSystem.CheckAzureShield(nTotalDamage));

                pTarget.BeAttack(HitByMagic(), m_pOwner, nLifeLost, true);

                var pNpc = pTarget as DynamicNpc;
                if (pNpc != null && pNpc.IsAwardScore())
                {
                    user?.AwardSynWarScore(pNpc, nLifeLost);
                }

                if (user != null &&
                    (pTarget.IsMonster() || (pNpc != null && pNpc.IsGoal() && pNpc.Level < m_pOwner.Level)))
                {
                    nTotalExp += user.AdjustExperience(pTarget, nLifeLost, false);

                    if (!pTarget.IsAlive)
                    {
                        int nBonusExp = (int)(pTarget.MaxLife * (5 / 100));
                        user.BattleSystem.OtherMemberAwardExp(pTarget, nTotalExp);
                        nTotalExp += user.AdjustExperience(pTarget, nBonusExp, true);
                    }

                    int nWeaponExp = (int)nTotalExp / 2;
                    if (user.UserPackage[ItemPosition.RightHand] != null)
                        user.AddWeaponSkillExp((ushort)user.UserPackage[ItemPosition.RightHand].GetItemSubtype(),
                            nWeaponExp);
                    if (user.UserPackage[ItemPosition.LeftHand] != null)
                        user.AddWeaponSkillExp((ushort)user.UserPackage[ItemPosition.LeftHand].GetItemSubtype(),
                            nWeaponExp / 2);
                }

                nTotalExp += nLifeLost;
            }

            if (!pTarget.IsAlive)
                m_pOwner.Kill(pTarget, GetDieMode());

            int nSkillExp = m_pOwner.Map.IsTrainingMap() ? 1 : 3;
            AwardExp(0, nTotalExp, nSkillExp, m_pMagic);
            return true;
        }
        #endregion
        #region 68 - BladeFlurry

        private bool ProcessBladeFlurry()
        {
            if (m_pMagic == null)
                return false;

            if (m_pOwner.QueryStatus(FlagInt.BLADE_FLURRY) != null && m_pMagic.IsReady()) // flurry active
            {
                m_pMagic.Delay();

                var pos = new Point();

                long nExp = 0;
                int nPowerSum = 0;

                var targetLocked = CollectTargetSet_Bomb(ref pos, 2, (int)m_pMagic.Range);

                var msg = new MsgMagicEffect
                {
                    CellX = m_pOwner.MapX,
                    CellY = m_pOwner.MapY,
                    Identity = m_pOwner.Identity,
                    SkillIdentity = m_pMagic.Type,
                    SkillLevel = m_pMagic.Level
                };

                Character user = m_pOwner as Character;
                int targetNum = 0;
                foreach (var obj in targetLocked)
                {
                    if (obj.Identity != m_pOwner.Identity
                        && obj.IsAttackable(m_pOwner))
                    {
                        if (IsImmunity(obj))
                            continue;

                        var special = InteractionEffect.None;
                        int nPower = m_pOwner.BattleSystem.CalcPower(HitByMagic(), m_pOwner, obj, ref special);

                        if (user != null)
                        {
                            if (user.CheckScapegoat(m_pOwner))
                            {
                                nPower = 0;
                            }
                        }

                        if (++targetNum >= _MAX_TARGET_NUM)
                        {
                            m_pOwner.Map.SendToRegion(msg, (ushort)m_pOwner.MapX, (ushort)m_pOwner.MapY);
                            msg = new MsgMagicEffect
                            {
                                CellX = (ushort)pos.X,
                                CellY = (ushort)pos.Y,
                                Identity = m_pOwner.Identity,
                                SkillIdentity = m_pMagic.Type,
                                SkillLevel = m_pMagic.Level
                            };
                            targetNum = 0;
                        }

                        msg.AppendTarget(obj.Identity, (uint)obj.BattleSystem.TestAzureShield(nPower), true, (byte)special, (uint)m_pMagic.GetElementPower(obj));

                        CheckCrime(obj);

                        var pNpc = obj as DynamicNpc;
                        int nLifeLost = (int)Math.Min(obj.BattleSystem.CheckAzureShield(nPower), obj.Life);
                        nPowerSum += nLifeLost;
                        if (pNpc != null && pNpc.IsAwardScore())
                        {
                            user?.AwardSynWarScore(pNpc, nLifeLost);
                        }

                        obj.BeAttack(HitByMagic(), m_pOwner, nLifeLost, true);

                        if (user != null && (obj.IsMonster()
                            || pNpc != null && pNpc.IsGoal() && pNpc.Level < m_pOwner.Level))
                        {
                            nExp += user.AdjustExperience(obj, nLifeLost, false);

                            if (!obj.IsAlive)
                            {
                                int nBonusExp = (int)(obj.MaxLife * (5 / 100));
                                user.BattleSystem.OtherMemberAwardExp(obj, nExp);
                                nExp += user.AdjustExperience(obj, nBonusExp, true);
                                m_pOwner.Kill(obj, GetDieMode());
                            }
                        }
                    }
                }

                m_pOwner.Map.SendToRegion(msg, (ushort)m_pOwner.MapX, (ushort)m_pOwner.MapY);
                AwardExp(0, nExp, 0);
                m_nMagicState = MAGICSTATE_NONE;
            }
            else
            {
                int status = (int)m_pMagic.Status;
                int power = m_pMagic.Power;
                int step = (int)m_pMagic.StepSeconds;
                int times = (int)m_pMagic.ActiveTimes;

                AbortMagic(true);

                if (!m_pOwner.IsAlive)
                    return false;

                if (m_pOwner.IsWing || !(m_pOwner is Character user) || !user.IsAssassin)
                    return false;

                m_pOwner.AttachStatus(m_pOwner, status, power, step, times, 0);
            }

            return true;
        }

        #endregion
        #region 69 - BlisteringWave

        private bool ProcessBlisteringWave()
        {
            if (m_pMagic == null)
                return false;

            Magic pMagic = m_pMagic;
            var setRole = new Dictionary<uint, Role>();
            var setPower = new Dictionary<uint, int>();
            var pos = new Point();
            int nRange = (int)m_pMagic.Distance + 2;
            long nExp = 0;
            int nPowerSum = 0;

            if (m_pMagic.Ground != 0)
            {
                pos.X = m_pOwner.MapX;
                pos.Y = m_pOwner.MapY;
            }
            else
            {
                Role pTarget = m_pOwner.FindAroundRole(m_idTarget) as Role;
                if (pTarget == null || !pTarget.IsAlive) return false;

                pos.X = pTarget.MapX;
                pos.Y = pTarget.MapY;
                setRole.Add(pTarget.Identity, pTarget);
            }

            var setTarget = m_pOwner.Map.CollectMapThing(nRange, pos);

            Calculations.CalculateTriangle(pos, m_pPos, (int) m_pMagic.Distance, (int) m_pMagic.Range, out var p1, out var p2);

            foreach (var pScrnObj in setTarget)
            {
                var posThis = new Point(pScrnObj.MapX, pScrnObj.MapY);
                if (pScrnObj.Identity == m_pOwner.Identity || pScrnObj.Identity == m_idTarget || pScrnObj is DynamicNpc || !Calculations.IsInTriangle(posThis, pos, p1, p2))
                    continue;

                var pRole = pScrnObj;
                if (pRole.IsAttackable(m_pOwner)
                    && !IsImmunity(pRole))
                    setRole.Add(pRole.Identity, pRole);
            }

            var msg = new MsgMagicEffect
            {
                Identity = m_pOwner.Identity,
                SkillIdentity = m_pMagic.Type,
                SkillLevel = m_pMagic.Level,
                CellX = (ushort)m_pPos.X,
                CellY = (ushort)m_pPos.Y
            };
            var setRemove = new List<uint>();
            foreach (var pRole in setRole.Values)
            {
                if (pRole is Character)
                {
                    if ((pRole as Character).CheckScapegoat(m_pOwner))
                    {
                        setRemove.Add(pRole.Identity);
                        continue;
                    }
                }

                var special = InteractionEffect.None;
                int nPower = m_pOwner.BattleSystem.CalcPower(HitByMagic(), m_pOwner, pRole, ref special);

                setPower.Add(pRole.Identity, nPower);

                if (msg.TargetCount > 29)
                {
                    m_pOwner.Map.SendToRegion(msg, m_pOwner.MapX, m_pOwner.MapY);
                    msg = new MsgMagicEffect
                    {
                        Identity = m_pOwner.Identity,
                        SkillIdentity = m_pMagic.Type,
                        SkillLevel = m_pMagic.Level,
                        CellX = (ushort)m_pPos.X,
                        CellY = (ushort)m_pPos.Y
                    };
                }

                msg.AppendTarget(pRole.Identity, (uint)pRole.BattleSystem.TestAzureShield(nPower), true, (uint)special, (uint)m_pMagic.GetElementPower(pRole));
            }
            m_pOwner.Map.SendToRegion(msg, m_pOwner.MapX, m_pOwner.MapY);

            foreach (var i in setRemove)
                setRole.Remove(i);

            CheckCrime(setRole);

            Character owner = m_pOwner as Character;
            bool bMgc2Dealt = false;
            foreach (var pRole in setRole.Values)
            {
                var pNpc = pRole as DynamicNpc;

                int nLifeLost = (int)Math.Min(pRole.Life, pRole.BattleSystem.CheckAzureShield(setPower[pRole.Identity]));
                nPowerSum += nLifeLost;
                pRole.BeAttack(HitByMagic(), m_pOwner, nLifeLost, true);

                if (pNpc != null && pNpc.IsAwardScore() && owner != null)
                {
                    owner.AwardSynWarScore(pNpc, nLifeLost);
                }

                if (owner != null && (pRole.IsMonster() || pNpc != null && pNpc.IsGoal() && m_pOwner.Level >= pNpc.Level))
                {
                    nExp += owner.AdjustExperience(pRole, nLifeLost, false);
                    if (!pRole.IsAlive)
                    {
                        int nBonusExp = (int)(pRole.MaxLife * 20 / 100);
                        owner.BattleSystem.OtherMemberAwardExp(pRole, nBonusExp);
                        nExp += owner.AdjustExperience(pRole, nBonusExp, false);
                    }
                }

                if (!pRole.IsAlive)
                    m_pOwner.Kill(pRole, GetDieMode());

                if (!bMgc2Dealt && Calculations.ChanceCalc(10f) && m_pOwner is Character)
                {
                    (m_pOwner as Character).SendWeaponMagic2(pRole);
                    bMgc2Dealt = true;
                }
            }

            AwardExp(nExp, nExp, false, pMagic);
            return true;
        }

        #endregion

        #endregion

        #region Collect Target Set

        private List<Role> CollectTargetSet_Bomb(ref Point pos, int nLockType, int nRange)
        {
            if (m_pMagic == null) return new List<Role>();

            int nSize = nRange * 2 + 1;
            int nBufSize = nSize * nSize;

            List<Role> setTarget = new List<Role>();

            //int nDir = Owner.GetDir();
            if (m_pMagic.Ground == 1)
            {
                pos.X = m_pOwner.MapX;
                pos.Y = m_pOwner.MapY;
            }
            else if (pos.X > 0 && pos.Y > 0)
            {
                // bypass
            }
            else if (m_pMagic.Status == FlagInt.VORTEX)
            {
                pos.X = m_pOwner.MapX;
                pos.Y = m_pOwner.MapY;
            }
            else
            {
                Role pTarget = m_pOwner.FindAroundRole(m_idTarget) as Role;
                if (pTarget == null
                    || !pTarget.IsAttackable(m_pOwner))
                    return new List<Role>();
                pos.X = pTarget.MapX;
                pos.Y = pTarget.MapY;
            }

            var setTempTarget = m_pOwner.Map.CollectMapThing(nRange, pos);

            if (setTempTarget == null || setTempTarget.Count <= 0)
                return new List<Role>();

            foreach (var pRole in setTempTarget)
            {
                if (!Calculations.IsInCircle(new Point(pRole.MapX, pRole.MapY), pos, nRange))
                    continue;

                if (!IsImmunity(pRole))
                {
                    if (pRole is DynamicNpc)
                        if (!pRole.IsAttackable(m_pOwner))
                            continue;

                    if (pRole.Identity == m_pOwner.Identity)
                        continue;

                    if ((nLockType == 1 && pRole.Identity == m_idTarget)
                        || nLockType == 0)
                    {
                        setTarget.Add(pRole);
                    }
                }
            }
            return setTarget;
        }

        public List<Role> FetchCloseRoles(int maxDistance, int maxTargets)
        {
            List<Role> setTarget = new List<Role>();

            Role firstTarget = null;
            Point pos = new Point(m_pPos.X, m_pPos.Y);
            if (m_pMagic.Ground == 1)
            {
                pos.X = m_pOwner.MapX;
                pos.Y = m_pOwner.MapY;
            }
            else if (pos.X > 0 && pos.Y > 0)
            {
                // bypass
            }
            else if (m_pMagic.Status == FlagInt.VORTEX)
            {
                pos.X = m_pOwner.MapX;
                pos.Y = m_pOwner.MapY;
            }
            else
            {
                firstTarget = m_pOwner.FindAroundRole(m_idTarget) as Role;
                if (firstTarget == null
                    || !firstTarget.IsAttackable(m_pOwner))
                    return new List<Role>();
                pos.X = firstTarget.MapX;
                pos.Y = firstTarget.MapY;
            }

            var setTempTarget = m_pOwner.Map.CollectMapThing((int) m_pMagic.Distance, pos);
            if (setTempTarget == null || setTempTarget.Count <= 0)
                return new List<Role>();

            setTempTarget = setTempTarget.Where(x => x.Identity != m_pOwner.Identity).ToList();

            Role baseTarget = null;
            if (firstTarget == null)
            {
                baseTarget = setTempTarget[0];
            }
            else
            {
                baseTarget = firstTarget;
            }

            setTarget.Add(setTempTarget[0]);
            setTempTarget.RemoveAt(0);

            while (setTarget.Count < Math.Min(maxTargets, setTempTarget.Count))
            {
                Role nextTarget = setTempTarget.Where(x => x.Identity != m_pOwner.Identity)
                    .OrderBy(x => x.GetDistance(baseTarget.MapX, baseTarget.MapY)).FirstOrDefault();

                if (nextTarget == null)
                    break;

                setTempTarget.Remove(nextTarget);
                if (!nextTarget.IsAttackable(m_pOwner) || setTarget.Any(x => x.Identity == nextTarget.Identity))
                    continue;

                setTarget.Add(nextTarget);
                baseTarget = nextTarget;
            }

            return setTarget;
        }

        #endregion

        public Magic QueryMagic()
        {
            if (m_pMagic == null && m_nMagicState != MAGICSTATE_NONE)
            {
                m_nMagicState = MAGICSTATE_NONE;
                return null;
            }

            return m_pMagic;
        }

        #region Magic State

        public void SetMagicState(int state)
        {
            m_nMagicState = state;
        }

        public bool IsIntone()
        {
            return m_nMagicState == MAGICSTATE_INTONE;
        }

        public bool IsInLaunch()
        {
            return m_nMagicState == MAGICSTATE_LAUNCH;
        }

        public bool IsActive()
        {
            return m_nMagicState == MAGICSTATE_NONE;
        }

        private void ResetDelay()
        {
            if (m_pMagic == null) return;
            m_nDelay = 0;
            m_nMagicState = MAGICSTATE_DELAY;
            m_tDelay.Update();

            Role pTarget = m_pOwner.BattleSystem.FindRole(m_idTarget);
            if (m_pOwner is Character user && pTarget?.Status != null && pTarget.Status.HasDeadMark())
            {
                user.AdrenalineRush();
            }
            else
            {
                m_pMagic.SetDelay();
            }
        }

        #endregion

        #region Experience

        public bool AwardExpOfLife(Role pTarget, int nLifeLost, bool bMagicRecruit = false)
        {
            Character pOwner = m_pOwner as Character;
            if (pTarget is DynamicNpc target)
            {
                pOwner?.AwardSynWarScore(target, nLifeLost);
            }

            if ((pTarget.IsMonster() || pTarget is DynamicNpc npc && npc.IsGoal()) && pOwner != null
            ) // check if monster
            {
                long nExp = pOwner.AdjustExperience(pTarget, nLifeLost, false);

                if (!pTarget.IsAlive && !bMagicRecruit)
                {
                    int nBonusExp = (int) (pTarget.MaxLife * (5 / 100));
                    nExp += nBonusExp;

                    if (!pOwner.Map.IsTrainingMap() && nBonusExp > 0)
                        pOwner.SendSysMessage(string.Format(Language.StrKillingExperience, nBonusExp));
                }

                AwardExp(0, (int) nExp, (int) nExp);
            }

            return true;
        }

        public bool AwardExp(int nType, long nBattleExp, long nExp, Magic pMagic = null)
        {
            if (pMagic == null)
                return AwardExp(nBattleExp, nExp, true, m_pMagic);
            return AwardExp(nBattleExp, nExp, true, pMagic);
        }

        public bool AwardExp(long nBattleExp, long nExp, bool bIgnoreFlag, Magic pMagic = null)
        {
            if (nBattleExp <= 0 && nExp == 0) return false;

            if (pMagic == null)
                pMagic = m_pMagic;

            if (m_pOwner.Map.IsTrainingMap())
            {
                if (nBattleExp > 0)
                {
                    if (m_pOwner.IsBowman)
                        nBattleExp /= 2;
                    nBattleExp = Calculations.CutTrail(1, Calculations.MulDiv(nBattleExp, 10, 100));
                }
            }

            if (nBattleExp > 0 && m_pOwner is Character user)
                user.AwardBattleExp(nBattleExp, true);

            if (pMagic == null)
                return false;

            if (!CheckAwardExpEnable((ushort) ((m_pOwner as Character)?.Profession ?? ProfessionType.Error)))
                return false;

            if (pMagic.NeedExp > 0
                && (pMagic.AutoActive & 16) == 0
                || bIgnoreFlag)
            {
                if (m_pOwner is Character)
                    nExp = (int)(nExp * ((1 + ((m_pOwner as Character).GetSkillGemEffect() / 100f))));

                pMagic.Experience += (uint) nExp;

                if ((pMagic.AutoActive & 8) == 0)
                    pMagic.SendSkill();
                UpLevelMagic(true, pMagic);
                return true;
            }

            if (pMagic.NeedExp == 0
                && pMagic.Target == 4)
            {
                if (m_pOwner is Character)
                    nExp = (int)(nExp * ((1 + ((m_pOwner as Character).GetSkillGemEffect() / 100f))));

                pMagic.Experience += (uint) nExp;

                if ((pMagic.AutoActive & 8) == 0)
                    pMagic.SendSkill();
                UpLevelMagic(true, pMagic);
            }

            return false;
        }

        public bool UpLevelMagic(bool synchro, Magic pMagic)
        {
            if (pMagic == null)
                return false;

            //if (!m_pMagic.IsWeaponMagic())
            //    return false;

            int nNeedExp = pMagic.NeedExp;

            if (!(nNeedExp > 0
                  && (pMagic.Experience >= nNeedExp
                      || pMagic.OldLevel > 0
                      && pMagic.Level >= pMagic.OldLevel / 2
                      && pMagic.Level < pMagic.OldLevel)))
                return false;

            ushort nNewLevel = (ushort) (pMagic.Level + 1);
            pMagic.Experience = 0;
            pMagic.Level = nNewLevel;
            pMagic.SendSkill();

            return true;
        }

        public bool CheckAwardExpEnable(ushort dwProf)
        {
            if (m_pMagic == null)
                return false;
            return m_pOwner.Level >= m_pMagic.NeedLevel
                   && m_pMagic.NeedExp > 0
                   && m_pOwner.MapIdentity != 1005
                /*&& CheckProfession(dwProf, m_pMagic.NeedProf)*/;
        }

        #endregion

        #region Crime

        public bool CheckCrime(Role pRole)
        {
            if (pRole == null || m_pMagic == null) return false;

            if (m_pMagic.Crime <= 0)
                return false;

            return m_pOwner.CheckCrime(pRole);
        }

        public bool CheckCrime(Dictionary<uint, Role> pRoleSet)
        {
            if (pRoleSet == null || m_pMagic == null) return false;

            if (m_pMagic.Crime <= 0)
                return false;

            foreach (var pRole in pRoleSet.Values)
                if (m_pOwner.Identity != pRole.Identity && m_pOwner.CheckCrime(pRole))
                    return true;
            return false;
        }

        #endregion

        public bool ProcessCollideFail(ushort x, ushort y, int nDir)
        {
            ushort nTargetX = (ushort)(m_pPos.X + ScreenObject.DeltaX[nDir]);
            ushort nTargetY = (ushort)(m_pPos.Y + ScreenObject.DeltaY[nDir]);
            int nPower = 0;

            if (!m_pOwner.Map.IsStandEnable(nTargetX, nTargetY))
            {
                m_pOwner.Send(new MsgTalk(Language.StrInvalidMsg, ChatTone.TopLeft));
                if (m_pOwner is Character owner)
                    ServerKernel.UserManager.KickoutSocket(owner, "INVALID COORDINATES ProcessCollideFail");
                return false;
            }

            MsgInteract pMsg = new MsgInteract
            {
                EntityIdentity = m_pOwner.Identity,
                TargetIdentity = 0,
                CellX = nTargetX,
                CellY = nTargetY,
                Action = InteractionType.ACT_ITR_DASH,
                Damage = (ushort)(nDir * 0x01000000 + nPower)
            };
            if (m_pOwner is Character character)
            {
                character.Screen.Send(pMsg, true);
                character.ProcessOnMove(MovementType.COLLIDE);
                character.MoveToward((FacingDirection)nDir, false);
            }
            else
            {
                m_pOwner.Map.SendToRegion(pMsg, m_pOwner.MapX, m_pOwner.MapY);
            }

            return true;
        }

        public bool HitByWeapon()
        {
            if (m_pMagic == null)
                return true;

            if (m_pMagic.WeaponHit == 1)
                return true;

            Item pItem;
            if (m_pOwner is Character character
                && (pItem = character.UserPackage[ItemPosition.RightHand]) != null
                && pItem.Itemtype.MagicAtk <= 0)
                return true;

            return false;
        }

        public bool IsImmunity(Role pRole)
        {
            if (m_pMagic == null) return true;
            if (pRole.IsWing
                && !m_pOwner.IsWing
                && m_pMagic.WeaponHit > 0
                && m_pMagic.WeaponSubtype != 500
                && m_pMagic.WeaponSubtype != 610)
                return true;

            return m_pOwner.IsImmunity(pRole);
        }

        public void ShowMiss()
        {
            if (m_pMagic == null)
                return;

            if (!(m_pOwner is Character user))
                return;

            if (m_pMagic.Ground != 0)
                user.Screen.Send(new MsgMagicEffect
                {
                    Identity = m_pOwner.Identity,
                    SkillIdentity = m_pMagic.Type,
                    SkillLevel = m_pMagic.Level,
                    CellX = m_pOwner.MapX,
                    CellY = m_pOwner.MapY
                }, true);
            else
                user.Screen.Send(new MsgMagicEffect
                {
                    Identity = m_pOwner.Identity,
                    SkillIdentity = m_pMagic.Type,
                    SkillLevel = m_pMagic.Level,
                    Target = m_idTarget
                }, true);
        }

        private uint GetDieMode()
        {
            return (uint) (HitByMagic() > 0 ? 3 : m_pOwner.IsBowman ? 5 : 1);
        }

        private int HitByMagic()
        {
            // 0 none, 1 normal, 2 xp
            if (m_pMagic == null) return 0;

            if (m_pMagic.WeaponHit == 0)
            {
                return m_pMagic.UseXp == 2 ? 2 : 1;
            }

            if (m_pOwner is Character)
            {
                var pRole = m_pOwner as Character;
                if (pRole.UserPackage[ItemPosition.RightHand] != null && m_pMagic.WeaponHit == 2 &&
                    pRole.UserPackage[ItemPosition.RightHand].Itemtype.MagicAtk > 0)
                {
                    return m_pMagic.UseXp == 2 ? 2 : 1;
                }
            }

            return 0;
        }

        public bool AbortMagic(bool bSynchro)
        {
            if (m_nMagicState == MAGICSTATE_LAUNCH)
            {
                //m_tApply.Clear();
                return false;
            }

            BreakAutoAttack();

            if (m_nMagicState == MAGICSTATE_DELAY)
            {
                //m_tDelay.Clear();
                return false;
            }

            m_pMagic = null;

            if (m_nMagicState == MAGICSTATE_INTONE)
            {
                m_tIntone.Clear();
            }

            m_bAutoAttack = false;
            m_nMagicState = MAGICSTATE_NONE;

            if (bSynchro && m_pOwner is Character)
            {
                m_pOwner.Send(new MsgAction(m_pOwner.Identity, 0, 0, GeneralActionType.AbortMagic));
            }

            return true;
        }

        public void BreakAutoAttack()
        {
            m_bAutoAttack = false;
        }

        public bool IsWeaponMagic(ushort type)
        {
            return type >= 10000 && type < 10256;
        }

        public bool CheckLevel(ushort type, ushort level)
        {
            return Magics.Values.FirstOrDefault(x => x.Type == type && x.Level == level) != null;
        }

        public bool CheckType(ushort type)
        {
            return Magics.ContainsKey(type);
        }

        public bool Create(ushort type, byte level)
        {
            Magic pMagic = new Magic(m_pOwner);
            if (pMagic.Create(type, level))
            {
                return Magics.TryAdd(type, pMagic);
            }
            return false;
        }

        public bool UpLevelByTask(ushort type)
        {
            Magic pMagic;
            if (!Magics.TryGetValue(type, out pMagic))
                return false;
            if (!IsWeaponMagic(pMagic.Type))
                return false;

            byte nNewLevel = (byte) (pMagic.Level + 1);
            if (!FindMagicType(type, nNewLevel))
                return false;

            pMagic.Experience = 0;
            pMagic.Level = nNewLevel;

            return true;
        }

        public bool FindMagicType(ushort type, byte pLevel)
        {
            return ServerKernel.Magictype.Values.FirstOrDefault(x => x.Type == type && x.Level == pLevel) != null;
        }

        public void OnTimer()
        {
            if (m_pMagic == null)
            {
                m_nMagicState = MAGICSTATE_NONE;
                return;
            }

            switch (m_nMagicState)
            {
                case MAGICSTATE_INTONE: // intone
                    {
                        if (m_tIntone != null && !m_tIntone.IsTimeOut())
                            return;

                        if (m_tIntone != null && m_tIntone.IsTimeOut() && !Launch())
                        {
                            m_tApply.Clear();
                            ResetDelay();
                        }

                        m_nMagicState = 2;
                        m_nApplyTimes = (int)Math.Max(1, m_pMagic.ActiveTimes);

                        if (m_tApply == null)
                            m_tApply = new TimeOutMS((int) m_pMagic.StepSeconds);

                        m_tApply.Startup((int) m_pMagic.StepSeconds);
                        break;
                    }
                case MAGICSTATE_LAUNCH: // launch{
                    {
                        if (!m_tApply.IsActive() || m_tApply.TimeOver())
                        {
                            if (m_pMagic.Sort == MAGICSORT_ATTACK && m_idTarget != 0)
                            {
                                var pTarget = m_pOwner.BattleSystem.FindRole(m_idTarget);
                                if (pTarget != null && !pTarget.IsAlive && pTarget.IsAttackable(m_pOwner))
                                    m_pOwner.Kill(pTarget, (uint) GetDieMode());
                            }
                            ResetDelay();
                            m_nMagicState = MAGICSTATE_NONE;
                            AbortMagic(false);
                        }
                        break;
                    }
                case MAGICSTATE_DELAY: // delay
                    {
                        if (m_pOwner.Map.IsTrainingMap()
                            && m_tDelay.IsActive()
                            && m_pMagic.Sort != MAGICSORT_ATKSTATUS)
                        {
                            if (m_tDelay.IsTimeOut())
                            {
                                m_nMagicState = MAGICSTATE_NONE;
                                if (!m_pOwner.ProcessMagicAttack(m_pMagic.Type, m_idTarget, (ushort)m_pPos.X,
                                        (ushort)m_pPos.Y,
                                        0))
                                    m_nMagicState = MAGICSTATE_DELAY;
                            }
                            return;
                        }

                        if (!m_tDelay.IsActive())
                        {
                            m_nMagicState = MAGICSTATE_NONE;
                            AbortMagic(true);
                            return;
                        }

                        if (m_bAutoAttack && m_tDelay.ToNextTime())
                        {
                            if ((m_tDelay.IsActive() && !m_tDelay.TimeOver()))
                                return;

                            m_nMagicState = MAGICSTATE_NONE;
                            m_pOwner.ProcessMagicAttack(m_pMagic.Type, m_idTarget, (ushort)m_pPos.X, (ushort)m_pPos.Y,
                                0);

                            //if (!m_pOwner.Map.IsTrainingMap())
                            if (m_idTarget != 0 && m_pOwner.BattleSystem.FindRole(m_idTarget)?.IsPlayer() == true)
                                AbortMagic(false);
                        }

                        if (m_tDelay.IsActive() && m_tDelay.TimeOver())
                        {
                            m_nMagicState = MAGICSTATE_NONE;
                            AbortMagic(false);
                        }
                        break;
                    }
            }
        }

        public bool Delete(ushort nType)
        {
            Magic trash;
            if (Magics.TryRemove(nType, out trash))
            {
                m_pOwner.Send(new MsgAction(m_pOwner.Identity, nType, 0, 0, GeneralActionType.DropMagic));
                return trash.Delete();
            }

            return false;
        }

        public Magic this[ushort nType] => Magics.TryGetValue(nType, out var ret) ? ret : null;
    }
}
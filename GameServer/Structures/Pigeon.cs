﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - Pigeon.cs
// Last Edit: 2019/11/24 19:04
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System.Collections.Generic;
using System.Linq;
using FtwCore;
using FtwCore.Common;
using FtwCore.Common.Enums;
using FtwCore.Database.Entities;
using FtwCore.Database.Repositories;
using FtwCore.Networking.Packets;
using GameServer.Structures.Entities;

#endregion

namespace GameServer.Structures
{
    public class Pigeon
    {
        private const int _PIGEON_PRICE = 100;
        private const int _PIGEON_ADDITION = 100;
        private const int _PIGEON_TOP_ADDITION = 1500;

        private uint m_dwNext = 1;
        private readonly List<Broadcast> m_lLast = new List<Broadcast>();
        private readonly List<Broadcast> m_lNext = new List<Broadcast>();
        private Broadcast m_pActive;
        private readonly BroadcastLogRepository m_pAdLogRepo = new BroadcastLogRepository();
        private readonly BroadcastQueueRepository m_pAdQueueRepo = new BroadcastQueueRepository();

        /// <summary>
        ///     I think broadcasts wont hurt to use locks :)
        /// </summary>
        private readonly object m_pLock = new object();

        private readonly TimeOut m_pNext = new TimeOut(60);

        public Pigeon()
        {
            m_pNext.Startup(60);

            // if server restarted or whatever, it will fetch the latest and order by addition
            var possibleNext = m_pAdQueueRepo.FetchAll();
            foreach (var bc in possibleNext.OrderByDescending(x => x.Addition).ThenBy(x => x.Identity))
            {
                m_lNext.Add(new Broadcast
                {
                    Identity = m_dwNext++,
                    OwnerIdentity = bc.UserIdentity,
                    OwnerName = bc.UserName,
                    Message = bc.Message,
                    Addition = bc.Addition
                });
                m_pAdQueueRepo.Delete(bc);
            }
        }

        // only for GMs
        public bool Push(string szMessage)
        {
            var newBc = new Broadcast
            {
                Identity = m_dwNext++,
                OwnerIdentity = 0,
                OwnerName = "[GM]",
                Message = szMessage
            };

            lock (m_pLock)
                m_lNext.Insert(0, newBc);
            return true;
        }

        public bool Push(Character pRole, string szMessage, bool bShowError)
        {
            if (szMessage.Length > 80)
            {
                if (bShowError)
                    pRole.SendSysMessage(Language.StrPigeonSendErrStringTooLong);
                return false;
            }

            if (szMessage.Length <= 0)
            {
                if (bShowError)
                    pRole.SendSysMessage(Language.StrPigeonSendErrEmptyString);
                return false;
            }

            if (UserNextMessagesCount(pRole.Identity) >= 5)
            {
                if (bShowError)
                    pRole.SendSysMessage(Language.StrPigeonSendOver5Pieces);
                return false;
            }

            if (pRole.Emoney < _PIGEON_PRICE)
            {
                if (bShowError)
                    pRole.SendSysMessage(Language.StrPigeonSendErrNoEmoney);
                return false;
            }

            if (!pRole.SpendEmoney(_PIGEON_PRICE, EmoneySourceType.Broadcast, null))
            {
                if (bShowError)
                    pRole.SendSysMessage(Language.StrPigeonSendErrNoEmoney);
                return false;
            }

            var newBc = new Broadcast
            {
                Identity = m_dwNext++,
                Message = szMessage,
                OwnerIdentity = pRole.Identity,
                OwnerName = pRole.Name
            };

            lock (m_pLock)
                m_lNext.Add(newBc);

            //m_pAdQueueRepo.Save(new BroadcastQueueEntity
            //{
            //    NextIdentity = m_dwNext++,
            //    UserIdentity = pRole.Identity,
            //    Message = szMessage,
            //    UserName = pRole.Name,
            //    Time = (uint)UnixTimestamp.Now()
            //});

            pRole.SendSysMessage(Language.StrPigeonSendProducePrompt);
            return true;
        }

        private int UserNextMessagesCount(uint idRole)
        {
            lock (m_pLock)
                return m_lNext.Count(x => x.OwnerIdentity == idRole);
        }

        public void Addition(Character pSender, uint idMsg, uint dwAmount)
        {
            Broadcast bc = null;
            int position = 0;

            lock (m_pLock)
            {
                for (int i = 0; i < m_lNext.Count; i++)
                {
                    position = i;
                    var broc = m_lNext[i];

                    if (broc.OwnerIdentity != pSender.Identity || idMsg != broc.Identity) continue;
                    bc = broc;
                    break;
                }

                if (bc == null)
                {
                    pSender.SendSysMessage(Language.StrPigeonAdditionUnexist);
                    return;
                }

                int newPos = 0;
                switch (dwAmount)
                {
                    case _PIGEON_ADDITION:
                        if (!pSender.SpendEmoney(_PIGEON_ADDITION, EmoneySourceType.Broadcast, null))
                        {
                            pSender.SendSysMessage(Language.StrPigeonUrgentErrNoEmoney);
                            return;
                        }

                        newPos = position - 5;
                        if (newPos < 0)
                            newPos = 0;
                        break;
                    case _PIGEON_TOP_ADDITION:
                        if (!pSender.SpendEmoney(_PIGEON_TOP_ADDITION, EmoneySourceType.Broadcast, null))
                        {
                            pSender.SendSysMessage(Language.StrPigeonUrgentErrNoEmoney);
                            return;
                        }

                        newPos = 0;
                        break;
                }

                bc.Addition += (ushort) dwAmount;

                m_lNext.RemoveAt(position);
                m_lNext.Insert(newPos, bc);
            }

            pSender.SendSysMessage(Language.StrPigeonSendProducePrompt);
        }

        public void OnTimer()
        {
            if (m_lNext.Count <= 0)
                return;

            if (!m_pNext.IsTimeOut() && m_lLast.Count > 0)
                return;

            m_pNext.Update();
            var bc = m_lNext[0];
            m_pActive = bc;
            lock (m_pLock)
            {
                m_lLast.Add(bc);
                m_lNext.RemoveAt(0);
            }

            m_pAdLogRepo.Save(new BroadcastLogEntity
            {
                Addition = bc.Addition,
                Message = bc.Message,
                Time = (uint) UnixTimestamp.Now(),
                UserIdentity = bc.OwnerIdentity,
                UserName = bc.OwnerName
            });

            ServerKernel.UserManager.SendToAllUser(new MsgTalk(bc.Message, ChatTone.Broadcast) {Sender = bc.OwnerName});
        }

        public void Request5Last(Character pRole, MsgPigeon pMsg)
        {
            var pNewMsg = new MsgPigeonQuery {Param = pMsg.DwParam};
            ushort wTempPos = 0;
            lock (m_pLock)
            {
                foreach (var bc in m_lNext)
                {
                    if (pNewMsg.Total >= 5)
                        break;
                    pNewMsg.AddBroadcast(bc.Identity, wTempPos++, bc.OwnerIdentity, bc.OwnerName, bc.Addition,
                        bc.Message);
                }
            }

            pRole.Owner.Send(pNewMsg);
        }

        public void RequestNextPage(Character pRole, MsgPigeon pMsg)
        {
            var pNewMsg = new MsgPigeonQuery {Param = 0};
            ushort wTempPos = 0;
            lock (m_pLock)
            {
                foreach (var bc in m_lNext)
                {
                    if (pNewMsg.Total >= 8)
                    {
                        pRole.Owner.Send(pNewMsg);
                        pNewMsg = new MsgPigeonQuery {Param = 0};
                    }

                    pNewMsg.AddBroadcast(bc.Identity, wTempPos++, bc.OwnerIdentity, bc.OwnerName, bc.Addition,
                        bc.Message);
                }
            }

            pRole.Owner.Send(pNewMsg);
        }

        public void SendLastMessage(Character pRole)
        {
            if (m_lLast.Count > 0)
            {
                lock (m_pLock)
                {
                    pRole.Send(new MsgTalk(m_pActive.Message, ChatTone.Broadcast) {Sender = m_pActive.OwnerName});
                }
            }
        }

        public void SaveUnsent()
        {
            foreach (var msg in m_lNext)
            {
                m_pAdQueueRepo.Save(new BroadcastQueueEntity
                {
                    NextIdentity = m_dwNext++,
                    UserIdentity = msg.OwnerIdentity,
                    Message = msg.Message,
                    UserName = msg.OwnerName,
                    Time = msg.Addition
                });
            }
        }
    }

    public sealed class Broadcast
    {
        public ushort Addition;

        public uint Identity;
        private string m_szMessage;

        public uint OwnerIdentity;
        public string OwnerName;

        public string Message
        {
            get => m_szMessage;
            set => m_szMessage = value.Length > 80 ? value.Substring(0, 80) : value;
        }
    }
}
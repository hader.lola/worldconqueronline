﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - UserManager.cs
// Last Edit: 2019/11/24 19:04
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System;
using System.Collections.Concurrent;
using System.Drawing;
using System.Linq;
using FtwCore;
using FtwCore.Common;
using FtwCore.Common.Enums;
using FtwCore.Database.Entities;
using FtwCore.Database.Repositories;
using FtwCore.Networking.Packets;
using FtwCore.Networking.Sockets;
using GameServer.Networking.GameServer;
using GameServer.Structures.Entities;

#endregion

namespace GameServer.Structures.Managers
{
    public sealed class UserManager
    {
        public static CharacterRepository UserRepository = new CharacterRepository();

        private ConcurrentDictionary<uint, User> m_cdCreation;

        private ConcurrentDictionary<uint, Character> m_cdUsers;
        private ConcurrentDictionary<uint, Character> m_cdGms;

        private int m_nMaxPlayers;

        public UserManager()
        {
            m_cdCreation = new ConcurrentDictionary<uint, User>();
            m_cdUsers = new ConcurrentDictionary<uint, Character>();
            m_cdGms = new ConcurrentDictionary<uint, Character>();
        }

        public ConcurrentDictionary<uint, Character> Players => new ConcurrentDictionary<uint, Character>(m_cdUsers);

        public ushort Count => (ushort) (m_cdUsers.Count + m_cdCreation.Count);
        
        public bool AddToCreationQueue(User user)
        {
            return m_cdCreation.TryAdd(user.Identity, user);
        }

        public bool RemoveFromCreationQueue(User user)
        {
            return m_cdCreation.TryRemove(user.Identity, out var _);
        }

        public bool IsOnCreationQueue(User user)
        {
            return m_cdCreation.ContainsKey(user.Identity);
        }

        public Character CreateUser(CharacterEntity user, User account)
        {
            if (user.Identity == 0)
                return null;

            Character pUser = GetUser(user.Identity);
            if (pUser != null)
            {
                LogoutUser(pUser);
                KickoutSocket(pUser, "User was already logged in.");
                account.Send(ServerMessages.Login.AccountInJeopardy);
                account.Disconnect();
                return null;
            }

            pUser = new Character(user.Identity, account, user);
            if (!pUser.Create())
            {
                KickoutSocket(pUser, "Could not create user entity while connecting.");
                return null;
            }

            return account.Character = pUser;
        }

        public bool LoginUser(Character user)
        {
            if (user == null)
                return false;

            if (m_cdUsers.Count >= ServerKernel.MaxOnlinePlayers && !user.IsGm())
            {
                user.Send(new MsgConnectEx(RejectionType.ServerFull));
                return false;
            }

            if (user.IsGm())
                m_cdGms.TryAdd(user.Identity, user);

            user.Send(new MsgTalk(Language.AnswerOk, ChatTone.Login));

            if (user.Owner.IpAddress != user.Owner.LastIpAddress)
            {
                user.Send(new MsgUserIpInfo
                {
                    LoginTime = user.Owner.LastLogin,
                    Type = LastLoginTypes.DifferentPlace,
                    DifferentAddress = true
                });
            }
            else
            {
                user.Send(new MsgUserIpInfo
                {
                    LoginTime = user.Owner.LastLogin,
                    Type = LastLoginTypes.LastLogin,
                    DifferentAddress = false
                });
            }
            
            user.Send(new MsgServerInfo());

            if (user.Life == 0)
                user.Life = 1;
            
            MsgUserInfo info = new MsgUserInfo(user.Name, "", user.Mate)
            {
                Agility = user.Agility,
                AncestorProfession = (byte) user.FirstProfession,
                Attributes = user.AdditionalPoints,
                BoundEmoney = user.BoundEmoney,
                ConquerPoints = user.Emoney,
                EnlightenExp = 0,
                Enlighten = 0,
                Experience = user.Experience,
                Hairstyle = user.Hair,
                Health = (ushort) user.Life,
                Mana = (ushort) user.Mana,
                Identity = user.Identity,
                Level = user.Level,
                Mesh = user.Lookface,
                Vitality = user.Vitality,
                Spirit = user.Spirit,
                Metempsychosis = user.Metempsychosis,
                Strength = user.Force,
                QuizPoints = user.StudentPoints,
                Silver = user.Silver,
                PreviousProfession = (byte) user.LastProfession,
                Profession = (byte) user.Profession,
                PlayerTitle = (byte) user.Title,
                PkPoints = user.PkPoints,
                CountryFlag = user.Country,
                RidePoints = 0,
                Subclass = (byte) user.ActiveSubclass,
                VipLevel = user.VipLevel,
                NameDisplayed = true,
            };

            user.Send(info);

            user.Send(new MsgData());

            user.Send(new MsgVipFunctionValidNotify());

            m_cdUsers.TryAdd(user.Identity, user);

            if (Count > Analytics.Statistic.MaxOnlinePlayers)
                ServerKernel.Analytics.SetMaxOnlinePlayers(Count);

            ServerKernel.Analytics.SetLastLoginUser(user);

            Program.WriteLog(!user.IsGm()
                ? $"{user.Name} has logged in."
                : $"GM {user.Name} has logged in.");

            return true;
        }

        public bool LogoutUser(Character user)
        {
            if (user == null) return true;

            RemoveUser(user.Identity);

            Program.WriteLog(!user.IsGm()
                ? $"{user.Name} has logged out."
                : $"GM {user.Name} has logged out.");
            return true;
        }

        public void LogoutAllUser()
        {
            foreach (var user in m_cdUsers.Values)
            {
                user.Send(new MsgTalk(Language.LogoutAllUserWarning, ChatTone.System));
                user.Disconnect();
                LogoutUser(user);
            }

            Program.WriteLog(Language.LogoutAllUserLogMsg, LogType.WARNING);
        }

        public bool RemoveUser(uint idUser)
        {
            m_cdGms.TryRemove(idUser, out _);
            return m_cdUsers.TryRemove(idUser, out _);
        }

        public Character GetUserByAccount(uint idAccount)
        {
            return m_cdUsers.Values.FirstOrDefault(x => x.Owner.AccountIdentity == idAccount);
        }

        public Character GetUser(uint idUser)
        {
            return m_cdUsers.TryGetValue(idUser, out var user) ? user : null;
        }

        public Character GetUser(string name)
        {
            return m_cdUsers.Values.FirstOrDefault(x => x.Name == name);
        }

        public bool KickoutSocket(Character pUser, string reason = null)
        {
            if (pUser == null)
                return false;

            // todo save user
            pUser.Disconnect();
            //pUser.Owner.UserState = PlayerState.Disconnecting;

            if (!string.IsNullOrEmpty(reason))
                Program.WriteLog($"Kickoutsocket: ({pUser.Identity}:{pUser.Name}) {reason}", LogType.WARNING);

            return true;
        }

        public void OnTimer()
        {
            foreach (var player in m_cdUsers.Values)
            {
                try
                {
                    if ((DateTime.Now - player.Owner.LastActivity).TotalSeconds > 40 && player.Owner.UserState < PlayerState.Disconnecting && player.Owner.IpAddress != "127.0.0.1")
                    {
                        KickoutSocket(player, "Inactivity");
                        continue;
                    }

                    if (player.Owner.UserState != PlayerState.Connected)
                        continue;

                    player.OnTimer();
                }
                catch (Exception ex)
                {
                    Program.WriteLog(ex.ToString(), LogType.EXCEPTION);
                }
            }
        }

        public void OnSecondaryTimer()
        {
            foreach (var player in m_cdUsers.Values)
            {
                try
                {
                    player.OnSecondaryTimer();
                }
                catch (Exception ex)
                {
                    Program.WriteLog(ex.ToString(), LogType.EXCEPTION);
                }
            }
        }
        
        public void SendToAllUser(string message, ChatTone tone = ChatTone.TopLeft)
        {
            SendToAllUser(message, Color.Red, tone);
        }

        public void SendToAllUser(string message, Color color, ChatTone tone = ChatTone.TopLeft)
        {
            MsgTalk msg = new MsgTalk(message, tone, color);
            foreach (var player in m_cdUsers.Values)
                player.Send(msg);
        }

        public void SendToAllUser(byte[] msg)
        {
            foreach (var player in m_cdUsers.Values)
                player.Send(msg);
        }

        public void UpdateNobilityForAllUsers()
        {
            foreach (var player in m_cdUsers.Values)
            {
                player.Nobility?.SendNobilityIcon();
            }
        }

        public void SendPlayersToLogin()
        {
            foreach (var player in m_cdUsers.Values)
            {
                ServerKernel.LoginServer.Send(new MsgLoginUserInfo
                {
                    AccountIdentity = player.Owner.AccountIdentity,
                    IpAddress = player.Owner.IpAddress,
                    MacAddress = player.Owner.MacAddress,
                    UserIdentity = player.Identity,
                    UserName = player.Name,
                    Mode = LoginUserInfoMode.Connect
                });
            }
        }
    }
}
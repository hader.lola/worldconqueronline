﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - TrainingVitality.cs
// Last Edit: 2019/11/24 19:04
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using FtwCore.Common;
using FtwCore.Common.Enums;
using FtwCore.Database.Entities;
using FtwCore.Database.Repositories;
using GameServer.Structures.Entities;

#endregion

namespace GameServer.Structures.Managers
{
    public static class TrainingVitalityManager
    {
        public const int CHISAVE_VALUE1 = 1;
        public const int CHISAVE_VALUE2 = 2;
        public const int CHISAVE_VALUE3 = 4;
        public const int CHISAVE_VALUE4 = 8;
        public const int CHISAVE_NONE = 0;
        public const int CHISAVE_ALL = CHISAVE_VALUE1 | CHISAVE_VALUE2 | CHISAVE_VALUE3 | CHISAVE_VALUE4;

        public static readonly ushort[] CHIVALUE_MIN = {0, 10, 10, 10, 10, 10, 1000, 500, 500, 50, 50, 50, 50, 50};

        public static readonly ushort[] CHIVALUE_MAX =
            {0, 200, 200, 200, 200, 200, 3500, 2000, 2500, 250, 500, 300, 500, 300};

        public static TrainingVitalityRepository Repository; // = new TrainingVitalityRepository();

        private static ConcurrentDictionary<uint, TrainingVitalityStruct> m_dragon =
            new ConcurrentDictionary<uint, TrainingVitalityStruct>();

        private static ConcurrentDictionary<uint, TrainingVitalityStruct> m_phoenix =
            new ConcurrentDictionary<uint, TrainingVitalityStruct>();

        private static ConcurrentDictionary<uint, TrainingVitalityStruct> m_tiger =
            new ConcurrentDictionary<uint, TrainingVitalityStruct>();

        private static ConcurrentDictionary<uint, TrainingVitalityStruct> m_turtle =
            new ConcurrentDictionary<uint, TrainingVitalityStruct>();

        public static void Initialize()
        {
            Repository = new TrainingVitalityRepository();

            var allRank = new FateRankRepository().FetchAll();

            if (allRank != null)
            {
                foreach (var rank in allRank)
                {
                    ServerKernel.FateRank.Add(rank);
                }
            }

            var allChi = Repository.GetAll();
            if (allChi != null)
            {
                foreach (var chi in allChi)
                {
                    CharacterEntity user = UserManager.UserRepository.SearchByIdentity(chi.UserIdentity);
                    if (user == null)
                    {
                        Repository.Delete(chi);
                        Program.WriteLog(
                            $"Chi for user({chi.UserIdentity}) type: {chi.Mode} attr({chi.Power1}, {chi.Power2}, {chi.Power3}, {chi.Power4}) deleted",
                            LogType.WARNING);
                        continue;
                    }

                    TrainingVitalityStruct obj = new TrainingVitalityStruct
                    {
                        Entity = chi,
                        PlayerName = user.Name,
                        Level = user.Level,
                        Profession = (ProfessionType) user.Profession
                    };
                    switch ((ChiPowerType) chi.Mode)
                    {
                        case ChiPowerType.Dragon:
                            m_dragon.TryAdd(chi.UserIdentity, obj);
                            break;
                        case ChiPowerType.Phoenix:
                            m_phoenix.TryAdd(chi.UserIdentity, obj);
                            break;
                        case ChiPowerType.Tiger:
                            m_tiger.TryAdd(chi.UserIdentity, obj);
                            break;
                        case ChiPowerType.Turtle:
                            m_turtle.TryAdd(chi.UserIdentity, obj);
                            break;
                    }
                }
            }
        }

        public static TrainingVitalityStruct Create(Character user, ChiPowerType type)
        {
            TrainingVitalityStruct obj = new TrainingVitalityStruct
            {
                Entity = new TrainingVitalityEntity
                {
                    Mode = (uint) type,
                    UserIdentity = user.Identity
                },
                PlayerName = user.Name,
                Profession = user.Profession,
                Level = user.Level
            };

            switch (type)
            {
                case ChiPowerType.Dragon:
                    m_dragon.TryAdd(user.Identity, obj);
                    break;
                case ChiPowerType.Phoenix:
                    m_phoenix.TryAdd(user.Identity, obj);
                    break;
                case ChiPowerType.Tiger:
                    m_tiger.TryAdd(user.Identity, obj);
                    break;
                case ChiPowerType.Turtle:
                    m_turtle.TryAdd(user.Identity, obj);
                    break;
            }

            Repository.Save(obj.Entity);
            return obj;
        }

        public static bool GetDragon(uint idUser, out TrainingVitalityStruct obj)
        {
            return m_dragon.TryGetValue(idUser, out obj);
        }

        public static bool GetPhoenix(uint idUser, out TrainingVitalityStruct obj)
        {
            return m_phoenix.TryGetValue(idUser, out obj);
        }

        public static bool GetTiger(uint idUser, out TrainingVitalityStruct obj)
        {
            return m_tiger.TryGetValue(idUser, out obj);
        }

        public static bool GetTurtle(uint idUser, out TrainingVitalityStruct obj)
        {
            return m_turtle.TryGetValue(idUser, out obj);
        }

        public static List<TrainingVitalityStruct> GetDragonTop50()
        {
            return m_dragon.Values.OrderByDescending(x => x.GetTotalPoints()).ThenByDescending(x => x.Level)
                .Take(50)
                .ToList();
        }

        public static uint GetUserDragonRanking(uint idUser)
        {
            uint pos = 0;
            bool found = false;
            foreach (var usr in GetDragonTop50())
            {
                if (usr.UserIdentity == idUser)
                {
                    found = true;
                    break;
                }
                pos++;
            }
            if (!found)
                return uint.MaxValue;
            return pos;
        }

        public static List<TrainingVitalityStruct> GetPhoenixTop50()
        {
            return m_phoenix.Values.OrderByDescending(x => x.GetTotalPoints()).ThenByDescending(x => x.Level)
                .Take(50)
                .ToList();
        }

        public static uint GetUserPhoenixRanking(uint idUser)
        {
            uint pos = 0;
            bool found = false;
            foreach (var usr in GetPhoenixTop50())
            {
                if (usr.UserIdentity == idUser)
                {
                    found = true;
                    break;
                }
                pos++;
            }
            if (!found)
                return uint.MaxValue;
            return pos;
        }

        public static List<TrainingVitalityStruct> GetTigerTop50()
        {
            return m_tiger.Values.OrderByDescending(x => x.GetTotalPoints()).ThenByDescending(x => x.Level)
                .Take(50)
                .ToList();
        }

        public static uint GetUserTigerRanking(uint idUser)
        {
            uint pos = 0;
            bool found = false;
            foreach (var usr in GetTigerTop50())
            {
                if (usr.UserIdentity == idUser)
                {
                    found = true;
                    break;
                }
                pos++;
            }
            if (!found)
                return uint.MaxValue;
            return pos;
        }

        public static List<TrainingVitalityStruct> GetTurtleTop50()
        {
            return m_turtle.Values.OrderByDescending(x => x.GetTotalPoints()).ThenByDescending(x => x.Level)
                .Take(50)
                .ToList();
        }

        public static uint GetUserTurtleRanking(uint idUser)
        {
            uint pos = 0;
            bool found = false;
            foreach (var usr in GetTurtleTop50())
            {
                if (usr.UserIdentity == idUser)
                {
                    found = true;
                    break;
                }
                pos++;
            }
            if (!found)
                return uint.MaxValue;
            return pos;
        }
    }

    public struct TrainingVitalityStruct
    {
        public TrainingVitalityEntity Entity;
        public string PlayerName;
        public byte Level;
        public ProfessionType Profession;

        public uint UserIdentity => Entity.UserIdentity;
        public ChiPowerType Mode => (ChiPowerType) Entity.Mode;

        public uint GetTotalPoints()
        {
            uint total = (uint) ((Power(Entity.Power1) / ((float) TrainingVitalityManager.CHIVALUE_MAX[Type(Entity.Power1)] - TrainingVitalityManager.CHIVALUE_MIN[Type(Entity.Power1)])) * 100);
            total += (uint)((Power(Entity.Power2) / ((float)TrainingVitalityManager.CHIVALUE_MAX[Type(Entity.Power2)] - TrainingVitalityManager.CHIVALUE_MIN[Type(Entity.Power2)])) * 100);
            total += (uint)((Power(Entity.Power3) / ((float)TrainingVitalityManager.CHIVALUE_MAX[Type(Entity.Power3)] - TrainingVitalityManager.CHIVALUE_MIN[Type(Entity.Power3)])) * 100);
            total += (uint)((Power(Entity.Power4) / ((float)TrainingVitalityManager.CHIVALUE_MAX[Type(Entity.Power4)] - TrainingVitalityManager.CHIVALUE_MIN[Type(Entity.Power4)])) * 100);
            return total;
        }

        private int Type(uint value)
        {
            return (int) (value / 10000);
        }

        public uint Power(uint value)
        {
            return (value % 10000) - TrainingVitalityManager.CHIVALUE_MIN[Type(value)];
        }

        public uint GetMaxPower(uint value)
        {
            return (uint) (TrainingVitalityManager.CHIVALUE_MAX[Type(value)] - TrainingVitalityManager.CHIVALUE_MIN[Type(value)]);
        }
    }
}
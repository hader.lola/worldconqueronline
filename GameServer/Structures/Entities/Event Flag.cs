﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - Event DeliverFlag.cs
// Last Edit: 2020/01/16 01:23
// Created: 2020/01/16 01:23
// ////////////////////////////////////////////////////////////////////////////////////

using FtwCore.Common.Enums;
using FtwCore.Networking.Packets;
using GameServer.Structures.Events;

namespace GameServer.Structures.Entities
{
    public sealed class EventFlag : ScreenObject
    {
        public const int ALIVE_SECONDS = 60;

        private MsgPlayer m_pMsg;

        public EventFlag(MsgPlayer msg, uint idMap) 
            : base(msg.Identity)
        {
            m_pMsg = msg;
            m_idMap = idMap;
            m_usMapX = msg.MapX;
            m_usMapY = msg.MapY;
        }

        public override uint Identity => m_pMsg.Identity;

        public override ushort MapX
        {
            get => base.MapX;
            set
            {
                base.MapX = value;
                m_pMsg.MapX = value;
            }
        }

        public override ushort MapY
        {
            get => base.MapY;
            set
            {
                base.MapY = value;
                m_pMsg.MapY = value;
            }
        }

        public bool Grab(Character pUser)
        {
            try
            {
                ServerKernel.CaptureTheFlag.AddPoints(pUser, 1, CaptureTheFlagPointType.GrabFlag);
                pUser.AttachStatus(pUser, FlagInt.CTF_FLAG, 0, ALIVE_SECONDS, 0, 0);
                pUser.Send(new MsgWarFlag
                {
                    Type = WarFlagType.GrabFlagEffect,
                    Identity = pUser.Identity
                });
                pUser.Send(new MsgWarFlag {Type = WarFlagType.GenerateTimer, Identity = ALIVE_SECONDS});
                LeaveMap();
                return true;
            }
            catch
            {
                return false;
            }
        }

        #region Map

        public override void SendSpawnTo(Character user)
        {
            user?.Send(m_pMsg);
        }

        #endregion
    }
}
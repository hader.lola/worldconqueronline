﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - Weapon Skill.cs
// Last Edit: 2019/11/24 19:04
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using FtwCore.Database.Entities;
using FtwCore.Database.Repositories;
using FtwCore.Networking.Packets;
using GameServer.Structures.Entities;

#endregion

namespace GameServer.Structures
{
    public class WeaponSkill
    {
        private readonly Character m_pOwner;
        public ConcurrentDictionary<ushort, WeaponSkillEntity> Skills;

        public WeaponSkill(Character pOwner)
        {
            m_pOwner = pOwner;
            Skills = new ConcurrentDictionary<ushort, WeaponSkillEntity>();

            IList<WeaponSkillEntity> ws = new WeaponSkillRepository().FetchAll(m_pOwner.Identity);
            if (ws != null)
            {
                foreach (var wes in ws)
                {
                    Skills.TryAdd((ushort) wes.Type, wes);
                }
            }
        }

        public bool Create(ushort nType, byte nLevel)
        {
            if (Skills.ContainsKey(nType))
                return false;

            WeaponSkillEntity dbSkill = new WeaponSkillEntity
            {
                Experience = 0,
                Level = Math.Max((byte) 1, nLevel),
                Type = nType,
                OwnerIdentity = m_pOwner.Identity
            };
            new WeaponSkillRepository().Save(dbSkill);

            m_pOwner.Send(new MsgWeaponSkill(nType, nLevel, 0));
            return Skills.TryAdd(nType, dbSkill);
        }

        public bool AwardExperience(ushort nType, int nExp)
        {
            WeaponSkillEntity dbSkill;
            if (!Skills.TryGetValue(nType, out dbSkill))
            {
                dbSkill = new WeaponSkillEntity
                {
                    Experience = 0,
                    Level = 0,
                    Type = nType,
                    OwnerIdentity = m_pOwner.Identity
                };
                Skills.TryAdd(nType, dbSkill);
                new WeaponSkillRepository().Save(dbSkill);
            }

            if (dbSkill.Level >= 20)
                return true;

            dbSkill.Experience += (uint) nExp;
            if (dbSkill.Level < 20 && dbSkill.Experience > MsgWeaponSkill.EXP_PER_LEVEL[dbSkill.Level])
            {
                dbSkill.Experience = 0;
                dbSkill.Level += 1;
            }

            new WeaponSkillRepository().Save(dbSkill);

            m_pOwner.Send(new MsgWeaponSkill(nType, dbSkill.Level, dbSkill.Experience));
            return true;
        }

        public void SendAll()
        {
            foreach (var skill in Skills.Values)
            {
                var pMsg = new MsgWeaponSkill(skill.Type, skill.Level, skill.Experience);
                m_pOwner.Send(pMsg);
            }
        }
    }
}
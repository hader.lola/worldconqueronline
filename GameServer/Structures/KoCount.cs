﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - KoCount.cs
// Last Edit: 2019/11/24 19:04
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using FtwCore.Database.Entities;
using FtwCore.Database.Repositories;

#endregion

namespace GameServer.Structures
{
    public struct IKoCount
    {
        private SupermanEntity m_dbSuper;

        public IKoCount(SupermanEntity pSuper)
        {
            m_dbSuper = pSuper;
        }

        public uint Identity => m_dbSuper.Identity;
        public string Name => m_dbSuper.Name;

        public uint KoCount
        {
            get => m_dbSuper.Amount;
            set
            {
                m_dbSuper.Amount = value;
                Save();
            }
        }

        public bool Save()
        {
            return new SupermanRepository().Save(m_dbSuper);
        }

        public bool Delete()
        {
            return new SupermanRepository().Delete(m_dbSuper);
        }
    }
}
﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - Emperor.cs
// Last Edit: 2019/11/28 18:32
// Created: 2019/11/28 18:18
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System;
using FtwCore;
using FtwCore.Database.Entities;
using GameServer.Structures.Entities;

#endregion

namespace GameServer.Structures.Monarchy
{
    public class Emperor
    {
        private MonarchyEntity m_pEntity;

        public Emperor(MonarchyEntity entity)
        {
            m_pEntity = entity;
            EmperorIdentity = entity?.EmperorIdentity ?? 0;
            EmperorName = entity?.EmperorName ?? Language.StrNone;
        }

        public uint EmperorIdentity { get; }

        public string EmperorName { get; }

        public Character User => ServerKernel.UserManager.GetUser(EmperorIdentity);

        public long CurrentMoney
        {
            get => m_pEntity.CurrentMoney;
            set
            {
                m_pEntity.CurrentMoney = value;
                Save();
            }
        }

        public long StartMoney => m_pEntity.StartMoney;

        public long EndMoney
        {
            get => m_pEntity.AbdicateMoney;
            private set
            {
                m_pEntity.AbdicateMoney = value;
                Save();
            }
        }

        public long CurrentEmoney
        {
            get => m_pEntity.CurrentEMoney;
            set
            {
                m_pEntity.CurrentEMoney = value;
                Save();
            }
        }

        public long StartEmoney => m_pEntity.StartEMoney;

        public long EndEmoney
        {
            get => m_pEntity.AbdicateEMoney;
            private set
            {
                m_pEntity.AbdicateEMoney = value;
                Save();
            }
        }

        public void Abdicate()
        {
            EndMoney = CurrentMoney;
            EndEmoney = CurrentEmoney;
            m_pEntity.Abdication = DateTime.Now;
            Save();
        }

        public bool Save()
        {
            return ServerKernel.Empire.MonarchyRepository.Save(m_pEntity);
        }

        public bool Delete()
        {
            return ServerKernel.Empire.MonarchyRepository.Delete(m_pEntity);
        }
    }
}
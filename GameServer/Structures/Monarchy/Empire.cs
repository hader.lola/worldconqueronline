﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - Empire.cs
// Last Edit: 2019/11/27 17:39
// Created: 2019/11/27 17:39
// ////////////////////////////////////////////////////////////////////////////////////

using System;
using FtwCore.Common;
using FtwCore.Common.Enums;
using FtwCore.Database.Entities;
using FtwCore.Database.Repositories;
using GameServer.Structures.Entities;

namespace GameServer.Structures.Monarchy
{
    public sealed class Empire
    {
        private const double _MONEY_PERCENTUAL_D = 0.08d;
        private const double _EMONEY_PERCENTUAL_D = 0.05d;

        public MonarchyRepository MonarchyRepository;

        private Emperor m_pEmperor;

        public Emperor Emperor => m_pEmperor;

        public bool Initialize()
        {
            MonarchyRepository = new MonarchyRepository();
            MonarchyEntity monarchyEntity = MonarchyRepository.GetEmperor();

            bool systemMonarchy = false;
            if (monarchyEntity == null)
            {
                ServerKernel.Log.SaveLog($"No emperor set for server!", LogType.WARNING);

                monarchyEntity = new MonarchyEntity
                {
                    EmperorIdentity = 0,
                    EmperorName = "SYSTEM",
                    StartDate = DateTime.Now,
                    CurrentMoney = 1000000,
                    CurrentEMoney = 0,
                    AbdicateMoney = 0,
                    AbdicateEMoney = 0,
                    StartMoney = 0,
                    StartEMoney = 0,
                    Abdication = null
                };
                systemMonarchy = true;
            }

            m_pEmperor = new Emperor(monarchyEntity);

            if (systemMonarchy)
                m_pEmperor.Save();
            return true;
        }

        public long Money
        {
            get => m_pEmperor?.CurrentMoney ?? 0;
            set
            {
                if (m_pEmperor == null)
                    return;
                m_pEmperor.CurrentMoney = value;
            }
        }

        public long Emoney
        {
            get => m_pEmperor?.CurrentEmoney ?? 0;
            set
            {
                if (m_pEmperor == null)
                    return;
                m_pEmperor.CurrentEmoney = value;
            }
        }

        #region Currency

        /// <summary>
        /// Add an required amount of money to the empire funds.
        /// </summary>
        /// <param name="amount">The amount without taxes.</param>
        public void AwardMoney(int amount)
        {
            Money += (int)Math.Max(amount*_MONEY_PERCENTUAL_D, 1);
        }

        public bool SpendMoney(int amount)
        {
            if (Money < amount)
                return false;
            Money -= amount;
            return true;
        }

        public void AwardEmoney(int amount)
        {
            Emoney += (int) Math.Max(1, amount*_EMONEY_PERCENTUAL_D);
        }

        public bool SpendEmoney(int amount)
        {
            if (Emoney < amount)
                return false;

            Emoney -= amount;
            return true;
        }

        #endregion

        #region Prizes

        public void AppendPrize(EmpireBoost boost, Character user)
        {
            if (user.Identity != Emperor?.EmperorIdentity)
            {
                //user.SendSysMessage();
                return;
            }
        }

        #endregion
    }
}
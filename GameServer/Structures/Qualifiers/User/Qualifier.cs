﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - Qualifier.cs
// Last Edit: 2019/11/24 19:04
// Created: 2019/10/17 12:08
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using FtwCore;
using FtwCore.Common;
using FtwCore.Database.Entities;
using FtwCore.Networking.Packets;
using GameServer.Structures.Entities;
using GameServer.Structures.Managers;
using GameServer.World;

#endregion

namespace GameServer.Structures.Qualifiers.User
{
    public sealed class ArenaQualifierManager
    {
        public const int MIN_LEVEL = 40;
        public const int ARENA_CHECK_INTERVAL_MS = 750;
        public const int PRICE_PER_1500_POINTS = 9000000;

        private static readonly uint[] m_dwStartPoints =
        {
            1500, // over 70
            2200, // over 90
            2700, // over 100
            3200, // over 110
            4000 // over 120
        };

        /// <summary>
        ///     Users in the Queue awaiting for a match.
        /// </summary>
        private readonly ConcurrentDictionary<uint, Character> m_dicObjects =
            new ConcurrentDictionary<uint, Character>();

        /// <summary>
        ///     Users registered in the Arena Qualifier.
        /// </summary>
        private readonly ConcurrentDictionary<uint, PlayerQualifierUser> m_dicRanking =
            new ConcurrentDictionary<uint, PlayerQualifierUser>();

        private Map m_pBaseMap;

        public static readonly IdentityGenerator MapIdentity = new IdentityGenerator(900001, 999999);

        private readonly TimeOutMS m_tArenaCheck = new TimeOutMS(ARENA_CHECK_INTERVAL_MS);

        /// <summary>
        ///     Matches being played.
        /// </summary>
        private readonly ConcurrentDictionary<uint, PlayerQualifierMatch> m_tMatches =
            new ConcurrentDictionary<uint, PlayerQualifierMatch>();

        public QualifierState Status { get; private set; } = QualifierState.Disabled;

        public bool Create()
        {
            if (!ServerKernel.Maps.TryGetValue(900000, out m_pBaseMap))
            {
                Program.WriteLog("COULD NOT START ARENA QUALIFIER MAP EXCEPTION", LogType.ERROR);
                return false;
            }

            IList<ArenaEntity> allArena = Database.Arena.FetchAll(0);
            if (allArena != null)
            {
                foreach (var arena in allArena)
                {
                    if (!m_dicRanking.TryAdd(arena.PlayerIdentity, new PlayerQualifierUser(arena)))
                        Program.WriteLog($"Duplicate key for user {arena.PlayerIdentity}:{arena.Name}",
                            LogType.WARNING);
                }
            }

            IniFileName allRewards = new IniFileName($"{Environment.CurrentDirectory}\\ini\\HonorRewards.ini");
            foreach (string pos in allRewards.GetEntryNames("Rewards"))
            {
                string[] values = allRewards.GetEntryValue("Rewards", pos)?.ToString().Split('/');
                if (values != null)
                    ServerKernel.HonorRewards.Add(new HonorReward
                    {
                        Position = int.Parse(pos),
                        ArenaDailyReward = int.Parse(values[0]),
                        ArenaWeeklyReward = int.Parse(values[1])
                    });
            }

            StreamReader reader = new StreamReader($"{Environment.CurrentDirectory}\\ini\\TeamArenaHonorRewards.ini");
            string szLine = "";
            int nRank = 1;
            while ((szLine = reader.ReadLine()) != null)
            {
                if (szLine.ToLower().StartsWith("rankamount"))
                    continue;
                HonorReward honor = ServerKernel.HonorRewards.FirstOrDefault(x => x.Position == nRank++);
                if (honor.Position == 0)
                    continue;
                honor.TeamHonorReward = int.Parse(szLine);
            }

            Status = QualifierState.Ready;
            return true;
        }

        public static uint GetInitialPoints(byte level)
        {
            if (level < MIN_LEVEL)
                return 0;
            if (level < 90)
                return m_dwStartPoints[0];
            if (level < 100)
                return m_dwStartPoints[1];
            if (level < 110)
                return m_dwStartPoints[2];
            if (level < 120)
                return m_dwStartPoints[3];
            return m_dwStartPoints[4];
        }

        public bool GenerateFirstData(Character user)
        {
            if (m_dicRanking.TryGetValue(user.Identity, out _)) // existing user
                return false;

            PlayerQualifierUser player = new PlayerQualifierUser(user);
            return m_dicRanking.TryAdd(user.Identity, player);
        }

        public bool ContainsUser(uint identity)
        {
            return m_dicRanking.ContainsKey(identity);
        }

        public PlayerQualifierUser GetUser(uint identity)
        {
            return m_dicRanking.TryGetValue(identity, out var value) ? value : null;
        }

        public bool IsUserInsideMatch(uint identity)
        {
            return m_tMatches.Values.FirstOrDefault(x =>
                       x.User0.Identity == identity || x.User1.Identity == identity) != null;
        }

        public PlayerQualifierMatch FindMatch(uint identity)
        {
            return m_tMatches.Values.FirstOrDefault(x => x.User0.Identity == identity || x.User1.Identity == identity);
        }

        public bool IsUserQueued(uint identity)
        {
            return m_dicObjects.ContainsKey(identity);
        }

        public bool Inscribe(Character user)
        {
            if (Status != QualifierState.Ready)
            {
                user.SendSysMessage(Language.StrQualifyingNotReady);
                return false;
            }

            if (!ContainsUser(user.Identity) && !GenerateFirstData(user))
            {
                user.SendSysMessage(Language.StrQualifyingCouldNotRegister, Color.White);
                return false;
            }

            if (user.Level < 40)
            {
                user.SendSysMessage(Language.StrQualifyingNotEnoughLevel, Color.White);
                return false;
            }

            if (user.Map.IsPrisionMap())
            {
                user.SendSysMessage(Language.StrQualifyingPrisonMap);
                return false;
            }

            PlayerQualifierUser player = GetUser(user.Identity);
            if (player.IsBanned)
            {
                if (player.BanRemainingHours > 0)
                    user.SendSysMessage(string.Format(Language.StrQualifyingBanned0, player.BanRemainingHours));
                else
                    user.SendSysMessage(Language.StrQualifyingBanned1);
                player.Status = ArenaWaitStatus.NOT_SIGNED_UP;
                return false;
            }

            if (IsUserInsideMatch(user.Identity))
            {
                //
                user.SendSysMessage(Language.StrQualifyingTryAgain, Color.White);
                return false;
            }

            player.JoinTime = DateTime.Now;

            if (m_dicObjects.ContainsKey(user.Identity))
            {
                player.Status = ArenaWaitStatus.WAITING_FOR_OPPONENT;
                user.SendSysMessage(Language.StrQualifyingRejoin, Color.White);
                return true;
            }

            player.Status = ArenaWaitStatus.WAITING_FOR_OPPONENT;
            return m_dicObjects.TryAdd(user.Identity, user);
        }

        public bool Uninscribe(Character user)
        {
            if (Status != QualifierState.Ready)
                return false;

            if (!ContainsUser(user.Identity) && !GenerateFirstData(user))
            {
                // might not happen. if I'm uninscribing, I'm already regitered.?
                Program.WriteLog($"User {user.Identity}:{user.Name} tried to quit from qualifier and is not inscribed.",
                    LogType.WARNING);
                return false;
            }

            if (!IsUserInsideMatch(user.Identity) && !IsUserQueued(user.Identity))
            {
                user.PlayerQualifier.Status = ArenaWaitStatus.NOT_SIGNED_UP;
                return false; // user is not inscribed.
            }

            if (IsUserInsideMatch(user.Identity))
            {
                PlayerQualifierMatch match = FindMatch(user.Identity);
                match.QuitMatch(user.Identity, !match.IsRunning());
            }

            PlayerQualifierUser player = GetUser(user.Identity);
            if (player == null)
                return false; // ????

            player.JoinTime = DateTime.MinValue;
            player.Status = ArenaWaitStatus.NOT_SIGNED_UP;
            return m_dicObjects.TryRemove(user.Identity, out _);
        }

        public void OnTimer()
        {
            if (!m_tArenaCheck.ToNextTime(ARENA_CHECK_INTERVAL_MS))
                return;

            foreach (var player in m_dicObjects.Values.Where(x =>
                x.PlayerQualifier.Status == ArenaWaitStatus.WAITING_FOR_OPPONENT))
            {
                try
                {
                    CreateMatch(player);
                }
                catch (Exception ex)
                {
                    Program.WriteLog(ex.ToString(), LogType.EXCEPTION);
                }
            }

            foreach (var match in m_tMatches.Values)
            {
                try
                {
                    if (match.Status >= ArenaStatus.Disposed)
                    {
                        m_tMatches.TryRemove(match.MapIdentity, out _);
                        MapIdentity.ReturnIdentity(match.MapIdentity);

                        Character user = ServerKernel.UserManager.GetUser(match.User0.Identity);
                        if (user != null)
                        {
                            user.PlayerQualifier.Status = ArenaWaitStatus.WAITING_INACTIVE;
                            m_dicObjects.TryAdd(user.Identity, user);
                        }

                        user = ServerKernel.UserManager.GetUser(match.User1.Identity);
                        if (user != null)
                        {
                            user.PlayerQualifier.Status = ArenaWaitStatus.WAITING_INACTIVE;
                            m_dicObjects.TryAdd(user.Identity, user);
                        }

                        ServerKernel.Maps.TryRemove(match.MapIdentity, out _);
                        m_tMatches.TryRemove(match.MapIdentity, out _);

                        MapIdentity.ReturnIdentity(match.MapIdentity);
                    }
                    else
                    {
                        match.OnTimer();
                    }
                }
                catch (Exception ex)
                {
                    Program.WriteLog(ex.ToString(), LogType.EXCEPTION);
                }
            }
        }

        public int GetUserPosition(uint idUser)
        {
            if (!ContainsUser(idUser))
                return 0;
            if (GetUser(idUser).GamesToday <= 0)
                return 0;

            int pos = 1;
            foreach (var user in m_dicRanking.Values.Where(x => x.GamesToday > 0)
                .OrderByDescending(x => x.Points)
                .ThenByDescending(x => x.Wins)
                .ThenBy(x => x.Loss))
            {
                if (user.Identity == idUser)
                    return pos;
                pos++;
            }

            return pos > 1000 ? 0 : pos;
        }

        public void CreateMatch(Character user)
        {
            if (user.PlayerQualifier.Status != ArenaWaitStatus.WAITING_FOR_OPPONENT)
                return;

            Character target = FindTarget(user.PlayerQualifier.Points, user.Identity);
            if (target == null)
                return;

            if (user.Map.IsPrisionMap())
            {
                Uninscribe(user);
                return;
            }

            if (target.Map.IsPrisionMap())
            {
                Uninscribe(target);
                return;
            }

            m_dicObjects.TryRemove(user.Identity, out _);
            m_dicObjects.TryRemove(target.Identity, out _);

            PlayerQualifierMatch match = new PlayerQualifierMatch(PrepareMap());
            if (!match.Create(user.PlayerQualifier, target.PlayerQualifier))
            {
                Uninscribe(user);
                Uninscribe(target);
                return;
            }

            user.PlayerQualifier.Status = ArenaWaitStatus.WAITING_INACTIVE;
            user.PlayerQualifier.Status = ArenaWaitStatus.WAITING_INACTIVE;

            m_tMatches.TryAdd(match.MapIdentity, match);
        }

        private Map PrepareMap()
        {
            DynamicMapEntity dynaMap = new DynamicMapEntity
            {
                Identity = (uint) MapIdentity.GetNextIdentity,
                MapDoc = m_pBaseMap.MapDoc,
                Name = "ArenaQualifier",
                Type = 7,
                FileName = "newarena.DMap"
            };
            Map temp = new Map(dynaMap);
            temp.Load();
            return temp;
        }

        public Character FindTarget(uint dwPoints, uint idSender)
        {
            PlayerQualifierUser sender = GetUser(idSender);
            List<Character> possibleTargets = new List<Character>();
            foreach (var player in m_dicObjects.Values
                .Where(x => x.Identity != idSender &&
                            x.PlayerQualifier.Status == ArenaWaitStatus.WAITING_FOR_OPPONENT &&
                            x.PlayerQualifier.IsMatchEnable(sender.Grade))
                .OrderByDescending(x => x.PlayerQualifier?.Points)
                .ThenByDescending(x => x.PlayerQualifier?.Wins))
            {
                possibleTargets.Add(player);
            }

            if (possibleTargets.Count <= 0)
                return m_dicObjects.Values
                    .Where(x => x.Identity != idSender &&
                                x.PlayerQualifier.Status == ArenaWaitStatus.WAITING_FOR_OPPONENT &&
                                x.PlayerQualifier.IsMatchEnable(sender.Grade))
                    .OrderByDescending(x => x.PlayerQualifier.Points)
                    .ThenByDescending(x => x.PlayerQualifier.Wins)
                    .FirstOrDefault();

            return possibleTargets[ThreadSafeRandom.RandGet(0, possibleTargets.Count) % possibleTargets.Count];
        }

        public MsgQualifyingFightersList FightersList(int nPage = 0)
        {
            MsgQualifyingFightersList msg = new MsgQualifyingFightersList
            {
                Showing = 0,
                PlayerAmount = (uint) m_dicObjects.Count,
            };
            int i = 0, a = 0;
            foreach (var match in m_tMatches.Values.Where(x => x.IsRunning()))
            {
                if (nPage > 0 && i++ < nPage * 6)
                    continue;

                if (a++ >= 6)
                    break;

                msg.AddMatch(match.User0.Identity, match.User0.Lookface, match.User0.Name, match.User0.Level,
                    (uint) match.User0.Profession,
                    (uint) match.User0.Ranking, match.User0.Points, match.User0.Wins, match.User0.Loss,
                    match.User0.HonorPoints, match.User0.TotalHonorPoints,
                    match.User1.Identity, match.User1.Lookface, match.User1.Name, match.User1.Level,
                    (uint) match.User1.Profession,
                    (uint) match.User1.Ranking, match.User1.Points, match.User1.Wins, match.User1.Loss,
                    match.User1.HonorPoints, match.User1.TotalHonorPoints);
            }

            msg.Showing = (uint) (msg.MatchesCount - nPage);
            return msg;
        }

        public MsgQualifyingSeasonRankList GetTop10(MsgQualifyingSeasonRankList msg)
        {
            int count = 0;
            foreach (var latest in m_dicRanking.Values.Where(x => x.LastRanking > 0).OrderBy(x => x.LastRanking))
            {
                if (count++ >= 10)
                    break;
                msg.AddPlayer(latest.Identity, latest.Name, latest.Lookface, latest.Level, (uint) latest.Profession,
                    latest.LastPoints, latest.LastRanking, latest.LastWins, latest.LastLoss);
            }

            return msg;
        }

        public MsgQualifyingRank GetRanking(MsgQualifyingRank msg)
        {
            switch (msg.RankType)
            {
                case ArenaRankType.QUALIFIER_RANK:
                {
                    var rank = m_dicRanking.Values.Where(x => x.GamesToday > 0)
                        .OrderByDescending(x => x.Points)
                        .ThenByDescending(x => x.Wins)
                        .ThenBy(x => x.Loss)
                        .ToList();

                    msg.Count = (uint) rank.Count;

                    int idx = (msg.PageNumber - 1) * 10;
                    int nStartIdx = idx;

                    for (; idx < nStartIdx + 10; idx++)
                    {
                        if (idx >= rank.Count)
                            break;
                        var obj = rank[idx];
                        msg.AddPlayer((ushort) (idx + 1), obj.Name, 0, obj.Points, (uint) obj.Profession, obj.Level,
                            obj.Identity);
                    }

                    break;
                }

                case ArenaRankType.HONOR_HISTORY:
                {
                    var rank = m_dicRanking.Values.Where(x => x.TotalHonorPoints > 0)
                        .OrderByDescending(x => x.TotalHonorPoints)
                        .ThenByDescending(x => x.Wins)
                        .ThenBy(x => x.Loss)
                        .ToList();

                    msg.Count = (uint) rank.Count;

                    int idx = (msg.PageNumber - 1) * 10;
                    int nStartIdx = idx;

                    for (; idx < nStartIdx + 10; idx++)
                    {
                        if (idx >= rank.Count)
                            break;
                        var obj = rank[idx];
                        msg.AddPlayer((ushort) (idx + 1), obj.Name, 6004, obj.TotalHonorPoints, (uint) obj.Profession,
                            obj.Level, obj.Identity);
                    }

                    break;
                }

                default:
                {
                    ServerKernel.Log.SaveLog($"MsgQualifyingRank::{msg.RankType}");
                    break;
                }
            }

            return msg;
        }

        public void Reset()
        {
            foreach (var player in m_dicObjects.Values)
            {
                player.PlayerQualifier.SetLastRank((uint) player.PlayerQualifier.Ranking);
            }

            foreach (var player in m_dicRanking.Values)
                player.Reset();
        }

        public void WatchMatch(Character watcher, uint idRole)
        {
            PlayerQualifierMatch match = FindMatch(idRole);
            if (match == null) // match does not exist
                return;

            if (FindMatch(watcher.Identity) != null) // user is already in a match
                return;

            if (!match.IsRunning())
                return;

            if (watcher.Map.IsRecordDisable())
            {
                uint idMap = 1002;
                Point pos = new Point(430, 378);
                if (watcher.Map.GetRebornMap(ref idMap, ref pos))
                {
                    watcher.SetRecordPos(idMap, (ushort) pos.X, (ushort) pos.Y);
                }
                else
                {
                    watcher.SetRecordPos(1002, 430, 378);
                }
            }
            else
            {
                watcher.SetRecordPos(watcher.MapIdentity, watcher.MapX, watcher.MapY);
            }

            watcher.SetWitness(true);
            Tile target = match.GetRandomPoint();
            watcher.FlyMap(match.MapIdentity, target.X, target.Y);

            watcher.Send(new MsgArenicWitness
            {
                Cheers1 = match.Waving1,
                Cheers2 = match.Waving2
            });

            match.SendToMap();
        }

        public void QuitWatch(Character user)
        {
            if (!user.IsArenaWitness || user.MapIdentity < 900000)
                return;

            user.Send(new MsgArenicWitness
            {
                Action = MsgArenicWitness.Leave
            });
            user.SetWitness(false);
            user.FlyMap(user.RecordMapIdentity, user.RecordMapX, user.RecordMapY);
        }

        public void WitnessVote(Character voter, uint idTarget)
        {
            PlayerQualifierMatch match = FindMatch(idTarget);
            if (match == null) // match does not exist
                return;

            if (match.MapIdentity != voter.MapIdentity)
                return;

            match.WaveForUser(idTarget, voter);
            match.SendToMap();
        }
    }

    public enum QualifierState
    {
        Disabled,
        Loading,
        Ready
    }
}
﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - PlayerQualifierMatch.cs
// Last Edit: 2019/11/24 19:04
// Created: 2019/10/19 15:03
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System;
using System.Collections.Generic;
using System.Drawing;
using FtwCore;
using FtwCore.Common;
using FtwCore.Common.Enums;
using FtwCore.Networking.Packets;
using GameServer.Structures.Entities;
using GameServer.World;

#endregion

namespace GameServer.Structures.Qualifiers.User
{
    public sealed class PlayerQualifierMatch
    {
        public const int PROTECTION_TIME = 10;
        public const int INVITATION_TIME = 60;
        public const int MATCH_TIME = 60 * 5;

        private List<uint> m_listVotes = new List<uint>();

        private PlayerQualifierUser m_User0;
        private PlayerQualifierUser m_User1;

        private TimeOut m_tStartup = new TimeOut(PROTECTION_TIME);
        private TimeOut m_tInvitation = new TimeOut(INVITATION_TIME);
        private TimeOut m_tMatchTime = new TimeOut(MATCH_TIME);

        private bool m_bAccept0;
        private bool m_bAccept1;

        public uint m_idWinner;

        private Map m_pMap;

        public PlayerQualifierMatch(Map map)
        {
            m_pMap = map;

            ServerKernel.Maps.TryAdd(map.Identity, map);

            Status = ArenaStatus.None;
        }

        public ArenaStatus Status { get; private set; }

        public PlayerQualifierUser User0 => m_User0;
        public PlayerQualifierUser User1 => m_User1;

        public uint Points1;
        public uint Points2;

        public uint Watchers => (uint) Math.Max(0, m_pMap.PlayerSet.Count - 2);
        public uint MapIdentity => m_pMap?.Identity ?? 0u;

        public uint Waving1 = 0u;
        public uint Waving2 = 0u;

        public bool Create(PlayerQualifierUser user0, PlayerQualifierUser user1)
        {
            if (m_pMap == null)
                return false;

            if (user0 == null || user1 == null)
                return false;

            if (user0.IsBanned || user1.IsBanned)
                return false;

            m_User0 = user0;
            m_User1 = user1;

            MsgQualifyingInteractive msg = new MsgQualifyingInteractive
            {
                Type = ArenaType.START_COUNT_DOWN,
                Identity = (uint) ArenaWaitStatus.WAITING_INACTIVE
            };

            user0.Send(msg);
            user1.Send(msg);

            Status = ArenaStatus.WaitingApproval;
            m_tInvitation.Startup(INVITATION_TIME);
            return true;
        }

        public void Accept(uint idUser)
        {
            if (idUser == m_User0.Identity)
                m_bAccept0 = true;
            else if (idUser == m_User1.Identity)
                m_bAccept1 = true;
        }

        /// <returns>If the match has been accepted by both users and is waiting approval.</returns>
        public bool IsReadyToStart()
        {
            return m_bAccept0 && m_bAccept1 && Status == ArenaStatus.WaitingApproval;
        }

        /// <returns>If the match has already teleported both users and is ready to start.</returns>
        public bool IsRunning()
        {
            return Status >= ArenaStatus.NotStarted && Status < ArenaStatus.Finished;
        }

        /// <returns>If the users inside are already fighting.</returns>
        public bool HasStarted()
        {
            return Status > ArenaStatus.NotStarted && Status < ArenaStatus.Finished;
        }

        /// <returns>If the match has ended, both users teleported and this instance can be deleted.</returns>
        public bool ReadyToDispose()
        {
            return Status == ArenaStatus.Disposed;
        }

        public bool IsAttackEnable()
        {
            return m_tStartup.IsTimeOut() && Status == ArenaStatus.Running;
        }

        public void EnterMatch()
        {
            Status = ArenaStatus.NotStarted;

            PrepareUser(User0);
            PrepareUser(User1);

            m_tStartup.Startup(PROTECTION_TIME);

            SendToMap();
        }

        private void PrepareUser(PlayerQualifierUser user)
        {
            Character player = user.User;

            player.DetachBadlyStatus();
            player.DetachStatus(FlagInt.RIDING);

            if (!player.IsAlive)
                player.Reborn(false, true);

            if (player.Map.IsRecordDisable() && !player.Map.IsPrisionMap())
            {
                uint idMap = 0;
                Point pos = new Point();
                if (!player.Map.GetRebornMap(ref idMap, ref pos))
                {
                    player.SetRecordPos(1002, 430, 378);
                }
                else
                {
                    player.SetRecordPos(idMap, (ushort) pos.X, (ushort) pos.Y);
                }
            }
            else
            {
                player.SetRecordPos(player.MapIdentity, player.MapX, player.MapY);
            }

            Tile tpPos = m_pMap.GetRandomPosition();
            player.FlyMap(m_pMap.Identity, tpPos.X, tpPos.Y);

            user.OldPkMode = player.PkMode;
            player.ChangePkMode(PkModeType.FreePk);

            if (player.Identity == User0.Identity)
                player.SendArenaInformation(User1.User);
            else if (player.Identity == User1.Identity)
                player.SendArenaInformation(User0.User);

            player.Send(new MsgArenicScore
            {
                Damage1 = Points1,
                Damage2 = Points2,
                Name1 = User0.Name,
                Name2 = User1.Name,
                EntityIdentity1 = User0.Identity,
                EntityIdentity2 = User1.Identity
            });
            player.Send(new MsgQualifyingInteractive {Type = ArenaType.MATCH, Option = 5});

            player.UpdateAttributes();
            player.Life = player.MaxLife;
            player.Mana = player.MaxMana;
        }

        /// <param name="bImmediate">If the game has not begun, we end it immediatly.</param>
        public void EndMatch(bool bImmediate = false)
        {
            PlayerQualifierUser winner = null;
            PlayerQualifierUser loser = null;
            if (m_idWinner == 0)
            {
                if (Points1 > Points2)
                {
                    winner = User0;
                    m_idWinner = User0.Identity;
                    loser = User1;
                }
                else
                {
                    winner = User1;
                    m_idWinner = User1.Identity;
                    loser = User0;
                }
            }
            else
            {
                if (m_idWinner == User0.Identity)
                {
                    winner = User0;
                    m_idWinner = User0.Identity;
                    loser = User1;
                }
                else
                {
                    winner = User1;
                    m_idWinner = User1.Identity;
                    loser = User0;
                }
            }

            winner.Wins += 1;
            winner.TotalWins += 1;
            winner.Points = (uint) (winner.Points * 1.030d);
            if (winner.TotalWins == 9)
            {
                winner.ChangeHonorPoints(5000);
            }

            if (winner.GamesToday == 20)
            {
                winner.ChangeHonorPoints(5000);
            }

            winner.Send(new MsgQualifyingInteractive
            {
                Type = ArenaType.DIALOG,
                Option = 1
            });

            loser.Loss += 1;
            loser.TotalLoss += 1;
            loser.Points = (uint) (loser.Points * 0.970d);
            if (loser.GamesToday == 20)
            {
                loser.ChangeHonorPoints(5000);
            }

            loser.Send(new MsgQualifyingInteractive
            {
                Type = ArenaType.DIALOG,
                Option = 3
            });

            winner.Save();
            loser.Save();

            if (winner.SyndicateIdentity == 0 && loser.SyndicateIdentity == 0)
            {
                ServerKernel.UserManager.SendToAllUser(
                    string.Format(Language.StrArenicMatchEnd0, winner.Name, loser.Name, winner.Ranking), Color.Red,
                    ChatTone.Qualifier);
            }
            else if (winner.SyndicateIdentity != 0 && loser.SyndicateIdentity == 0)
            {
                ServerKernel.UserManager.SendToAllUser(
                    string.Format(Language.StrArenicMatchEnd1, winner.SyndicateName, winner.Name, loser.Name,
                        winner.Ranking), Color.Red, ChatTone.Qualifier);
            }
            else if (winner.SyndicateIdentity == 0 && loser.SyndicateIdentity != 0)
            {
                ServerKernel.UserManager.SendToAllUser(
                    string.Format(Language.StrArenicMatchEnd2, winner.Name, loser.SyndicateName, loser.Name,
                        winner.Ranking), Color.Red, ChatTone.Qualifier);
            }
            else
            {
                ServerKernel.UserManager.SendToAllUser(
                    string.Format(Language.StrArenicMatchEnd3, winner.SyndicateName, winner.Name, loser.SyndicateName,
                        loser.Name, winner.Ranking), Color.Red, ChatTone.Qualifier);
            }

            if (bImmediate)
            {
                Status = ArenaStatus.Disposed;
                ShowFinalDialog();
            }
            else
            {
                m_tStartup.Startup(3);
                Status = ArenaStatus.Finished;
            }
        }

        /// <summary>
        /// Sends an exit request.
        /// </summary>
        /// <param name="idRequest">The player who's asking for the exit.</param>
        public void QuitMatch(uint idRequest, bool force = false)
        {
            if (idRequest == User0.Identity)
                m_idWinner = User1.Identity;
            if (idRequest == User1.Identity)
                m_idWinner = User0.Identity;
            EndMatch(force);
        }

        public void ShowFinalDialog()
        {
            User0.Send(new MsgQualifyingInteractive
            {
                Type = ArenaType.END_DIALOG,
                Option = (uint) (m_idWinner == User0.Identity ? 1 : 0)
            });
            User1.Send(new MsgQualifyingInteractive
            {
                Type = ArenaType.END_DIALOG,
                Option = (uint) (m_idWinner == User1.Identity ? 1 : 0)
            });
        }

        public void Dispose()
        {
            MsgArenicScore pMsg = new MsgArenicScore();
            MsgArenicWitness pWatchers = new MsgArenicWitness
            {
                Action = MsgArenicWitness.Leave
            };

            try
            {
                if (User0.User?.IsAlive == false)
                    User0.User.Reborn(false, true);
                User0.User?.ChangePkMode(PkModeType.Capture);
                User0.Send(pMsg);
                User0.Send(pWatchers);
                User0.User?.SendArenaScreen();
                User0.User?.FlyMap(User0.User.RecordMapIdentity, User0.User.RecordMapX, User0.User.RecordMapY);
            }
            catch (Exception ex)
            {
                Program.WriteLog(ex.ToString(), LogType.EXCEPTION);
            }

            try
            {
                if (User1.User?.IsAlive == false)
                    User1.User.Reborn(false, true);
                User1.User?.ChangePkMode(PkModeType.Capture);
                User1.Send(pMsg);
                User1.Send(pWatchers);
                User1.User?.SendArenaScreen();
                User1.User?.FlyMap(User1.User.RecordMapIdentity, User1.User.RecordMapX, User1.User.RecordMapY);
            }
            catch (Exception ex)
            {
                Program.WriteLog(ex.ToString(), LogType.EXCEPTION);
            }

            try
            {
                ShowFinalDialog();
            }
            catch (Exception ex)
            {
                Program.WriteLog(ex.ToString(), LogType.EXCEPTION);
            }

            foreach (var player in m_pMap.PlayerSet.Values)
            {
                try
                {
                    player.Send(pMsg);
                    player.Send(pWatchers);
                    player.FlyMap(player.RecordMapIdentity, player.RecordMapX, player.RecordMapY);
                }
                catch (Exception ex)
                {
                    Program.WriteLog(ex.ToString(), LogType.EXCEPTION);
                }
            }

            ServerKernel.Maps.TryRemove(MapIdentity, out _);
            Status = ArenaStatus.Disposed;
        }

        public void OnTimer()
        {
            switch (Status)
            {
                case ArenaStatus.WaitingApproval:
                {
                    if (m_tInvitation.IsTimeOut(60))
                    {
                        if (m_bAccept0 && !m_bAccept1)
                            m_idWinner = User0.Identity;
                        else if (m_bAccept1 && !m_bAccept0)
                            m_idWinner = User1.Identity;
                        else
                        {
                            if (Calculations.ChanceCalc(50f))
                                m_idWinner = User0.Identity;
                            else
                                m_idWinner = User1.Identity;
                        }

                        EndMatch(true);
                    }

                    break;
                }

                case ArenaStatus.NotStarted:
                {
                    if (m_tStartup.IsActive() && m_tStartup.IsTimeOut(10))
                    {
                        m_tMatchTime.Startup(MATCH_TIME);
                        Status = ArenaStatus.Running;
                    }

                    break;
                }

                case ArenaStatus.Running:
                {
                    if (m_tMatchTime.IsTimeOut(MATCH_TIME))
                    {
                        try
                        {
                            EndMatch();
                        }
                        finally
                        {
                            Status = ArenaStatus.Finished;
                        }
                    }

                    break;
                }

                case ArenaStatus.Finished:
                {
                    if (m_tStartup.IsActive() && m_tStartup.IsTimeOut())
                        Dispose();
                    break;
                }
            }
        }

        public void AddPoints(uint idTarget, uint nLoseLife)
        {
            if (idTarget == User0.Identity)
                Points2 += nLoseLife;
            else if (idTarget == User1.Identity)
                Points1 += nLoseLife;

            SendToMap();
        }

        public void SendToMap()
        {
            MsgArenicScore pScore = new MsgArenicScore
            {
                Damage1 = Points1,
                Damage2 = Points2,
                Name1 = User0.Name,
                Name2 = User1.Name,
                EntityIdentity1 = User0.Identity,
                EntityIdentity2 = User1.Identity
            };

            MsgArenicWitness pWitness = new MsgArenicWitness
            {
                Action = MsgArenicWitness.Watchers,
                Cheers1 = Waving1,
                Cheers2 = Waving2
                //WatcherCount = (uint) Math.Max(0, m_pMap.PlayerSet.Count - 2)
            };
            foreach (var player in m_pMap.PlayerSet.Values)
            {
                if (player.Identity == User0.Identity || player.Identity == User1.Identity)
                    continue;
                pWitness.AppendName(player.Name, player.Lookface, player.Identity, player.Level, (uint) player.Profession, (uint) player.PlayerQualifier.Ranking);
            }
            m_pMap.SendToMap(pScore);
            m_pMap.SendToMap(pWitness);
            //foreach (var player in m_pMap.PlayerSet.Values)
            //{
            //    if (player.Identity == User0.Identity || player.Identity == User1.Identity)
            //        continue;
            //    player.Send(pWitness);
            //}
        }

        public Tile GetRandomPoint()
        {
            return m_pMap.GetRandomPosition();
        }

        public void WaveForUser(uint idTarget, Character sender)
        {
            if (m_listVotes.Contains(sender.Identity))
                return;

            m_listVotes.Add(sender.Identity);

            if (idTarget == User0.Identity)
                Waving1 += 1;
            else if (idTarget == User1.Identity)
                Waving2 += 1;
        }
    }

    public enum ArenaStatus
    {
        None,
        WaitingApproval,
        NotStarted,
        Running,
        Finished,
        Disposed
    }
}
﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - TeamQualifierMatch.cs
// Last Edit: 2020/01/17 23:28
// Created: 2020/01/16 16:02
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System;
using System.Collections.Generic;
using System.Linq;
using FtwCore.Common;
using FtwCore.Networking.Packets;
using GameServer.Structures.Entities;
using GameServer.Structures.Qualifiers.User;
using GameServer.World;

#endregion

namespace GameServer.Structures.Qualifiers.Teams
{
    public sealed class TeamQualifierMatch
    {
        public const int PROTECTION_TIME = 10;
        public const int INVITATION_TIME = 60;
        public const int MATCH_TIME = 60 * 10;
        public ArenaStatus Status;

        private readonly List<uint> m_listVotes = new List<uint>();

        private readonly TimeOut m_tStartup = new TimeOut(PROTECTION_TIME);
        private readonly TimeOut m_tInvitation = new TimeOut(INVITATION_TIME);
        private TimeOut m_tMatchTime = new TimeOut(MATCH_TIME);

        private readonly List<uint> m_pidAccept0 = new List<uint>();
        private readonly List<uint> m_pidAccept1 = new List<uint>();

        private uint m_idWinner;

        private readonly Map m_pMap;

        public uint Points1;
        public uint Points2;

        public uint Waving1;
        public uint Waving2;

        public TeamQualifierMatch(Map map)
        {
            m_pMap = map;
            ServerKernel.Maps.TryAdd(map.Identity, map);
            Status = ArenaStatus.None;
        }

        public Team Team0 { get; private set; }
        public Team Team1 { get; private set; }

        public uint Watchers =>
            (uint) Math.Max(0, m_pMap.PlayerSet.Count - Team0.MembersCount() - Team1.MembersCount());

        public uint MapIdentity => m_pMap?.Identity ?? 0u;

        /// <returns>If the match has been accepted by both users and is waiting approval.</returns>
        public bool IsReadyToStart()
        {
            if (Team0.Members.Values.Any(member => !m_pidAccept0.Contains(member.Identity)))
            {
                return false;
            }

            if (Team1.Members.Values.Any(member => !m_pidAccept1.Contains(member.Identity)))
            {
                return false;
            }

            return Status == ArenaStatus.WaitingApproval;
        }

        /// <returns>If the match has already teleported both users and is ready to start.</returns>
        public bool IsRunning()
        {
            return Status >= ArenaStatus.NotStarted && Status < ArenaStatus.Finished;
        }

        /// <returns>If the users inside are already fighting.</returns>
        public bool HasStarted()
        {
            return Status > ArenaStatus.NotStarted && Status < ArenaStatus.Finished;
        }

        /// <returns>If the match has ended, both users teleported and this instance can be deleted.</returns>
        public bool ReadyToDispose()
        {
            return Status == ArenaStatus.Disposed;
        }

        public bool IsAttackEnable()
        {
            return m_tStartup.IsTimeOut() && Status == ArenaStatus.Running;
        }

        public bool Create(Team team0, Team team1)
        {
            if (m_pMap == null)
                return false;

            if (Team0.MembersCount() <= 0 || Team1.MembersCount() <= 0)
                return false;

            if (Team0.CanJoinTeamQualifier(false) || Team1.CanJoinTeamQualifier(false))
                return false;

            Team0 = team0;
            Team1 = team1;

            MsgQualifyingInteractive msg = new MsgQualifyingInteractive
            {
                Type = ArenaType.START_COUNT_DOWN,
                Identity = (uint) ArenaWaitStatus.WAITING_INACTIVE
            };
            Team0.Send(msg);
            Team1.Send(msg);

            Status = ArenaStatus.WaitingApproval;
            m_tInvitation.Startup(INVITATION_TIME);
            return true;
        }

        public void OnTimer()
        {
        }

        public void ForceWinner(uint idWinner, bool bImmediate)
        {
            m_idWinner = idWinner;
            EndMatch(bImmediate);
        }

        public void EndMatch(bool bImmediate)
        {
            Team winner = null;
            Team loser = null;
            if (m_idWinner == 0)
            {
                if (Points1 > Points2)
                {
                    winner = Team0;
                    m_idWinner = Team0.Leader.Identity;
                    loser = Team1;
                }
                else
                {
                    winner = Team1;
                    m_idWinner = Team1.Leader.Identity;
                    loser = Team0;
                }
            }
            else
            {
                if (m_idWinner == Team0.Leader.Identity)
                {
                    winner = Team0;
                    m_idWinner = Team0.Leader.Identity;
                    loser = Team1;
                }
                else
                {
                    winner = Team1;
                    m_idWinner = Team1.Leader.Identity;
                    loser = Team0;
                }
            }

            foreach (var player in winner.Members.Values)
            {
                Win(player);
            }

            foreach (var player in loser.Members.Values)
            {
                Lose(player, false);
            }


        }

        public void Win(Character user)
        {
            user.TeamPlayerQualifier.Wins += 1;
            user.TeamPlayerQualifier.TotalWins += 1;
            user.TeamPlayerQualifier.Points = (uint) (user.TeamPlayerQualifier.Points * 1.030d);

            if (user.TeamPlayerQualifier.Wins == 9)
            {
                user.TeamPlayerQualifier.ChangeHonorPoints(5000);
            }

            if (user.TeamPlayerQualifier.GamesToday == 20)
            {
                user.TeamPlayerQualifier.ChangeHonorPoints(5000);
            }

            user.TeamPlayerQualifier.Save();

            user.Send(new MsgTeamArenaInteractive
            {
                Type = ArenaType.DIALOG,
                Mode = MsgTeamArenaInteractive.DialogButton.Win
            });
        }

        public void Lose(Character user, bool bDisconnect)
        {
            user.TeamPlayerQualifier.Loss += 1;
            user.TeamPlayerQualifier.TotalLoss += 1;
            user.TeamPlayerQualifier.Points = (uint) (user.TeamPlayerQualifier.Points * 0.97d);

            if (user.TeamPlayerQualifier.GamesToday == 20)
            {
                user.TeamPlayerQualifier.ChangeHonorPoints(5000);
            }

            user.TeamPlayerQualifier.Save();

            if (!bDisconnect)
            {
                user.Send(new MsgTeamArenaInteractive
                {
                    Type = ArenaType.DIALOG,
                    Mode = MsgTeamArenaInteractive.DialogButton.Lose
                });
            }
        }

        public Tile GetRandomPoint()
        {
            return m_pMap.GetRandomPosition();
        }

        public void WaveForUser(uint idTarget, Character sender)
        {
            if (m_listVotes.Contains(sender.Identity))
                return;

            m_listVotes.Add(sender.Identity);

            if (idTarget == Team0.Leader.Identity)
                Waving1 += 1;
            else if (idTarget == Team1.Leader.Identity)
                Waving2 += 1;
        }
    }
}
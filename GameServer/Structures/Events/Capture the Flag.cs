﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - Capture the DeliverFlag.cs
// Last Edit: 2020/01/16 01:38
// Created: 2020/01/15 15:19
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using FtwCore;
using FtwCore.Common;
using FtwCore.Common.Enums;
using FtwCore.Database.Entities;
using FtwCore.Database.Repositories;
using FtwCore.Networking.Packets;
using GameServer.Structures.Entities;
using GameServer.Structures.Groups.Syndicates;
using GameServer.World;

#endregion

namespace GameServer.Structures.Events
{
    public sealed class CaptureTheFlag
    {
        private const uint _MAP_ID_U = 2057;
        private const uint _FLAG_MESH = 513;
        private const int _FLAG_MAX_AMOUNT = 30;
        private const int _STARTUP_TIME = 6200000;
        private const int _END_TIME = 6210000;
        private const string _ERROR_FILE = "ctferror";
        private const uint _RANK_TYPE_U = 6;

        public const int
            DURATION = ((_END_TIME % 1000000 / 10000) - (_STARTUP_TIME % 1000000 / 10000)) * (60 * 60); // in seconds

        public static readonly long[] CtfMoneyReward =
        {
            1200000000,
            1000000000,
            800000000,
            650000000,
            500000000,
            400000000,
            300000000,
            200000000
        };

        public static readonly uint[] CtfEmoneyReward =
        {
            30000,
            20000,
            10000,
            6000,
            5000,
            4000,
            3000,
            2000
        };

        private readonly TimeOutMS m_tFlagGen = new TimeOutMS(2000);

        // value 1 -> user points in this session
        // value 2 -> user points total
        // value 3 -> user flags delivered this session
        // value 4 -> user flags deliverel total
        // value 5 -> user kills in this session
        // value 6 -> user kills total
        // value 7 -> user deaths in this session
        // value 8 -> user deaths in total
        private readonly ConcurrentDictionary<uint, GeneralDynamicRankingEntity> m_pUserRank =
            new ConcurrentDictionary<uint, GeneralDynamicRankingEntity>();

        // value 1 -> syndicate players inside or user flag num (delivered)
        // value 2 -> syndicate total players or user lifetime flag num (delivered)
        // value 3 -> syndicate points or user points
        // value 4 -> syndicate lifetime points or user lifetime points
        // value 5 -> syndicate silver reward or user awarded silver
        // value 6 -> syndicate emoney reward or user awarded emoney
        private readonly ConcurrentDictionary<uint, GeneralDynamicRankingEntity> m_pRanking =
            new ConcurrentDictionary<uint, GeneralDynamicRankingEntity>();

        private readonly GeneralDynamicRankingRepository m_pRepository = new GeneralDynamicRankingRepository();

        private readonly List<Point> m_pValidTiles = new List<Point>();

        private Map m_pMap;

        private CaptureTheFlagState m_pState = CaptureTheFlagState.Closed;
        private readonly TimeOut m_tRankSend = new TimeOut(10);

        private DateTime m_dtStartTime = DateTime.MinValue;

        public CaptureTheFlag()
        {
            m_tFlagGen.Clear();
        }

        public bool IsRunning => m_pState == CaptureTheFlagState.Running;

        public bool Initialize()
        {
            if (!ServerKernel.Maps.TryGetValue(_MAP_ID_U, out m_pMap))
            {
                Program.WriteLog($"Could not load map {_MAP_ID_U} for CTF", LogType.ERROR);
                return false;
            }

            if (!ParseMap())
                return false;

            var allRank = m_pRepository.FetchByType(_RANK_TYPE_U);
            if (allRank != null)
            {
                foreach (GeneralDynamicRankingEntity rank in allRank.Where(x => x.PlayerIdentity == 0))
                {
                    m_pRanking.TryAdd(rank.ObjectIdentity, rank);
                }

                foreach (var rank in allRank.Where(x => x.PlayerIdentity > 0))
                {
                    m_pUserRank.TryAdd(rank.PlayerIdentity, rank);
                }
            }

            m_pState = CaptureTheFlagState.Idle;
            return true;
        }

        private bool ParseMap()
        {
            try
            {
                if (!m_pMap.Loaded)
                    m_pMap.Load();

                for (int x = 0; x < m_pMap.Width; x++)
                {
                    for (int y = 0; y < m_pMap.Height; y++)
                    {
                        if (m_pMap[x, y].Access == TileType.Available)
                            m_pValidTiles.Add(new Point(x, y));
                    }
                }

                m_pMap.SetStatus(1, true);
                return true;
            }
            catch
            {
                Program.WriteLog("CTF: could not parse map coordinates", LogType.ERROR);
                return false;
            }
        }

        public void ForceStartup()
        {
            if (m_pState != CaptureTheFlagState.Idle)
                return; // we wont start unless the event is free

            m_dtStartTime = DateTime.Now;
            m_pState = CaptureTheFlagState.Idle;
        }

        public void OnTimer()
        {
            if (m_pState == CaptureTheFlagState.Closed)
                return;

            int weekday = ((int) DateTime.Now.DayOfWeek % 7 == 0 ? 7 : (int) DateTime.Now.DayOfWeek) * 1000000;
            DateTime now = DateTime.Now;
            int timeNow = int.Parse(now.ToString("HHmmss"));

            int defHour = _STARTUP_TIME % 1000000 / 10000;
            int defMinute = _STARTUP_TIME % 10000 / 100;
            int defSecond = _STARTUP_TIME % 100;
            DateTime defaultStartHour = new DateTime(now.Year, now.Month, now.Day, defHour, defMinute, defSecond,
                DateTimeKind.Local);

            switch (m_pState)
            {
                case CaptureTheFlagState.Idle:
                {
                    if ((m_dtStartTime != DateTime.MinValue && now >= m_dtStartTime &&
                        now < m_dtStartTime.AddSeconds(DURATION))
                        || (weekday == _STARTUP_TIME / 1000000 && now >= defaultStartHour &&
                        now < defaultStartHour.AddSeconds(DURATION)))
                    {
                        foreach (var rank in m_pRanking.Values)
                        {
                            rank.Value1 = 0;
                            rank.Value3 = 0;
                            m_pRepository.Save(rank);
                        }

                        foreach (var rank in m_pUserRank.Values)
                        {
                            rank.Value1 = 0;
                            rank.Value3 = 0;
                            rank.Value5 = 0;
                            rank.Value7 = 0;
                            m_pRepository.Save(rank);
                        }

                        foreach (var npc in m_pMap.RoleSet.Values.Where(x => x is DynamicNpc a && a.IsCtfFlag())
                            .Cast<DynamicNpc>())
                        {
                            npc.SetAttrib(ClientUpdateType.Hitpoints, npc.MaxLife);
                            npc.SetOwnerIdentity(0);
                        }

                        m_dtStartTime = DateTime.Now;
                        m_pState = CaptureTheFlagState.Starting;
                    }

                    break;
                }
                case CaptureTheFlagState.Starting:
                {
                    GenerateFlags();
                    m_pState = CaptureTheFlagState.Running;
                    break;
                }
                case CaptureTheFlagState.Running:
                {
                    if (now < m_dtStartTime || now >= m_dtStartTime.AddSeconds(DURATION))
                    {
                        m_pState = CaptureTheFlagState.Ending;
                        break;
                    }

                    if (m_tRankSend.ToNextTime())
                    {
                        SendRanking();
                    }

                    if (m_tFlagGen.ToNextTime())
                        GenerateFlags();

                    break;
                }
                case CaptureTheFlagState.Ending:
                {
                    m_dtStartTime = DateTime.MinValue;
                    RemoveAllPlayers();
                    DeliverRewards();
                    m_pState = CaptureTheFlagState.Idle;
                    break;
                }
            }
        }

        public void AddPoints(Character pUser, int nAmount, CaptureTheFlagPointType type = CaptureTheFlagPointType.DeliverFlag)
        {
            if (pUser.Syndicate == null || !IsRunning)
                return;

            int nCount = m_pMap.PlayerSet.Values.Count(x => x.SyndicateIdentity == pUser.SyndicateIdentity);
            if (!m_pRanking.TryGetValue(pUser.SyndicateIdentity, out var pRec))
            {
                pRec = new GeneralDynamicRankingEntity
                {
                    ObjectIdentity = pUser.SyndicateIdentity,
                    ObjectName = pUser.SyndicateName,
                    RankType = _RANK_TYPE_U,
                    PlayerIdentity = 0,
                    PlayerName = ""
                };
                m_pRepository.Save(pRec);
                m_pRanking.TryAdd(pRec.ObjectIdentity, pRec);
            }

            pRec.Value1 = nCount; // now player num
            if (nCount > pRec.Value2)
                pRec.Value2 = nCount; // total player num
            pRec.Value3 += nAmount; // points
            pRec.Value4 += nAmount; // lifetime points
            m_pRepository.Save(pRec);

            if (!m_pUserRank.TryGetValue(pUser.Identity, out var pUserRec))
            {
                pUserRec = new GeneralDynamicRankingEntity
                {
                    ObjectIdentity = pUser.SyndicateIdentity,
                    ObjectName = pUser.SyndicateName,
                    PlayerIdentity = pUser.Identity,
                    PlayerName = pUser.Name,
                    RankType = _RANK_TYPE_U
                };
                m_pRepository.Save(pUserRec);
                m_pUserRank.TryAdd(pUserRec.PlayerIdentity, pUserRec);
            }
            else if (pUserRec.ObjectIdentity != pUser.SyndicateIdentity)
            {
                pUserRec.ObjectIdentity = pUser.SyndicateIdentity;
                pUserRec.ObjectName = pUser.Syndicate.Name;
                pUserRec.Value3 = 0;
                pUserRec.Value4 = 0;
            }

            switch (type)
            {
                case CaptureTheFlagPointType.DeliverFlag:
                    pUserRec.Value1 += 1; // now flag num
                    pUserRec.Value1 += 1; // total flag num
                    break;
                case CaptureTheFlagPointType.Kill:
                    pUserRec.Value5 += 1; // now kill num
                    pUserRec.Value6 += 1; // total kill num
                    break;
                case CaptureTheFlagPointType.Death:
                    pUserRec.Value7 += 1; // now death num
                    pUserRec.Value8 += 1; // total death num
                    break;
            }

            pUserRec.Value3 += nAmount; // now ctf points
            pUserRec.Value4 += nAmount; // total ctf points
            m_pRepository.Save(pUserRec);
        }

        public void GenerateFlags()
        {
            int flags = FlagCount();
            if (flags < _FLAG_MAX_AMOUNT)
            {
                for (int i = flags; i < _FLAG_MAX_AMOUNT; i++)
                {
                    Point pos = default;
                    if (!GetRandomMapPosition(ref pos))
                        continue;
                    EventFlag pRole = new EventFlag(FlagPacket((ushort) pos.X, (ushort) pos.Y), m_pMap.Identity);
                    pRole.EnterMap();
                }
            }
        }

        public int FlagCount()
        {
            return m_pMap.RoleSet.Values.Count(flag =>
                       flag.Identity > IdentityRange.TRAPID_FIRST && flag.Identity < IdentityRange.TRAPID_LAST) +
                   m_pMap.PlayerSet.Values.Count(flag => flag.QueryStatus(FlagInt.CTF_FLAG) != null);
        }

        public bool GetRandomMapPosition(ref Point pRet)
        {
            try
            {
                pRet = m_pValidTiles[ThreadSafeRandom.RandGet(m_pValidTiles.Count) % m_pValidTiles.Count];
                if (m_pMap[pRet.X, pRet.Y].Access != TileType.Available)
                    return false;
                if (m_pMap.QueryRegion(7, (ushort) pRet.X, (ushort) pRet.Y))
                    return false;
                if (m_pMap.QueryRegion(8, (ushort) pRet.X, (ushort) pRet.Y))
                    return false;
                if (m_pMap.QueryRole((ushort) pRet.X, (ushort) pRet.Y) != null)
                    return false;
                return true;
            }
            catch
            {
                return false;
            }
        }

        public void DeliverRewards()
        {
            List<GeneralDynamicRankingEntity> topSyndicates = m_pRanking.Values
                .Where(x => x.PlayerIdentity == 0 && x.Value3 > 0).OrderByDescending(x => x.Value3).ToList();
            for (int i = 0; i < 8; i++)
            {
                GeneralDynamicRankingEntity rank = topSyndicates[i];
                Syndicate syn = ServerKernel.SyndicateManager.GetSyndicate(topSyndicates[i].ObjectIdentity);

                syn.SilverDonation += (ulong)CtfMoneyReward[i];
                syn.EmoneyDonation += CtfEmoneyReward[i];

                syn.Send(string.Format(Language.StrCaptureTheFlagSynReward, CtfMoneyReward[i], CtfEmoneyReward[i], i + 1));

                if (syn == null || (syn.MoneyPrize == 0 && syn.EmoneyPrize == 0))
                    continue;

                long moneyDelivered = 0;
                uint emoneyDelivered = 0;

                ServerKernel.Log.GmLog("CaptureTheFlag", $"Syndicate [id:{syn.Identity},name:{syn.Name}]: Points {rank.Value3}, Ranking: {i + 1}");
                ServerKernel.Log.GmLog("CaptureTheFlag", $"Syndicate award[id:{syn.Identity},name:{syn.Name}]: {CtfMoneyReward[i]} silvers and {CtfEmoneyReward[i]} CPs");

                foreach (var syndicateMembers in GetSyndicateMembers(syn.Identity).Where(x => x.Value1 > 0)
                    .OrderByDescending(x => x.Value1))
                {
                    double percent = syndicateMembers.Value1 / (double) rank.Value1;
                    long nAwardMoney = 0;
                    int nAwardEmoney = 0;

                    if (syn.MoneyPrize > 0)
                    {
                        nAwardMoney = (long) (syn.MoneyPrize * percent);
                        moneyDelivered += nAwardEmoney;
                    }

                    if (syn.EmoneyPrize > 0)
                    {
                        nAwardEmoney = (int) (syn.EmoneyPrize * percent);
                        emoneyDelivered += (uint) nAwardEmoney;
                    }

                    ServerKernel.Log.GmLog("CaptureTheFlag", $"Syndicate user reward[id:{syn.Identity},name:{syn.Name}][id:{syndicateMembers.PlayerIdentity},name:{syndicateMembers.PlayerName},points:{syndicateMembers.Value1}]: {nAwardMoney} silvers and {nAwardEmoney} CPs");
                    ServerKernel.UserManager.GetUser(syndicateMembers.PlayerIdentity)?.SendMessage(string.Format(Language.StrCaptureTheFlagAwardPrize, 
                        syndicateMembers.Value1, nAwardMoney, nAwardEmoney), Color.White, ChatTone.Talk);
                    SendPrizeEmail(syndicateMembers.PlayerIdentity, syndicateMembers.PlayerName, nAwardMoney, nAwardEmoney);
                }

                ServerKernel.Log.GmLog("CaptureTheFlag", $"Syndicate delivery[id:{syn.Identity},name:{syn.Name}]: {moneyDelivered} silvers and {emoneyDelivered} CPs");
            }
        }

        public void SendPrizeEmail(uint idUser, string szName, long nMoney, int nEmoney)
        {
            UserMailBox.SendEmail(idUser, szName, Language.StrCaptureTheFlagRewardEmailTitle, Language.StrCaptureTheFlagRewardEmail, UnixTimestamp.TIME_SECONDS_DAY*7,
                (uint) Math.Min(uint.MaxValue, nMoney), (uint) nEmoney);
        }

        public void SendRanking()
        {
            MsgWarFlag msgInit = new MsgWarFlag
            {
                Type = WarFlagType.Initialize
            };
            MsgWarFlag msgCtfRank = new MsgWarFlag
            {
                Type = WarFlagType.WarFlagTop4,
                Identity = 2
            };

            uint idRank = 0;
            foreach (var syn in m_pRanking.Values.Where(x => x.Value3 > 0).OrderByDescending(x => x.Value3).ThenByDescending(x => x.Value4))
            {
                msgCtfRank.Append(idRank++, (uint) syn.Value3, syn.ObjectName);

                if (idRank >= 3)
                    break;
            }

            foreach (var player in m_pMap.PlayerSet.Values)
            {
                player.Send(msgInit);
                player.Send(msgCtfRank);
                SendBaseRank(player);
            }
        }

        public void SendInterfaceRanking(Character user, MsgSelfSynMemAwardRank msg)
        {
            if (m_pState != CaptureTheFlagState.Closed && m_pRanking.Count > 0)
            {
                if (m_pState == CaptureTheFlagState.Idle)
                {
                    int nCount = 0;
                    foreach (var syn in m_pRanking.Values
                        .Where(x => x.Value3 > 0)
                        .OrderByDescending(x => x.Value3)
                        .ThenByDescending(x => x.Value1))
                    {
                        if (nCount++ >= 8)
                            break;
                        msg.AddToRanking(syn.ObjectName, (uint) syn.Value3, (uint) syn.Value2,
                            CtfMoneyReward[nCount - 1], CtfEmoneyReward[nCount - 1]);
                    }

                    for (; nCount < 8; nCount++)
                    {
                        msg.AddToRanking("None", 0, 0, CtfMoneyReward[nCount], CtfEmoneyReward[nCount]);
                    }
                }
                else // if (m_pState == CaptureTheFlagState.Running)
                {
                    msg.Unknown8 = 1;
                    int nCount = 0;
                    foreach (var syn in m_pRanking.Values
                        .Where(x => x.Value3 > 0)
                        .OrderByDescending(x => x.Value3)
                        .ThenByDescending(x => x.Value1))
                    {
                        if (nCount++ >= 8)
                            break;
                        msg.AddToRanking(syn.ObjectName, (uint) syn.Value3, (uint) syn.Value1, syn.Value5,
                            (uint) syn.Value6);
                    }

                    if (m_pUserRank.TryGetValue(user.Identity, out var pUserObj))
                    {
                        msg.Exploits = (uint) pUserObj.Value3;
                    }

                    if (m_pRanking.TryGetValue(user.SyndicateIdentity, out var pSynObj))
                    {
                        msg.SetMoney = (uint) pSynObj.Value5;
                        msg.SetEmoney = (uint) pSynObj.Value6;
                    }
                }
            }
            else
            {
                for (int i = 0; i < 8; i++)
                    msg.AddToRanking(Language.StrNone, 0, 0, CtfMoneyReward[i], CtfEmoneyReward[i]);
            }

            user.Send(msg);
        }

        public bool IsInBase(Character pUser, int idBase = 0)
        {
            foreach (var obj in pUser.Screen.GetAroundRoles)
            {
                if (obj is DynamicNpc pNpc)
                {
                    if (!pNpc.IsCtfFlag())
                        continue;
                    if (idBase != 0 && pNpc.Identity % 10 != idBase)
                        continue;
                    if (pNpc.GetDistance(pUser.MapX, pUser.MapY) > 10)
                        continue;
                    if (pNpc.OwnerType == 2 && pNpc.OwnerIdentity == pUser.SyndicateIdentity)
                        return true;
                }
            }

            return false;
        }

        public void SendBaseRank(Character pUser, int idBase = 0)
        {
            foreach (var obj in pUser.Screen.GetAroundRoles)
            {
                if (obj is DynamicNpc pNpc)
                {
                    if (!pNpc.IsCtfFlag())
                        continue;
                    if (idBase != 0 && pNpc.Identity % 10 != idBase)
                        continue;
                    if (pNpc.GetDistance(pUser.MapX, pUser.MapY) > 10)
                        continue;

                    pNpc.SendCtfRanking(pUser);
                }
            }
        }

        public void DeliverFlag(Character user)
        {
            if (IsRunning && ServerKernel.CaptureTheFlag.IsInBase(user) && user.QueryStatus(FlagInt.CTF_FLAG) != null)
            {
                ServerKernel.CaptureTheFlag.AddPoints(user, 15);
                user.DetachStatus(FlagInt.CTF_FLAG);
                user.Screen.Send(new MsgWarFlag {Type = WarFlagType.GrabFlagEffect, Identity = user.Identity}, true);
                user.Screen.Send(new MsgWarFlag {Type = WarFlagType.DeliverFlagEffect, Identity = user.Identity}, true);
            }
        }

        public List<GeneralDynamicRankingEntity> GetSyndicateMembers(uint idSyn)
        {
            return m_pUserRank.Values.Where(x => x.ObjectIdentity == idSyn).ToList();
        }

        public bool IsInside(uint idRole)
        {
            return m_pMap.PlayerSet.ContainsKey(idRole);
        }

        public void RemoveAllPlayers()
        {
            foreach (var player in m_pMap.PlayerSet.Values)
            {
                player.SendMessage(Language.StrTeleportTwinCity, Color.White, ChatTone.Talk);
                player.FlyMap(1002, 430, 378);
            }
        }

        public void RemoveMember(uint idUser)
        {
            if (!m_pUserRank.TryRemove(idUser, out var user))
                return;
            m_pRepository.Delete(user);
        }

        public MsgPlayer FlagPacket(ushort x, ushort y)
        {
            return new MsgPlayer((uint) ServerKernel.RoleManager.TrapIdentity.GetNextIdentity)
            {
                Mesh = _FLAG_MESH,
                MapX = x,
                MapY = y,
                Level = 1,
                MonsterLevel = 1,
                StringCount = 1,
                Name = "Flag"
            };
        }
    }

    public enum CaptureTheFlagState
    {
        /// <summary>
        ///     The event could not be loaded.
        /// </summary>
        Closed,

        /// <summary>
        ///     The event is just waiting to start.
        /// </summary>
        Idle,

        /// <summary>
        ///     The event is starting, reseting ranks, setting up flags and doind cleanups from latest event.
        /// </summary>
        Starting,

        /// <summary>
        ///     The event is running.
        /// </summary>
        Running,

        /// <summary>
        ///     The event is ending, giving rewards, removing flags and setting things on the database.
        /// </summary>
        Ending
    }

    public enum CaptureTheFlagPointType
    {
        DeliverFlag,
        GrabFlag,
        Kill,
        Death,
        Revive
    }
}
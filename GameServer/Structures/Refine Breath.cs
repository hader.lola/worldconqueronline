﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - Refine Breath.cs
// Last Edit: 2019/11/24 19:04
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System;
using System.Collections.Generic;
using System.Linq;
using FtwCore;
using FtwCore.Common;
using FtwCore.Common.Enums;
using FtwCore.Database.Entities;
using FtwCore.Networking.Packets;
using GameServer.Structures.Entities;
using GameServer.Structures.Managers;

#endregion

namespace GameServer.Structures
{
    /// <summary>
    ///     This class contains the Chi System for the user. It does have all methods to create and update Chi and
    ///     the Chi System is pre-loaded when the server starts. No one should even try to create or append new Chi
    ///     out of the game system with the server online, or it might crash the database.
    /// </summary>
    public sealed class RefineBreath
    {
        private readonly Dictionary<ChiPowerType, TrainingVitalityStruct> m_myChi =
            new Dictionary<ChiPowerType, TrainingVitalityStruct>(4);

        private readonly Character m_pOwner;

        public RefineBreath(Character owner)
        {
            m_pOwner = owner;
        }

        public bool Init()
        {
            if (m_pOwner == null)
                return false;

            TrainingVitalityStruct obj;
            if (!TrainingVitalityManager.GetDragon(m_pOwner.Identity, out obj))
                return true;
            m_myChi.Add(obj.Mode, obj);
            if (!TrainingVitalityManager.GetPhoenix(m_pOwner.Identity, out obj))
                return true;
            m_myChi.Add(obj.Mode, obj);
            if (!TrainingVitalityManager.GetTiger(m_pOwner.Identity, out obj))
                return true;
            m_myChi.Add(obj.Mode, obj);
            if (!TrainingVitalityManager.GetTurtle(m_pOwner.Identity, out obj))
                return true;
            m_myChi.Add(obj.Mode, obj);
            return true;
        }

        public bool OpenChi(ChiPowerType type)
        {
            if (!IsLocked(type))
                return false;
            TrainingVitalityStruct newChi = TrainingVitalityManager.Create(m_pOwner, type);
            m_myChi.Add(newChi.Mode, newChi);
            GenerateValue(type);
            SendInfo(true);
            return true;
        }

        public void GenerateValue(ChiPowerType type, int nFlag = TrainingVitalityManager.CHISAVE_NONE)
        {
            List<ChiAttributeType> types = new List<ChiAttributeType>();
            for (ChiAttributeType i = ChiAttributeType.Criticalstrike; i <= ChiAttributeType.Magicdamagereduction; i++)
            {
                if (CanGenerateType(type, i))
                    types.Add(i);
            }

            if (!m_myChi.ContainsKey(type))
                return;

            TrainingVitalityStruct chi = m_myChi[type];
            for (int i = 0; i < 4; i++)
            {
                if ((nFlag & (1 << i)) != 0)
                    continue;

                int rand = ThreadSafeRandom.RandGet() % types.Count;
                ChiAttributeType mode = types[rand];

                int nMin = TrainingVitalityManager.CHIVALUE_MIN[(int) mode];
                int nMax = TrainingVitalityManager.CHIVALUE_MAX[(int) mode];
                int nPower = Math.Min(Math.Max(nMin, ThreadSafeRandom.RandGet(nMin, nMax)), nMax);
                nPower += (int) mode * 10000;

                if (GetPoints(nPower) >= 90)
                {
                    if (Calculations.ChanceCalc(60))
                    {
                        i--;
                        continue;
                    }

                    float baseChance = Math.Max(1.5f, 5f - (GetPoints(nPower) % 10)/2f);
                    if (Calculations.ChanceCalc(baseChance))
                    {
                        nPower = (int) mode * 10000;
                        nPower += nMax;
                    }

                    ServerKernel.UserManager.SendToAllUser(new MsgTrainingVitalityScore
                    {
                        Name = m_pOwner.Name,
                        Type = type,
                        Power = (uint) nPower
                    });
                }

                types.Remove(mode);
                switch (i)
                {
                    case 0:
                        chi.Entity.Power1 = (uint) nPower;
                        break;
                    case 1:
                        chi.Entity.Power2 = (uint) nPower;
                        break;
                    case 2:
                        chi.Entity.Power3 = (uint) nPower;
                        break;
                    case 3:
                        chi.Entity.Power4 = (uint) nPower;
                        break;
                }
            }

            chi.Entity.StageLock = (byte) nFlag;

            TrainingVitalityManager.Repository.Save(chi.Entity);
            SendInfo(true);
            m_pOwner.UpdateAttributes();
        }

        public bool CanGenerateType(ChiPowerType type, ChiAttributeType attr)
        {
            if (type == ChiPowerType.None || type > ChiPowerType.Turtle)
                return false;

            if (IsLocked(type))
                return false;

            return GetAttrType(m_myChi[type].Entity.Power1) != attr
                   && GetAttrType(m_myChi[type].Entity.Power2) != attr
                   && GetAttrType(m_myChi[type].Entity.Power3) != attr
                   && GetAttrType(m_myChi[type].Entity.Power4) != attr;
        }

        public bool IsLocked(ChiPowerType type)
        {
            if (type == ChiPowerType.None || type > ChiPowerType.Turtle)
                return true;
            return !m_myChi.ContainsKey(type);
        }

        public void SendInfo(bool update = false)
        {
            MsgTrainingVitalityInfo msg = new MsgTrainingVitalityInfo
            {
                Type = (ushort) (update ? 1 : 0),
                ChiPoints = m_pOwner.ChiPoints,
                UserIdentity = m_pOwner.Identity,
                Unknown = 0
            };
            foreach (var chi in m_myChi.Values)
            {
                msg.Unknown |=  (uint)((int)chi.Entity.StageLock << 4*(((int)chi.Mode)-1));
                msg.Append((byte) chi.Mode, chi.Entity.Power1, chi.Entity.Power2, chi.Entity.Power3, chi.Entity.Power4);
            }
            m_pOwner.Send(msg);
        }

        public void SendInfo(Character user, bool update = false)
        {
            MsgTrainingVitalityInfo msg = new MsgTrainingVitalityInfo
            {
                Type = (ushort) (update ? 1 : 0),
                ChiPoints = 0,
                UserIdentity = m_pOwner.Identity
            };
            foreach (var chi in m_myChi.Values)
                msg.Append((byte) chi.Mode, chi.Entity.Power1, chi.Entity.Power2, chi.Entity.Power3, chi.Entity.Power4);
            user.Send(msg);
        }

        public void BroadcastHighMsg(ChiPowerType type, int value, bool isFull = false)
        {
            string typeName = "Error";
            switch ((ChiAttributeType) (value / 10000))
            {
                case ChiAttributeType.Criticalstrike:
                    typeName = string.Format(Language.StrTrainingVitalityAttribDesc001, value % 10000 / 10f);
                    break;
                case ChiAttributeType.Skillcriticalstrike:
                    typeName = string.Format(Language.StrTrainingVitalityAttribDesc002, value % 10000 / 10f);
                    break;
                case ChiAttributeType.Immunity:
                    typeName = string.Format(Language.StrTrainingVitalityAttribDesc003, value % 10000 / 10f);
                    break;
                case ChiAttributeType.Breakthrough:
                    typeName = string.Format(Language.StrTrainingVitalityAttribDesc004, value % 10000 / 10f);
                    break;
                case ChiAttributeType.Counteraction:
                    typeName = string.Format(Language.StrTrainingVitalityAttribDesc005, value % 10000 / 10f);
                    break;
                case ChiAttributeType.Health:
                    typeName = string.Format(Language.StrTrainingVitalityAttribDesc006, value % 10000);
                    break;
                case ChiAttributeType.Attack:
                    typeName = string.Format(Language.StrTrainingVitalityAttribDesc007, value % 10000);
                    break;
                case ChiAttributeType.Magicattack:
                    typeName = string.Format(Language.StrTrainingVitalityAttribDesc008, value % 10000);
                    break;
                case ChiAttributeType.Mdefense:
                    typeName = string.Format(Language.StrTrainingVitalityAttribDesc009, value % 10000);
                    break;
                case ChiAttributeType.Finalattack:
                    typeName = string.Format(Language.StrTrainingVitalityAttribDesc010, value % 10000);
                    break;
                case ChiAttributeType.Finalmagicattack:
                    typeName = string.Format(Language.StrTrainingVitalityAttribDesc011, value % 10000);
                    break;
                case ChiAttributeType.Damagereduction:
                    typeName = string.Format(Language.StrTrainingVitalityAttribDesc012, value % 10000);
                    break;
                case ChiAttributeType.Magicdamagereduction:
                    typeName = string.Format(Language.StrTrainingVitalityAttribDesc013, value % 10000);
                    break;
            }

            string fullMsg = string.Empty;
            switch (type)
            {
                case ChiPowerType.Dragon:
                    fullMsg = string.Format(
                        !isFull ? Language.StrTrainingVitalityDragonHigh : Language.StrTrainingVitalityDragonFull,
                        m_pOwner.Name, typeName);
                    break;
                case ChiPowerType.Phoenix:
                    fullMsg = string.Format(
                        !isFull ? Language.StrTrainingVitalityPhoenixHigh : Language.StrTrainingVitalityPhoenixFull,
                        m_pOwner.Name, typeName);
                    break;
                case ChiPowerType.Tiger:
                    fullMsg = string.Format(
                        !isFull ? Language.StrTrainingVitalityTigerHigh : Language.StrTrainingVitalityTigerFull,
                        m_pOwner.Name, typeName);
                    break;
                case ChiPowerType.Turtle:
                    fullMsg = string.Format(
                        !isFull ? Language.StrTrainingVitalityTurtleHigh : Language.StrTrainingVitalityTurtleFull,
                        m_pOwner.Name, typeName);
                    break;
            }

            ServerKernel.UserManager.SendToAllUser(fullMsg);
        }

        public Dictionary<ChiAttributeType, int> GetPowers()
        {
            Dictionary<ChiAttributeType, int> powers = new Dictionary<ChiAttributeType, int>();
            foreach (var chiFull in m_myChi)
            {
                var chi = chiFull.Value;
                for (int i = 0; i < 4; i++)
                {
                    ChiAttributeType type = ChiAttributeType.None;
                    int power = 0;
                    switch (i)
                    {
                        case 0:
                            type = (ChiAttributeType) (chi.Entity.Power1 / 10000);
                            power = (int) (chi.Entity.Power1 % 10000);
                            break;
                        case 1:
                            type = (ChiAttributeType) (chi.Entity.Power2 / 10000);
                            power = (int) (chi.Entity.Power2 % 10000);
                            break;
                        case 2:
                            type = (ChiAttributeType) (chi.Entity.Power3 / 10000);
                            power = (int) (chi.Entity.Power3 % 10000);
                            break;
                        case 3:
                            type = (ChiAttributeType) (chi.Entity.Power4 / 10000);
                            power = (int) (chi.Entity.Power4 % 10000);
                            break;
                    }

                    if (powers.ContainsKey(type))
                        powers[type] += power;
                    else
                        powers.Add(type, power);
                }

                uint pos = 0;
                switch (chiFull.Key)
                {
                    case ChiPowerType.Dragon:
                        pos = TrainingVitalityManager.GetUserDragonRanking(m_pOwner.Identity);
                        break;
                    case ChiPowerType.Phoenix:
                        pos = TrainingVitalityManager.GetUserPhoenixRanking(m_pOwner.Identity);
                        break;
                    case ChiPowerType.Tiger:
                        pos = TrainingVitalityManager.GetUserTigerRanking(m_pOwner.Identity);
                        break;
                    case ChiPowerType.Turtle:
                        pos = TrainingVitalityManager.GetUserTurtleRanking(m_pOwner.Identity);
                        break;
                }

                if (chiFull.Value.GetTotalPoints() == 400)
                    pos = 0;

                if (pos < 50)
                {
                    AppendRankAttributes(chiFull.Key, pos, ref powers);
                }
            }

            return powers;
        }

        public void SendRank()
        {
            foreach (var chiFull in m_myChi)
            {
                uint pos = 0;
                switch (chiFull.Key)
                {
                    case ChiPowerType.Dragon:
                        pos = TrainingVitalityManager.GetUserDragonRanking(m_pOwner.Identity);
                        break;
                    case ChiPowerType.Phoenix:
                        pos = TrainingVitalityManager.GetUserPhoenixRanking(m_pOwner.Identity);
                        break;
                    case ChiPowerType.Tiger:
                        pos = TrainingVitalityManager.GetUserTigerRanking(m_pOwner.Identity);
                        break;
                    case ChiPowerType.Turtle:
                        pos = TrainingVitalityManager.GetUserTurtleRanking(m_pOwner.Identity);
                        break;
                }

                MsgRank msg = new MsgRank
                {
                    Mode = 2,
                    FlowerIdentity = (uint) (60000000 + chiFull.Key)
                };
                msg.AddToRanking(pos+1, chiFull.Value.GetTotalPoints(), chiFull.Value.UserIdentity,
                    chiFull.Value.PlayerName);
                m_pOwner.Send(msg);
            }
        }

        private void AppendRankAttributes(ChiPowerType fate, uint pos, ref Dictionary<ChiAttributeType, int> powers)
        {
            FateRankEntity rank = ServerKernel.FateRank.FirstOrDefault(x => x.Fate == (int) fate && x.Ranking == pos + 1);
            if (rank != null)
            {
                ChiAttributeType type = GetAttrType(rank.Power0);
                int power = (int) (rank.Power0 % 10000);
                if (powers.ContainsKey(type))
                    powers[type] += power;
                else
                    powers.Add(type, power);

                type = GetAttrType(rank.Power1);
                power = (int) (rank.Power1 % 10000);
                if (powers.ContainsKey(type))
                    powers[type] += power;
                else
                    powers.Add(type, power);

                type = GetAttrType(rank.Power2);
                power = (int) (rank.Power2 % 10000);
                if (powers.ContainsKey(type))
                    powers[type] += power;
                else
                    powers.Add(type, power);

                type = GetAttrType(rank.Power3);
                power = (int) (rank.Power3 % 10000);
                if (powers.ContainsKey(type))
                    powers[type] += power;
                else
                    powers.Add(type, power);
            }
        }

        public ChiAttributeType GetAttrType(uint power)
        {
            return (ChiAttributeType) (power / 10000);
        }

        public int GetPoints(int power)
        {
            ChiAttributeType type = (ChiAttributeType) (power / 10000);
            if (type == ChiAttributeType.None || type > ChiAttributeType.Magicdamagereduction)
                return 0;
            return (int) ((power - TrainingVitalityManager.CHIVALUE_MIN[(int) type]) % 10000 /
                          (float) (TrainingVitalityManager.CHIVALUE_MAX[(int) type] -
                                   TrainingVitalityManager.CHIVALUE_MIN[(int) type]) * 100);
        }

        public int GetTotalPoints(ChiPowerType type)
        {
            return (int) (m_myChi.TryGetValue(type, out var obj) ? obj.GetTotalPoints() : 0);
        }
    }
}
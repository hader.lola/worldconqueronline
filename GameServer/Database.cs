﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - Database.cs
// Last Edit: 2019/12/07 20:48
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using FtwCore.Common;
using FtwCore.Database.Entities;
using FtwCore.Database.Repositories;
using GameServer.Structures;
using GameServer.Structures.Actions;
using GameServer.Structures.Entities;
using GameServer.Structures.Events;
using GameServer.Structures.Groups.Families;
using GameServer.Structures.Managers;
using GameServer.Structures.Managers.GameEvents;
using GameServer.Structures.Monarchy;
using GameServer.World;
using GameServer.World.MapStruct;

#endregion

namespace GameServer
{
    public static class Database
    {
        public const string DATABASE_LOCATION = "\\";
        public const string DMAPS_LOCATION = DATABASE_LOCATION + "Maps\\DMaps\\";
        public const string MAPS_LOCATION = DATABASE_LOCATION + "Maps\\";

        public static bool Initialize()
        {
            CharacterRepository = new CharacterRepository();
            ItemRepository = new ItemRepository();
            WeaponSkillRepository = new WeaponSkillRepository();
            MagicRepository = new MagicRepository();
            KillDeathRepository = new KillDeathRepository();
            Statistics = new StatisticRepository();
            Arena = new ArenaRepository();
            Family = new FamilyRepository();
            FamilyMember = new FamilyMemberRepository();

            #region Name Change Load

            NameChangeLogRepository nRepo = new NameChangeLogRepository();
            var allChanges = nRepo.FetchAllQueued();

            if (allChanges != null)
            {
                foreach (var change in allChanges)
                {
                    if (change.Changed == 0)
                    {
                        CharacterRepository.ChangeName(change.UserIdentity, change.NewName, change.OldName);
                        Program.WriteLog($"{change.OldName} has its name changed to {change.NewName}.", LogType.DEBUG);
                        change.Changed = 1;
                        nRepo.Save(change);
                    }
                }
            }

            #endregion

            ServerKernel.UserManager = new UserManager();

            #region Nobility

            var allRanks = new DynamicRankingRecordsRepository().FetchByType(DynamicRankingType.Nobility);
            if (allRanks != null)
            {
                foreach (var rank in allRanks)
                {
                    ServerKernel.Nobility.TryAdd(rank.UserIdentity, rank);
                }
            }

            #endregion

            ServerKernel.SyndicateManager = new SyndicateManager();
            Program.WriteLog("Loading Syndicate information");
            ServerKernel.SyndicateManager.LoadSyndicates();
            ServerKernel.SyndicateRecruitment.Create();
            ServerKernel.FlowerRanking = new FlowerManager();
            ServerKernel.RoleManager = new RoleManager();
            ServerKernel.QuizShow = new QuizShow();
            ServerKernel.QuizShow.Start();
            ServerKernel.Empire = new Empire();
            ServerKernel.Empire.Initialize();

            #region Loading Families

            ICollection<FamilyEntity> allFamily = Family.FetchAll();
            if (allFamily != null)
            {
                foreach (var FamilyEntity in allFamily)
                {
                    Family pFamily = new Family();
                    if (!pFamily.Create(FamilyEntity))
                        continue;

                    ICollection<FamilyMemberEntity> allMember = FamilyMember.FetchByFamily(pFamily.Identity);
                    if (allMember != null)
                    {
                        foreach (var member in allMember)
                        {
                            CharacterEntity pUser = CharacterRepository.SearchByIdentity(member.Identity);
                            if (pUser == null)
                            {
                                FamilyMember.Delete(member);
                                continue;
                            }

                            FamilyMember pMember = new FamilyMember(pFamily);
                            if (!pMember.Create(pUser, member))
                                continue;

                            if (!pFamily.Members.TryAdd(pMember.Identity, pMember))
                            {
                                ServerKernel.Log.SaveLog(
                                    $"ALERT: could not add member id:{pMember.Identity} to family:{pFamily.Identity}");
                            }
                        }
                    }
                    else
                    {
                        pFamily.Delete();
                        ServerKernel.Log.GmLog("family_delete",
                            $"Family[id:{pFamily.Identity},name:{pFamily.Name}] has been deleted because no members were found at server loading");
                        // should I delete allies and enemies? xDDD nah
                        continue;
                    }

                    ServerKernel.Families.TryAdd(pFamily.Identity, pFamily);
                }
            }

            foreach (var family in ServerKernel.Families.Values)
            {
                family.LoadRelations();
            }

            #endregion

            #region Load Map Data

            Program.WriteLog("Decoding map files...");
            DecodeGamemapFile();
            Program.WriteLog("Filling server with map information...");
            ICollection<MapEntity> collection = new MapRepository().FetchAll();
            ICollection<DynamicMapEntity> dynaCollection = new DynamicMapRepository().FetchAll();
            if (collection != null)
            {
                // Load the maps since the directory seems to be there:
                bool problem = false;
                foreach (var databaseMap in collection)
                {
                    // Was a problem detected?
                    if (!problem)
                    {
                        // Get the map:
                        var map = new Map(databaseMap);
                        ServerKernel.Maps.TryAdd(map.Identity, map);

                        Program.WriteLog($"Map [{databaseMap.Identity}] Loaded...");

                        // If the file does not exist, convert the dmap file into a compressed conquer map file:
                        if (!File.Exists(Environment.CurrentDirectory + MAPS_LOCATION + map.Path)
                            && !map.Load())
                            problem = true;
                    }
                }

                if (problem)
                    return false;

                // Loading dynamic collection at startup
                if (dynaCollection != null)
                {
                    // Load the maps since the directory seems to be there:
                    //Parallel.ForEach(dynaCollection, (databaseMap) =>
                    foreach (var databaseMap in dynaCollection)
                    {
                        // Was a problem detected?
                        if (!problem)
                        {
                            // Get the map:
                            var map = new Map(databaseMap);
                            ServerKernel.Maps.TryAdd(map.Identity, map);

                            Program.WriteLog($"Map [{databaseMap.Identity}] Loaded...");

                            // If the file does not exist, convert the dmap file into a compressed conquer map file:
                            if (!File.Exists(Environment.CurrentDirectory + MAPS_LOCATION + map.Path)
                                && !map.Load()) problem = true;
                        }
                    }

                    if (problem) // Display an error message if there was a problem converting maps:
                        Console.WriteLine(@"Please ensure that you have placed the client's map folder contents,"
                                          + @"specifically the map and scene folders, in the map server's 'DMaps' folder, "
                                          + @"located in the 'Database' folder. Then restart the server and try again.");
                }
            }

            var lreg = new RegionRepository().FetchAll();
            if (lreg != null)
            {
                foreach (var reg in lreg)
                {
                    if (ServerKernel.Maps.ContainsKey(reg.MapIdentity))
                        ServerKernel.Maps[reg.MapIdentity].Regions.TryAdd(reg.Identity, reg);
                }
            }

            #endregion

            ServerKernel.CaptureTheFlag = new CaptureTheFlag();
            ServerKernel.CaptureTheFlag.Initialize();
            ServerKernel.ArenaQualifier.Create();
            ServerKernel.TeamQualifier.Create();
            ServerKernel.LineSkillPk = new LineSkillPkTournament();

            #region Loading Portals Data

            Program.WriteLog("Loading portals data...");

            var passways = new PasswayRepository().GetAllPassways();
            if (passways != null)
            {
                foreach (var passway in passways)
                {
                    var portal = new PortalRepository().GetByIndex(passway.TargetMapId, passway.TargetPortal);
                    if (portal != null)
                    {
                        IPassway pw = new IPassway
                        {
                            Identity = passway.Identity,
                            PasswayIndex = passway.MapIndex,
                            PasswayMap = passway.MapId,
                            PortalMap = portal.MapId,
                            PortalX = portal.PortalX,
                            PortaLy = portal.PortalY
                        };

                        Map map;
                        if (ServerKernel.Maps.TryGetValue(pw.PasswayMap, out map))
                        {
                            if (!map.Portals.TryAdd(pw.Identity, pw))
                                Program.WriteLog(
                                    $"Could not add portal ({pw.Identity}) to map ({map.Identity}). Index already exists",
                                    LogType.WARNING);
                        }
                        else
                        {
                            Program.WriteLog($"Map ({pw.PasswayMap}) doesnt exist for portal ({pw.Identity})",
                                LogType.WARNING);
                        }
                    }
                    else
                    {
                        Program.WriteLog($"Could not find portal for [{passway.MapId}][{passway.MapIndex}]",
                            LogType.WARNING);
                    }
                }
            }

            #endregion

            #region NPCs

            Program.WriteLog("Loading NPCs and Dynamic NPCs");
            var npcs = new NpcRepository().FetchAll();

            if (npcs != null)
            {
                foreach (var npc in npcs)
                {
                    Npc newNpc = new Npc(npc.Id, npc);
                    ServerKernel.RoleManager.AddRole(newNpc);
                }
            }

            var dynaNpcs = new DynamicNpcRepository().FetchAll();
            if (dynaNpcs != null)
            {
                foreach (var dyna in dynaNpcs)
                {
                    DynamicNpc npc = new DynamicNpc(dyna);
                    if (npc.Initialize())
                        ServerKernel.RoleManager.AddRole(npc);
                }
            }

            #endregion

            #region Item data

            Program.WriteLog("Loading Itemaddition table");
            var itemAdd = new ItemAdditionRepository().FetchAll();

            if (itemAdd != null)
            {
                foreach (var itAdd in itemAdd)
                {
                    ServerKernel.ItemAddition.Add(itAdd);
                }
            }

            Program.WriteLog("Loading Itemtype table");
            var itemType = new ItemtypeRepository().FetchAll();

            if (itemType != null)
            {
                foreach (var itType in itemType)
                {
                    ServerKernel.Itemtype.TryAdd(itType.Type, itType);
                }
            }

            Program.WriteLog("Loading Refinery table");
            var refineries = new RefineryRepository().LoadAllRefineries();
            if (refineries != null)
            {
                foreach (var refinery in refineries)
                {
                    ServerKernel.Refinery.TryAdd(refinery.Id, refinery);
                }
            }

            Program.WriteLog("Loading Goods table");
            var goods = new GoodsRepository().FetchAll();

            if (goods != null)
            {
                foreach (var item in goods)
                {
                    ServerKernel.Goods.Add(item);
                }
            }

            #endregion

            #region Task and Action

            Program.WriteLog("Loading action and tasks...");

            var actions = new GameActionRepository().FetchAll();
            var tasks = new GameTaskRepository().FetchAll();

            foreach (var action in actions)
            {
                ServerKernel.Actions.TryAdd(action.Identity, action);
            }

            foreach (var task in tasks)
            {
                ServerKernel.Tasks.TryAdd(task.Id, task);
            }

            #endregion

            #region Load Skills

            Program.WriteLog("Loading magic data...");

            var mgcType = new MagictypeRepository().FetchAll();
            if (mgcType != null)
            {
                foreach (var mgc in mgcType)
                {
                    ServerKernel.Magictype.TryAdd(mgc.Id, mgc);
                }
            }

            var mstMgc = new MonsterMagicRepository().FetchAll();
            if (mstMgc != null)
            {
                foreach (var mgc in mstMgc)
                {
                    ServerKernel.MonsterMagics.Add(mgc);
                }
            }

            var mgcOp = new MagictypeOperationRepository().FetchAll();
            if (mgcOp != null)
            {
                foreach (var op in mgcOp)
                {
                    MagicTypeOp pMgc = new MagicTypeOp(op);
                    ServerKernel.Magictypeops.TryAdd(op.Id, pMgc);
                }
            }

            #endregion

            #region Level Experience

            Program.WriteLog("Loading level experience...");

            var allExp = new LevelExperienceRepository().FetchAll();
            if (allExp != null)
            {
                foreach (var level in allExp)
                {
                    if (!ServerKernel.LevelExperience.ContainsKey(level.Level))
                        ServerKernel.LevelExperience.TryAdd(level.Level, level);
                }
            }

            ServerKernel.MaxLevel = ServerKernel.LevelExperience.Count + 1;

            Program.WriteLog("Loading rebirth table...");

            var allRebirth = new RebirthRepository().FetchAll();
            if (allRebirth != null)
            {
                foreach (var rebirth in allRebirth)
                {
                    ServerKernel.Rebirths.TryAdd(rebirth.Identity, rebirth);
                }
            }

            #endregion

            #region Load Point Allot Data

            Program.WriteLog("Loading auto allot information...");

            ICollection<PointAllotEntity> pointAllot = new PointAllotRepository().FetchAll();

            if (pointAllot != null)
            {
                foreach (var pa in pointAllot)
                {
                    ServerKernel.PointAllot.Add(pa);
                }
            }

            #endregion

            #region Load Superman

            var kotable = new SupermanRepository().FetchAll();
            if (kotable != null)
            {
                foreach (var ko in kotable)
                {
                    IKoCount koc = new IKoCount(ko);
                    if (!ServerKernel.Superman.TryAdd(ko.Identity, koc))
                        Program.WriteLog($"kocount(id:{koc.Name}) loading error");
                }
            }

            #endregion

            #region Generator

            Program.WriteLog("Loading monster data...");

            var monsters = new MonstertypeRepository().FetchAll();
            if (monsters != null)
            {
                foreach (var mob in monsters)
                    ServerKernel.Monsters.TryAdd(mob.Id, mob);
            }

            IniFileName quenchRule = new IniFileName(Environment.CurrentDirectory + @"\ini\QuenchDropRule.ini");
            foreach (var szMobId in quenchRule.GetSectionNames())
            {
                uint idMob = 0;
                if (!uint.TryParse(szMobId, out idMob))
                {
                    Program.WriteLog($"ALERT: QuenchDropRule could not parse mob id: {szMobId}", LogType.WARNING);
                    continue;
                }

                MonstertypeEntity dbMob;
                if (!ServerKernel.Monsters.TryGetValue(idMob, out dbMob))
                {
                    Program.WriteLog($"ALERT: unexistent monstertype({idMob}) for droprule", LogType.WARNING);
                    continue;
                }

                string szName = "";
                byte level = 0;
                byte tolerance = 0;
                byte dropNum = 0;
                uint idDefault = 0;
                int actionNum = 0;

                try
                {
                    szName = quenchRule.GetEntryValue(szMobId, "Name").ToString();
                    level = byte.Parse(quenchRule.GetEntryValue(szMobId, "Level").ToString());
                    tolerance = byte.Parse(quenchRule.GetEntryValue(szMobId, "LevelTolerance").ToString());
                    dropNum = byte.Parse(quenchRule.GetEntryValue(szMobId, "DropNum").ToString());
                    idDefault = uint.Parse(quenchRule.GetEntryValue(szMobId, "DefaultAction").ToString());
                    actionNum = int.Parse(quenchRule.GetEntryValue(szMobId, "Action").ToString());
                }
                catch
                {
                    Program.WriteLog($"EXCEPTION: could not parse data for drop rule {idMob}", LogType.EXCEPTION);
                    continue;
                }

                SpecialDrop pDrop = new SpecialDrop
                {
                    MonsterName = szName,
                    MonsterIdentity = idMob,
                    Level = level,
                    LevelTolerance = tolerance,
                    DefaultAction = idDefault,
                    DropNum = dropNum
                };
                pDrop.Actions = new List<KeyValuePair<uint, ushort>>();
                for (int i = 0; i < actionNum; i++)
                {
                    uint idAction = 0;
                    ushort usChance = 10000;
                    string[] szInfo = quenchRule.GetEntryValue(szMobId, $"Action{i}").ToString().Split(' ');

                    if (szInfo.Length < 2
                        || !uint.TryParse(szInfo[0], out idAction)
                        || !ushort.TryParse(szInfo[1], out usChance))
                    {
                        continue;
                    }

                    pDrop.Actions.Add(new KeyValuePair<uint, ushort>(idAction, usChance));
                }

                ServerKernel.SpecialDrop.Add(pDrop);
            }

            Program.WriteLog("Loading generators...");
            var generators = new GeneratorRepository().FetchAll();
            if (generators != null)
            {
                foreach (var gen in generators)
                {
                    Generator pGen = new Generator(gen);
                    if (!pGen.Create())
                    {
                        Program.WriteLog($"Could not load generator (id:{gen.Id})", LogType.WARNING);
                        continue;
                    }

                    ServerKernel.Generators.TryAdd(pGen.Identity, pGen);
                    pGen.FirstGeneration();
                }
            }

            #endregion

            Program.WriteLog("Loading ChiSystem for all users");
            TrainingVitalityManager.Initialize();

            ServerKernel.Pigeon = new Pigeon();

            Program.WriteLog("Database set initialized successfully");

            new GameAction().ProcessAction(80000000, null, null, null, "");
            return true;
        }

        /// <summary>
        ///     This function decodes the binary gamemap.dat file into a plain text file composed of MySQL insert
        ///     commands. These commands can be run by the MySQL service to add new maps to the database for the very
        ///     first time. The file can be loaded in the map server's database folder.
        /// </summary>
        public static void DecodeGamemapFile()
        {
            // Initialize file stream:
            FileStream stream = File.OpenRead(Environment.CurrentDirectory + DATABASE_LOCATION + "GameMap.dat");
            var reader = new BinaryReader(stream);
            var values = new Dictionary<int, string>();

            // Read in the file:
            int amount = reader.ReadInt32();
            for (int index = 0; index < amount; index++)
            {
                try
                {
                    int identity = reader.ReadInt32();
                    string path = Encoding.ASCII.GetString(reader.ReadBytes(reader.ReadInt32())).Remove(0, 8);
                    reader.BaseStream.Seek(4L, SeekOrigin.Current); // puzzle
                    values.Add(identity, path.Replace(".DMap", ".cqm"));
                    ServerKernel.MapPathData.Add((uint) identity, path.Replace(".7z", ".DMap"));
                }
                catch (Exception ex)
                {
                    Program.WriteLog(ex.Message);
                }
            }

            // Dispose of file stream:
            stream.Close();
            reader.Close();
            stream.Dispose();
            reader.Dispose();

            // Write the data to a plain text file:
            string plainTextPath = Environment.CurrentDirectory + DATABASE_LOCATION + "GameMap.txt";
            if (File.Exists(plainTextPath)) File.Delete(plainTextPath);
            StreamWriter writer = File.CreateText(plainTextPath);
            foreach (int identity in values.Keys)
                writer.WriteLine("INSERT INTO `cq_map` (`id`, `mapdoc`, `file_name`) VALUES"
                                 + "('" + identity + "', '" + identity + "', '" + values[identity] + "');");
            writer.Close();
            writer.Dispose();
        }

        #region Most Used repositories

        // must initialize all
        public static ItemRepository ItemRepository;
        public static MagicRepository MagicRepository;
        public static WeaponSkillRepository WeaponSkillRepository;
        public static CharacterRepository CharacterRepository;
        public static KillDeathRepository KillDeathRepository;
        public static StatisticRepository Statistics;
        public static ArenaRepository Arena;
        public static FamilyRepository Family;
        public static FamilyMemberRepository FamilyMember;

        #endregion
    }
}
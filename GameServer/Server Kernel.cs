﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - Server Kernel.cs
// Last Edit: 2019/12/07 20:49
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Windows.Forms;
using FtwCore.Common;
using FtwCore.Database;
using FtwCore.Database.Entities;
using GameServer.Networking.GameServer;
using GameServer.Networking.LoginClient;
using GameServer.Structures;
using GameServer.Structures.Events;
using GameServer.Structures.Flower;
using GameServer.Structures.Groups.Families;
using GameServer.Structures.Groups.Syndicates;
using GameServer.Structures.Login;
using GameServer.Structures.Managers;
using GameServer.Structures.Managers.GameEvents;
using GameServer.Structures.Monarchy;
using GameServer.Structures.Qualifiers;
using GameServer.Structures.Qualifiers.Teams;
using GameServer.Structures.Qualifiers.User;
using GameServer.World;

#endregion

namespace GameServer
{
    public static class ServerKernel
    {
        public static string Version;

        /// <summary>
        ///     The default log writer for the server.
        /// </summary>
        public static LogWriter Log = new LogWriter($"{Environment.CurrentDirectory}\\");

        public static Analytics Analytics;
        public static UserManager UserManager; // = new UserManager();
        public static SyndicateManager SyndicateManager; // = new SyndicateManager();
        public static FlowerManager FlowerRanking;
        public static RoleManager RoleManager;
        public static Pigeon Pigeon;
        public static ArenaQualifierManager ArenaQualifier = new ArenaQualifierManager();
        public static TeamArenaQualifierManager TeamQualifier = new TeamArenaQualifierManager();
        public static LineSkillPkTournament LineSkillPk;
        public static QuizShow QuizShow;
        public static Empire Empire;
        public static CaptureTheFlag CaptureTheFlag;

        #region Syndicate Recruitment

        public static SyndicateRecruitment SyndicateRecruitment = new SyndicateRecruitment();

        #endregion

        static ServerKernel()
        {
#if DEBUG
            Version = Application.ProductVersion;
#else
            Version = Assembly.GetExecutingAssembly().GetName().Version.ToString();
#endif
        }

        #region Server Configuration

        /// <summary>
        ///     The database configuration for accessing account information.
        /// </summary>
        public static MySqlConfig AccountDbConfig = default;

        /// <summary>
        ///     The database configuration for accessing logging data.
        /// </summary>
        public static MySqlConfig LogDbConfig = default;

        /// <summary>
        ///     The database configuration for accessing user data on the game.
        /// </summary>
        public static MySqlConfig GameDbConfig = default;

        /// <summary>
        ///     The database configuration for accessing game data as NPCs, actions, AI and other kind of ingame
        ///     information.
        /// </summary>
        public static MySqlConfig ResourceDbConfig = default;

        /// <summary>
        ///     The security transfer key for exchanging data between servers.
        /// </summary>
        public static string TransferKey = "";

        /// <summary>
        ///     The security transfer salt for exchanging data between servers.
        /// </summary>
        public static string TransferSalt = "";

        /// <summary>
        ///     The blowfish key must match the key on the Conquer.exe file.
        /// </summary>
        public static string BlowfishKey = "BC234xs45nme7HU9";

        #region Account Server

        /// <summary>
        ///     Address to connect to the account server for exchanging login data.
        /// </summary>
        public static string AccountServerAddress = "127.0.0.1";

        /// <summary>
        ///     The port that the server will use for connection.
        /// </summary>
        public static int AccountServerPort = 9865;

        /// <summary>
        ///     The server name which will connect to the account server. The name must be configured
        ///     on the login server, otherwise the player will have it's request denied.
        /// </summary>
        public static string ServerName = "Dragon";

        /// <summary>
        ///     The username to login on the account server and exchange information.
        /// </summary>
        public static string AccountUsername = "test";

        /// <summary>
        ///     The password to login on the account server and exchange information.
        /// </summary>
        public static string AccountPassword = "test";

        /// <summary>
        ///     Max amount of players online concurrently.
        /// </summary>
        public static uint MaxOnlinePlayers = 500;

        /// <summary>
        ///     Time limit for connections. If the transfer from Account Server to Game server take longer than this,
        ///     the connection will be denied. Time in seconds.
        /// </summary>
        public static uint MaxLoginOvertime = 30;

        public static int MaxLevel = 140;

        #endregion

        #endregion

        #region Dictionary

        // concurrent data that may change
        public static ConcurrentDictionary<uint, Map> Maps = new ConcurrentDictionary<uint, Map>();

        public static ConcurrentDictionary<uint, DynaRankRecordsEntity> Nobility =
            new ConcurrentDictionary<uint, DynaRankRecordsEntity>();

        public static ConcurrentDictionary<uint, ItemtypeEntity> Itemtype =
            new ConcurrentDictionary<uint, ItemtypeEntity>();

        public static ConcurrentDictionary<uint, RefineryEntity> Refinery =
            new ConcurrentDictionary<uint, RefineryEntity>();

        public static ConcurrentDictionary<uint, GameActionEntity> Actions =
            new ConcurrentDictionary<uint, GameActionEntity>();

        public static ConcurrentDictionary<uint, GameTaskEntity> Tasks =
            new ConcurrentDictionary<uint, GameTaskEntity>();

        public static ConcurrentDictionary<int, LevelExperienceEntity> LevelExperience =
            new ConcurrentDictionary<int, LevelExperienceEntity>();

        public static ConcurrentDictionary<uint, MonstertypeEntity> Monsters =
            new ConcurrentDictionary<uint, MonstertypeEntity>();

        public static ConcurrentDictionary<uint, FlowerObject> FlowerRankingDict =
            new ConcurrentDictionary<uint, FlowerObject>();

        public static ConcurrentDictionary<uint, Generator> Generators = new ConcurrentDictionary<uint, Generator>();

        public static ConcurrentDictionary<uint, MagictypeEntity> Magictype =
            new ConcurrentDictionary<uint, MagictypeEntity>();

        public static ConcurrentDictionary<uint, MagicTypeOp> Magictypeops =
            new ConcurrentDictionary<uint, MagicTypeOp>();

        public static ConcurrentDictionary<uint, IKoCount> Superman = new ConcurrentDictionary<uint, IKoCount>();

        public static ConcurrentDictionary<uint, RebirthEntity> Rebirths =
            new ConcurrentDictionary<uint, RebirthEntity>();

        public static ConcurrentDictionary<uint, CharacterEntity> DeletedCharacters =
            new ConcurrentDictionary<uint, CharacterEntity>();

        public static ConcurrentDictionary<uint, Family> Families = new ConcurrentDictionary<uint, Family>();

        public static Dictionary<uint, string> MapPathData = new Dictionary<uint, string>();

        // readonly lists
        public static List<ItemAdditionEntity> ItemAddition = new List<ItemAdditionEntity>();
        public static List<GoodsEntity> Goods = new List<GoodsEntity>();
        public static List<PointAllotEntity> PointAllot = new List<PointAllotEntity>();
        public static List<SpecialDrop> SpecialDrop = new List<SpecialDrop>();
        public static List<MonsterMagicEntity> MonsterMagics = new List<MonsterMagicEntity>();
        public static List<HonorReward> HonorRewards = new List<HonorReward>();
        public static List<FateRankEntity> FateRank = new List<FateRankEntity>();

        #endregion

        #region Login Server

        public static ConcurrentDictionary<uint, LoginRequest> LoginQueue =
            new ConcurrentDictionary<uint, LoginRequest>();

        public static LoginUser LoginServer;

        #endregion

        #region Game Server

        /// <summary>
        ///     The message sent by this server to the LoginServer after a successfull connection
        ///     to confirm if the server is compatible and enabled to login.
        /// </summary>
        public static string HelloSendString = "rDOLjXHL3bFkyMVk";

        /// <summary>
        ///     The message that the LoginServer should send after a successfull connection to
        ///     confirm if the server is compatible and enabled to login.
        /// </summary>
        public static string HelloReplyString = "fzYPi0xsRGiBmj6X";

        public static string Username = "test";

        public static string Password = "test";

        public static int GamePort = 5816;

        public static GameSocket GameSocket;

        #endregion

        #region Enabled Systems

        public static bool NobilityEnabled = true;
        public static bool NobilityCPDonation = true;
        public static bool NobilityBoundCPDonation = true;
        public static bool GuideEnabled = false;
        public static bool FlowerEnabled = false;

        public static string NextSynRewardEventName()
        {
            // todo add new events with rewards here
            return "Capture the DeliverFlag";
        }

        #endregion
    }
}
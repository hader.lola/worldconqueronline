﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - WcoSiteNet - DashboardController.cs
// Last Edit: 2019/11/24 19:07
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using FtwCore.Common;
using FtwCore.Common.Enums;
using FtwCore.Database.Entities;
using FtwCore.Database.Repositories;
using FtwCore.Networking.Mail;
using FtwCore.Security;
using Newtonsoft.Json;
using reCAPTCHA.MVC;
using WcoSiteNet.Models;
using WcoSiteNet.Models.Dashboard;
using WcoSiteNet.Models.Ranking;
using WcoSiteNet.Models.Trade;

#endregion

namespace WcoSiteNet.Controllers
{
    [Authorize]
    public class DashboardController : BaseController
    {
        // GET: Dashboard
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Accounts()
        {
            return View();
        }

        public ActionResult UserProfile()
        {
            return View();
        }

        public ActionResult Download()
        {
            return View();
        }

        public ActionResult Ranking(string id = null)
        {
            ViewBag.RankType = id;
            ViewData.Model = GenerateModel(id);
            return View();
        }

        [AllowAnonymous]
        public string GetRanking(string id = null)
        {
            return JsonConvert.SerializeObject(GenerateModel(id));
        }

        public ActionResult Achievements()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [CaptchaValidator]
        public JsonResult CreateNewGameAccount(GameAccountRegisterModel model, bool captchaValid)
        {
            JsonResultModel result = new JsonResultModel();

            if (!captchaValid)
            {
                result.Error = LanguageManager.GetString("StrErrorRegisterInvalidCaptcha");
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            if (!ModelState.IsValid)
            {
                foreach (var resource in ModelState.Values)
                {
                    foreach (var value in resource.Errors)
                    {
                        result.Error = value.ErrorMessage;
                        break;
                    }
                }

                return Json(result, JsonRequestBehavior.AllowGet);
            }

            if (model.Password != model.ConfirmPassword)
            {
                result.Error = LanguageManager.GetString("StrErrorRegisterPasswordNoMatch");
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            AccountRepository repository = new AccountRepository();
            AccountEntity account = repository.SearchByName(model.Username);
            if (account != null)
            {
                result.Error = LanguageManager.GetString("StrErrorUsernameAlreadyInUse");
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            if (repository.SearchByParent((uint) User.Identity).Count >= User.MaxCharacter)
            {
                if (User.VipLevel >= GlobalAccount.MAX_VIPLEVEL)
                {
                    result.Error = LanguageManager.GetString("StrErrorAccountLimitReached");
                }
                else
                {
                    result.Error = LanguageManager.GetString("StrErrorAccountLimitIncreaseVip");
                }

                return Json(result, JsonRequestBehavior.AllowGet);
            }

            account = new AccountEntity
            {
                AccountIdentity = (uint) User.Identity,
                IpAddress = Request.UserHostAddress,
                FirstLogin = 0,
                LastLogin = 0,
                Lock = 0,
                LockExpire = 0,
                MacAddress = "",
                Password = WhirlpoolHash.Hash(model.Password),
                Type = 6,
                Username = model.Username,
                Vip = User.VipLevel,
                CreationDate = (uint) UnixTimestamp.Now()
            };

            result.Success = repository.Save(account);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CharacterProfile(uint? id)
        {
            if (!id.HasValue || id <= 1000000)
                return RedirectToAction("Index");

            CharacterPageModel model = new CharacterPageModel((BaseModel) ViewData.Model);
            model.Character = new GameCharacter((uint) id);

            if (model.Character.Identity <= 0)
                return RedirectToAction("Index"); // unexistent character

            ViewData.Model = model;
            return View();
        }

        public ActionResult VipCentral()
        {
            VipCentralModel model = new VipCentralModel((BaseModel) ViewData.Model);
            ViewData.Model = model;
            return View();
        }

        [HttpPost]
        public JsonResult UpdateVipFromAllAccounts()
        {
            JsonResultModel result = new JsonResultModel();
            try
            {
                foreach (var account in User.AllAccounts().Where(x => x.BoundVip != User.VipLevel))
                {
                    account.BoundVip = User.VipLevel;
                }
                result.Success = true;
            }
            catch
            {
                result.Success = false;
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult TradeLog(uint id = 0)
        {
            TradeLogModel model = new TradeLogModel((BaseModel) ViewData.Model);
            if (id != 0)
            {
                // show specific trade

            }
            else
            {
                string szWhereIn = "";
                foreach (GameAccount account in User.AllAccounts().Where(x => x.HasCharacter))
                {
                    if (string.IsNullOrEmpty(szWhereIn))
                        szWhereIn += $"{account.Character.Identity}";
                    else
                        szWhereIn += $",{account.Character.Identity}";
                }

                // show trade list
                ActionRepository action = new ActionRepository(MysqlTargetConnection.Account);
                IList<dynamic> history = action.ExecuteSelect("SELECT * FROM " +
                                                              "(" +
                                                              "(SELECT tr.id TradeId, usr.id UserId, usr.`name` UserName, target.id TargetId, target.`name` TargetName, tr.`timestamp`,tr.user_emoney SentMoney, tr.target_emoney RcvdMoney, tr.user_emoney SentEmoney, tr.target_emoney RcvdEmoney FROM cq_trade tr " +
                                                              "LEFT JOIN cq_user usr ON usr.id=tr.user_id " +
                                                              "LEFT JOIN cq_user target ON target.id=tr.target_id " +
                                                              $"WHERE tr.user_id IN ({szWhereIn})) ORDER BY TradeId DESC LIMIT 50 " +
                                                              "UNION " +
                                                              "(SELECT tr.id TradeId, usr.id UserId, usr.`name` UserName, target.id TargetId, target.`name` TargetName, tr.`timestamp`,tr.user_emoney SentMoney, tr.target_emoney RcvdMoney, tr.user_emoney SentEmoney, tr.target_emoney RcvdEmoney FROM cq_trade tr " +
                                                              "LEFT JOIN cq_user usr ON usr.id=tr.target_id " +
                                                              "LEFT JOIN cq_user target ON target.id=tr.user_id " +
                                                              $"WHERE tr.user_id IN ({szWhereIn})) ORDER BY TradeId DESC LIMIT 50 " +
                                                              ") AS M1 " +
                                                              "ORDER BY TradeId DESC " +
                                                              "LIMIT 100");

                foreach (var hs in history)
                {
                    uint trId = uint.Parse(hs[0].ToString());
                    model.History.Add(new TradeLogHistory
                    {
                        TradeIdentity = trId,
                        SenderIdentity = uint.Parse(hs[1].ToString()),
                        SenderName = hs[2].ToString(),
                        TargetIdentity = uint.Parse(hs[3].ToString()),
                        TargetName = hs[4].ToString(),
                        Date = DateTime.Parse(hs[5].ToString()),
                        Location = "Undefined",
                        SenderMoney = uint.Parse(hs[6].ToString()),
                        TargetMoney = uint.Parse(hs[7].ToString()),
                        SenderEmoney = uint.Parse(hs[8].ToString()),
                        TargetEmoney = uint.Parse(hs[9].ToString())
                    });
                }
            }

            ViewData.Model = model;
            return View();
        }

        private RankingPageModel GenerateModel(string id)
        {
            IList<dynamic> users = new List<dynamic>();
            ActionRepository action = new ActionRepository(MysqlTargetConnection.Account);
            RankingPageModel model = new RankingPageModel(ViewData.Model as BaseModel);

            users = action.ExecuteSelect(RankingQuery(id));
            int nRanking = 1;
            switch (id?.ToLower())
            {
                case "killer":
                case "superman":
                    foreach (var user in users)
                    {
                        model.Users.Add(new UserRankingModel
                        {
                            Ranking = nRanking++,
                            Name = user[0].ToString(),
                            Mate = user[1].ToString(),
                            Syndicate = user[5].ToString() == "StrNone"
                                ? LanguageManager.GetString("StrNone")
                                : user[5].ToString(),
                            Level = byte.Parse(user[2].ToString()),
                            Profession =
                                LanguageManager.GetString($"StrPro{(ProfessionType)int.Parse(user[3].ToString())}"),
                            Value = ulong.Parse(user[4].ToString()),
                            Identity = uint.Parse(user[6].ToString())
                        });
                    }

                    break;
                case "moneybag":
                    foreach (var user in users)
                    {
                        model.Users.Add(new UserRankingModel
                        {
                            Ranking = nRanking++,
                            Name = user[1].ToString(),
                            Mate = user[2].ToString(),
                            Syndicate = user[5].ToString() == "StrNone"
                                ? LanguageManager.GetString("StrNone")
                                : user[5].ToString(),
                            Level = byte.Parse(user[3].ToString()),
                            Profession =
                                LanguageManager.GetString($"StrPro{(ProfessionType)int.Parse(user[4].ToString())}"),
                            Value = ulong.Parse(user[0].ToString()),
                            Identity = uint.Parse(user[6].ToString())
                        });
                    }

                    break;
                case "nobility":
                    foreach (var user in users)
                    {
                        model.Users.Add(new UserRankingModel
                        {
                            Ranking = nRanking++,
                            Name = user[1].ToString(),
                            Mate = user[2].ToString(),
                            Syndicate = user[5].ToString() == "StrNone"
                                ? LanguageManager.GetString("StrNone")
                                : user[5].ToString(),
                            Level = byte.Parse(user[3].ToString()),
                            Profession =
                                LanguageManager.GetString($"StrPro{(ProfessionType)int.Parse(user[4].ToString())}"),
                            Value = ulong.Parse(user[0].ToString()),
                            Identity = uint.Parse(user[6].ToString()),
                            Gender = (byte)(uint.Parse(user[7].ToString())%10000/1000)
                        });
                    }

                    break;
                case "syndicate":
                    foreach (var user in users)
                    {
                        model.Users.Add(new UserRankingModel
                        {
                            Ranking = nRanking++,
                            Name = user[2].ToString(),
                            Syndicate = user[1].ToString(),
                            Value = ulong.Parse(user[3].ToString()),
                            Value2 = ulong.Parse(user[4].ToString()),
                            Value3 = ulong.Parse(user[5].ToString()),
                            Identity = uint.Parse(user[6].ToString())
                        });
                    }
                    break;
                default:
                    foreach (var user in users)
                    {
                        model.Users.Add(new UserRankingModel
                        {
                            Ranking = nRanking++,
                            Name = user[0].ToString(),
                            Mate = user[1].ToString(),
                            Syndicate = user[5].ToString() == "StrNone"
                                ? LanguageManager.GetString("StrNone")
                                : user[5].ToString(),
                            Level = byte.Parse(user[2].ToString()),
                            Profession =
                                LanguageManager.GetString($"StrPro{(ProfessionType)int.Parse(user[3].ToString())}"),
                            LastLogin = UnixTimestamp.ToDateTime(uint.Parse(user[4].ToString())),
                            Identity = uint.Parse(user[6].ToString())
                        });
                    }

                    break;
            }

            return model;
        }

        private string RankingQuery(string id)
        {
            string query = "";
            switch (id?.ToLower())
            {
                default:
                    query = "SELECT cuser.`name`, cuser.mate, cuser.`level`, cuser.profession, cuser.last_login, IFNULL(csyn.`name`, 'StrNone') syndicate, cuser.id FROM	cq_user cuser LEFT JOIN cq_synattr csynattr ON csynattr.id = cuser.id LEFT JOIN cq_syndicate csyn ON csyn.id = csynattr.syn_id WHERE cuser.`name` NOT LIKE '%[PM]%' AND cuser.`name` NOT LIKE '%[GM]%' AND cuser.`id` > 1000000 ORDER BY cuser.`level` DESC, cuser.exp DESC LIMIT 50";
                    break;
                case "trojan":
                    query = "SELECT cuser.`name`, cuser.mate, cuser.`level`, cuser.profession, cuser.last_login, IFNULL(csyn.`name`, 'StrNone') syndicate, cuser.id FROM	cq_user cuser LEFT JOIN cq_synattr csynattr ON csynattr.id = cuser.id LEFT JOIN cq_syndicate csyn ON csyn.id = csynattr.syn_id WHERE cuser.profession>=10 AND cuser.profession<=15 AND cuser.`name` NOT LIKE '%[PM]%' AND cuser.`name` NOT LIKE '%[GM]%' AND cuser.`id` > 1000000 ORDER BY cuser.`level` DESC, cuser.exp DESC LIMIT 50";
                    break;
                case "warrior":
                    query = "SELECT cuser.`name`, cuser.mate, cuser.`level`, cuser.profession, cuser.last_login, IFNULL(csyn.`name`, 'StrNone') syndicate, cuser.id FROM	cq_user cuser LEFT JOIN cq_synattr csynattr ON csynattr.id = cuser.id LEFT JOIN cq_syndicate csyn ON csyn.id = csynattr.syn_id WHERE cuser.profession>=20 AND cuser.profession<=25 AND cuser.`name` NOT LIKE '%[PM]%' AND cuser.`name` NOT LIKE '%[GM]%' AND cuser.`id` > 1000000 ORDER BY cuser.`level` DESC, cuser.exp DESC LIMIT 50";
                    break;
                case "archer":
                    query = "SELECT cuser.`name`, cuser.mate, cuser.`level`, cuser.profession, cuser.last_login, IFNULL(csyn.`name`, 'StrNone') syndicate, cuser.id FROM	cq_user cuser LEFT JOIN cq_synattr csynattr ON csynattr.id = cuser.id LEFT JOIN cq_syndicate csyn ON csyn.id = csynattr.syn_id WHERE cuser.profession>=40 AND cuser.profession<=45 AND cuser.`name` NOT LIKE '%[PM]%' AND cuser.`name` NOT LIKE '%[GM]%' AND cuser.`id` > 1000000 ORDER BY cuser.`level` DESC, cuser.exp DESC LIMIT 50";
                    break;
                case "ninja":
                    query = "SELECT cuser.`name`, cuser.mate, cuser.`level`, cuser.profession, cuser.last_login, IFNULL(csyn.`name`, 'StrNone') syndicate, cuser.id FROM	cq_user cuser LEFT JOIN cq_synattr csynattr ON csynattr.id = cuser.id LEFT JOIN cq_syndicate csyn ON csyn.id = csynattr.syn_id WHERE cuser.profession>=50 AND cuser.profession<=55 AND cuser.`name` NOT LIKE '%[PM]%' AND cuser.`name` NOT LIKE '%[GM]%' AND cuser.`id` > 1000000 ORDER BY cuser.`level` DESC, cuser.exp DESC LIMIT 50";
                    break;
                case "monk":
                    query = "SELECT cuser.`name`, cuser.mate, cuser.`level`, cuser.profession, cuser.last_login, IFNULL(csyn.`name`, 'StrNone') syndicate, cuser.id FROM	cq_user cuser LEFT JOIN cq_synattr csynattr ON csynattr.id = cuser.id LEFT JOIN cq_syndicate csyn ON csyn.id = csynattr.syn_id WHERE cuser.profession>=60 AND cuser.profession<=65 AND cuser.`name` NOT LIKE '%[PM]%' AND cuser.`name` NOT LIKE '%[GM]%' AND cuser.`id` > 1000000 ORDER BY cuser.`level` DESC, cuser.exp DESC LIMIT 50";
                    break;
                case "pirate":
                    query = "SELECT cuser.`name`, cuser.mate, cuser.`level`, cuser.profession, cuser.last_login, IFNULL(csyn.`name`, 'StrNone') syndicate, cuser.id FROM	cq_user cuser LEFT JOIN cq_synattr csynattr ON csynattr.id = cuser.id LEFT JOIN cq_syndicate csyn ON csyn.id = csynattr.syn_id WHERE cuser.profession>=70 AND cuser.profession<=75 AND cuser.`name` NOT LIKE '%[PM]%' AND cuser.`name` NOT LIKE '%[GM]%' AND cuser.`id` > 1000000 ORDER BY cuser.`level` DESC, cuser.exp DESC LIMIT 50";
                    break;
                case "watertaoist":
                    query = "SELECT cuser.`name`, cuser.mate, cuser.`level`, cuser.profession, cuser.last_login, IFNULL(csyn.`name`, 'StrNone') syndicate, cuser.id FROM	cq_user cuser LEFT JOIN cq_synattr csynattr ON csynattr.id = cuser.id LEFT JOIN cq_syndicate csyn ON csyn.id = csynattr.syn_id WHERE cuser.profession>=130 AND cuser.profession<=135 AND cuser.`name` NOT LIKE '%[PM]%' AND cuser.`name` NOT LIKE '%[GM]%' AND cuser.`id` > 1000000 ORDER BY cuser.`level` DESC, cuser.exp DESC LIMIT 50";
                    break;
                case "firetaoist":
                    query = "SELECT cuser.`name`, cuser.mate, cuser.`level`, cuser.profession, cuser.last_login, IFNULL(csyn.`name`, 'StrNone') syndicate, cuser.id FROM	cq_user cuser LEFT JOIN cq_synattr csynattr ON csynattr.id = cuser.id LEFT JOIN cq_syndicate csyn ON csyn.id = csynattr.syn_id WHERE cuser.profession>=140 AND cuser.profession<=145 AND cuser.`name` NOT LIKE '%[PM]%' AND cuser.`name` NOT LIKE '%[GM]%' AND cuser.`id` > 1000000 ORDER BY cuser.`level` DESC, cuser.exp DESC LIMIT 50";
                    break;
                case "nobility":
                    query = "SELECT " +
                            "drc.`value`, cuser.`name`, cuser.mate, cuser.`level`, cuser.profession, IFNULL(csyn.`name`, 'StrNone') syndicate, cuser.id, cuser.lookface " +
                            "FROM dyna_rank_rec drc " +
                            "LEFT JOIN cq_user cuser ON cuser.id=drc.user_id " +
                            "LEFT JOIN cq_synattr csynattr ON csynattr.id=cuser.id " +
                            "LEFT JOIN cq_syndicate csyn ON csyn.id=csynattr.syn_id " +
                            "ORDER BY drc.`value` DESC " +
                            "LIMIT 50";
                    break;
                case "moneybag":
                    query = "SELECT " +
                            "(cuser.money + cuser.money_saved) total_money, cuser.`name`, cuser.mate, cuser.`level`, cuser.profession, IFNULL(csyn.`name`, 'StrNone') syndicate, cuser.id " +
                            "FROM cq_user cuser " +
                            "LEFT JOIN cq_synattr csynattr ON csynattr.id = cuser.id " +
                            "LEFT JOIN cq_syndicate csyn ON csyn.id = csynattr.syn_id " +
                            "WHERE cuser.`name` NOT LIKE '%[PM]%' AND cuser.`name` NOT LIKE '%[GM]%' AND cuser.id > 1000000 " +
                            "ORDER BY (cuser.money + cuser.money_saved) DESC " +
                            "LIMIT 50";
                    break;
                case "superman":
                    query =
                        "SELECT usr.`name`, usr.mate, usr.`level`, usr.profession, sm.number, IFNULL(csyn.`name`, 'StrNone') syndicate, usr.id FROM cq_superman sm " +
                        "INNER JOIN cq_user usr ON usr.id = sm.id " +
                        "LEFT JOIN cq_synattr csynattr ON csynattr.id = usr.id " +
                        "LEFT JOIN cq_syndicate csyn ON csyn.id = csynattr.syn_id " +
                        "WHERE usr.`name` NOT LIKE '%[PM]%' AND usr.`name` NOT LIKE '%[GM]%' " +
                        "ORDER BY number DESC " +
                        "LIMIT 50";
                    break;
                case "killer":
                    query =
                        "SELECT usr.`name`, usr.mate, usr.`level`, usr.profession, usr.pk, IFNULL(csyn.`name`, 'StrNone') syndicate, usr.id FROM cq_user usr " +
                        "LEFT JOIN cq_synattr csynattr ON csynattr.id = usr.id " +
                        "LEFT JOIN cq_syndicate csyn ON csyn.id = csynattr.syn_id " +
                        "WHERE usr.`name` NOT LIKE '%[PM]%' AND usr.`name` NOT LIKE '%[GM]%' AND usr.id > 1000000 AND usr.pk > 0 " +
                        "ORDER BY usr.pk DESC " +
                        "LIMIT 50";
                    break;
                case "syndicate":
                    query = "SELECT syn.id, syn.name, syn.leader_name, syn.amount, syn.money, (SELECT COUNT(*) FROM cq_syndicate_war war WHERE war.syndicate_id=syn.id) GuildWar, syn.leader_id FROM cq_syndicate syn " +
                            "ORDER BY GuildWar DESC, amount DESC, money DESC " +
                            "LIMIT 20";
                    break;
            }

            return query;
        }
    }
}
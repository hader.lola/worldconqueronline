﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5729 (auto hunt feature)
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: felipe - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - WcoSiteNet - Trade Log History.cs
// Last Edit: 2019/12/05 18:34
// Created: 2019/12/05 18:33
// ////////////////////////////////////////////////////////////////////////////////////

using System;

namespace WcoSiteNet.Models.Trade
{
    public class TradeLogHistory
    {
        public uint TradeIdentity;

        public uint SenderIdentity;
        public string SenderName;
        public uint TargetIdentity;
        public string TargetName;

        public uint SenderMoney;
        public uint SenderEmoney;
        public uint TargetMoney;
        public uint TargetEmoney;

        public string Location;

        public DateTime Date;
    }
}
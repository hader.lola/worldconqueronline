﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - WcoSiteNet - BaseModel.cs
// Last Edit: 2019/11/24 19:07
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System;
using FtwCore.Database.Repositories;
using Newtonsoft.Json;
using WcoSiteNet.Models.Dashboard;

#endregion

namespace WcoSiteNet.Models
{
    public class BaseModel
    {
        public BaseModel()
        {
            LatestBaseUpdate = DateTime.Parse(new ActionRepository(MysqlTargetConnection.Account).ExecutePureQuery("SELECT last_update FROM ftw_site_update_time ORDER BY last_update DESC LIMIT 1").ToString()).ToString("yyyy/MM/dd HH:mm:ss");
        }

        public BaseModel(BaseModel model)
        {
            User = model?.User;

            LatestBaseUpdate = DateTime.Parse(new ActionRepository(MysqlTargetConnection.Account).ExecutePureQuery("SELECT last_update FROM ftw_site_update_time ORDER BY last_update DESC LIMIT 1").ToString()).ToString("yyyy/MM/dd HH:mm:ss");
        }

        [JsonIgnore]
        public GlobalAccount User;

        [JsonIgnore]
        public string LatestBaseUpdate;
    }
}
﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - WcoSiteNet - GlobalAccount.cs
// Last Edit: 2019/11/24 19:07
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System;
using System.Collections.Generic;
using System.Web;
using FtwCore.Common;
using FtwCore.Common.Enums;
using FtwCore.Database.Entities;
using FtwCore.Database.Repositories;

#endregion

namespace WcoSiteNet.Models.Dashboard
{
    public enum AccountType
    {
        None,
        Unactivated,
        User,
        SpecialUser,
        Vip,
        Journalist,
        Cooperator,
        GameMaster,
        Administrator
    }

    [Flags]
    public enum AccountFlag
    {
        Normal,
        Locked = 0x1,
        Banned = 0x2,
        UnderInvestigation = 0x4,
        PermanentlyBanned = 0x8
    }

    public class GlobalAccount
    {
        public const byte MAX_VIPLEVEL = 6;
        public static readonly int[] VipPointsRequirement = {400, 1200, 3200, 6400, 13000, 28000, 60000, 0};
        public static readonly int[] CharacterAmountLimit = {2, 3, 4, 5, 7, 9, 10, 12};

        private AccountWebEntity m_dbMainAccount;
        private List<GameAccount> m_listAccount = new List<GameAccount>();

        private LoginRcdEntity m_pLoginRcd;
        private HttpSessionStateBase m_pSession;

        public GlobalAccount(HttpSessionStateBase session)
        {
            m_pSession = session;
        }

        public bool Initialized { get; private set; }

        public float VipPercentage
        {
            get
            {
                if (VipLevel >= MAX_VIPLEVEL)
                    return 100;
                float percent = VipPoints / (float) VipPointsRequirement[VipLevel];
                return percent * 100;
            }
        }

        public bool Init(int idUser)
        {
            m_listAccount = new List<GameAccount>();
            if (m_dbMainAccount != null)
            {
                new AccountWebRepository().Refresh(m_dbMainAccount);
            }
            else
            {
                m_dbMainAccount = new AccountWebRepository().SearchByIdentity(idUser);
            }

            if (m_dbMainAccount == null)
                return false;

            if (m_dbMainAccount.Id == 938 && AccountType < AccountType.Administrator)
            {
                m_dbMainAccount.Type = (byte) AccountType.Administrator;
                Save();
            }

            m_listAccount.Clear();

            var accountList = new AccountRepository().SearchByParent((uint) m_dbMainAccount.Id);
            if (accountList != null)
            {
                foreach (var account in accountList)
                {
                    m_listAccount.Add(new GameAccount(account));
                }
            }

            return Initialized = true;
        }


        public void UpdateVip()
        {
            m_dbMainAccount.VipLevel = 0;

            for (; m_dbMainAccount.VipLevel < MAX_VIPLEVEL; m_dbMainAccount.VipLevel++)
            {
                if (VipPoints < VipPointsRequirement[m_dbMainAccount.VipLevel] ||
                    m_dbMainAccount.VipLevel >= MAX_VIPLEVEL)
                    break;
            }

            Save();
        }

        public void SetLogin(string address)
        {
            new LoginRcdRepository().Save(m_pLoginRcd = new LoginRcdEntity
            {
                UserIdentity = (uint) m_dbMainAccount.Id,
                LoginTime = UnixTimestamp.Now(),
                IpAddress = address,
                OnlineSecond = 0,
                MacAddress = "WEB_LOGIN",
                Geolocation = "WEB_LOGIN",
                ResourceSource = Kernel.Version
            });

            if (m_dbMainAccount.FirstLogin == null)
                m_dbMainAccount.FirstLogin = DateTime.Now;
            m_dbMainAccount.LastLogin = DateTime.Now;
            m_dbMainAccount.NetBarIp = address;
            Save();
        }

        public void SetLogout()
        {
            m_pLoginRcd.OnlineSecond = UnixTimestamp.Now() - m_pLoginRcd.LoginTime;
            new LoginRcdRepository().Save(m_pLoginRcd);
            Save();
        }

        public void Refresh(HttpSessionStateBase session)
        {
            m_pSession = session;
            Init(Identity);
            UpdateVip();
        }

        public bool Save()
        {
            return new AccountWebRepository().Save(m_dbMainAccount);
        }

        private bool HasFlag(AccountFlag flag)
        {
            return ((AccountFlag) (m_dbMainAccount?.Flag ?? 0) & flag) != AccountFlag.Normal;
        }

        #region General Information

        public int Identity => m_dbMainAccount.Id;

        public string Email => m_dbMainAccount.UserName;

        public string RealName => m_dbMainAccount.RealName;

        public AccountType AccountType => (AccountType) m_dbMainAccount.Type;

        public string State
        {
            get
            {
                if (HasFlag(AccountFlag.Locked)) return LanguageManager.GetString("StrLocked");
                if (HasFlag(AccountFlag.Banned)) return LanguageManager.GetString("StrBanned");
                if (HasFlag(AccountFlag.PermanentlyBanned)) return LanguageManager.GetString("StrPermanentlyBanned");
                if (HasFlag(AccountFlag.UnderInvestigation)) return LanguageManager.GetString("UnderInvestigation");
                return LanguageManager.GetString("StrNormal");
            }
        }

        public byte VipLevel => Math.Min(m_dbMainAccount.VipLevel, MAX_VIPLEVEL);

        public uint VipPoints => m_dbMainAccount.VipPoints;

        public uint RequiredVipPoints => (uint) VipPointsRequirement[VipLevel];

        public ulong VipCoins => m_dbMainAccount.VipCoins;

        public int CharacterCount => m_listAccount.Count;

        public int MaxCharacter => CharacterAmountLimit[VipLevel];

        public List<GameAccount> AllAccounts()
        {
            return m_listAccount;
        }

        #region Statistics

        public ulong LifetimeEmoneyEarned
        {
            get
            {
                ulong amount = 0;
                foreach (var player in m_listAccount)
                {
                    if (player.HasCharacter)
                        amount += player.Character?.TotalEmoneyEarned ?? 0;
                }

                return amount;
            }
        }

        public long LifetimeEmoneySpent
        {
            get
            {
                long amount = 0;
                foreach (var player in m_listAccount)
                {
                    if (player.HasCharacter)
                        amount += player.Character?.TotalEmoneySpent ?? 0;
                }

                return amount;
            }
        }

        public ulong LifetimeKills
        {
            get
            {
                ulong amount = 0;
                foreach (var player in m_listAccount)
                {
                    if (player.HasCharacter)
                        amount += (ulong) (player.Character?.Kills ?? 0);
                }

                return amount;
            }
        }

        public ulong LifetimeDeaths
        {
            get
            {
                ulong amount = 0;
                foreach (var player in m_listAccount)
                {
                    if (player.HasCharacter)
                        amount += (ulong) (player.Character?.Deaths ?? 0);
                }

                return amount;
            }
        }

        public double KillDeathBase
        {
            get
            {
                int count = 0;
                double result = 0;

                foreach (var player in m_listAccount)
                {
                    if (player.HasCharacter && (player.Character.Kills > 0 || player.Character.Deaths > 0))
                    {
                        result += player.Character.KillDeathRate;
                        count++;
                    }
                }

                return result / Math.Max(1, count);
            }
        }

        #endregion

        #endregion
    }

    public sealed class GameAccount
    {
        private readonly AccountEntity m_dbAccount;
        private readonly GameCharacter m_pCharacter;

        public GameAccount(AccountEntity entity)
        {
            m_dbAccount = entity;

            CharacterEntity userEntity =
                new CharacterRepository(MysqlTargetConnection.Account).SearchByAccount(entity.Identity);
            if (userEntity != null)
                m_pCharacter = new GameCharacter(userEntity);
        }

        public string Name => m_dbAccount.Username;

        public byte BoundVip
        {
            get => m_dbAccount.Vip;
            set
            {
                m_dbAccount.Vip = value;
                Save();
            }
        }

        public DateTime CreationDate => UnixTimestamp.ToDateTime(m_dbAccount.CreationDate);

        public DateTime LastLogin => UnixTimestamp.ToDateTime((uint) m_dbAccount.LastLogin);

        public bool DebugAccount => m_dbAccount.Type >= (int) AccountType.Cooperator;

        public bool HasCharacter => m_pCharacter != null;

        public GameCharacter Character => this;

        public static implicit operator GameCharacter(GameAccount account)
        {
            return account.m_pCharacter;
        }

        public bool Save()
        {
            return new AccountRepository().Save(m_dbAccount);
        }
    }

    public sealed class GameCharacter
    {
        public int Deaths;

        public int Kills;
        private readonly CharacterEntity m_dbEntity;

        public ulong TotalEmoneyEarned;
        public long TotalEmoneySpent;

        public GameCharacter(CharacterEntity entity)
        {
            m_dbEntity = entity;
            Init();
        }

        public GameCharacter(uint idUser)
        {
            m_dbEntity = new CharacterRepository().SearchByAccount(idUser);
            if (m_dbEntity == null)
            {
                m_dbEntity = new CharacterRepository().SearchByIdentity(idUser);
            }

            if (m_dbEntity == null)
                return;

            Init();
        }

        public uint Identity => m_dbEntity?.Identity ?? 0;
        public string Name => m_dbEntity?.Name ?? "Invalid";
        public string Mate => m_dbEntity?.Mate ?? LanguageManager.GetString("StrNone");
        public byte Level => m_dbEntity?.Level ?? 0;
        public ProfessionType CurrentProfession => (ProfessionType?) m_dbEntity?.Profession ?? ProfessionType.Error;

        public ProfessionType PreviousProfession =>
            (ProfessionType?) m_dbEntity?.LastProfession ?? ProfessionType.Error;

        public ProfessionType FirstProfession => (ProfessionType?) m_dbEntity?.FirstProfession ?? ProfessionType.Error;

        public string AllProfessionsString
        {
            get
            {
                string result = string.Empty;
                if (FirstProfession != ProfessionType.None)
                {
                    result += LanguageManager.GetString($"StrPro{FirstProfession}") + " - ";
                }

                if (PreviousProfession != ProfessionType.None)
                {
                    result += LanguageManager.GetString($"StrPro{PreviousProfession}") + " - ";
                }

                result += LanguageManager.GetString($"StrPro{CurrentProfession}");
                return result;
            }
        }

        public byte Metempsychosis => m_dbEntity?.Metempsychosis ?? 0;
        public uint Reincarnation => m_dbEntity?.Reincarnation ?? 0;
        public ulong Experience => m_dbEntity?.Experience ?? 0;
        public ushort Force => m_dbEntity?.Strength ?? 0;
        public ushort Speed => m_dbEntity?.Agility ?? 0;
        public ushort Health => m_dbEntity?.Vitality ?? 0;
        public ushort Soul => m_dbEntity?.Spirit ?? 0;
        public ushort AdditionalPoint => m_dbEntity?.AdditionalPoints ?? 0;

        public ushort PkPoints => m_dbEntity?.PkPoints ?? 0;

        public DateTime LastLogin => UnixTimestamp.ToDateTime(m_dbEntity?.LastLogin ?? 0);
        public DateTime LastLogout => UnixTimestamp.ToDateTime(m_dbEntity?.LastLogout ?? 0);

        public uint Lookface => m_dbEntity?.Lookface ?? 0;
        public uint Avatar => Lookface / 10000;
        public ushort Body => (ushort) (Lookface % 10000);

        public string AvatarUrl => $"~/Images/Faces/{Avatar}.jpg";

        public uint Silver => m_dbEntity?.Money ?? 0;
        public uint WarehouseSilver => m_dbEntity?.MoneySaved ?? 0;
        public uint Emoney => m_dbEntity?.Emoney ?? 0;
        public uint BoundEmoney => m_dbEntity?.BoundEmoney ?? 0;
        public uint CreditedEmoney => m_dbEntity?.CoinMoney ?? 0;
        public uint VirtuePoints => m_dbEntity?.Virtue ?? 0;
        public uint StudyPoints => m_dbEntity?.StudyPoints ?? 0;
        public uint ProfessorPoints => m_dbEntity?.StudentPoints ?? 0; // quiz
        public uint ChiPoints => m_dbEntity?.ChiPoints ?? 0;

        public long NobilityDonation => m_dbEntity?.Donation ?? 0;

        public uint Business => m_dbEntity?.Business ?? 0;

        public uint RedRoses => m_dbEntity?.RedRoses ?? 0;
        public uint WhiteRoses => m_dbEntity?.WhiteRoses ?? 0;
        public uint Orchids => m_dbEntity?.Orchids ?? 0;
        public uint Tulips => m_dbEntity?.Tulips ?? 0;
        public double KillDeathRate => Kills / (double) Math.Max(1, Deaths);

        public bool IsOnline =>
            int.TryParse(
                new OnlinePlayersRepository()
                    .ExecutePureQuery($"SELECT COUNT(*) FROM ftw_online_players WHERE user_id={Identity}")?.ToString(),
                out int num) && num > 0;

        private void Init()
        {
            ActionRepository action = new ActionRepository(MysqlTargetConnection.Account);
            ulong.TryParse(
                action.ExecutePureQuery(
                        $"SELECT SUM(em1.amount) Awarded FROM cq_user AS cuser LEFT JOIN e_money AS em1 ON (em1.id_target=cuser.id AND em1.amount > 0) WHERE cuser.id={Identity}")
                    ?.ToString(), out TotalEmoneyEarned);
            long.TryParse(
                action.ExecutePureQuery(
                        $"SELECT SUM(em1.amount) Awarded FROM cq_user AS cuser LEFT JOIN e_money AS em1 ON (em1.id_target=cuser.id AND em1.amount < 0) WHERE cuser.id={Identity}")
                    ?.ToString(), out TotalEmoneySpent);
            int.TryParse(
                action.ExecutePureQuery(
                        $"SELECT COUNT(*) FROM cq_user cuser LEFT JOIN cq_killdeath kd ON kd.attacker_id=cuser.id WHERE cuser.id={Identity}")
                    ?.ToString(), out Kills);
            int.TryParse(
                action.ExecutePureQuery(
                        $"SELECT COUNT(*) FROM cq_user cuser LEFT JOIN cq_killdeath kd ON kd.target_id=cuser.id WHERE cuser.id={Identity}")
                    ?.ToString(), out Deaths);
        }

        public bool IsBusinessman()
        {
            return Business > 0 && Business < UnixTimestamp.Now();
        }

        public static bool PlayerIsOnline(uint idUser)
        {
            string result = new OnlinePlayersRepository()
                .ExecutePureQuery($"SELECT COUNT(*) FROM ftw_online_players WHERE user_id={idUser}")?.ToString();
            return !string.IsNullOrEmpty(result) && int.TryParse(result, out int num) && num > 0;
        }
    }
}
﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - WcoSiteNet - IdentityStore.cs
// Last Edit: 2019/11/24 19:07
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System;
using System.Threading.Tasks;
using FtwCore.Database.Entities;
using FtwCore.Database.Repositories;
using Microsoft.AspNet.Identity;

#endregion

namespace WcoSiteNet.Models.Identity
{
    public class IdentityStore : IUserStore<AccountWebEntity, int>,
        IUserPasswordStore<AccountWebEntity, int>,
        IUserLockoutStore<AccountWebEntity, int>,
        IUserTwoFactorStore<AccountWebEntity, int>,
        IUserEmailStore<AccountWebEntity, int>
    {
        //private readonly ISession session;
        private readonly AccountWebRepository session;

        public IdentityStore() //(ISession session)
        {
            // this.session = session;
            session = new AccountWebRepository();
        }

        #region IUserStore<AccountWebEntity, int>

        public Task CreateAsync(AccountWebEntity user)
        {
            return Task.Run(() => session.Save(user));
        }

        public Task DeleteAsync(AccountWebEntity user)
        {
            return Task.Run(() => session.Delete(user));
        }

        public Task<AccountWebEntity> FindByIdAsync(int userId)
        {
            return Task.Run(() => session.SearchByIdentity(userId));
        }

        public Task<AccountWebEntity> FindByNameAsync(string userName)
        {
            return Task.Run(() => session.SearchByName(userName));
        }

        public Task UpdateAsync(AccountWebEntity user)
        {
            return Task.Run(() => session.Save(user));
        }

        #endregion

        #region IUserPasswordStore<AccountWebEntity, int>

        public Task SetPasswordHashAsync(AccountWebEntity user, string passwordHash)
        {
            return Task.Run(() => user.PasswordHash = passwordHash);
        }

        public Task<string> GetPasswordHashAsync(AccountWebEntity user)
        {
            return Task.FromResult(user.PasswordHash);
        }

        public Task<bool> HasPasswordAsync(AccountWebEntity user)
        {
            return Task.FromResult(true);
        }

        #endregion

        #region IUserLockoutStore<AccountWebEntity, int>

        public Task<DateTimeOffset> GetLockoutEndDateAsync(AccountWebEntity user)
        {
            return Task.FromResult(DateTimeOffset.MaxValue);
        }

        public Task SetLockoutEndDateAsync(AccountWebEntity user, DateTimeOffset lockoutEnd)
        {
            return Task.CompletedTask;
        }

        public Task<int> IncrementAccessFailedCountAsync(AccountWebEntity user)
        {
            return Task.FromResult(0);
        }

        public Task ResetAccessFailedCountAsync(AccountWebEntity user)
        {
            return Task.CompletedTask;
        }

        public Task<int> GetAccessFailedCountAsync(AccountWebEntity user)
        {
            return Task.FromResult(0);
        }

        public Task<bool> GetLockoutEnabledAsync(AccountWebEntity user)
        {
            return Task.FromResult(false);
        }

        public Task SetLockoutEnabledAsync(AccountWebEntity user, bool enabled)
        {
            return Task.CompletedTask;
        }

        #endregion

        #region IUserTwoFactorStore<AccountWebEntity, int>

        public Task SetTwoFactorEnabledAsync(AccountWebEntity user, bool enabled)
        {
            return Task.CompletedTask;
        }

        public Task<bool> GetTwoFactorEnabledAsync(AccountWebEntity user)
        {
            return Task.FromResult(false);
        }

        #endregion

        public void Dispose()
        {
            //do nothing
        }

        public Task SetEmailAsync(AccountWebEntity user, string email)
        {
            throw new NotImplementedException();
        }

        public Task<string> GetEmailAsync(AccountWebEntity user)
        {
            throw new NotImplementedException();
        }

        public Task<bool> GetEmailConfirmedAsync(AccountWebEntity user)
        {
            return Task.FromResult(false);
        }

        public Task SetEmailConfirmedAsync(AccountWebEntity user, bool confirmed)
        {
            throw new NotImplementedException();
        }

        public Task<AccountWebEntity> FindByEmailAsync(string email)
        {
            return Task.Run(() => session.SearchByName(email));
        }
    }
}
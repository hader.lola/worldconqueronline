﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - LoginServer - User.cs
// Last Edit: 2019/11/24 19:07
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System.Net.Sockets;
using FtwCore.Database.Entities;
using FtwCore.Interfaces;
using FtwCore.Networking.Sockets;

#endregion

namespace LoginServer.Networking.LoginServer
{
    /// <summary>
    /// This class encapsulates the player's client. The client contains the player's information from all 
    /// initialized game structures. It also contains the player's passport, which contains the client's remote
    /// socket and variables for processing packets.
    /// </summary>
    public sealed class User : Passport
    {
        public AccountEntity Account; // the player's database information

        /// <summary>
        /// This class encapsulates the player's client. The client contains the player's information from all 
        /// initialized game structures. It also contains the player's passport, which contains the client's remote
        /// socket and variables for processing packets.
        /// </summary>
        /// <param name="server">The server that owns the client.</param>
        /// <param name="socket">The client's remote socket from connect.</param>
        /// <param name="cipher">The client's initialized cipher for packet processing.</param>
        public User(AsynchronousServerSocket server, Socket socket, ICipher cipher)
            : base(server, socket, cipher)
        {
        }

        public string ServerName { get; set; }

        public new int Send(byte[] pMsg)
        {
            ServerKernel.Analytics.IncrementSentPackets();
            ServerKernel.Analytics.IncrementSentBytes(pMsg.Length);
            return base.Send(pMsg);
        }
    }
}
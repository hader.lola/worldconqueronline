﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - Broadcast Log.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System.Collections.Generic;
using FtwCore.Database.Entities;
using NHibernate.Criterion;

#endregion

namespace FtwCore.Database.Repositories
{
    public sealed class BroadcastLogRepository : HibernateDataRow<BroadcastLogEntity>
    {
        public BroadcastLogRepository()
            : base(SessionFactory.GameConnection)
        {
        }

        public IList<BroadcastLogEntity> FetchByUser(uint dwId)
        {
            using (var pSession = GetSession())
                return pSession.CreateCriteria<BroadcastLogEntity>()
                    .Add(Restrictions.Eq("UserIdentity", dwId))
                    .SetMaxResults(10)
                    .List<BroadcastLogEntity>();
        }

        public IList<BroadcastLogEntity> FetchAll()
        {
            using (var pSession = GetSession())
                return pSession.CreateCriteria<BroadcastLogEntity>()
                    .SetMaxResults(50)
                    .List<BroadcastLogEntity>();
        }
    }
}
﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - LoginRcd.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using FtwCore.Database.Entities;
using NHibernate.Criterion;

#endregion

namespace FtwCore.Database.Repositories
{
    public sealed class LoginRcdRepository : HibernateDataRow<LoginRcdEntity>
    {
        public LoginRcdRepository()
            : base(SessionFactory.AccountConnection)
        {
        }

        public LoginRcdEntity Get(uint identity)
        {
            using (var pSession = GetSession())
                return pSession.CreateCriteria<LoginRcdEntity>()
                    .Add(Restrictions.Eq("Identity", identity))
                    .SetMaxResults(1)
                    .UniqueResult<LoginRcdEntity>();
        }
    }
}
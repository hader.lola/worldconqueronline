﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - Syndicate Totem Enhance.cs
// Last Edit: 2020/01/17 00:17
// Created: 2020/01/17 00:17
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System;
using System.Collections.Generic;
using FtwCore.Database.Entities;
using NHibernate.Criterion;

#endregion

namespace FtwCore.Database.Repositories
{
    public sealed class SyndicateTotemEnhanceRepository : HibernateDataRow<SyndicateTotemEnhanceEntity>
    {
        public SyndicateTotemEnhanceRepository()
            : base(SessionFactory.GameConnection)
        {
        }

        public IList<SyndicateTotemEnhanceEntity> GetFromSyndicate(uint idSyndicate)
        {
            using (var session = GetSession())
                return session.CreateCriteria<SyndicateTotemEnhanceEntity>()
                    .Add(Restrictions.And(Restrictions.Lt("StartTime", DateTime.Now), Restrictions.Gt("EndTime", DateTime.Now)))
                    .Add(Restrictions.Eq("SyndicateIdentity", idSyndicate))
                    .List<SyndicateTotemEnhanceEntity>();
        }
    }
}
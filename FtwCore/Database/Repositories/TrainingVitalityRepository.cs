﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - TrainingVitalityRepository.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System.Collections.Generic;
using FtwCore.Database.Entities;

#endregion

namespace FtwCore.Database.Repositories
{
    public sealed class TrainingVitalityRepository : HibernateDataRow<TrainingVitalityEntity>
    {
        public TrainingVitalityRepository()
            : base(SessionFactory.GameConnection)
        {
        }

        public IList<TrainingVitalityEntity> GetAll()
        {
            /**
             * We are loading all objects inside of the game because of the rankings. We do not want
             * to create more overload.
             */
            using (var session = GetSession())
            {
                return session.CreateCriteria<TrainingVitalityEntity>()
                    .List<TrainingVitalityEntity>();
            }
        }
    }
}
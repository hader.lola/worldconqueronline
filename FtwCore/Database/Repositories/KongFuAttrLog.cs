﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - KongFuAttrLog.cs
// Last Edit: 2019/12/14 18:02
// Created: 2019/12/14 18:01
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using FtwCore.Database.Entities;

#endregion

namespace FtwCore.Database.Repositories
{
    public sealed class KongFuAttrLogRepository : HibernateDataRow<KongFuAttrLogEntity>
    {
        public KongFuAttrLogRepository()
            : base(SessionFactory.LogConnection)
        {
            
        }

        public int FreeCoursesToday(uint idUser)
        {
            try
            {
                using (var session = GetSession())
                {
                    return int.Parse(ExecutePureQuery($"SELECT COUNT(*) FROM `{TableName.KONGFU_LOG}` where user_id={idUser} AND DATE(`usage`) = CURDATE() AND free_course<>0")
                            .ToString());
                }
            }
            catch
            {
                return 0;
            }
        }

        public int PaidCoursesToday(uint idUser)
        {
            try
            {
                using (var session = GetSession())
                {
                    return int.Parse(ExecutePureQuery($"SELECT COUNT(*) FROM `{TableName.KONGFU_LOG}` where user_id={idUser} AND DATE(`usage`) = CURDATE() AND free_course=0")
                        .ToString());
                }
            }
            catch
            {
                return 0;
            }
        }
    }
}
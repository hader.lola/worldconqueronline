﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - Monarchy Config.cs
// Last Edit: 2019/11/28 17:02
// Created: 2019/11/28 16:25
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using FluentNHibernate.Mapping;
using FtwCore.Database.Entities;

#endregion

namespace FtwCore.Database.Mappings
{
    internal class MonarchyConfigMapping : ClassMap<MonarchyConfigEntity>
    {
        public MonarchyConfigMapping()
        {
            Table(TableName.MONARCHY_CONFIG);
            LazyLoad();
            Id(x => x.Identity, "id").GeneratedBy.Identity().Not.Nullable();
            Map(x => x.ConfigStr, "config_str").Not.Nullable();
            Map(x => x.Value, "value").Not.Nullable();
            Map(x => x.ValueStr, "value_str").Not.Nullable();
            Map(x => x.StartTime, "start_time").Not.Nullable();
            Map(x => x.EndTime, "end_time").Not.Nullable();
            Map(x => x.EmperorIdentity, "emperor_id").Not.Nullable();
        }
    }
}
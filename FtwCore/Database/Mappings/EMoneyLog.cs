﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - EMoneyLog.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using FluentNHibernate.Mapping;
using FtwCore.Database.Entities;

#endregion

namespace FtwCore.Database.Mappings
{
    internal class EMoneyLogMapping : ClassMap<EmoneyLogEntity>
    {
        public EMoneyLogMapping()
        {
            Table(TableName.EMONEY);
            LazyLoad();
            Id(x => x.Identity).GeneratedBy.Identity().Not.Nullable().Column("id");
            Map(x => x.TargetIdentity).Not.Nullable().Default("0").Column("id_target");
            Map(x => x.SourceIdentity).Not.Nullable().Default("0").Column("id_source");
            Map(x => x.SourceType).Not.Nullable().Default("0").Column("source_type");
            Map(x => x.OperationType).Not.Nullable().Default("0").Column("op_type");
            Map(x => x.Amount).Not.Nullable().Default("0").Column("amount");
            Map(x => x.MapIdentity).Not.Nullable().Default("0").Column("map_id");
            Map(x => x.MapX).Not.Nullable().Default("0").Column("pos_x");
            Map(x => x.MapY).Not.Nullable().Default("0").Column("pos_y");
            Map(x => x.Timestamp).Not.Nullable().Column("when");
            Map(x => x.SourceBalance).Not.Nullable().Default("0").Column("source_balance");
            Map(x => x.TargetBalance).Not.Nullable().Default("0").Column("target_balance");
        }
    }
}
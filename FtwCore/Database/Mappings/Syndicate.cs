﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - Syndicate.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using FluentNHibernate.Mapping;
using FtwCore.Database.Entities;

#endregion

namespace FtwCore.Database.Mappings
{
    public class SyndicateMapping : ClassMap<SyndicateEntity>
    {
        public SyndicateMapping()
        {
            Table(TableName.SYNDICATE);
            LazyLoad();
            Id(x => x.Identity).Column("id").GeneratedBy.Identity().Not.Nullable();
            Map(x => x.Name).Column("name").Not.Nullable();
            Map(x => x.Announce).Column("announce").Not.Nullable();
            Map(x => x.AnnounceDate).Column("announce_date").Not.Nullable();
            Map(x => x.LeaderIdentity).Column("leader_id").Not.Nullable();
            Map(x => x.LeaderName).Column("leader_name").Not.Nullable();
            Map(x => x.Money).Column("money").Not.Nullable();
            Map(x => x.EMoney).Column("emoney").Not.Nullable();
            Map(x => x.DelFlag).Column("del_flag").Not.Nullable();
            Map(x => x.Amount).Column("amount").Not.Nullable();
            Map(x => x.TotemPole).Column("totem_pole").Not.Nullable();
            Map(x => x.LastTotem).Column("last_totem").Not.Nullable();
            Map(x => x.ReqLevel).Column("req_lev").Not.Nullable();
            Map(x => x.ReqClass).Column("req_class").Not.Nullable();
            Map(x => x.ReqMetempsychosis).Column("req_metempsychosis").Not.Nullable();
            Map(x => x.CreationDate).Column("creation_date").Not.Nullable().Default("0");
            Map(x => x.MoneyPrize).Column("money_prize").Not.Nullable().Default("0");
            Map(x => x.EmoneyPrize).Column("emoney_prize").Not.Nullable().Default("0");
        }
    }
}
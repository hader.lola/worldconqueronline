﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - GameAction.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using FluentNHibernate.Mapping;
using FtwCore.Database.Entities;

#endregion

namespace FtwCore.Database.Mappings
{
    public class GameActionMapping : ClassMap<GameActionEntity>
    {
        public GameActionMapping()
        {
            Table(TableName.ACTION);
            LazyLoad();
            Id(x => x.Identity).GeneratedBy.Identity().Column("id");
            Map(x => x.IdNext).Column("id_next").Not.Nullable().Default("0");
            Map(x => x.IdNextfail).Column("id_nextfail").Not.Nullable().Default("0");
            Map(x => x.Type).Column("type").Not.Nullable().Default("0");
            Map(x => x.Data).Column("data").Not.Nullable().Default("0");
            Map(x => x.Param).Column("param").Not.Nullable().Default("");
        }
    }
}
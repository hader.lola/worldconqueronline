﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - KongFuAttr.cs
// Last Edit: 2019/12/14 17:57
// Created: 2019/12/14 17:50
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using FluentNHibernate.Mapping;
using FtwCore.Database.Entities;

#endregion

namespace FtwCore.Database.Mappings
{
    internal class KongFuAttrMapping : ClassMap<KongFuAttrEntity>
    {
        public KongFuAttrMapping()
        {
            Table(TableName.KONGFU_ATTR);
            LazyLoad();
            Id(x => x.Identity, "id").GeneratedBy.Identity();
            Map(x => x.UserIdentity, "user_id").Not.Nullable();
            Map(x => x.Stage, "stage").Not.Nullable();
            Map(x => x.Star, "star").Not.Nullable();
            Map(x => x.Attribute, "attr").Not.Nullable();
            Map(x => x.Quality, "quality").Not.Nullable();
            Map(x => x.LearnTime, "learn_time").Not.Nullable();
        }
    }
}
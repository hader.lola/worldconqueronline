﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - Syndicate Totem Enhance.cs
// Last Edit: 2020/01/17 00:16
// Created: 2020/01/17 00:15
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using FluentNHibernate.Mapping;
using FtwCore.Database.Entities;

#endregion

namespace FtwCore.Database.Mappings
{
    internal class SyndicateTotemEnhanceMapping : ClassMap<SyndicateTotemEnhanceEntity>
    {
        public SyndicateTotemEnhanceMapping()
        {
            Table(TableName.SYNDICATE_TOTEM_ENHANCE);
            LazyLoad();
            Id(x => x.Identity, "id").GeneratedBy.Identity();
            Map(x => x.UserIdentity, "user_id").Not.Nullable();
            Map(x => x.SyndicateIdentity, "syn_id").Not.Nullable();
            Map(x => x.Totem, "totem").Not.Nullable();
            Map(x => x.Enhancement, "enhance").Not.Nullable();
            Map(x => x.StartTime, "start_time").Not.Nullable();
            Map(x => x.EndTime, "end_time").Not.Nullable();
        }
    }
}
﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - SyndicateMember.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using FluentNHibernate.Mapping;
using FtwCore.Database.Entities;

#endregion

namespace FtwCore.Database.Mappings
{
    public class SyndicateMemberMapping : ClassMap<SyndicateMemberEntity>
    {
        public SyndicateMemberMapping()
        {
            Table(TableName.SYNDICATE_ATTR);
            LazyLoad();
            Id(x => x.Id).Column("id").Not.Nullable();
            Map(x => x.SynId).Column("syn_id").Not.Nullable();
            Map(x => x.Rank).Column("rank").Not.Nullable();
            Map(x => x.Proffer).Column("proffer").Not.Nullable();
            Map(x => x.ProfferTotal).Column("proffer_total").Not.Nullable();
            Map(x => x.Emoney).Column("emoney").Not.Nullable();
            Map(x => x.EmoneyTotal).Column("emoney_total").Not.Nullable();
            Map(x => x.Pk).Column("pk").Not.Nullable();
            Map(x => x.PkTotal).Column("pk_total").Not.Nullable();
            Map(x => x.Guide).Column("guide").Not.Nullable();
            Map(x => x.GuideTotal).Column("guide_total").Not.Nullable();
            Map(x => x.Exploit).Column("exploit").Not.Nullable();
            Map(x => x.Arsenal).Column("arsenal").Not.Nullable();
            Map(x => x.Expiration).Column("expiration").Not.Nullable();
            Map(x => x.JoinDate).Column("join_date").Not.Nullable();
        }
    }
}
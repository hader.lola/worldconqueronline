﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - Trade.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using FluentNHibernate.Mapping;
using FtwCore.Database.Entities;

#endregion

namespace FtwCore.Database.Mappings
{
    internal class TradeMapping : ClassMap<TradeEntity>
    {
        public TradeMapping()
        {
            Table(TableName.TRADE);
            LazyLoad();
            Id(x => x.Identity).GeneratedBy.Identity().Not.Nullable().Column("id");
            Map(x => x.UserIdentity).Not.Nullable().Default("0").Column("user_id");
            Map(x => x.TargetIdentity).Not.Nullable().Default("0").Column("target_id");
            Map(x => x.UserMoney).Not.Nullable().Default("0").Column("user_money");
            Map(x => x.TargetMoney).Not.Nullable().Default("0").Column("target_money");
            Map(x => x.UserEmoney).Not.Nullable().Default("0").Column("user_emoney");
            Map(x => x.TargetEmoney).Not.Nullable().Default("0").Column("target_emoney");
            Map(x => x.MapIdentity).Not.Nullable().Default("0").Column("map_id");
            Map(x => x.UserX).Not.Nullable().Default("0").Column("user_x");
            Map(x => x.UserY).Not.Nullable().Default("0").Column("user_y");
            Map(x => x.TargetX).Not.Nullable().Default("0").Column("target_x");
            Map(x => x.TargetY).Not.Nullable().Default("0").Column("target_y");
            Map(x => x.Timestamp).Not.Nullable().Column("timestamp");
            Map(x => x.UserIpAddress).Not.Nullable().Default("0.0.0.0").Column("user_ip_addr");
            Map(x => x.TargetIpAddress).Not.Nullable().Default("0.0.0.0").Column("target_ip_addr");
            Map(x => x.UserMacAddress).Not.Nullable().Default("").Column("user_mac_addr");
            Map(x => x.TargetMacAddress).Not.Nullable().Default("").Column("target_mac_addr");
        }
    }
}
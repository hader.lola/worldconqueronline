﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - Dynamic Ranking Records.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using FluentNHibernate.Mapping;
using FtwCore.Database.Entities;

#endregion

namespace FtwCore.Database.Mappings
{
    internal class DynamicRankingRecordsMapping : ClassMap<DynaRankRecordsEntity>
    {
        public DynamicRankingRecordsMapping()
        {
            Table(TableName.DYNA_RANK_REC);
            LazyLoad();
            Id(x => x.Identity).Column("id").GeneratedBy.Identity().Not.Nullable();
            Map(x => x.RankType).Column("rank_type").Not.Nullable();
            Map(x => x.Value).Column("value").Not.Nullable();
            Map(x => x.ObjectId).Column("obj_id").Not.Nullable();
            Map(x => x.ObjectName).Column("obj_name").Nullable();
            Map(x => x.UserIdentity).Column("user_id").Not.Nullable();
            Map(x => x.Username).Column("user_Name").Nullable();
        }
    }
}
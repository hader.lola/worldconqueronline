﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - General Dynamic Ranking.cs
// Last Edit: 2020/01/15 18:51
// Created: 2019/11/05 00:23
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using FluentNHibernate.Mapping;
using FtwCore.Database.Entities;

#endregion

namespace FtwCore.Database.Mappings
{
    internal class GeneralDynamicRankingMap : ClassMap<GeneralDynamicRankingEntity>
    {
        public GeneralDynamicRankingMap()
        {
            Table(TableName.DYNAMICRANKREC);
            LazyLoad();
            Id(x => x.Identity).GeneratedBy.Identity().Column("id");
            Map(x => x.RankType).Column("rank_type").Default("0").Not.Nullable();
            Map(x => x.ObjectIdentity).Column("object_id").Default("0").Not.Nullable();
            Map(x => x.ObjectName).Column("object_name").Default("UNDEFINED").Not.Nullable();
            Map(x => x.PlayerIdentity).Column("player_id").Default("0").Not.Nullable();
            Map(x => x.PlayerName).Column("player_name").Default("UNDEFINED").Not.Nullable();
            Map(x => x.Value1).Column("value1").Default("0").Not.Nullable();
            Map(x => x.Value2).Column("value2").Default("0").Not.Nullable();
            Map(x => x.Value3).Column("value3").Default("0").Not.Nullable();
            Map(x => x.Value4).Column("value4").Default("0").Not.Nullable();
            Map(x => x.Value5).Column("value5").Default("0").Not.Nullable();
            Map(x => x.Value6).Column("value6").Default("0").Not.Nullable();
            Map(x => x.Value7).Column("value7").Default("0").Not.Nullable();
            Map(x => x.Value8).Column("value8").Default("0").Not.Nullable();
        }
    }
}
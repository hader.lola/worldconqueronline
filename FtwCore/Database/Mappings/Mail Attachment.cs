﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - Mail Attachment.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using FluentNHibernate.Mapping;
using FtwCore.Database.Entities;

#endregion

namespace FtwCore.Database.Mappings
{
    internal class MailAttachmentMapping : ClassMap<MailAttachmentEntity>
    {
        public MailAttachmentMapping()
        {
            Table(TableName.MAIL_ATTACHMENT);
            LazyLoad();
            Id(x => x.Identity, "id").Not.Nullable().GeneratedBy.Identity();
            Map(x => x.MailIdentity, "mail_id").Not.Nullable().Default("0");
            Map(x => x.ItemIdentity, "item_id").Not.Nullable().Default("0");
            Map(x => x.Itemtype, "itemtype").Not.Nullable().Default("0");
            Map(x => x.ClaimTime, "claim_time").Nullable();
            Map(x => x.Flag, "flag").Not.Nullable().Default("0");
        }
    }
}
﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - Region.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using FluentNHibernate.Mapping;
using FtwCore.Database.Entities;

#endregion

namespace FtwCore.Database.Mappings
{
    public class RegionMapping : ClassMap<RegionEntity>
    {
        public RegionMapping()
        {
            Table(TableName.REGION);
            LazyLoad();
            Id(x => x.Identity).Column("id").GeneratedBy.Identity().Not.Nullable();
            Map(x => x.MapIdentity).Column("mapid").Not.Nullable();
            Map(x => x.Type).Column("type").Not.Nullable();
            Map(x => x.BoundX).Column("bound_x").Not.Nullable();
            Map(x => x.BoundY).Column("bound_y").Not.Nullable();
            Map(x => x.BoundCX).Column("bound_cx").Not.Nullable();
            Map(x => x.BoundCY).Column("bound_cy").Not.Nullable();
            Map(x => x.DataString).Column("datastr").Not.Nullable();
            Map(x => x.Data0).Column("data0").Not.Nullable();
            Map(x => x.Data1).Column("data1").Not.Nullable();
            Map(x => x.Data2).Column("data2").Not.Nullable();
            Map(x => x.Data3).Column("data3").Not.Nullable();
            Map(x => x.ActionId).Column("actionid").Not.Nullable();
        }
    }
}
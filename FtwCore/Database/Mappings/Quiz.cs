﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - Quiz.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/11/20 20:13
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using FluentNHibernate.Mapping;
using FtwCore.Database.Entities;

#endregion

namespace FtwCore.Database.Mappings
{
    internal class QuizMapping : ClassMap<QuizEntity>
    {
        public QuizMapping()
        {
            Table(TableName.QUIZ);
            LazyLoad();
            Id(x => x.Identity).GeneratedBy.Identity().Column("id");
            Map(x => x.Question).Column("question").Not.Nullable();
            Map(x => x.Answer0).Column("answer0").Not.Nullable();
            Map(x => x.Answer1).Column("answer1").Not.Nullable();
            Map(x => x.Answer2).Column("answer2").Not.Nullable();
            Map(x => x.Answer3).Column("answer3").Not.Nullable();
            Map(x => x.Correct).Column("correct").Not.Nullable().Default("1");
        }
    }
}
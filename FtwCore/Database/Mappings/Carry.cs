﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - Carry.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using FluentNHibernate.Mapping;
using FtwCore.Database.Entities;

#endregion

namespace FtwCore.Database.Mappings
{
    public class CarryMapping : ClassMap<CarryEntity>
    {
        public CarryMapping()
        {
            Table(TableName.CARRY);
            LazyLoad();
            Id(x => x.Identity).GeneratedBy.Increment().Not.Nullable().Column("id");
            Map(x => x.ItemIdentity).Default("0").Column("item_id").Not.Nullable();
            Map(x => x.Name).Default("").Column("name").Not.Nullable();
            Map(x => x.RecordMapId).Default("0").Column("recordmapid").Not.Nullable();
            Map(x => x.RecordMapX).Default("0").Column("record_x").Not.Nullable();
            Map(x => x.RecordMapY).Default("0").Column("record_y").Not.Nullable();
        }
    }
}
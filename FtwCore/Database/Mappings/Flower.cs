﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - Flower.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using FluentNHibernate.Mapping;
using FtwCore.Database.Entities;

#endregion

namespace FtwCore.Database.Mappings
{
    internal class Flower : ClassMap<FlowerEntity>
    {
        public Flower()
        {
            Table(TableName.FLOWER);
            LazyLoad();
            Id(x => x.PlayerIdentity).Column("player_id").Not.Nullable();
            Map(x => x.PlayerName).Column("player_name").Not.Nullable().Default("");
            Map(x => x.RedRoses).Column("flower_r").Not.Nullable().Default("0");
            Map(x => x.WhiteRoses).Column("flower_w").Not.Nullable().Default("0");
            Map(x => x.Orchids).Column("flower_lily").Not.Nullable().Default("0");
            Map(x => x.Tulips).Column("flower_tulip").Not.Nullable().Default("0");
        }
    }
}
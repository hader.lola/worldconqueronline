﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - General Dynamic Ranking.cs
// Last Edit: 2020/01/15 18:51
// Created: 2019/11/05 00:23
// ////////////////////////////////////////////////////////////////////////////////////

namespace FtwCore.Database.Entities
{
    public class GeneralDynamicRankingEntity
    {
        public virtual uint Identity { get; set; }
        public virtual uint RankType { get; set; }
        public virtual uint ObjectIdentity { get; set; }
        public virtual string ObjectName { get; set; }
        public virtual uint PlayerIdentity { get; set; }
        public virtual string PlayerName { get; set; }
        public virtual long Value1 { get; set; }
        public virtual long Value2 { get; set; }
        public virtual long Value3 { get; set; }
        public virtual long Value4 { get; set; }
        public virtual long Value5 { get; set; }
        public virtual long Value6 { get; set; }
        public virtual long Value7 { get; set; }
        public virtual long Value8 { get; set; }
    }
}
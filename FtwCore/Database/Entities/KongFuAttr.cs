﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - KongFuAttr.cs
// Last Edit: 2019/12/14 17:57
// Created: 2019/12/13 00:07
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System;

#endregion

namespace FtwCore.Database.Entities
{
    public class KongFuAttrEntity
    {
        public virtual uint Identity { get; protected set; }
        public virtual uint UserIdentity { get; set; }
        public virtual byte Stage { get; set; }
        public virtual byte Star { get; set; }
        public virtual byte Attribute { get; set; }
        public virtual byte Quality { get; set; }
        public virtual DateTime LearnTime { get; set; }
    }
}
﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - Npc.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

namespace FtwCore.Database.Entities
{
    public class NpcEntity
    {
        public virtual uint Id { get; set; }
        public virtual uint Ownerid { get; set; }
        public virtual uint Playerid { get; set; }
        public virtual string Name { get; set; }
        public virtual ushort Type { get; set; }
        public virtual ushort Lookface { get; set; }
        public virtual int Idxserver { get; set; }
        public virtual uint Mapid { get; set; }
        public virtual ushort Cellx { get; set; }
        public virtual ushort Celly { get; set; }
        public virtual uint Task0 { get; set; }
        public virtual uint Task1 { get; set; }
        public virtual uint Task2 { get; set; }
        public virtual uint Task3 { get; set; }
        public virtual uint Task4 { get; set; }
        public virtual uint Task5 { get; set; }
        public virtual uint Task6 { get; set; }
        public virtual uint Task7 { get; set; }
        public virtual int Data0 { get; set; }
        public virtual int Data1 { get; set; }
        public virtual int Data2 { get; set; }
        public virtual int Data3 { get; set; }
        public virtual string Datastr { get; set; }
        public virtual uint Linkid { get; set; }
        public virtual ushort Life { get; set; }
        public virtual ushort Maxlife { get; set; }
        public virtual uint Base { get; set; }
        public virtual ushort Sort { get; set; }
        public virtual uint Itemid { get; set; }
        public virtual ushort Defence { get; set; }
        public virtual ushort MagicDef { get; set; }
    }
}
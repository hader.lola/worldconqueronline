﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - DynamicMap.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

namespace FtwCore.Database.Entities
{
    public class DynamicMapEntity
    {
        public virtual uint Identity { get; set; }
        public virtual string Name { get; set; }
        public virtual string Description { get; set; }
        public virtual uint MapDoc { get; set; }
        public virtual uint Type { get; set; }
        public virtual uint OwnerId { get; set; }
        public virtual uint Mapgroup { get; set; }
        public virtual uint Idxserver { get; set; }
        public virtual uint Weather { get; set; }
        public virtual uint Bgmusic { get; set; }
        public virtual uint BgmusicShow { get; set; }
        public virtual uint Portal0X { get; set; }
        public virtual uint Portal0Y { get; set; }
        public virtual uint RebornMapid { get; set; }
        public virtual uint RebornPortal { get; set; }
        public virtual byte ResLev { get; set; }
        public virtual byte OwnerType { get; set; }
        public virtual uint LinkMap { get; set; }
        public virtual ushort LinkX { get; set; }
        public virtual ushort LinkY { get; set; }
        public virtual byte DelFlag { get; set; }
        public virtual uint Color { get; set; }
        public virtual string FileName { get; set; }
    }
}
﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - Trap Type.cs
// Last Edit: 2019/12/07 22:28
// Created: 2019/12/07 22:28
// ////////////////////////////////////////////////////////////////////////////////////

namespace FtwCore.Database.Entities
{
    public class TrapTypeEntity
    {
        public virtual uint Id { get; set; }
        public virtual uint Sort { get; set; }
        public virtual uint Look { get; set; }
        public virtual uint ActionId { get; set; }
        public virtual byte Level { get; set; }
        public virtual int AttackMax { get; set; }
        public virtual int AttackMin { get; set; }
        public virtual int Dexterity { get; set; }
        public virtual int AttackSpeed { get; set; }
        public virtual int ActiveTimes { get; set; }
        public virtual ushort MagicType { get; set; }
        public virtual int MagicHitrate { get; set; }
        public virtual int Size { get; set; }
        public virtual int AtkMode { get; set; }
    }
}
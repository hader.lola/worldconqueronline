﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - GameTask.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

namespace FtwCore.Database.Entities
{
    public class GameTaskEntity
    {
        public virtual uint Id { get; set; }
        public virtual uint IdNext { get; set; }
        public virtual uint IdNextfail { get; set; }
        public virtual string Itemname1 { get; set; }
        public virtual string Itemname2 { get; set; }
        public virtual uint Money { get; set; }
        public virtual uint Profession { get; set; }
        public virtual uint Sex { get; set; }
        public virtual int MinPk { get; set; }
        public virtual int MaxPk { get; set; }
        public virtual uint Team { get; set; }
        public virtual uint Metempsychosis { get; set; }
        public virtual ushort Query { get; set; }
        public virtual short Marriage { get; set; }
        public virtual ushort ClientActive { get; set; }
    }
}
﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - Syndicate.cs
// Last Edit: 2020/01/16 23:04
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

namespace FtwCore.Database.Entities
{
    public class SyndicateEntity
    {
        public virtual ushort Identity { get; set; }
        public virtual string Name { get; set; }
        public virtual string Announce { get; set; }
        public virtual uint AnnounceDate { get; set; }
        public virtual uint LeaderIdentity { get; set; }
        public virtual string LeaderName { get; set; }
        public virtual ulong Money { get; set; }
        public virtual uint EMoney { get; set; }

        /// <summary>
        ///     Tells the server if the guild has already been deleted.
        /// </summary>
        public virtual byte DelFlag { get; set; }

        public virtual uint Amount { get; set; }
        public virtual int TotemPole { get; set; }

        /// <summary>
        ///     The date of the last totem opened. (yyyymmdd)
        /// </summary>
        public virtual uint LastTotem { get; set; }

        public virtual byte ReqLevel { get; set; }
        public virtual byte ReqClass { get; set; }
        public virtual byte ReqMetempsychosis { get; set; }

        public virtual uint CreationDate { get; set; }

        public virtual long MoneyPrize { get; set; } // 20160202
        public virtual uint EmoneyPrize { get; set; } // 20160202
    }
}
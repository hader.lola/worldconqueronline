﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - Region.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

namespace FtwCore.Database.Entities
{
    public class RegionEntity
    {
        public virtual uint Identity { get; set; }
        public virtual uint MapIdentity { get; set; }
        public virtual uint Type { get; set; }
        public virtual uint BoundX { get; set; }
        public virtual uint BoundY { get; set; }
        public virtual uint BoundCX { get; set; }
        public virtual uint BoundCY { get; set; }
        public virtual string DataString { get; set; }
        public virtual uint Data0 { get; set; }
        public virtual uint Data1 { get; set; }
        public virtual uint Data2 { get; set; }
        public virtual uint Data3 { get; set; }
        public virtual uint ActionId { get; set; } // 2015-01-13
    }
}
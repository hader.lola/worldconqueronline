﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - Mail.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System;

#endregion

namespace FtwCore.Database.Entities
{
    public class MailEntity
    {
        public virtual uint Identity { get; set; }
        public virtual uint SenderIdentity { get; set; }
        public virtual string SenderName { get; set; }
        public virtual uint TargetIdentity { get; set; }
        public virtual string TargetName { get; set; }
        public virtual string Title { get; set; }
        public virtual string Content { get; set; }
        public virtual uint Money { get; set; }
        public virtual uint Emoney { get; set; }
        public virtual DateTime SendTime { get; set; }
        public virtual DateTime? ReadTime { get; set; }
        public virtual int Flag { get; set; }
        public virtual DateTime? ExpireTime { get; set; }
    }
}
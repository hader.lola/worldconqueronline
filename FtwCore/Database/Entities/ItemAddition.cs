﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - ItemAddition.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

namespace FtwCore.Database.Entities
{
    public class ItemAdditionEntity
    {
        /// <summary>
        ///     The unique identity of the item addition.
        /// </summary>
        public virtual uint Id { get; set; }

        /// <summary>
        ///     The gear type. Example: 130000 gets items from 130000-130009
        /// </summary>
        public virtual uint TypeId { get; set; }

        /// <summary>
        ///     The Plus level. 1 = (+1)
        /// </summary>
        public virtual byte Level { get; set; }

        public virtual ushort Life { get; set; }
        public virtual ushort AttackMax { get; set; }
        public virtual ushort AttackMin { get; set; }
        public virtual ushort Defense { get; set; }
        public virtual ushort MagicAtk { get; set; }
        public virtual ushort MagicDef { get; set; }
        public virtual ushort Dexterity { get; set; }
        public virtual ushort Dodge { get; set; }
    }
}
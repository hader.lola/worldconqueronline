﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - 1046 - MsgMailList.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

namespace FtwCore.Networking.Packets
{
    public sealed class MsgMailList : PacketStructure
    {
        public MsgMailList()
            : base(PacketType.MsgMailList, 28, 20)
        {
        }

        public MsgMailList(byte[] pBuffer)
            : base(pBuffer)
        {
        }

        public uint Count
        {
            get => ReadUInt(4);
            set
            {
                Resize((int) (value * 88 + 28));
                WriteHeader(Length - 8, PacketType.MsgMailList);
                WriteUInt(value, 4);
            }
        }

        public uint Page
        {
            get => ReadUInt(8);
            set => WriteUInt(value, 8);
        }

        public uint MaxPages
        {
            get => ReadUInt(14);
            set => WriteUInt(value, 14);
        }

        public void AppendEmail(uint idMail, string szSender, string szHeader, uint dwMoney, uint dwEmoney,
            uint dwTime, uint idItemtype, uint idAttachment)
        {
            int nOffset = (int) (16 + Count * 88);
            Count += 1;
            WriteUInt(idMail, nOffset);
            WriteString(szSender, 32, nOffset + 4);
            WriteString(szHeader, 32, nOffset + 36);
            WriteUInt(dwMoney, nOffset + 68);
            WriteUInt(dwEmoney, nOffset + 72);
            WriteUInt(dwTime, nOffset + 76);
            WriteUInt(idItemtype, nOffset + 80);
            WriteUInt(idAttachment, nOffset + 84);
        }
    }
}
﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - 26 - MsgUsrLogin.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using FtwCore.Common;

#endregion

namespace FtwCore.Networking.Packets
{
    public sealed class MsgUsrLogin : PacketStructure
    {
        private const uint _ADDICTION_U = 1000000000;

        public MsgUsrLogin(byte[] pBuffer)
            : base(pBuffer)
        {
        }

        public MsgUsrLogin(uint dwUserId, uint dwPacketHash)
            : base(PacketType.MsgLoginRequestUserSignin, 96, 96)
        {
            RequestTime = (uint) UnixTimestamp.Now();
            UserIdentity = dwUserId;
            PacketHash = dwPacketHash;

            for (int i = 36; i < 64; i++)
                WriteByte((byte) ThreadSafeRandom.RandGet(1, 255), i);
        }

        public uint RequestTime
        {
            get => ReadUInt(4);
            set => WriteUInt(value, 4);
        }

        public uint UserIdentity
        {
            get => ReadUInt(8) - _ADDICTION_U;
            set => WriteUInt(value + _ADDICTION_U, 8);
        }

        public uint PacketHash
        {
            get => ReadUInt(12);
            set => WriteUInt(value, 12);
        }

        public byte VipLevel
        {
            get => ReadByte(16);
            set => WriteByte(value, 16);
        }

        public byte Authority
        {
            get => ReadByte(17);
            set => WriteByte(value, 17);
        }

        public string IpAddress
        {
            get => ReadString(ReadByte(19), 20);
            set => WriteStringWithLength(value, 19);
        }

        public string MacAddress
        {
            get => ReadString(ReadByte(39), 40);
            set => WriteStringWithLength(value, 39);
        }

        public string LastIpAddress
        {
            get => ReadString(59, 60);
            set => WriteStringWithLength(value, 59);
        }
    }
}
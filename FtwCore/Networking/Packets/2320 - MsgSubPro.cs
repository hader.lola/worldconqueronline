﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - 2320 - MsgSubPro.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

namespace FtwCore.Networking.Packets
{
    public enum SubClassActions : ushort
    {
        Switch = 0,
        Activate = 1,
        RequestUplev = 2,
        MartialUplev = 3,
        Learn = 4,
        MartialPromoted = 5,
        Info = 6,
        ShowGui = 7,
        UpdateStudy = 8,
        LearnRemote = 9
    }

    public enum SubClasses : byte
    {
        None = 0,
        MartialArtist = 1,
        Warlock = 2,
        ChiMaster = 3,
        Sage = 4,
        Apothecary = 5,
        Performer = 6,
        Wrangler = 9
    }

    public sealed class MsgSubPro : PacketStructure
    {
        private byte m_amount;

        public MsgSubPro()
            : base(PacketType.MsgSubPro, 49, 41)
        {
        }

        public MsgSubPro(byte[] packet)
            : base(packet)
        {
        }

        public SubClassActions Action
        {
            get => (SubClassActions) ReadUShort(8);
            set => WriteUShort((ushort) value, 8);
        }

        public SubClasses Subclass
        {
            get => (SubClasses) ReadByte(10);
            set => WriteByte((byte) value, 10);
        }

        public ulong StudyPoints
        {
            get => ReadULong(10);
            set => WriteULong(value, 10);
        }

        public uint AwardedStudy
        {
            get => ReadUInt(18);
            set => WriteUInt(value, 18);
        }

        public byte Amount
        {
            get => ReadByte(26);
            set
            {
                m_amount = value;
                Resize(49 + value * 3);
                WriteHeader(Length - 8, PacketType.MsgSubPro);
                WriteByte(m_amount, 26);
            }
        }

        public void Append(SubClasses sClass, byte level, byte phase)
        {
            Amount += 1;
            var offset = (ushort) (30 + (Amount - 1) * 3);
            WriteByte((byte) sClass, offset++);
            WriteByte(level, offset++);
            WriteByte(phase, offset);
        }
    }
}
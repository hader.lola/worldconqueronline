﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - 1313 - MsgFamilyOccupy.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/11/24 18:48
// ////////////////////////////////////////////////////////////////////////////////////

namespace FtwCore.Networking.Packets
{
    public enum FamilyPromptType
    {
        RequestNpc = 6, // Npc Click Client -> Server -> Client
        AnnounceWarBegin = 7, // Call to war Server -> Client
        AnnounceWarAccept = 8 // Answer Ok to annouce Client -> Server
    }

    /// <summary>
    ///     This packet is called when the user clicks on the Clan War NPCs. It shows who owns that map and also have some
    ///     types with
    ///     invitations or requests.
    /// </summary>
    public sealed class MsgFamilyOccupy : PacketStructure
    {
        public MsgFamilyOccupy()
            : base(PacketType.MsgFamilyOccupy, 144, 136)
        {
        }

        public MsgFamilyOccupy(byte[] packet)
            : base(packet)
        {
        }

        public FamilyPromptType Type
        {
            get => (FamilyPromptType) ReadUInt(4);
            set => WriteUInt((uint) value, 4);
        }

        public uint Identity
        {
            get => ReadUInt(8);
            set => WriteUInt(value, 8);
        }

        public uint RequestNpc
        {
            get => ReadUInt(12);
            set => WriteUInt(value, 12);
        }

        public uint Type2
        {
            get => ReadUInt(16);
            set => WriteUInt(value, 16);
        }

        public string Winner
        {
            get => ReadString(16, 20);
            set => WriteString(value, 16, 20);
        }

        public string CityName
        {
            get => ReadString(16, 56);
            set => WriteString(value, 16, 56);
        }

        public uint OccupyDays
        {
            get => ReadUInt(96);
            set => WriteUInt(value, 96);
        }

        public uint DailyPrize
        {
            get => ReadUInt(100);
            set => WriteUInt(value, 100);
        }

        public uint WeeklyPrize
        {
            get => ReadUInt(104);
            set => WriteUInt(value, 104);
        }

        public uint GoldFee
        {
            get => ReadUInt(120);
            set => WriteUInt(value, 120);
        }
    }
}
﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - 1108 - MsgItemInfoEx.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using FtwCore.Common.Enums;

#endregion

namespace FtwCore.Networking.Packets
{
    public sealed class MsgItemInfoEx : PacketStructure
    {
        public MsgItemInfoEx()
            : base(PacketType.MsgItemInfoEx, 92, 84)
        {
        }

        // Offset 48 is the monster shit that is in chinese and isnt used
        // Offset 54 is locked
        // Offset 68 is the item remaining time
        // Offset 76 does give a new icon to the item???? o_O WriteUInt(410339, 76);
        public uint Identity
        {
            get => ReadUInt(4);
            set => WriteUInt(value, 4);
        } // 4

        public uint TargetIdentity
        {
            get => ReadUInt(8);
            set => WriteUInt(value, 8);
        } // 8

        public uint Price
        {
            get => ReadUInt(12);
            set => WriteUInt(value, 12);
        } // 12

        public uint Itemtype
        {
            get => ReadUInt(16);
            set => WriteUInt(value, 16);
        } // 16

        public ushort Durability
        {
            get => ReadUShort(20);
            set => WriteUShort(value, 20);
        } // 20

        public ushort MaximumDurability
        {
            get => ReadUShort(22);
            set => WriteUShort(value, 22);
        } // 22

        public ushort ViewType
        {
            get => ReadUShort(24);
            set => WriteUShort(value, 24);
        } // 24

        public ItemPosition Position
        {
            get => (ItemPosition) ReadUShort(26);
            set => WriteUShort((ushort) value, 26);
        } // 26

        public uint SocketProgress
        {
            get => ReadUInt(28);
            set => WriteUInt(value, 28);
        } // 28

        public SocketGem SocketOne
        {
            get => (SocketGem) ReadByte(32);
            set => WriteByte((byte) value, 32);
        } // 32

        public SocketGem SocketTwo
        {
            get => (SocketGem) ReadByte(33);
            set => WriteByte((byte) value, 33);
        } // 33

        public uint Unknown1
        {
            get => ReadUInt(34);
            set => WriteUInt(value, 34);
        } // 34

        public ushort Unknown2
        {
            get => ReadUShort(38);
            set => WriteUShort(value, 38);
        } // 38

        public byte Unknown3
        {
            get => ReadByte(40);
            set => WriteByte(value, 40);
        } // 40

        public byte Plus
        {
            get => ReadByte(41);
            set => WriteByte(value, 41);
        } // 41

        public byte Bless
        {
            get => ReadByte(42);
            set => WriteByte(value, 42);
        } // 42

        public bool Bound
        {
            get => ReadBoolean(43);
            set => WriteBoolean(value, 43);
        } // 43

        public byte Enchant
        {
            get => ReadByte(44);
            set => WriteByte(value, 44);
        } // 44

        public bool Suspicious
        {
            get => ReadBoolean(52);
            set => WriteBoolean(value, 52);
        } // 53

        public bool Locked
        {
            get => ReadBoolean(54);
            set => WriteBoolean(value, 54);
        }

        public ItemColor Color
        {
            get => (ItemColor) ReadByte(56);
            set => WriteByte((byte) value, 56);
        } // 56

        public uint Composition
        {
            get => ReadUInt(60);
            set => WriteUInt(value, 60);
        } // 60 

        public uint RemainingTime
        {
            get => ReadUInt(72);
            set => WriteUInt(value, 72);
        }

        public uint StackAmount
        {
            get => ReadUInt(76);
            set => WriteUInt(value, 76);
        }

        public uint Purification
        {
            get => ReadUInt(80);
            set => WriteUInt(value, 80);
        }
    }
}
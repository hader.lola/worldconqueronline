﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - 1016 - MsgWeather.cs
// Last Edit: 2019/12/07 23:26
// Created: 2019/12/07 23:26
// ////////////////////////////////////////////////////////////////////////////////////

namespace FtwCore.Networking.Packets
{
    public sealed class MsgWeather : PacketStructure
    {
        public MsgWeather()
            : base(PacketType.MsgWeather, 32, 24)
        {
        }

        public uint WeatherType
        {
            get => ReadUInt(8);
            set => WriteUInt(value, 8);
        }

        public uint Intensity
        {
            get => ReadUInt(12);
            set => WriteUInt(value, 12);
        }

        public uint Direction
        {
            get => ReadUInt(16);
            set => WriteUInt(value, 16);
        }

        public uint ColorArgb
        {
            get => ReadUInt(20);
            set => WriteUInt(value, 20);
        }
    }
}
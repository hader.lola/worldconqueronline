﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - 2244 - MsgTeamArenaYTop10List.cs
// Last Edit: 2020/01/17 16:59
// Created: 2020/01/17 16:50
// ////////////////////////////////////////////////////////////////////////////////////

namespace FtwCore.Networking.Packets
{
    public sealed class MsgTeamArenaYTop10List : PacketStructure
    {
        private int m_nCount;

        public MsgTeamArenaYTop10List(byte[] buffer)
            : base(buffer)
        {
        }

        public MsgTeamArenaYTop10List()
            : base(PacketType.MsgTeamArenaYTop10List, 0, 0)
        {
        }

        public uint Mode
        {
            get => ReadUInt(4);
            set => WriteUInt(value, 4);
        }

        public void Append(string name, uint lastrank, uint lookface, uint profession, uint level, uint lastPoints,
            uint lastWin, uint lastLoss)
        {
            int offset = 8 + m_nCount++ * 44;
            Resize(16 + m_nCount * 44);
            WriteHeader(Length - 8, PacketType.MsgTeamArenaYTop10List);

            WriteString(name, 16, offset);
            WriteUInt(lastrank, offset + 16);
            WriteUInt(lookface, offset + 20);
            WriteUInt(profession, offset + 24);
            WriteUInt(level, offset + 28);
            WriteUInt(lastPoints, offset + 32);
            WriteUInt(lastWin, offset + 36);
            WriteUInt(lastLoss, offset + 40);
        }
    }
}
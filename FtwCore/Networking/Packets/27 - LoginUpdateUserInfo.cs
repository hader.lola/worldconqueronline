﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - 27 - LoginUpdateUserInfo.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

namespace FtwCore.Networking.Packets
{
    public class MsgLoginUserMacAddr 
        : PacketStructure
    {
        public MsgLoginUserMacAddr(byte[] receivedPacket) 
            : base(receivedPacket)
        {
        }

        public MsgLoginUserMacAddr()
            : base(PacketType.MsgLoginUserMacAddr, 8, 8)
        {
        }

        public LoginUserUpdateType Request
        {
            get => (LoginUserUpdateType) ReadUShort(4);
            set => WriteUShort((ushort) value, 4);
        }

        public ushort Count
        {
            get => ReadUShort(6);
            set => WriteUShort(value, 6);
        }

        public void AddValue(long num)
        {
            int offset = 8 + Count * 8;
            Count++;
            Resize(8 + Count * 8);
            WriteHeader(Length, PacketType.MsgLoginUserMacAddr);
            WriteLong(num, offset);
        }

        public long[] GetValues()
        {
            long[] values = new long[Count];
            for (int i = 0; i < Count; i++)
            {
                values[i] = ReadLong(8 + i * 8);
            }
            return values;
        }
    }

    public enum LoginUserUpdateType : ushort
    {
        None,
        MacAddress
    }
}
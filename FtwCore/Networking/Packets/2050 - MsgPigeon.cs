﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - 2050 - MsgPigeon.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System.Collections.Generic;

#endregion

namespace FtwCore.Networking.Packets
{
    public sealed class MsgPigeon : PacketStructure
    {
        public const byte RELEASE_SOON = 1,
            MY_RELEASE = 2,
            BROADCAST_SEND = 3,
            URGENT_15_CPS = 4,
            URGENT_5_CPS = 5;

        public MsgPigeon()
            : base(PacketType.MsgPigeon, 24, 16)
        {
        }

        public MsgPigeon(byte[] msg)
            : base(msg)
        {
        }

        public byte Type
        {
            get => ReadByte(4);
            set => WriteByte(value, 4);
        }

        public uint DwParam
        {
            get => ReadUInt(8);
            set => WriteUInt(value, 8);
        }

        public byte StringCount
        {
            get => ReadByte(12);
            set
            {
                int newSize = 16 + value + m_totalStringLength + 8;
                Resize(newSize);
                WriteHeader(newSize - 8, PacketType.MsgPigeon);
                WriteByte(value, 12);
            }
        }

        private int m_totalStringLength;

        public void AddString(string text)
        {
            m_totalStringLength += text.Length;
            StringCount += 1;
            var offset = (ushort) (13 + (StringCount - 1) + (m_totalStringLength - text.Length));
            WriteStringWithLength(text, offset);
        }

        public List<string> ToList()
        {
            var temp = new List<string>();

            int offset = 13;
            for (int i = 0; i < StringCount; i++)
            {
                string szTemp = ReadString(ReadByte(offset++), offset);
                temp.Add(szTemp);
                offset += szTemp.Length;
            }

            return temp;
        }
    }
}
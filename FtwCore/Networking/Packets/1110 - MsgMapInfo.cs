﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - 1110 - MsgMapInfo.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

namespace FtwCore.Networking.Packets
{
    public sealed class MsgMapInfo : PacketStructure
    {
        public MsgMapInfo()
            : base(PacketType.MsgMapInfo, 28, 20)
        {
        }

        public MsgMapInfo(uint mapId, uint mapDoc, ulong flags)
            : base(PacketType.MsgMapInfo, 28, 20)
        {
            MapId = mapId;
            MapDoc = mapDoc;
            Flags = flags;
        }

        public uint MapId
        {
            get { return ReadUInt(4); }
            set { WriteUInt(value, 4); }
        }

        public uint MapDoc
        {
            get { return ReadUInt(8); }
            set { WriteUInt(value, 8); }
        }

        public ulong Flags
        {
            get { return ReadULong(12); }
            set { WriteULong(value, 12); }
        }
    }
}
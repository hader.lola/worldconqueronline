﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - 1047 - MsgMailNotify.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

namespace FtwCore.Networking.Packets
{
    public sealed class MsgMailNotify : PacketStructure
    {
        public MsgMailNotify()
            : base(PacketType.MsgMailNotify, 20, 12)
        {
        }

        public MsgMailNotify(byte[] pBuffer)
            : base(pBuffer)
        {
        }

        public MailNotification Notify
        {
            get => (MailNotification) ReadUShort(4);
            set => WriteUShort((ushort) value, 4);
        }

        
    }

    public enum MailNotification : ushort
    {
        DELETION_FAILED = 1,
        UNREAD_MAIL = 4
    }
}
﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - 1026 - MsgTeamMember.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

namespace FtwCore.Networking.Packets
{
    public sealed class MsgTeamMember : PacketStructure
    {
        public const byte ADD_MEMBER = 0, DROP_MEMBER = 1;

        public MsgTeamMember()
            : base(PacketType.MsgTeamMember, 44, 36)
        {
            WriteByte(1, 7);
        }

        public MsgTeamMember(byte[] packet)
            : base(packet)
        {
        }

        public byte Action
        {
            get => ReadByte(4);
            set => WriteByte(value, 4);
        }

        public ushort Count
        {
            get => ReadUShort(5);
            set
            {
                Resize(44 + Count * 28);
                WriteHeader(Length - 8, PacketType.MsgTeamMember);
                WriteUShort(value, 5);
            }
        }

        public string Name
        {
            get => ReadString(16, 8);
            set => WriteString(value, value.Length, 8);
        }

        public uint Entity
        {
            get => ReadUInt(24);
            set => WriteUInt(value, 24);
        }

        public uint Mesh
        {
            get => ReadUInt(28);
            set => WriteUInt(value, 28);
        }

        public ushort MaxLife
        {
            get => ReadUShort(32);
            set => WriteUShort(value, 32);
        }

        public ushort Life
        {
            get => ReadUShort(34);
            set => WriteUShort(value, 34);
        }

        public void AppendMember(string name, uint id, uint mesh, ushort maxLife, ushort life)
        {
            int offset = 8 + Count * 28;
            Count += 1;
            WriteString(name, 16, offset);
            WriteUInt(id, offset + 16);
            WriteUInt(mesh, offset + 20);
            WriteUShort(maxLife, offset + 24);
            WriteUShort(life, offset + 26);
        }
    }
}
﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - Packet Handler Attribute.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System;

#endregion

namespace FtwCore.Networking.Packets
{
    /// <summary>
    /// This attribute class provides the server with an associating declarative for packet handlers. Any packet
    /// handling method declared using this attribute will be added to the packet processor (the red-black tree
    /// used for storing packet handlers by packet identity).
    /// </summary>
    public sealed class PacketHandlerType : Attribute, IPacketAttribute
    {
        /// <summary>
        /// This attribute class provides the server with an associating declarative for packet handlers. Any packet
        /// handling method declared using this attribute will be added to the packet processor (the red-black tree
        /// used for storing packet handlers by packet identity).
        /// </summary>
        /// <param name="packetType">The type of packet the handler will process.</param>
        public PacketHandlerType(PacketType packetType)
        {
            Type = packetType;
        }

        public IComparable Type { get; set; }
    }

    /// <summary> This interface defines a packet attribute class for the packet processor class. </summary>
    public interface IPacketAttribute
    {
        IComparable Type { get; set; }
    }
}
﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - 1105 - MsgMagicEffect.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

namespace FtwCore.Networking.Packets
{
    public sealed class MsgMagicEffect : PacketStructure
    {
        public MsgMagicEffect()
            : base(PacketType.MsgMagicEffect, 68, 60)
        {
        }

        public MsgMagicEffect(byte[] packet)
            : base(packet)
        {
        }

        /// <summary>
        /// The entity that is using the magic.
        /// </summary>
        public uint Identity
        {
            get => ReadUInt(4);
            set => WriteUInt(value, 4);
        }

        public ushort CellX
        {
            get => ReadUShort(8);
            set => WriteUShort(value, 8);
        }

        public ushort CellY
        {
            get => ReadUShort(10);
            set => WriteUShort(value, 10);
        }

        public ushort SkillIdentity
        {
            get => ReadUShort(12);
            set => WriteUShort(value, 12);
        }

        public ushort SkillLevel
        {
            get => ReadUShort(14);
            set => WriteUShort(value, 14);
        }

        public ushort TargetCount
        {
            get => ReadUShort(17);
            set
            {
                Resize((int) (60 + value * 32 + 8));
                WriteHeader(Length - 8, PacketType.MsgMagicEffect);
                WriteUShort(value, 17);
            }
        }

        public uint Target
        {
            get => ReadUInt(16);
            set => WriteUInt(value, 16);
        }
        
        public void AppendTarget(uint target, uint damage, bool hit, uint actType, uint actValue)
        {
            TargetCount += 1;
            var offset = (ushort) (20 + (TargetCount - 1) * 32);
            WriteUInt(target, offset);
            WriteUInt(damage, offset + 4);
            WriteBoolean(hit, offset + 8);
            WriteUInt(actType, offset + 12);
            WriteUInt(actValue, offset + 16);
        }
    }
}
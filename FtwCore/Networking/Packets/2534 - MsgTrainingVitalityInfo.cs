﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - 2534 - MsgTrainingVitalityInfo.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

namespace FtwCore.Networking.Packets
{
    public sealed class MsgTrainingVitalityInfo : PacketStructure
    {
        public MsgTrainingVitalityInfo()
            : base(PacketType.MsgTrainingVitalityInfo, 44, 36)
        {
        }

        public MsgTrainingVitalityInfo(byte[] buffer)
            : base(buffer)
        {
        }

        public ushort Type
        {
            get => ReadUShort(4);
            set => WriteUShort(value, 4);
        }

        public uint UserIdentity
        {
            get => ReadUInt(6);
            set => WriteUInt(value, 6);
        }

        public uint ChiPoints
        {
            get => ReadUInt(10);
            set => WriteUInt(value, 10);
        }

        public uint Unknown
        {
            get => ReadUInt(14);
            set => WriteUInt(value, 14);
        }

        public uint Amount
        {
            get => ReadUInt(18);
            set
            {
                WriteUInt(value, 18);
                Resize((int) (44 + value * 17));
                WriteHeader(Length - 8, PacketType.MsgTrainingVitalityInfo);
            }
        }

        public void Append(byte type, uint power1, uint power2, uint power3, uint power4)
        {
            int offset = (int) (22 + Amount * 17);
            Amount += 1;
            WriteByte(type, offset);
            WriteUInt(power1, offset + 1);
            WriteUInt(power2, offset + 5);
            WriteUInt(power3, offset + 9);
            WriteUInt(power4, offset + 13);
        }
    }
}
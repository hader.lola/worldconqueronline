﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - 1001 - MsgRegister.cs
// Last Edit: 2020/01/13 16:05
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

namespace FtwCore.Networking.Packets
{
    /// <summary>
    ///     Packet Type: 1001. This packet is sent to the server after the player has finished customizing and naming a
    ///     new character from the character creation window. The packet contains the player's selected gender, profession,
    ///     and name, and the player's mac address.
    /// </summary>
    public sealed class MsgRegister : PacketStructure
    {
        /// <summary>
        ///     Packet Type: 1001. This packet is sent to the server after the player has finished customizing and naming a
        ///     new character from the character creation window. The packet contains the player's selected gender, profession,
        ///     and name, and the player's mac address.
        /// </summary>
        /// <param name="array">The received packet.</param>
        public MsgRegister(byte[] array)
            : base(array)
        {
        }

        /// <summary>
        ///     Offset 4 - True if the creation window is being closed or the player clicked on the back button to return
        ///     to the main login screen.
        /// </summary>
        public bool CancelRequest => ReadBoolean(4);

        /// <summary>
        ///     Offset 20 - The name of the character being created. It must be checked by the database and server
        ///     before being used on the created character.
        /// </summary>
        public string Name => ReadString(15, 24);

        /// <summary>
        ///     Offset 52 - The type of body for the character being created. Must be checked by the server since a
        ///     bot can easily change this value to any type of entity supported by the client.
        /// </summary>
        public ushort Body => ReadUShort(72);

        /// <summary>
        ///     Offset 54 - The profession of the new character being created. It must be checked by the server since a
        ///     bot can easily change this value to any profession supported by the client (but not supported by the
        ///     server).
        /// </summary>
        public ushort Profession => ReadUShort(74);

        /// <summary>
        ///     Offset 60 - The mac address of the client connected to the server. Used to track special server offers
        ///     (one offer per person - not per account).
        /// </summary>
        public string MacAddress => ReadString(12, 80);
    }
}
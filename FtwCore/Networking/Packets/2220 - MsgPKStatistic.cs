﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - 2220 - MsgPKStatistic.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/11/20 19:38
// ////////////////////////////////////////////////////////////////////////////////////

namespace FtwCore.Networking.Packets
{
    public sealed class MsgPkStatistic : PacketStructure
    {
        public MsgPkStatistic()
            : base(PacketType.MsgPkStatistic, 60, 52)
        {
        }

        public MsgPkStatistic(byte[] pBuffer)
            : base(pBuffer)
        {
        }

        public uint Subtype // prolly a type 
        {
            get => ReadUInt(8);
            set => WriteUInt(value, 8);
        }

        public uint Values
        {
            get => ReadUInt(12);
            set => WriteUInt(value, 12);
        }

        public uint MaxValues
        {
            get => ReadUInt(16);
            set => WriteUInt(value, 16);
        }

        public void AddTarget(string szName, uint idMap, uint lastDieTime, uint dwTimes,
            ushort usLevel,
            uint dwBattlePower)
        {
            int nOffset = (int) (20 + MaxValues * 32);
            MaxValues += 1;
            Resize((int) (40 + MaxValues * 32));
            WriteHeader(Length - 8, PacketType.MsgPkStatistic);
            WriteString(szName, 16, nOffset);
            WriteUInt(dwTimes, nOffset + 16);
            WriteUInt(lastDieTime, nOffset + 20);
            WriteUInt(dwBattlePower, nOffset + 24);
            WriteUInt(idMap, nOffset + 28);
        }
    }
}
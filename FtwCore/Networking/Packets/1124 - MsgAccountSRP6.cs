﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - 1124 - MsgAccountSRP6.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

namespace FtwCore.Networking.Packets
{
    public sealed unsafe class MsgAccountSRP6 : PacketStructure
    {
        public MsgAccountSRP6(byte[] pBuffer)
            : base(pBuffer)
        {
        }

        public MsgAccountSRP6(byte[] packet, int seed)
            : base(packet)
        {
        }

        public string Username
        {
            get { return ReadString(16, 8); }
            set { WriteString(value, 16, 8); }
        }

        public string ServerName
        {
            get { return ReadString(16, 136); }
            set { WriteString(value, 16, 136); }
        }

        public ushort AccountType
        {
            get { return ReadUShort(226); }
            set { WriteUShort(value, 226); }
        }

        public ulong Salt
        {
            get { return ReadULong(228); }
            set { WriteULong(value, 228); }
        }

        public string Password
        {
            private set { WriteString(value, 16, 72); }
            get { return ReadString(16, 72); }
        }

        public string Server
        {
            get { return ReadString(16, 136); }
        }
    }
}
﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - 2036 - MsgDataArray.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System.Collections.Generic;

#endregion

namespace FtwCore.Networking.Packets
{
    public enum DataArrayMode : uint
    {
        ComposeItem = 0,
        Unknown = 1,
        ComposeSteedOriginal = 2,
        ComposeSteedNew = 3,
        QuickCompose = 4,
        QuickComposeMount = 5,
        UpgradeItemLevel = 6,
        UpgradeItemQuality = 7
    }

    public sealed class MsgDataArray : PacketStructure
    {
        public MsgDataArray()
            : base(PacketType.MsgDataArray, 24, 16)
        {
        }

        public MsgDataArray(byte[] packet)
            : base(packet)
        {
        }

        public DataArrayMode Mode
        {
            get => (DataArrayMode) ReadByte(4);
            set => WriteByte((byte) value, 4);
        }

        public byte MinorCount
        {
            get => (byte) (ReadByte(5) - 1);
            set => WriteByte(value, 5);
        }

        public uint MainIdentity
        {
            get => ReadUInt(8);
            set => WriteUInt(value, 8);
        }

        public uint MinorIdentity
        {
            get => ReadUInt(12);
            set => WriteUInt(value, 12);
        }

        public List<uint> Minors
        {
            get
            {
                List<uint> ret = new List<uint>();
                for (int i = 0; i < MinorCount; i++)
                    ret.Add(ReadUInt(12 + i * 4));
                return ret;
            }
        }
    }
}
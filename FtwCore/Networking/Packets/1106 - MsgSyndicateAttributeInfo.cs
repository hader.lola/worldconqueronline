﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - 1106 - MsgSyndicateAttributeInfo.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using FtwCore.Common.Enums;

#endregion

namespace FtwCore.Networking.Packets
{
    public sealed class MsgSyndicateAttributeInfo : PacketStructure
    {
        public MsgSyndicateAttributeInfo()
            : base(PacketType.MsgSyndicateAttributeInfo, 100, 92)
        {
        }

        public MsgSyndicateAttributeInfo(byte[] packet)
            : base(packet)
        {
        }

        public uint SyndicateIdentity
        {
            get => ReadUInt(4);
            set => WriteUInt(value, 4);
        }

        public uint Proffer
        {
            get => ReadUInt(8);
            set => WriteUInt(value, 8);
        }

        public ulong MoneyFund
        {
            get => ReadULong(12);
            set => WriteULong(value, 12);
        }

        public uint EMoneyFund
        {
            get => ReadUInt(20);
            set => WriteUInt(value, 20);
        }

        public uint MemberAmount
        {
            get => ReadUInt(24);
            set => WriteUInt(value, 24);
        }

        public SyndicateRank Position
        {
            get => (SyndicateRank) ReadUShort(28);
            set => WriteUShort((ushort) value, 28);
        }

        public string LeaderName
        {
            get => ReadString(16, 32);
            set => WriteString(value, 16, 32);
        }

        public uint RequiredLevel
        {
            get => ReadUInt(48);
            set => WriteUInt(value, 48);
        }

        public uint RequiredMetempsychosis
        {
            get => ReadUInt(52);
            set => WriteUInt(value, 52);
        }

        public uint RequiredProfession
        {
            get => ReadUInt(56);
            set => WriteUInt(value, 56);
        }

        public byte SyndicateLevel
        {
            get => ReadByte(60);
            set => WriteByte(value, 60);
        }

        public uint PositionExpire
        {
            get => ReadUInt(63);
            set => WriteUInt(value, 63);
        }

        /// <summary>
        /// yyyymmdd
        /// </summary>
        public uint EnrollmentDate
        {
            get => ReadUInt(67);
            set => WriteUInt(value, 67);
        }
    }
}
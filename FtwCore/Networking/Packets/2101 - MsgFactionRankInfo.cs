﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - 2101 - MsgFactionRankInfo.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using FtwCore.Common.Enums;

#endregion

namespace FtwCore.Networking.Packets
{
    public sealed class MsgFactionRankInfo : PacketStructure
    {
        /// <summary>
        /// This packet fills the ranking in the Donation board of the guild. There is a subtype for each kind of ranking.
        /// </summary>
        public MsgFactionRankInfo()
            : base(PacketType.MsgFactionRankInfo, 24, 16)
        {
        }

        public MsgFactionRankInfo(byte[] pMsg)
            : base(pMsg)
        {
        }

        public ushort Subtype
        {
            get => ReadUShort(4);
            set => WriteUShort(value, 4);
        }

        public ushort Count
        {
            get => ReadUShort(6);
            set => WriteUShort(value, 6);
        }

        public uint MaxCount
        {
            get => ReadUInt(8);
            set => WriteUInt(value, 8);
        }

        public void AddMember(uint idUser, SyndicateRank pos, uint dwRank, int dwSilver, uint dwEmoney, int dwPk,
            uint dwGuide, uint dwArsenal,
            uint dwRose, uint dwWhite, uint dwOrchid, uint dwTulip, uint dwTotal, string szName)
        {
            int offset = 16 + Count * 68;
            Count++;
            Resize(32 + Count * 68);
            WriteHeader(Length - 8, PacketType.MsgFactionRankInfo);
            WriteUInt((uint) pos, offset);
            WriteUShort((ushort) dwRank, offset + 4);
            WriteInt(dwSilver, offset + 8);
            WriteUInt(dwEmoney, offset + 12);
            WriteInt(dwPk, offset + 20); // pk
            WriteUInt(dwGuide, offset + 16); // Guide
            //WriteUInt(0, offset + 24);
            WriteUInt(dwArsenal, offset + 24);
            WriteUInt(dwRose, offset + 28);
            WriteUInt(dwWhite, offset + 32);
            WriteUInt(dwOrchid, offset + 36);
            WriteUInt(dwTulip, offset + 40);
            WriteUInt(dwTotal, offset + 44);
            WriteString(szName, 16, offset + 48);
        }
    }
}
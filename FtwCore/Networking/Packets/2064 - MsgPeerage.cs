﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - 2064 - MsgPeerage.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using FtwCore.Common.Enums;

#endregion

namespace FtwCore.Networking.Packets
{
    public sealed class MsgPeerage : PacketStructure
    {
        private int m_dataAmount;
        private int DATA_OFFSET = 120;

        public MsgPeerage(byte[] buffer)
            : base(buffer)
        {
        }

        public MsgPeerage()
            : base(PacketType.MsgPeerage, 132, 124)
        {
        }

        public MsgPeerage(int size)
            : base(PacketType.MsgPeerage, size + 8, size)
        {
        }

        public NobilityAction Action
        {
            get => (NobilityAction) ReadUInt(4);
            set => WriteUInt((uint) value, 4);
        }

        /// <summary>
        /// ushort offset 8
        /// </summary>
        public ushort Data
        {
            get => ReadUShort(8);
            set => WriteUShort(value, 8);
        }

        /// <summary>
        /// long offset 8
        /// </summary>
        public long DataLong
        {
            get => ReadLong(8);
            set => WriteLong(value, 8);
        }

        /// <summary>
        /// ushort offset 10
        /// </summary>
        public ushort DataShort
        {
            get => ReadUShort(10);
            set => WriteUShort(value, 10);
        }

        public uint DataLow
        {
            get => ReadUInt(8);
            set => WriteUInt(value, 8);
        }

        public uint DataHigh
        {
            get => ReadUInt(12);
            set => WriteUInt(value, 12);
        }

        public ushort DataHighLow
        {
            get => ReadUShort(12);
            set => WriteUShort(value, 12);
        }

        public ushort DataHighHigh
        {
            get => ReadUShort(14);
            set => WriteUShort(value, 14);
        }

        /// <summary>
        /// Uint offset 16
        /// </summary>
        public uint Data2
        {
            get => ReadUInt(16);
            set => WriteUInt(value, 16);
        }

        public uint Data3
        {
            get => ReadUInt(20);
            set => WriteUInt(value, 20);
        }

        public uint Data4
        {
            get => ReadUInt(24);
            set => WriteUInt(value, 24);
        }

        public int DonationType
        {
            get => ReadInt(104);
            set => WriteInt(value, 104);
        }

        private int DATA_AMOUNT
        {
            get => m_dataAmount;
            set
            {
                m_dataAmount = value;
                Resize(124 + value * 48 + 8);
                WriteHeader(Length - 8, PacketType.MsgPeerage);
                WriteByte((byte) value, 12);
            }
        }

        public void WriteNobilityData(uint userId, uint mesh, string name, long donation,
            NobilityLevel rankType, int ranking)
        {
            DATA_AMOUNT += 1;
            int offset = DATA_OFFSET + (DATA_AMOUNT - 1) * 48;
            WriteUInt(userId, offset);
            WriteUInt(mesh, offset + 4);
            WriteUInt(mesh, offset + 8);
            WriteString(name, 16, offset + 12);
            WriteUInt(0, offset + 28);
            WriteLong(donation, offset + 32);
            WriteUInt((uint) rankType, offset + 40);
            WriteInt(ranking, offset + 44);
        }

        public void AddMinimum(byte rank, ulong money)
        {
            int offset = 8 + m_dataAmount * 16;
            m_dataAmount += 1;
            WriteULong(money, offset);
            WriteUInt(uint.MaxValue, offset + 8);
            WriteUInt(rank, offset + 12);
        }
    }
}
﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - 1025 - MsgWeaponSkill.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

namespace FtwCore.Networking.Packets
{
    public sealed class MsgWeaponSkill : PacketStructure
    {
        public const uint MAX_PROFICIENCY_LEVEL = 20;

        public static readonly uint[] EXP_PER_LEVEL = new uint[21]
        {
            0,
            1200,
            68000,
            250000,
            640000,
            1600000,
            4000000,
            10000000,
            22000000,
            40000000,
            90000000,
            95000000,
            142500000,
            213750000,
            320625000,
            480937500,
            721406250,
            1082109375,
            1623164063,
            2100000000,
            0
        };

        /// <summary>
        /// This method will create the empty packet.
        /// </summary>
        public MsgWeaponSkill()
            : base(PacketType.MsgWeaponSkill, 28, 20)
        {
        }

        /// <summary>
        /// This method will create the packet and set each value on his right place.
        /// </summary>
        /// <param name="type">The weapon subtype. (Id/1000)</param>
        /// <param name="level">The weapon level.</param>
        /// <param name="experience">The proficiency experience.</param>
        public MsgWeaponSkill(uint type, uint level, uint experience)
            : base(PacketType.MsgWeaponSkill, 28, 20)
        {
            Type = type;
            Level = level;
            Experience = experience;
            LevelExperience = 100; // >(o_O)< ué
        }

        public MsgWeaponSkill(byte[] packet)
            : base(packet)
        {
            // We never know xD hate editing dlls just to add minor things
        }

        public uint Type
        {
            get => ReadUInt(4);
            set => WriteUInt(value, 4);
        }

        public uint Level
        {
            get => ReadUInt(8);
            set => WriteUInt(value, 8);
        }

        public uint Experience
        {
            get => ReadUInt(12);
            set => WriteUInt(value, 12);
        }

        public uint LevelExperience
        {
            get => Level > MAX_PROFICIENCY_LEVEL ? 0 : EXP_PER_LEVEL[(int) Level];
            set => WriteUInt(Level > MAX_PROFICIENCY_LEVEL ? 0 : EXP_PER_LEVEL[(int) Level], 16);
        }
    }
}
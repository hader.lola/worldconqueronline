﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - 1107 - MsgSyndicate.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System.Collections.Generic;
using System.Linq;
using FtwCore.Common.Enums;

#endregion

namespace FtwCore.Networking.Packets
{
    public sealed class MsgSyndicate : PacketStructure
    {
        public List<string> Positions = new List<string>();

        public MsgSyndicate()
            : base(PacketType.MsgSyndicate, 36, 28)
        {
        }

        public MsgSyndicate(string name)
            : base(PacketType.MsgSyndicate, 52, 44)
        {
            Name = name;
        }

        public MsgSyndicate(byte[] packet)
            : base(packet)
        {
        }

        public SyndicateRequest Action
        {
            get => (SyndicateRequest) ReadUInt(4);
            set => WriteUInt((uint) value, 4);
        }

        public uint Param
        {
            get => ReadUInt(8);
            set => WriteUInt(value, 8);
        }

        public uint RequiredLevel
        {
            get => ReadUInt(12);
            set => WriteUInt(value, 12);
        }

        public uint RequiredMetempsychosis
        {
            get => ReadUInt(16);
            set => WriteUInt(value, 16);
        }

        public uint RequiredProfession
        {
            get => ReadUInt(20);
            set => WriteUInt(value, 20);
        }

        public byte DwInfo
        {
            get => ReadByte(24);
            set => WriteByte(value, 24);
        }

        public string Name
        {
            get => ReadString(ReadByte(25), 26);
            set
            {
                Resize(52);
                WriteHeader(Length - 8, PacketType.MsgSyndicate);
                WriteStringWithLength(value, value.Length);
            }
        }

        /// <summary>
        /// This method will fill the buffer with all positions listed on the dictionary.
        /// </summary>
        public void SetList()
        {
            int len = Positions.Sum(pos => pos.Length);
            Resize(28 + 16 + 8 + len);
            WriteHeader(Length - 8, PacketType.MsgSyndicate);
            DwInfo = (byte) Positions.Count;
            ushort start = 25;
            foreach (var Pos in Positions)
            {
                WriteStringWithLength(Pos, start);
                start = (ushort) (start + Pos.Length + 1);
            }
        }
    }
}
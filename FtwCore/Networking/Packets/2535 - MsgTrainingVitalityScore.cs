﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - 2535 - MsgTrainingVitalityScore.cs
// Last Edit: 2020/01/09 19:35
// Created: 2020/01/09 19:35
// ////////////////////////////////////////////////////////////////////////////////////

using FtwCore.Common.Enums;

namespace FtwCore.Networking.Packets
{
    public sealed class MsgTrainingVitalityScore : PacketStructure
    {
        public MsgTrainingVitalityScore()
            : base(PacketType.MsgTrainingVitalityScore, 33, 25)
        {
        }

        public MsgTrainingVitalityScore(byte[] buffer)
            : base(buffer)
        {
        }

        public ChiPowerType Type
        {
            get => (ChiPowerType) ReadByte(4);
            set => WriteByte((byte) value, 4);
        }

        public uint Power
        {
            get => ReadUInt(5);
            set => WriteUInt(value, 5);
        }

        public string Name
        {
            get => ReadString(16, 9);
            set => WriteString(value, 16, 9);
        }
    }
}
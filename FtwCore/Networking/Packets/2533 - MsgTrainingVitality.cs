﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - 2533 - MsgTrainingVitality.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using FtwCore.Common.Enums;

#endregion

namespace FtwCore.Networking.Packets
{
    public sealed class MsgTrainingVitality : PacketStructure
    {
        public MsgTrainingVitality()
            : base(PacketType.MsgTrainingVitality, 23, 15)
        {
        }

        public MsgTrainingVitality(byte[] buffer)
            : base(buffer)
        {
        }

        public uint UserIdentity
        {
            get => ReadUInt(4);
            set => WriteUInt(value, 4);
        }

        public ChiRequestType Type
        {
            get => (ChiRequestType) ReadUShort(8);
            set => WriteUShort((ushort) value, 8);
        }

        public byte Mode
        {
            get => ReadByte(10);
            set => WriteByte(value, 10);
        }

        public uint Param
        {
            get => ReadUInt(11);
            set => WriteUInt(value, 11);
        }
    }
}
﻿using System;

namespace FtwCore.Networking.Packets
{
    public sealed class MsgOwnKongfuImproveSummaryInfo : PacketStructure
    {
        public MsgOwnKongfuImproveSummaryInfo()
            : base(PacketType.MsgOwnKongfuImproveSummaryInfo, 58, 50)
        {
        }

        public MsgOwnKongfuImproveSummaryInfo(byte[] buffer)
            : base(buffer)
        {
        }

        public string Name
        {
            get => ReadString(16, 4);
            set => WriteString(value, 16, 4);
        }

        public byte Stage
        {
            get => ReadByte(20);
            set => WriteByte(value, 20);
        }

        public byte Talent
        {
            get => ReadByte(21);
            set => WriteByte(value, 21);
        }

        public uint Timer
        {
            get => ReadUInt(22);
            set => WriteUInt(value, 22);
        }

        public ulong Points
        {
            get => ReadULong(27);
            set => WriteULong(value, 27);
        }

        public uint FreeTalentToday
        {
            get => ReadUInt(35);
            set => WriteULong(value, 35);
        }

        public byte FreeTalentUsed
        {
            get => ReadByte(43);
            set => WriteByte(value, 43);
        }

        public byte BoughtTimes
        {
            get => ReadByte(44);
            set => WriteByte(value, 44);
        }

        private int m_nCount = 0;

        public void Append(ushort id)
        {
            if (m_nCount % 9 == 0)
            {
                Resize(58 + Math.Max(1, (m_nCount/9) + 1) * 18);
                WriteHeader(Length - 8, PacketType.MsgOwnKongfuImproveSummaryInfo);
            }
            m_nCount += 1;
            WriteUShort(id, 48 + (m_nCount-1) * 2);
        }
    }
}
﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - 1024 - MsgAllot.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

namespace FtwCore.Networking.Packets
{
    public sealed class MsgAllot : PacketStructure
    {
        public MsgAllot(byte[] packet)
            : base(packet)
        {
        }

        public MsgAllot()
            : base(PacketType.MsgAllot, 36, 28)
        {

        }

        public uint Identity
        {
            get => ReadUInt(4);
            set => WriteUInt(value, 4);
        }

        public uint Strength
        {
            get => ReadUInt(12);
            set => WriteUInt(value, 12);
        }

        public uint Agility
        {
            get => ReadUInt(16);
            set => WriteUInt(value, 16);
        }

        public uint Vitality
        {
            get => ReadUInt(20);
            set => WriteUInt(value, 20);
        }

        public uint Spirit
        {
            get => ReadUInt(24);
            set => WriteUInt(value, 24);
        }
    }
}
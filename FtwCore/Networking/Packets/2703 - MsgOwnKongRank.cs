﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - 2703 - MsgOwnKongRank.cs
// Last Edit: 2020/01/12 17:03
// Created: 2019/12/11 22:57
// ////////////////////////////////////////////////////////////////////////////////////

namespace FtwCore.Networking.Packets
{
    public sealed class MsgOwnKongRank : PacketStructure
    {
        public MsgOwnKongRank(byte[] buffer)
            : base(buffer)
        {
        }

        public MsgOwnKongRank()
            : base(PacketType.MsgOwnKongRank, 57, 49)
        {
        }

        public ushort Page
        {
            get => ReadUShort(4);
            set => WriteUShort(value, 4);
        }

        public byte UserRanking
        {
            get => ReadByte(5);
            set => WriteByte(value, 5);
        }

        public byte Count
        {
            get => ReadByte(6);
            set
            {
                WriteByte(value, 6);
                Resize(57 + 41 * value);
                WriteHeader(Length - 8, PacketType.MsgOwnKongRank);
            }
        }

        public byte Amount
        {
            get => ReadByte(7);
            set => WriteByte(value, 7);
        }

        public void Append(byte pos, uint dwInnerPower, byte level, string szPlayerName, string szInnerPowerName)
        {
            int offset = 8 + Count * 41;
            Count += 1;
            WriteByte(pos, offset);
            WriteUInt(dwInnerPower, offset + 1);
            WriteUInt(level, offset + 5);
            WriteString(szPlayerName, 16, offset + 9);
            WriteString(szInnerPowerName, 16, offset + 25);
        }
    }
}
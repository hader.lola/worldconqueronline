﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - 1109 - MsgNpcInfoEx.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

namespace FtwCore.Networking.Packets
{
    public sealed class MsgNpcInfoEx : PacketStructure
    {
        public MsgNpcInfoEx()
            : base(PacketType.MsgNpcInfoEx, 56, 48)
        {
        }

        public MsgNpcInfoEx(byte[] packet)
            : base(packet)
        {
        }

        public uint Identity
        {
            get => ReadUInt(4);
            set => WriteUInt(value, 4);
        }

        public uint MaxLife
        {
            get => ReadUInt(12);
            set => WriteUInt(value, 12);
        }

        public uint Life
        {
            get => ReadUInt(16);
            set => WriteUInt(value, 16);
        }

        public ushort MapX
        {
            get => ReadUShort(20);
            set => WriteUShort(value, 20);
        }

        public ushort MapY
        {
            get => ReadUShort(22);
            set => WriteUShort(value, 22);
        }

        public ushort Lookface
        {
            get => ReadUShort(24);
            set => WriteUShort(value, 24);
        }

        public ushort Flag
        {
            get => ReadUShort(26);
            set => WriteUShort(value, 26);
        }

        public ushort Type
        {
            get => ReadUShort(28);
            set => WriteUShort(value, 28);
        }

        public string Name
        {
            get => ReadByte(36) != 0 ? ReadString(ReadByte(37), 38) : null;
            set
            {
                WriteByte(1, 36);
                WriteStringWithLength(value, 37);
            }
        }
    }
}
﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - 1019 - MsgFriend.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

namespace FtwCore.Networking.Packets
{
    public enum RelationAction : byte
    {
        REQUEST_FRIEND = 10,
        NEW_FRIEND = 11,
        SET_ONLINE_FRIEND = 12,
        SET_OFFLINE_FRIEND = 13,
        REMOVE_FRIEND = 14,
        ADD_FRIEND = 15,
        SET_ONLINE_ENEMY = 16,
        SET_OFFLINE_ENEMY = 17,
        REMOVE_ENEMY = 18,
        ADD_ENEMY = 19
    }

    public class MsgFriend : PacketStructure
    {
        public MsgFriend()
            : base(PacketType.MsgFriend, 48, 40)
        {
        }

        public MsgFriend(RelationAction mode, uint targetId, string targetName, bool online)
            : base(PacketType.MsgFriend, 48, 40)
        {
            Mode = mode;
            Identity = targetId;
            Name = targetName;
            Online = online;
        }

        public MsgFriend(byte[] packet)
            : base(packet)
        {
        }

        public uint Identity
        {
            get => ReadUInt(4);
            set => WriteUInt(value, 4);
        }

        public RelationAction Mode
        {
            get => (RelationAction) ReadByte(8);
            set => WriteByte((byte) value, 8);
        }

        public bool Online
        {
            get => ReadBoolean(9);
            set => WriteBoolean(value, 9);
        }

        public ushort Unknown0
        {
            get => ReadUShort(10);
            set => WriteUShort(value, 10);
        }

        public uint Unknown1
        {
            get => ReadUInt(12);
            set => WriteUInt(value, 12);
        }

        public uint Unknown2
        {
            get => ReadUInt(16);
            set => WriteUInt(value, 16);
        }

        public string Name
        {
            get => ReadString(16, 20);
            set => WriteString(value, 16, 20);
        }

        public uint Unknown3
        {
            get => ReadUInt(36);
            set => WriteUInt(value, 36);
        }
    }
}
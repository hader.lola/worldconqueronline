﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - 2242 - MsgTeamArenaFightingTeamList.cs
// Last Edit: 2020/01/17 16:40
// Created: 2020/01/17 16:25
// ////////////////////////////////////////////////////////////////////////////////////

namespace FtwCore.Networking.Packets
{
    public sealed class MsgTeamArenaFightingTeamList : PacketStructure
    {
        public MsgTeamArenaFightingTeamList(byte[] buffer)
            : base(buffer)
        {
        }

        public MsgTeamArenaFightingTeamList()
            : base(PacketType.MsgTeamArenaFightingTeamList, 36, 28)
        {
        }

        public uint Page
        {
            get => ReadUInt(4);
            set => WriteUInt(value, 4);
        }

        public uint Type
        {
            get => ReadUInt(8);
            set => WriteUInt(value, 8);
        }

        public uint MatchesCount
        {
            get => ReadUInt(12);
            set
            {
                Resize((int) (36 + 48 * value));
                WriteHeader(Length - 8, PacketType.MsgTeamArenaFightingTeamList);
                WriteUInt(value, 12);
            }
        }

        public uint InscribedCount
        {
            get => ReadUInt(16);
            set => WriteUInt(value, 16);
        }

        public uint Unknown
        {
            get => ReadUInt(20);
            set => WriteUInt(value, 20);
        }

        public uint PageCount
        {
            get => ReadUInt(24);
            set => WriteUInt(value, 24);
        }

        public void Append(uint idLeader0, string szName0, int nAmount0, uint idLeader1, string szName1, int nAmount1)
        {
            int offset = (int) (28 + 48 * MatchesCount);
            MatchesCount += 1;

            WriteUInt(idLeader0, offset);
            WriteString(szName0, 16, offset + 4);
            WriteInt(nAmount0, offset + 20);

            WriteUInt(idLeader1, offset + 24);
            WriteString(szName1, 16, offset + 28);
            WriteInt(nAmount1, offset + 44);
        }
    }
}
﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - 1027 - MsgGemEmbed.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

namespace FtwCore.Networking.Packets
{
    public enum EmbedMode : ushort
    {
        GEM_ADD = 0,
        GEM_REMOVE = 1
    }

    public sealed class MsgGemEmbed : PacketStructure
    {
        public MsgGemEmbed()
            : base(PacketType.MsgGemEmbed, 28, 20)
        {
        }

        public MsgGemEmbed(byte[] packet)
            : base(packet)
        {
        }

        public uint MainIdentity
        {
            get => ReadUInt(12);
            set => WriteUInt(value, 12);
        }

        public uint MinorIdentity
        {
            get => ReadUInt(16);
            set => WriteUInt(value, 16);
        }

        public ushort HoleNum
        {
            get => ReadUShort(20);
            set => WriteUShort(value, 20);
        }

        public EmbedMode Mode
        {
            get => (EmbedMode) ReadUShort(22);
            set => WriteUShort((ushort) value, 22);
        }
    }
}
﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - 2247 - MsgTeamArenaFightingMemberInfo.cs
// Last Edit: 2020/01/17 18:32
// Created: 2020/01/17 18:11
// ////////////////////////////////////////////////////////////////////////////////////

namespace FtwCore.Networking.Packets
{
    public sealed class MsgTeamArenaFightingMemberInfo : PacketStructure
    {
        public MsgTeamArenaFightingMemberInfo()
            : base(PacketType.MsgTeamArenaFightingMemberInfo, 24, 16)
        {
        }

        public uint Mode
        {
            get => ReadUInt(4);
            set => WriteUInt(value, 4);
        }

        public uint LeaderIdentity
        {
            get => ReadUInt(8);
            set => WriteUInt(value, 8);
        }

        public uint Count
        {
            get => ReadUInt(12);
            set
            {
                Resize((int) (24 + value * 40));
                WriteHeader(Length - 8, PacketType.MsgTeamArenaFightingMemberInfo);
                WriteUInt(value, 12);
            }
        }

        public void Add(uint idUser, uint level, uint profession, uint lookface, uint todayRank, uint arenaPoints,
            string szName)
        {
            int offset = (int) (16 + 40 * Count);
            Count += 1;

            WriteUInt(idUser, offset);
            WriteUInt(level, offset + 4);
            WriteUInt(profession, offset + 8);
            WriteUInt(lookface, offset + 12);
            WriteUInt(todayRank, offset + 16);
            WriteUInt(arenaPoints, offset + 20);
            WriteString(szName, 16, offset + 24);
        }
    }
}
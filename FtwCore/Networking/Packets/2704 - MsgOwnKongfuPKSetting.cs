﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - 2704 - MsgOwnKongfuPKSetting.cs
// Last Edit: 2020/01/17 18:34
// Created: 2019/12/11 22:57
// ////////////////////////////////////////////////////////////////////////////////////

namespace FtwCore.Networking.Packets
{
    public sealed class MsgOwnKongfuPKSetting : PacketStructure
    {
        public MsgOwnKongfuPKSetting(byte[] buffer)
            : base(buffer)
        {
        }

        public MsgOwnKongfuPKSetting()
            : base(PacketType.MsgOwnKongfuPkSetting, 16, 8)
        {
        }

        public uint Mode
        {
            get => ReadUInt(4);
            set => WriteUInt(value, 4);
        }
    }
}
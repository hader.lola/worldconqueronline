﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - 1312 - MsgFamily.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/11/24 18:48
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using FtwCore.Common.Enums;

#endregion

namespace FtwCore.Networking.Packets
{
    /// <summary>
    ///     This packet is used to manage the family actions, such as join, quit, ally, enemy and etc.
    /// </summary>
    public sealed class MsgFamily : PacketStructure
    {
        private int m_nTextLength;

        public MsgFamily(byte[] pBuffer)
            : base(pBuffer)
        {
        }

        public MsgFamily()
            : base(PacketType.MsgFamily, 88, 80)
        {
        }

        public FamilyType Type
        {
            get => (FamilyType) ReadUInt(4);
            set => WriteUInt((uint) value, 4);
        }

        public uint Identity
        {
            get => ReadUInt(8);
            set => WriteUInt(value, 8);
        }

        public byte StringCount
        {
            get => ReadByte(16);
            set => WriteByte(value, 16);
        }

        public string Announcement
        {
            get => ReadString(ReadByte(17), 18);
            set
            {
                StringCount = 1;
                Resize(88 + value.Length + 2);
                WriteHeader(Length - 8, PacketType.MsgFamily);
                WriteStringWithLength(value, 17);
            }
        }

        public string Name
        {
            get => ReadString(16, 18);
            set
            {
                Resize(88 + 18);
                WriteHeader(Length - 8, PacketType.MsgFamily);
                WriteString(value, 16, 18);
            }
        }

        public void AddString(string szString)
        {
            StringCount += 1;
            Resize(88 + m_nTextLength + StringCount);
            WriteHeader(Length - 8, PacketType.MsgFamily);
            int offset = 16 + StringCount + m_nTextLength;
            WriteStringWithLength(szString, offset);
            m_nTextLength += szString.Length;
        }

        public void AddMember(string szName, byte pLevel, ushort pProf, FamilyRank pRank, bool bOnline, uint dwDonation)
        {
            StringCount += 1;

            Resize(88 + StringCount * 36);
            WriteHeader(Length - 8, PacketType.MsgFamily);

            int offset = 20 + (StringCount - 1) * 36;
            WriteString(szName, 16, offset);
            WriteByte(pLevel, offset + 16);
            WriteUShort((ushort) pRank, offset + 20);
            WriteBoolean(bOnline, offset + 22);
            WriteUShort(pProf, offset + 24);
            // WriteUShort(0, offset + 28);
            WriteUInt(dwDonation, offset + 32);
        }

        // must set the correct type to ally or enemy
        public void AddRelation(uint idFamily, string szName, string szLeader)
        {
            StringCount += 1;

            Resize(88 + StringCount * 36);
            WriteHeader(Length - 8, PacketType.MsgFamily);

            int offset = 20 + (StringCount - 1) * 56;
            WriteUInt((uint) (StringCount + 100), offset);
            WriteString(szName, 16, offset + 4);
            WriteString(szLeader, 16, offset + 40);
        }
    }
}
﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - 1101 - MsgMapItem.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

namespace FtwCore.Networking.Packets
{
    public sealed class MsgMapItem : PacketStructure
    {
        public MsgMapItem()
            : base(PacketType.MsgMapItem, 131, 123)
        {
        }

        public MsgMapItem(byte[] packet)
            : base(packet)
        {
        }

        public MsgMapItem(uint itemIdentity, uint itemtype, ushort mapX, ushort mapY, DropType dropType)
            : base(PacketType.MsgMapItem, 131, 123)
        {
            WriteUInt(Common.Time.Now, 4);
            Identity = itemIdentity;
            Itemtype = itemtype;
            MapX = mapX;
            MapY = mapY;
            ItemColor = 3;
            DropType = dropType;
        }

        public uint Identity
        {
            get => ReadUInt(8);
            set => WriteUInt(value, 8);
        }

        public uint Itemtype
        {
            get => ReadUInt(12);
            set => WriteUInt(value, 12);
        }

        public ushort MapX
        {
            get => ReadUShort(16);
            set => WriteUShort(value, 16);
        }

        public ushort MapY
        {
            get => ReadUShort(18);
            set => WriteUShort(value, 18);
        }

        public DropType DropType
        {
            get => (DropType) ReadUShort(22);
            set => WriteUShort((ushort) value, 22);
        }

        public byte ItemColor
        {
            get => ReadByte(20);
            set => WriteByte(value, 20);
        }

        //public byte Addition
        //{
        //    get => ReadByte(23);
        //    set => WriteByte(value, 23);
        //}

        //public uint OwnerIdentity
        //{
        //    get => ReadUInt(28);
        //    set => WriteUInt(value, 28);
        //}

        //public ushort OwnerX
        //{
        //    get => ReadUShort(61);
        //    set => WriteUShort(value, 61);
        //}

        //public ushort OwnerY
        //{
        //    get => ReadUShort(63);
        //    set => WriteUShort(value, 63);
        //}

        //public string ItemName
        //{
        //    get => ReadString(16, 91);
        //    set => WriteString(value, 16, 91);
        //}

        //public uint Quality
        //{
        //    get => ReadUInt(24);
        //    set => WriteUInt(value, 24);
        //}

        //public uint Addition
        //{
        //    get => ReadUInt(28);
        //    set => WriteUInt(value, 28);
        //}

        //public uint Socket
        //{
        //    get => ReadUInt(32);
        //    set => WriteUInt(value, 32);
        //}
    }

    public enum DropType
    {
        Unknown = 0,
        Drop = 1,
        Disappear = 2,
        Pickup = 3,
        CastTrap = 10,
        SynchroTrap = 11,
        DropTrap = 12
    }
}
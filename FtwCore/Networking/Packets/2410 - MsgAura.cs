﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - 2410 - MsgAura.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

namespace FtwCore.Networking.Packets
{
    public enum IconAction : uint
    {
        REMOVE = 2,
        ADD = 3
    }

    public enum AuraType : uint
    {
        TYRANT_AURA = 1,
        FEND_AURA = 2,
        METAL_AURA = 3,
        WOOD_AURA = 4,
        WATER_AURA = 5,
        FIRE_AURA = 6,
        EARTH_AURA = 7,
        MAGIC_DEFENDER = 8
    }

    public sealed class MsgAura : PacketStructure
    {
        public MsgAura(byte[] msg)
            : base(msg)
        {
        }

        public MsgAura()
            : base(PacketType.MsgAura, 40, 32)
        {
        }
        
        public IconAction Action
        {
            get => (IconAction) ReadUInt(4);
            set => WriteUInt((uint) value, 4);
        }

        public uint EntityIdentity
        {
            get => ReadUInt(8);
            set => WriteUInt(value, 8);
        }

        public uint Type
        {
            get => ReadUInt(12);
            set => WriteUInt(value, 12);
        }

        public uint Level
        {
            get => ReadUInt(16);
            set => WriteUInt(value, 16);
        }

        public uint Power0
        {
            get => ReadUInt(20);
            set => WriteUInt(value, 20);
        }

        public uint Power1
        {
            get => ReadUInt(24);
            set => WriteUInt(value, 24);
        }
    }
}
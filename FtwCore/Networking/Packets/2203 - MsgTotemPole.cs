﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - 2203 - MsgTotemPole.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

namespace FtwCore.Networking.Packets
{
    public sealed class MsgTotemPole : PacketStructure
    {
        public MsgTotemPole()
            : base(PacketType.MsgTotemPole, 52, 44)
        {
        }

        public MsgTotemPole(byte[] packet)
            : base(packet)
        {
        }

        public uint Type
        {
            get => ReadUInt(4);
            set => WriteUInt(value, 4);
        }

        public uint BeginAt
        {
            get => ReadUInt(8);
            set => WriteUInt(value, 8);
        }

        public uint EndAt
        {
            get => ReadUInt(12);
            set => WriteUInt(value, 12);
        }

        public uint ArsenalType
        {
            get => ReadUInt(16);
            set => WriteUInt(value, 16);
        }

        public int TotalInscribed
        {
            get => ReadInt(20);
            set => WriteInt(value, 20);
        }

        public uint SharedBattlepower
        {
            get => ReadUInt(24);
            set => WriteUInt(value, 24);
        }

        public uint Enchantment
        {
            get => ReadUInt(28);
            set => WriteUInt(value, 28);
        }

        public int EnchantmentExpirationDate
        {
            get => ReadInt(32);
            set => WriteInt(value, 32);
        }

        public uint Donation
        {
            get => ReadUInt(36);
            set => WriteUInt(value, 36);
        }

        public uint Count
        {
            get => ReadUInt(40);
            set
            {
                Resize((int) (44 + value * 40 + 8));
                WriteHeader(Length - 8, PacketType.MsgTotemPole);
                WriteUInt(value, 40);
            }
        }

        public void AppendItem(uint itemUid, uint position, string ownerName, uint staticId,
            byte plus, byte socketOne, byte socketTwo, uint battlePower, uint donation)
        {
            Count += 1;
            var offset = (ushort) (44 + 40 * (Count - 1));
            WriteUInt(itemUid, offset);
            WriteUInt(position, offset + 4);
            WriteString(ownerName, 16, offset + 8);
            WriteUInt(staticId, offset + 24);
            WriteByte((byte) (staticId % 10), offset + 28);
            WriteByte(plus, offset + 29);
            WriteByte(socketOne, offset + 30);
            WriteByte(socketTwo, offset + 31);
            WriteUInt(battlePower, offset + 32);
            WriteUInt(donation, offset + 36);
        }
    }
}
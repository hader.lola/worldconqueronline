﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - 1130 - MsgTitle.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

namespace FtwCore.Networking.Packets
{
    public sealed class MsgTitle : PacketStructure
    {
        public MsgTitle()
            : base(PacketType.MsgTitle, 20, 12)
        {
        }

        public MsgTitle(byte[] packet)
            : base(packet)
        {
        }

        public uint Identity
        {
            get => ReadUInt(4);
            set => WriteUInt(value, 4);
        }

        public UserTitle SelectedTitle
        {
            get => (UserTitle) ReadByte(8);
            set => WriteByte((byte) value, 8);
        }

        public TitleAction Action
        {
            get => (TitleAction) ReadByte(9);
            set => WriteByte((byte) value, 9);
        }

        public byte Count
        {
            get => ReadByte(10);
            set
            {
                Resize(20 + value);
                WriteHeader(Length - 8, PacketType.MsgTitle);
                WriteByte(value, 10);
            }
        }

        public void Append(byte title)
        {
            Count += 1;
            var offset = (ushort) (10 + Count);
            WriteByte(title, offset);
        }
    }

    public enum TitleAction : byte
    {
        HideTitle = 0,
        AddTitle = 1,
        RemoveTitle = 2,
        SelectTitle = 3,
        QueryTitle = 4
    }

    /// <summary>
    /// This enum is irrelevant, the user may create new ones on the client and make
    /// an action to add them. By the way, we will set this here, just in case we want
    /// to make a string or something with the name.
    /// </summary>
    public enum UserTitle : byte
    {
        None = 0,
        HourlySkillPkTournament = 1,
        LineSkillPkTournament = 2,
        GoldenRacer = 11,
        ElitePkFirstLow = 12,
        ElitePkSecondLow = 13,
        ElitePkThirdLow = 14,
        ElitePkEightLow = 15,
        ElitePkFirstHigh = 16,
        ElitePkSecondHigh = 17,
        ElitePkThirdHigh = 18,
        ElitePkEightHigh = 19,
        TopGuildLeader = 34,
        TopDeputyLeader = 35
    }
}
﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - 2225 - MsgSynRecruitAdvertising.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

namespace FtwCore.Networking.Packets
{
    public sealed class MsgSynRecuitAdvertising : PacketStructure
    {
        public MsgSynRecuitAdvertising()
            : base(PacketType.MsgSynRecuitAdvertising, 292, 284)
        {
        }

        public MsgSynRecuitAdvertising(byte[] pBuffer)
            : base(pBuffer)
        {
        }

        public uint Identity
        {
            get => ReadUInt(4);
            set => WriteUInt(value, 4);
        }

        public string Description
        {
            get => ReadString(256, 8);
            set
            {
                if (value.Length > 255)
                    WriteString(value.Substring(0, 255), 256, 8);
                else
                    WriteString(value, 255, 8);
            }
        }

        public ulong Amount
        {
            get => ReadULong(264);
            set => WriteULong(value, 264);
        }

        public bool IsAutoRecruit
        {
            get => ReadBoolean(272);
            set => WriteBoolean(value, 272);
        }

        public ushort LevelRequirement
        {
            get => ReadUShort(274);
            set => WriteUShort(value, 274);
        }

        public ushort RebornRequirement
        {
            get => ReadUShort(276);
            set => WriteUShort(value, 276);
        }

        public ushort ProfessionForbid
        {
            get => ReadUShort(278);
            set => WriteUShort(value, 278);
        }

        public ushort GenderForbid
        {
            get => ReadUShort(280);
            set => WriteUShort(value, 280);
        }

        public ushort Unknown
        {
            get => ReadUShort(282);
            set => WriteUShort(value, 282);
        }
    }
}
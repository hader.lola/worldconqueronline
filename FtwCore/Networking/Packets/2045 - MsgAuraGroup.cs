﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - 2045 - MsgAuraGroup.cs
// Last Edit: 2020/01/26 14:54
// Created: 2020/01/13 23:53
// ////////////////////////////////////////////////////////////////////////////////////

namespace FtwCore.Networking.Packets
{
    public sealed class MsgAuraGroup : PacketStructure
    {
        public MsgAuraGroup(byte[] buffer)
            : base(buffer)
        {
        }

        public MsgAuraGroup()
            : base(PacketType.MsgAuraGroup, 32, 24)
        {
        }

        public AuraGroupMode Mode
        {
            get => (AuraGroupMode) ReadUInt(4);
            set => WriteUInt((uint) value, 4);
        }

        public uint Identity
        {
            get => ReadUInt(8);
            set => WriteUInt(value, 8);
        }

        public uint LeaderIdentity
        {
            get => ReadUInt(12);
            set => WriteUInt(value, 12);
        }

        public uint Count
        {
            get => ReadUInt(16);
            set => WriteUInt(value, 16);
        }

        public uint Unknown
        {
            get => ReadUInt(20);
            set => WriteUInt(value, 20);
        }
    }

    public enum AuraGroupMode : uint
    {
        Leader = 1,
        Teammate = 2
    }
}
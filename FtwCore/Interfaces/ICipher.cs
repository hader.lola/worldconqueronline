﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - ICipher.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

namespace FtwCore.Interfaces
{
    /// <summary>
    /// This interface is used to define a cipher for packet processing. The socket systems use this interface to
    /// decrypt the header and body of packets. All packet ciphers should implement this method.
    /// </summary>
    public interface ICipher
    {
        byte[] Decrypt(byte[] buffer, int length);
        void Decrypt(byte[] packet, byte[] buffer, int length, int position);
        byte[] Encrypt(byte[] packet, int length);
        void GenerateKeys(int account, int authentication);
        void KeySchedule(byte[] key);
    }
}
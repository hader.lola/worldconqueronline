﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - Hardware Logging.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/14 15:58
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System;
using System.Management;

#endregion

namespace FtwCore.Win32Events
{
    public class HardwareLogging
    {
        public static void DoLog()
        {
            Console.WriteLine(@"Motherboard information");
            InsertInfo("Win32_BaseBoard", true);
            Console.WriteLine(@"Processor information");
            InsertInfo("Win32_Processor", true);
            Console.WriteLine(@"Memory Information");
            InsertInfo("Win32_PhysicalMemory", true);
        }

        private static void InsertInfo(string key, bool dontInsertNull)
        {
            ManagementObjectSearcher searcher = new ManagementObjectSearcher("select * from " + key);
            try
            {
                foreach (var o in searcher.Get())
                {
                    var share = (ManagementObject) o;
                    foreach (PropertyData pc in share.Properties)
                    {
                        string name = pc.Name;
                        string output = "No information available.";
                        if (pc.Value != null && pc.Value.ToString() != "")
                        {
                            switch (pc.Value.GetType().ToString())
                            {
                                case "System.String[]":
                                    string[] str = (string[]) pc.Value;

                                    string str2 = "";
                                    foreach (string st in str)
                                        str2 += st + " ";

                                    output = str2;
                                    break;
                                case "System.UInt16[]":
                                    ushort[] shortData = (ushort[]) pc.Value;


                                    string tstr2 = "";
                                    foreach (ushort st in shortData)
                                        tstr2 += st + " ";

                                    output = tstr2;
                                    break;

                                default:
                                    output = pc.Value.ToString();
                                    break;
                            }
                        }

                        Console.WriteLine($@"	{name}: {output}");
                    }
                }
            }
            catch
            {
            }
        }
    }
}
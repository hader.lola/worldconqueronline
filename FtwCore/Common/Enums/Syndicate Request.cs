﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - Syndicate Request.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

namespace FtwCore.Common.Enums
{
    public enum SyndicateRequest : uint
    {
        SYN_JOIN_REQUEST = 1,
        SYN_INVITE_REQUEST = 2,
        SYN_QUIT = 3,
        SYN_INFO = 6,
        SYN_ACCEPT_REQUEST = 29,
        SYN_ALLIED = 7,
        SYN_NEUTRAL1 = 8,
        SYN_ENEMIED = 9,
        SYN_NEUTRAL2 = 10,
        SYN_DONATE_SILVERS = 11,
        SYN_REFRESH = 12,
        SYN_DISBAND = 19,
        SYN_DONATE_CONQUER_POINTS = 20,
        SYN_SET_REQUIREMENTS = 24,
        SYN_SEND_REQUEST = 28,
        SYN_BULLETIN = 27,

        /// <summary>
        /// What
        /// </summary>
        SYN_DISCHARGE = 30,
        SYN_RESIGN = 32,

        /// <summary>
        /// The
        /// </summary>
        SYN_DISCHARGE2 = 33,
        SYN_PAID_PROMOTE = 34,
        SYN_EXTEND_PROMOTE = 35,

        /// <summary>
        /// Fuck?
        /// </summary>
        SYN_DISCHARGE3 = 36,
        SYN_PROMOTE = 37
    }
}
﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - Item Position.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

namespace FtwCore.Common.Enums
{
    public enum ItemPosition : ushort
    {
        Inventory = 0,
        EquipmentBegin = 1,
        Headwear = 1,
        Necklace = 2,
        Armor = 3,
        RightHand = 4,
        LeftHand = 5,
        Ring = 6,
        Gourd = 7,
        Boots = 8,
        Garment = 9,
        AttackTalisman = 10,
        DefenceTalisman = 11,
        Steed = 12,
        RightHandAccessory = 15,
        LeftHandAccessory = 16,
        SteedArmor = 17,
        Crop = 18,
        EquipmentEnd = 19,

        AltHead = 21,
        AltNecklace = 22,
        AltArmor = 23,
        AltWeaponR = 24,
        AltWeaponL = 25,
        AltRing = 26,
        AltBottle = 27,
        AltBoots = 28,
        AltGarment = 29,
        AltFan = 30,
        AltTower = 31,
        AltSteed = 32,

        WarehouseBegin = 230,
        WarehouseEnd = 236,
        HouseWarehouse = 237,

        Detained = 250,
        Floor = 254
    }
}
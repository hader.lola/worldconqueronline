﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - Chat Information.cs
// Last Edit: 2019/11/24 19:01
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System;

#endregion

namespace FtwCore.Common.Enums
{
    /// <summary>
    /// This enumeration type defines the types of messages that can be sent. Each type defines how the message
    /// is displayed in the client (where the message is displayed and how the client reacts).
    /// </summary>
    public enum ChatTone : ushort
    {
        Talk = 2000,
        Whisper = 2001,
        Action = 2002,
        Team = 2003,
        Guild = 2004,
        Family = 2006,
        System = 2007,
        Yell = 2008,
        Friend = 2009,
        Center = 2011,
        TopLeft = 2012,
        Ghost = 2013,
        Service = 2014,
        Tip = 2015,
        World = 2021,
        Qualifier = 2022,
        Study = 2024,
        Ally = 2025,
        CharacterCreation = 2100,
        Login = 2101,
        Shop = 2102,
        VendorHawk = 2104,
        Website = 2105,
        EventRanking = 2108,
        EventRankingNext = 2109,
        OfflineWhisper = 2110,
        GuildAnnouncement = 2111,
        Agate = 2115,
        TradeBoard = 2201,
        FriendBoard = 2202,
        TeamBoard = 2203,
        GuildBoard = 2204,
        OthersBoard = 2205,
        Broadcast = 2500,
        Monster = 2600
    }

    /// <summary>
    /// This enumeration type defines the style a message in the client can appear with. Messages can be sent with
    /// effects such as flash, scroll, and blast. Sending zero sends a normal message. Styles can be overlapped.
    /// </summary>
    [Flags]
    public enum ChatStyle : ushort
    {
        Normal = 0,
        Scroll = 1 << 0,
        Flash = 1 << 1,
        Blast = 1 << 2
    }
}
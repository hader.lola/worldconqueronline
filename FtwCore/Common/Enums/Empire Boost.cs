﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - Empire Boost.cs
// Last Edit: 2019/12/15 13:31
// Created: 2019/12/03 00:54
// ////////////////////////////////////////////////////////////////////////////////////

namespace FtwCore.Common.Enums
{
    public enum EmpireBoost
    {
        None,

        /// <summary>
        ///     Increases by 2x the amount of Silvers dropped by the monsters.
        /// </summary>
        MoneyDrop,

        /// <summary>
        ///     Increases by 3x the amount of Conquer Points dropped by the monsters.
        /// </summary>
        EmoneyDrop,

        /// <summary>
        ///     Increases by 2x the amount of Power Exp Balls in the mines.
        /// </summary>
        MinePowerExpRate,

        /// <summary>
        ///     Increases by 2x the amount of Permanent Stones in the mines.
        /// </summary>
        MinePermanentStoneRate,

        /// <summary>
        ///     Increases the experience awarded by the monsters (Multiply the consumible) for 14 days.
        /// </summary>
        ExperienceRate2x,

        /// <summary>
        ///     Increases the experience awarded by the monsters (Multiply the consumible) for 7 days.
        /// </summary>
        ExperienceRate3x,

        /// <summary>
        ///     Increases the experience awarded by the monsters (Multiply the consumible) for 72 hours.
        /// </summary>
        ExperienceRate5x
    }
}
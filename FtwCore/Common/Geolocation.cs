﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - Geolocation.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System.Data;
using System.Net;
using System.Xml;

#endregion

namespace FtwCore.Common
{
    public static class Geolocation
    {
        private const string _DOMAIN_S = "http://ws.cdyne.com/ip2geo/ip2geo.asmx/ResolveIP?ipAddress={0}&licenseKey=0";

        public static UserLocation Find(string ipAddress)
        {
            WebRequest objWebRequest = WebRequest.Create(string.Format(_DOMAIN_S, ipAddress));

            //Set the timeout in Seconds for the WebRequest 
            objWebRequest.Timeout = 2000;

            try
            {
                //Get the WebResponse  
                WebResponse objWebResponse = objWebRequest.GetResponse();
                //Read the Response in a XMLTextReader 
                XmlTextReader objXmlTextReader = new XmlTextReader(objWebResponse.GetResponseStream());

                //Create a new DataSet 
                DataSet objDataSet = new DataSet();
                //Read the Response into the DataSet 
                objDataSet.ReadXml(objXmlTextReader);
                DataTable dtResult = objDataSet.Tables[0];
                DataRow row = dtResult.Rows[0];

                return new UserLocation
                {
                    Country = row["Country"].ToString(),
                    AreaCode = int.Parse(row["AreaCode"].ToString()),
                    City = row["City"].ToString(),
                    State = int.Parse(row["StateProvince"].ToString()),
                    CountryCode = row["CountryCode"].ToString(),
                    Latitude = float.Parse(row["Latitude"].ToString()),
                    Longitude = float.Parse(row["Longitude"].ToString())
                };
            }
            catch
            {
                return null;
            }
        }
    }

    public sealed class UserLocation
    {
        public string City;
        public int State;
        public string Country;
        public float Latitude;
        public float Longitude;
        public int AreaCode;
        public string CountryCode;
    }
}
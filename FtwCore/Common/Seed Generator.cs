﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - Seed Generator.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

namespace FtwCore.Common
{
    public static class SeedGenerator
    {
        /// <summary>
        /// This function encapsulates a seed generator for Rivest Cipher 5 and NetDragon Websoft's Password Cipher 
        /// addition. It takes in a seed and spits out a 16 byte array. The returned array is used as the ciphers'
        /// initialization vector.
        /// </summary>
        /// <param name="seed">The seed being used to initialize the initialization vector.</param>
        public static byte[] Generate(int seed)
        {
            byte[] initializationVector = new byte[0x10];
            for (int index = 0; index < 0x10; index++)
            {
                seed *= 0x343fd;
                seed += 0x269ec3;
                initializationVector[index] = (byte) ((seed >> 0x10) & 0x7fff);
            }

            return initializationVector;
        }
    }
}
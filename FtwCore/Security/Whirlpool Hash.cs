﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - Whirlpool Hash.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System;
using System.Text;
using Org.BouncyCastle.Crypto.Digests;

#endregion

namespace FtwCore.Security
{
    public static class WhirlpoolHash
    {
        public static string Hash(string message)
        {
            if (string.IsNullOrEmpty(message) || string.IsNullOrWhiteSpace(message))
                return string.Empty;

            WhirlpoolDigest whirlpool = new WhirlpoolDigest();

            byte[] data = new UTF8Encoding().GetBytes(message);
            whirlpool.Reset();
            whirlpool.BlockUpdate(data, 0, data.Length);

            byte[] ret = new byte[whirlpool.GetDigestSize()];
            whirlpool.DoFinal(ret, 0);
            return ByteToString(ret);
        }

        public static string HardHash(string message)
        {
            if (string.IsNullOrEmpty(message) || string.IsNullOrWhiteSpace(message))
                return string.Empty;

            WhirlpoolDigest whirlpool = new WhirlpoolDigest();

            byte[] data = new UTF8Encoding().GetBytes(message);
            whirlpool.Reset();

            for (int i = 0; i < 3000; i++)
            {
                whirlpool.BlockUpdate(data, 0, data.Length);
            }

            byte[] ret = new byte[whirlpool.GetDigestSize()];
            whirlpool.DoFinal(ret, 0);
            return ByteToString(ret) + "00";
        }

        private static string ByteToString(byte[] buffer)
        {
            return BitConverter.ToString(buffer).Replace("-", "").ToLower();
        }
    }
}
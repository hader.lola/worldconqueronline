# World Conquer Online v3

Project World Conquer is a server project containing an Account and a Game Server. Also, the project provides an website that uses the same libraries as the server. The account server authenticates the players from many servers and redirect them for the correct game server, while the game server process the game world. The server is interoperable currently with the Conquer Online game client, targeting the patch 5808 (not provided by this project).

## Getting Started

To get started, download and install Visual Studio 2017 or higher with .Net Framework 4.6.2 or higher. After installing, open the .sln file to start working with the project and before trying to build anything, restore all Nuget packages, otherwise you wont be able to compile the project.

This project currently uses MySQL on version 5.7, you can download the latest version of MySQL Community Server and some tool to manage your data. MySQL Workbench is a free managing tool provided by Oracle and is recommended for usage with this project.

### Solution

The solution is divided in 5 main projects which have their own functions in the game environment. 

#### FtwCore

This dynamic library includes all shared methods and functions to be used between the projects. It contains:
 - Common content, which includes calculations, enumerators, random generators and etc
 - Data Structures, which as the name says contains data structures to be used with the projects
 - Database content, that manage all the transactions executed by the Fluent NHibernate library with MySQL
 - Interfaces
 - Networking content, sockets and packets structures
 - Security content, cryptography
 - Win32Events, events to be used with Win32 or Console applications, or Windows OS information getters

#### LoginServer

The login server does the authentication job, it's mostly done except for the SRP6 implementation that hasn't been done yet. You must use CSV3 dll and an injector in order to be able to login. The file LoginServer.xml is what you need to configure the server.

#### GameServer

The game server does everything. It process AI, security, all ingame calculations and etc. The file GameServer.xml is what you need to configure the server.

#### WcoSiteNet

The website project. Just configure the *web.config* file with your database information and you're good to go.

#### WebDbUpdate

This is what you need to keep your website database up to date. Just Schedule it for periodic executions and it will back up the tables that you selected on your webhost, so your website can have the ranking updated without the need of a direct connection with the game database.